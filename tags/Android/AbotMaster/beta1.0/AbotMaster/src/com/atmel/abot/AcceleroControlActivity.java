 package com.atmel.abot;

import com.atmel.abot.AbotManager.Abot;

import android.content.Context;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;
import android.widget.ToggleButton;


public class AcceleroControlActivity extends BaseControlActivity implements SensorEventListener {
	
	/** */
    private WindowManager mWindowManager;
    private Display mDisplay;
	
	/** Sensor handlers */
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	//private float mSensorX = 0;
	private float mSensorY = 0;
	//private float mSensorZ = 0;
	
	/** Control elements of dashboard */
	/** Control Buttons */
	private ToggleButton mGearLeverButton;
	private ImageButton mAcceleratorButton;
	
	/** Steering wheel which gives feedback of the angle of turn taken by Abot */
	private ImageView mSteeringWheelImage;
	
	/** Constants for controlling directions */
	private static final String FORWARD_STRING = "Forward";
	private static final String REVERSE_STRING = "Reverse"; 
	private static final int FORWARD_DIRECTION = 0x10;
	private static final int REVERSE_DIRECTION = 0x11;
	private int mDirection = FORWARD_DIRECTION;            // Default state forward

	private long POLLING_INTERVAL = 20;          // in milliseconds
	
	/** Abot */
	private AbotManager.Abot mAbot = null;
	
	/** Handler and thread to handle Abot control operations without overloading UI Thread */
	private ControlHandler mControlHandler;
	private HandlerThread mControlThread;
	
	class ControlHandler extends Handler {
		
		/** Mutex to access variables common among threads */
		public final Object mutex = new Object();
		
		/** Commands handled by this handler */
		public static final int STEERING_CONTROL_HANDLER = 0x10;
		public static final int COMMAND = 0x11;
			public static final int FORWARD = 0x01;
			public static final int REVERSE = 0x02;
			public static final int LEFT    = 0x03;
			public static final int RIGHT   = 0x04;
			public static final int STOP 	= 0x05;
			
		/** Initial speed used to drive forward/reverse */
		private static final byte INITIAL_SPEED = 50;
		
		public static final long COMMAND_INTERVAL = 50;        // in milliseconds
		
		/** Steering control parameters */
		/** Number of units of turn taken by Abot */
		private int mCurrentTurnCount = 0;
		/** Number of units of turn required */
		private int mReqTurnCount = 0;
		
		/** Status variable */
		private boolean mAccelerate;
		private int mDirection;
		private int mCount = 0;
		
		/** Parameters controlling the Abot motion based on accelerometers */
		/** NB: Fine tuning of these values can be done to control Abot in a better fashion */
		private static final int ACCELEROMETER_CONTROL_DELTA = 5;          	// Accelerometer Y-axis value will be used; Left(-6 to -1), Right(1 to 6) So delta is 5
		private static final int MOTOR_CONTROL_DELTA = 255;           		// Abot Servo motor has control range of 255 units (pulse width in microseconds) in clockwise and anticlockwise
		private static final int MOTOR_STEP = 10;                      		// Number of microseconds increased/decreased per command (left/right)
		private static final int MAX_STEP_COUNT = MOTOR_CONTROL_DELTA/MOTOR_STEP;
		private static final float RESOLUTION = (float)ACCELEROMETER_CONTROL_DELTA/(float)MAX_STEP_COUNT;
		
		/** Rotation angle of the steering wheel */
		private float mRotationAngle;
		
		public ControlHandler(Looper looper) {
			super(looper);
		}
		
		/** Sets the number of turns to be taken by Abot */
		public void requestTurnCount(int count) {
			synchronized(mutex) {
				mReqTurnCount = count;
			}
		}
		
		@SuppressWarnings("unused")
		private void resetTurnCounts() {
			synchronized(mutex) {
				mReqTurnCount = 0x00;
				mCurrentTurnCount = 0x00;
			}
		}
		
		/** Says whether Abot need to take anymore turns */
		public boolean isTurnRequired() {
			synchronized(mutex) {
				return (mReqTurnCount != 0);
			}
		}
		
		/** Gives rotation angle of the steering wheel */
		public float getRotationAngle() {
			return mRotationAngle;
		}
		
		/** Set the rotation angle of the steering wheel */
		private void setRotationAngle(float angle) {
			mRotationAngle = angle;
			rotateSteeringOnUiThread();
		}
		
		private void rotateSteeringOnUiThread() {
			runOnUiThread(new Runnable() {
				public void run() {
					rotateSteering();
				}
			});
		}
		@Override
		public void handleMessage(Message message) {
			switch(message.what) {
			case STEERING_CONTROL_HANDLER:
				synchronized(mutex) {
					// Set 'mRotationAngle' to rotate the steering wheel
					//if(mCurrentTurnCount != mReqTurnCount) {
						setRotationAngle(((float)mCurrentTurnCount*100)/MAX_STEP_COUNT);
					//}
					if(mCurrentTurnCount < mReqTurnCount) {
						//mAbot.right((byte)(mCurrentTurnCount*MOTOR_STEP));
						mCurrentTurnCount++;
					} else if(mCurrentTurnCount > mReqTurnCount) {
						//mAbot.left((byte)((-mCurrentTurnCount)*MOTOR_STEP));
						mCurrentTurnCount--;
					}
					
					// Steer Abot here
					if(mAccelerate && mCount >= 4) {
						mCount = 0;
						Log.d("Steering Abot","Turn Count " + mCurrentTurnCount);
						if(mCurrentTurnCount < 0) {
							mAbot.left((byte)((-mCurrentTurnCount)*MOTOR_STEP),(mDirection == FORWARD)?Abot.FORWARD:Abot.REVERSE);
						} else if(mCurrentTurnCount > 0) {
							mAbot.right((byte)(mCurrentTurnCount*MOTOR_STEP),(mDirection == FORWARD)?Abot.FORWARD:Abot.REVERSE);
						}
					} else {
						mCount++;
					}
				}
				sendEmptyMessageDelayed(STEERING_CONTROL_HANDLER, POLLING_INTERVAL);
				break;
			case COMMAND:
				switch(message.arg1) {
				case FORWARD:
					mDirection = FORWARD;
					if (!mAccelerate) {
						mAccelerate = true;
						mAbot.forward(INITIAL_SPEED);
					} else {
						mAbot.forward();
					}
					break;
				case REVERSE:
					mDirection = REVERSE;
					if (!mAccelerate) {
						mAccelerate = true;
						mAbot.reverse(INITIAL_SPEED);
					} else {
						mAbot.reverse();
					}
					break;
				case LEFT:
					
					break;
				case RIGHT:
					
					break;
				case STOP:
					mAbot.stop();
					mAccelerate = false;
					removeMessages(COMMAND);
					//resetTurnCounts();
					break;
				default:
					
					break;
				}
				// Don't drive Abot forward/reverse when it is turning since differential driving is used for turning.
				if(!isTurnRequired()) {
					if(message.arg1 == FORWARD || message.arg1 == REVERSE) {
						sendMessageDelayed(Message.obtain(message),COMMAND_INTERVAL);
					}
				}
			}
		}
	}

	/** Change steering wheel angle */
	private void rotateSteering() {
		//Log.d("AccelerometerControlActivity","rotateSteering");
		Matrix matrix = new Matrix();
		matrix.postRotate(mControlHandler.getRotationAngle(),mSteeringWheelImage.getWidth()/2,mSteeringWheelImage.getHeight()/2);
		mSteeringWheelImage.setImageMatrix(matrix);
	}
	
	/** Enable steering control such that steering control handler starts sending steering commands to Abot */
	private void startSteeringControl() {
		Log.d("AccelerometerControlActivity","startSteeringControl");
		mSensorY = 0;
		mControlHandler.sendEmptyMessage(ControlHandler.STEERING_CONTROL_HANDLER);
	}
	
	/** Disable steering control */
	private void stopSteeringControl() {
		Log.d("AccelerometerControlActivity","stopSteeringControl");
		mControlHandler.removeMessages(ControlHandler.STEERING_CONTROL_HANDLER);
		mControlHandler.setRotationAngle(0);
		rotateSteering();
	}
	
	/** Listener for accelerator on the dashboard */
	View.OnTouchListener mAcceleratorListener = new View.OnTouchListener() {
		
		public boolean onTouch(View view, MotionEvent event) {
			view.onTouchEvent(event);
			if(MotionEvent.ACTION_DOWN == event.getAction()) {
				// Start the steering control handler
				// startSteeringControl();
				// Check whether Abot needs to turn, if so then don't drive it forward/reverse
				//if(!mControlHandler.isTurnRequired()) {
					// Accelerator is pressed down
					if(mDirection == FORWARD_DIRECTION) {
						mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.FORWARD,0));
					} else if(mDirection == REVERSE_DIRECTION) {
						mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.REVERSE,0));
					}
				//}
			} else if (MotionEvent.ACTION_UP == event.getAction()) {
					// Accelerator is not pressed, so decelerate Abot
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.STOP,0));
					// stopSteeringControl();
			}
			return true;
		}
	};
	
	/** Listener for gear shifter on the dashboard */
	View.OnClickListener mGearLeverListener = new View.OnClickListener() {
		
		public void onClick(View view) {
			mDirection = mGearLeverButton.isChecked() ? FORWARD_DIRECTION : REVERSE_DIRECTION;
			Toast toast = Toast.makeText(getBaseContext(), mGearLeverButton.isChecked() ? FORWARD_STRING : REVERSE_STRING , Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set screen orientation to landscape
		Log.d("AccelerometerControlActivity","Reached onCreate");
		//this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
        // Get an instance of the WindowManager
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mDisplay = mWindowManager.getDefaultDisplay();
        
		// Show the accelerometer based control UI screen
		setContentView(R.layout.accelerometer_control);
		
		// Get control buttons
		mGearLeverButton = (ToggleButton) findViewById(R.id.gear_lever_button);
		mAcceleratorButton = (ImageButton) findViewById(R.id.accelerator_button);
		mSteeringWheelImage = (ImageView) findViewById(R.id.steering_wheel_image);
		mSteeringWheelImage.setScaleType(ScaleType.MATRIX);
		
		// Register listeners for the control buttons
		mGearLeverButton.setOnClickListener(mGearLeverListener);
		mAcceleratorButton.setOnTouchListener(mAcceleratorListener);
		
		// Get Sensor service
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		// Create control thread
		mControlThread = new HandlerThread("ControlThread");
		mControlThread.start();
		mControlHandler = new ControlHandler(mControlThread.getLooper());
		// Get Accelerometer sensor
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	}
	
	@Override
	protected void onStart() {
		super.onStart();

	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		// Get Abot here
		mAbot = mAbotManager.getSelectedAbot();
		if(mAbot == null) {
			Log.d("AccelerometerControlActivity","onResume - No Abot Selected");
			
		} else {
			mSensorManager.registerListener(this,mAccelerometer, SensorManager.SENSOR_DELAY_UI);
			startSteeringControl();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		try {
			mSensorManager.unregisterListener(this);
			stopSteeringControl();
		} catch (Exception e) {
			Log.e("AccelerometerControlActivity","onPause - Exception occured");
			e.printStackTrace();
		}
	}
	
	@Override 
	protected void onStop() {
		super.onStop();
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
		
	}

	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
			return;
		}
		// Store sensor value only if the mobile is in exact landscape inclination
		if(mDisplay.getOrientation() == Surface.ROTATION_90) {
			//mSensorX = event.values[0];
			mSensorY = event.values[1];
			//mSensorZ = event.values[2];
			
			if(mSensorY >= -1 && mSensorY <= 1) {
				mControlHandler.requestTurnCount(0);
			} else if (mSensorY > 6) {
				mControlHandler.requestTurnCount(ControlHandler.MAX_STEP_COUNT);
			} else if(mSensorY < -6) {
				mControlHandler.requestTurnCount(-ControlHandler.MAX_STEP_COUNT);
			} else {
				mControlHandler.requestTurnCount((int)(mSensorY/(float)ControlHandler.RESOLUTION));
			}
		}
	}
}
