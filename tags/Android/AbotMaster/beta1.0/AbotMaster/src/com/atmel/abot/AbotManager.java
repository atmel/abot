package com.atmel.abot;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/** AbotManager manages the AbotBridge connection 
 * and also the number of Abots connected to the Coordinator (RZUSBSTICK). */
public class AbotManager {

	/** Context */
	private Context mContext;
	
	/** Connection related members */
	private Socket mSocket = null;
	private final static int SOCKET_TIMEOUT = 5000;
	
	/** Abot Coordinator (RZUSBSTICK) */
	private AbotCoordinator mAbotCoordinator = null;  // Need to be initialized when creatng socket
	private Abot mSelectedAbot = null;                // Need to create when a Abot is choosen

	private ArrayList<AbotBridgeConnectionListener> mConnectionListeners = new ArrayList<AbotBridgeConnectionListener>();
	
	/** Broadcast strings to be used to intimate connection lost */
	public static final String ABOT_CONNECTION_LOST_ACTION = "com.atmel.abot.ABOT_CONNECTION_LOST";
	
	/** Constructor to get context */
	public AbotManager(Context context) {
		mContext = context;
	}
	
	/** Connect to 'abotbridge' with the given IP address and Port number */
	public boolean connect(String serverIp, String serverPortString) {
		// Try disconnecting if it's already connected
		disconnect();
		Log.d("AbotManager","Server IP - " + serverIp +", Server Port - " + serverPortString);
		InetAddress serverAddress;
		int serverPort;
		
		try {
			serverAddress = InetAddress.getByName(serverIp);
		} catch (UnknownHostException e) {
			// Invalid IP Address; Host not found;
			Log.d("AbotManager","Invalid IP Address; Host not found");
			e.printStackTrace();
			return false;
		}
		serverPort = Integer.parseInt(serverPortString);
		// Check whether the socket is already connected or not
		if(isConnected()) {
			return true;
		}
		// Create socket here
		Log.d("AbotManager","Creating socket...");
		try {
			mSocket = new Socket(serverAddress, serverPort);
			mSocket.setSoTimeout(SOCKET_TIMEOUT);
			
			// Initialize AbotCoordinator with input and output stream of the socket
			mAbotCoordinator = new AbotCoordinator(mSocket.getOutputStream(),mSocket.getInputStream());
			Log.d("AbotManager","Socket created successfully");
			
		} catch (IOException e) {
			Log.d("AbotManager","Error creating socket");
			e.printStackTrace();
			// Error creating socket; May be abotBridge not running !
			return false;
		}
		// Ping "AbotBridge" to ensure it is responding !
		if (pingAbotBridge()) {
			// Got connected to AbotBridge successfully, inform all listeners
			for (AbotBridgeConnectionListener listener : mConnectionListeners) {
				synchronized(listener) {
					listener.onConnect();
				}
			}
			return true;
		}
		return false;
	}
	
	/** Check connection with 'Abotbridge' */
	public boolean isConnected() {
		Log.d("AbotManager","isConnected()");
		if(mSocket == null) {
			return false;
		} else {
			return pingAbotBridge();
		}
	}
	
	/** Disconnect from abotbridge */
	public boolean disconnect() {
		Log.d("AbotManager","disconnect()");
		if(mSocket != null) {
			try {
				mSocket.close();
				mSocket = null;
				mAbotCoordinator = null;
			} catch (IOException e) {
				e.printStackTrace();
				// Error in closing socket
			}
		}
		mSocket = null;
		// Got disconnected from AbotBridge, inform all listeners
		for (AbotBridgeConnectionListener listener : mConnectionListeners) {
			synchronized(listener) {
				listener.onDisconnect();
			}
		}
		return true;
	}
	
	/** Gets array of IDs of the available Abots */
	byte[] getAvailableAbots() {
		byte[] abotIds = mAbotCoordinator.getAvailableAbots();
		return abotIds;
	}
	
	/** Selects a particular Abot */
	public boolean selectAbot(int abotId) {
		try {
			// Create Abot object
			mSelectedAbot =  new Abot(mSocket.getOutputStream(),mSocket.getInputStream(),(byte)abotId);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/** Selects all available abots (eventually uses broadcast to send commands) */
	public boolean selectAllAbots() {
		try {
			mSelectedAbot = new Abot(mSocket.getOutputStream(),mSocket.getInputStream());
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/** Gets the already selected Abot */
	Abot getSelectedAbot() {
		return mSelectedAbot;
	}
	
	/** Callback function called to intimate connection lost */
	private void lostConnection() {

		// Got disconnected from AbotBridge, inform all listeners
		Log.d("AbotManager","lostConnection");
		for (AbotBridgeConnectionListener listener : mConnectionListeners) {
			synchronized(listener) {
				listener.onDisconnect();
			}
		}
	}
	
	abstract static class AbotProtocol {
		
		protected static final byte ABOT_BROADCAST_ADDR = 0x2A;
		
		/** Source and destination Abot IDs */
		protected byte mSourceId = (byte) 0x89;    // default value for the Abot Remote (Android device)
		protected byte mDestId = ABOT_BROADCAST_ADDR;     		   // default value (Broadcast to all Abots)
		
		/** Output and Input Stream to send Abot frames */
		protected OutputStream mOut = null;
		protected InputStream mIn = null;
		
		/** Abot Protocol elements */
		protected static final byte MAX_PAYLOAD = 0x2f;
		
		protected static final byte SOF = 0x01;
		protected static final byte EOT = 0x04;
		protected static final byte FRAME_OVERHEAD = 5;
		
		/** Abot Commands */
		protected static final byte BELL  	= 0x07;
		protected static final byte ENQ   	= 0x05;
		protected static final byte PING_A 	= 0x3F;
		
		/** Command to query active Abots in network */
		protected static final byte CMD_NWK_QUERY = 0x30;                  // temporary value
		
		/** Direction commands to Abot */
		protected static final byte JOYSTICK = 0x50;
			protected static final byte KEY_STATE_UP 	= 0x01;
			protected static final byte KEY_STATE_DOWN 	= 0x02;
			protected static final byte KEY_STATE_LEFT 	= 0x04;
			protected static final byte KEY_STATE_RIGHT = 0x08;
		
		/** Motor commands to Abot */
		protected static final byte ABOT_MOTOR 	= 0x51;
			protected static final byte CALIBRATE_P 	= 0x02;
			protected static final byte CALIBRATE_N 	= 0x03;
			protected static final byte CAL_SAVE    	= 0x04;
			protected static final byte STOP 		  	= 0x05;
		
		protected static final byte ABOT_MOTOR_DIRECTION = 0x60;
			/** Sub commands for motor speed & direction control */
			protected static final byte FORWARD			= 0x06;
			protected static final byte REVERSE			= 0x07;
			protected static final byte LEFT_FORWARD	= 0x08;
			protected static final byte RIGHT_FORWARD	= 0x09;
			protected static final byte LEFT_REVERSE	= 0x0A;
			protected static final byte RIGHT_REVERSE 	= 0x0B;
		
		protected static final byte DISPLAY_TEXT = 0x52;
		
		protected static final byte GET_ABOT_SENSOR = 0x53;
		protected static final byte ABOT_SENSOR 	= 0x54;
		/** ABOT sensor codes */
		protected static final byte ALL_SENSORS			= 0x01; 	// All available sensors
			protected static final byte SUPPLY_VOLTAGE		= 0x02; // ABOT supply voltage (battery voltage) in mV
			protected static final byte VOLTAGE_0_VCC		= 0x03; // ABOT external voltage reading connected to pin J401.6
			protected static final byte VOLTAGE_0_5VCC		= 0x04; // ABOT external voltage reading connected to pin J401.5
			protected static final byte TEMP_SENSOR			= 0x05; // ABOT temperature in degree C x 10
			protected static final byte LIGHT_SENSOR		= 0x06; // NA
			protected static final byte AUDIO_TUNE_LIST		= 0x07; // ABOT returns play list
			protected static final byte AUDIO_TUNE_PLY		= 0x08; // ABOT play tune
			protected static final byte AUDIO_TUNE_STOP		= 0x09; // ABOT play tune
		
		/** Abot Responses */
		protected static final byte ACK 		= 0x06;
		protected static final byte NACK 		= 0x15;
		protected static final byte NO_SOH		= 0x01;
		protected static final byte NO_EOT		= 0x02;  
		protected static final byte A_TIMEOUT	= 0x03;
		protected static final byte P_TO_LONG	= 0x04;
		protected static final byte BUF_EMPTY	= 0x05;
		
		protected static int retryCount = 0;
		
		/** Constructor to initialize the input and output streams */
		public AbotProtocol(OutputStream outputStream, InputStream inputStream) {
			if(outputStream != null && inputStream != null) {
				mOut = outputStream;
				mIn = inputStream;
			}
		}
		
		/** To Change destination Abot dynamically */
		public void setDestinationAbot(int abotId) {
			mDestId = (byte) abotId;
		}

		/** Returns true if multiple abots are selected i.e the communication is broadcast */
		public boolean isMultipleAbotSelected() {
			return (mDestId == ABOT_BROADCAST_ADDR);
		}
		
		/** Wraps the given command and payload in Abot frame 
		 * and writes them out to the socket output stream */
		protected boolean sendCommand(byte[] data, int length) {
			ByteBuffer buffer = ByteBuffer.allocate(FRAME_OVERHEAD+length);
			buffer.put(SOF);
			buffer.put(mDestId);
			buffer.put(mSourceId);
			buffer.put((byte)(length-1));                // Payload length excluding command byte
			buffer.put(data);
			buffer.put(EOT);
			try {
				// Before sending command flush out receiver buffer
				if(mIn.available() > 0) {
					Log.d("AbotProtocol","Flushing input, bytes - " + mIn.available());
					mIn.read(new byte[mIn.available()]);                // Need to find a better way
				}
				mOut.write(buffer.array());
			} catch (IOException e) {
				e.printStackTrace();
				// Inform 'connection lost'
				lostConnection();
				return false;
			}
			// Successfully sent data
			return true;
		}
		
		/** Receives response frame and parses it to return response cmd and payload */
		protected byte[] receiveResponse() {
			Log.d("AbotProtocol","receiveResponse");
			// Check SOF
			int length = 0;
			int data = 0;
			try {
				data = mIn.read();
				if(data == -1) {
					Log.d("receiveResponse","No data read, end of stream reached");
					return null;
				}
				if((byte)data != SOF) {
					Log.d("receiveResponse","No SOF, " + "data - " + data);
					return null;
				}
				// Check if the frame is meant for me
				data = mIn.read(); 
				if(data == -1) {
					Log.d("receiveResponse","No data read, end of stream reached");
					return null;
				}
				if((byte)data  != mSourceId) {
					Log.d("receiveResponse","Frame is not for me, " + "data - " + data );
					return null;
				}
				// Source address 
				int temp = mIn.read();
				if(temp == -1) {
					Log.d("receiveResponse","No data read, end of stream reached");
					return null;
				}
				Log.d("receiveResponse","Source ID - " + temp);
				// Read payload length and get data
				length = mIn.read();
				if(length == -1) {
					Log.d("receiveResponse","No length");
					return null;
				} 
				// Create byte buffer to receive data
				byte[] receiveBuffer = new byte[length+1];            // command + payload
				mIn.read(receiveBuffer,0,length+1);
				 
				
				data = mIn.read();
				if(data == -1) {
					Log.d("receiveResponse","No data read, end of stream reached");
					return null;
				}
				if((byte)data != EOT) {
					Log.d("receiveResponse","No EOT, " + "data - " + data);
					return null;
				}
				// Successfully read data
				return receiveBuffer;
			} catch (IOException e) {
				e.printStackTrace();
				Log.d("receiveResponse","Exception");
				// Inform 'connection lost'
				lostConnection();
				return null;
			}	
			
		}
		
		protected boolean isAckReceived() {
			Log.d("AbotManager","isAckReceived()");
			byte[] temp = receiveResponse();
			if(temp==null) {
				Log.d("Response","Null :-(");
				return false;
			} else {
				Log.d("Response", " " + temp[0]);
				if(temp[0]!=ACK) {
					if(temp[0] == NACK && retryCount == 3) {
						Log.d("AbotManager","Negative Acknowledgement");
						retryCount = 0;
						lostConnection();
					} else {
						retryCount++;
					}
					return false;
				} else {
					retryCount = 0;
					return true;
				}
			}
		}
		
		abstract protected void lostConnection();
		
	}
	
	class Abot extends AbotProtocol {
		
		public static final byte FORWARD = 0x0C;
		public static final byte REVERSE = 0x0D;
		
		private static final byte MULTIPLE_ABOT_SPEED = (byte) 200;
		
		/** Constructor which creates Abot Object to address all abots (Broadcast frames) */
		public Abot(OutputStream outputStream, InputStream inputStream) {
			super(outputStream,inputStream);
		}
		
		/** Constructor to create uniquely identified Abot */
		public Abot(OutputStream outputStream, InputStream inputStream,byte abotId) {
			super(outputStream,inputStream);
			// Set Abot ID to uniquely identify it.
			mDestId = abotId;
		}
		
		/** Increases Abot speed by one unit in forward direction */
		public boolean forward() {
			if (isMultipleAbotSelected()) {
				// Drive multiple abots with constant speed 
				return forward(MULTIPLE_ABOT_SPEED);
			}
			byte[] data = new byte[2];
			data[0] = JOYSTICK;
			data[1] = KEY_STATE_UP;
			if(sendCommand(data,2)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Sets Abot in forward motion with the given parameter as speed (speed range 0-255)*/
		public boolean forward(byte value) {
			byte[] data = new byte[3];
			data[0] = ABOT_MOTOR_DIRECTION;
			data[1] = AbotProtocol.FORWARD;
			data[2] = value;
			if(sendCommand(data,3)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Increases Abot speed by one unit in reverse direction or decreases Abot speed by one unit if 
		 * Abot is already running in forward direction */
		public boolean reverse() {
			if (isMultipleAbotSelected()) {
				// Drive multiple abots with constant speed 
				return reverse(MULTIPLE_ABOT_SPEED);
			}
			byte[] data = new byte[2];
			data[0] = JOYSTICK;
			data[1] = KEY_STATE_DOWN;
			if(sendCommand(data,2)) {
				return isAckReceived();				
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Sets Abot in reverse motion with the given parameter as speed (speed range 0-255)*/
		public boolean reverse(byte value ) {
			byte[] data = new byte[3];
			data[0] = ABOT_MOTOR_DIRECTION;
			data[1] = AbotProtocol.REVERSE;
			data[2] = value;
			if(sendCommand(data,3)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Decreases left motor speed by one unit to turn the Abot in left direction (Differential driving) */
		public boolean left() {
			byte[] data = new byte[2];
			data[0] = JOYSTICK;
			data[1] = KEY_STATE_LEFT;
			if(sendCommand(data,2)) {
				return isAckReceived();				
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Drives Abot left with the angle of turning given by the parameter (range 0-255) */
		public boolean left(byte value, byte direction) {
			Log.d("Abot","Left - " + value);
			byte[] data = new byte[3];
			data[0] = ABOT_MOTOR_DIRECTION;
			data[1] = (direction == FORWARD) ? LEFT_FORWARD : (direction == REVERSE) ? LEFT_REVERSE : 0;
			data[2] = value;
			if(sendCommand(data,3)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Decreases right motor speed by one unit to turn the Abot in right direction (Differential driving) */
		public boolean right() {
			byte[] data = new byte[2];
			data[0] = JOYSTICK;
			data[1] = KEY_STATE_RIGHT;
			if(sendCommand(data,2)) {
				return isAckReceived();				
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Drives Abot right with the angle of turning given by the parameter (range 0-255) */
		public boolean right(byte value, byte direction) {
			Log.d("Abot","Right - " + value);
			byte[] data = new byte[3];
			data[0] = ABOT_MOTOR_DIRECTION;
			data[1] = (direction == FORWARD) ? RIGHT_FORWARD : (direction == REVERSE) ? RIGHT_REVERSE : 0;
			data[2] = value;
			if(sendCommand(data,3)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		
		}
		
		/** Stops Abot by halting both the motors */
		public boolean stop() {
			byte[] data = new byte[2];
			data[0] = ABOT_MOTOR;
			data[1] = STOP;
			if(sendCommand(data,2)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Increases Abot motor calibration offset by one unit */
		public boolean calibratePositive() {
			byte[] data = new byte[2];
			data[0] = ABOT_MOTOR;
			data[1] = CALIBRATE_P;
			if(sendCommand(data,2)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Decreases Abot motor calibration offset by one unit */
		public boolean calibrateNegative() {
			byte[] data = new byte[2];
			data[0] = ABOT_MOTOR;
			data[1] = CALIBRATE_N;
			if(sendCommand(data,2)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		/** Sends command to Abot to store the motor calibration offset in non-volatile memory of the Abot controller (on chip EEPROM) */
		public boolean saveCalibration() {
			byte[] data = new byte[2];
			data[0] = ABOT_MOTOR;
			data[1] = CAL_SAVE;
			if(sendCommand(data,2)) {
				return isAckReceived();
			} else {
				// Error in sending data :-(
				return false;
			}
		}
		
		@Override
		protected void lostConnection() {
			// Inform AbotManager about the connection lost;  TODO: Need to find a better way of handling connection lost;
			
			Log.d("Abot","lostConnection . . .");
			AbotManager.this.lostConnection();
		}
		
	}
	
	class AbotCoordinator extends AbotProtocol {
		
		public AbotCoordinator(OutputStream outputStream,InputStream inputStream) {
			super(outputStream, inputStream);
			mDestId = 0x42;
		}
		
		/** Ping AbotCoordinator (RZUSBSTICK) to ensure the connection is OK */
		public boolean ping() {
			Log.d("AbotCoordinator","ping()");
			byte[] data = {PING_A};
			if(sendCommand(data,1)){
				return isAckReceived();
			} else {
				Log.d("AbotCoordinator","No ACK received");
				// Error in pinging
				return false;
			}
			
		}
		
		/** Reads the Active abots that the RZUSBSTICK can see in its network and returns array of the Abot IDs */
		public byte[] getAvailableAbots() {
			byte[] data = {CMD_NWK_QUERY};
			if(sendCommand(data,1)) {
				byte[] response = receiveResponse();
				if(response != null) {
					Log.d("AbotCoordinator","Number of Abots found - " + (response.length-1));
					if((response.length-1) <=0) {
						// No Abots found
						return null;
					}
					byte[] abotIds = new byte[response.length-1];
					System.arraycopy(response, 1, abotIds, 0, response.length-1);
					return abotIds;
				} else {
					// No response received :-(
					return null;
				}
			} else {
				// Unable to send command :-(
				return null;
			}
		}

		@Override
		protected void lostConnection() {
			// Intimate AbotManager about the connection lost ;   - Need to find a better way;
			AbotManager.this.lostConnection();
			
		}
	}
	
	private boolean pingAbotBridge() {
		int retry = 4;
		Log.d("AbotManager","pingAbotBridge");
		if(mAbotCoordinator==null) {
			Log.d("AbotManager","mAbotCoordinator is null");
			return false;
		} else {
			while(retry >= 0){
				retry--;
				if(mAbotCoordinator.ping()) {
					return true;
				}
			}
			// AbotCoordinator Socket may be closed or RZUSBSTICK got stuck
			mAbotCoordinator = null;
			mSelectedAbot = null;
			mSocket = null;
			return false;
		}
	}
	
	/** Interface to be implemented by object which needs to know the connection 
	 * status of Abotbridge */
	public interface AbotBridgeConnectionListener {
		
		/** Callback function called when connection with Abotbridge is established */
		public void onConnect();
		
		/** Callback function called when connection with Abotbridge is lost */
		public void onDisconnect();
			
	}
	
	/** Registers an AbotBridgeConnectionListener */
	public boolean registerListener(AbotBridgeConnectionListener listener) {
		if (listener == null) {
			return false;
		}
		this.mConnectionListeners.add(listener);
		return true;
	}
	
	/** Unregisters an AbotBridgeConnectionListener */
	public boolean unregisterListener(AbotBridgeConnectionListener listener) {
		if (listener == null) {
			return false;
		}
		this.mConnectionListeners.remove(listener);
		return true;
	}
	
	@Override
	protected void finalize() throws Throwable {
		// Close Socket
		if(mSocket != null) {
			try {
				mSocket.close();
				mSocket = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		super.finalize();	
	}
	
	
}

