package com.atmel.abot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

public class MotorCalibrationActivity extends BaseControlActivity {

	/** Dialog IDs */
	private static final int PROMPT_SAVE_CALIBRATION = 0x10;
	private static final int FAILED_SAVE_CALIBRATION = 0x11;
	
	/** Control Buttons */
	private ImageButton mCalibrationPositiveButton;
	private ImageButton mCalibrationNegativeButton;
	private Button mMotorRunButton;
	private Button mMotorStopButton;
	private Button mCalibrationSaveButton;
	
	/** Separate thread for abot operations */
	HandlerThread mControlThread;
	
	Handler mControlHandler;
	
	/** Abot */
	AbotManager.Abot mAbot;
	
	
	class ControlHandler extends Handler {
		
		public static final long COMMAND_INTERVAL = 20;        // in milliseconds
		
		/** Commands for calibration */
		public static final int CALIBRATION_POSITIVE = 0x10;
		public static final int CALIBRATION_NEGATIVE = 0x11;
		public static final int CALIBRATION_SAVE     = 0x12;
		public static final int MOTOR_RUN            = 0x13;
		public static final int MOTOR_STOP           = 0x14;
		
		public ControlHandler(Looper looper) {
			super(looper);
		}
		
		private void exitActivityInUiThread() {
			
			Log.d("MotorCalibrationActivity","ControlHandler, exitActivityInUiThread()");
			runOnUiThread(new Runnable() {

				public void run() {
					finish();
				}
				
			});
		}
		
		private void notifyFailInUiThread() {
			
			Log.d("MotorCalibrationActivity","ControlHandler, notifyFailInUiThread() ");
			runOnUiThread(new Runnable() {
				
				public void run() {
					showDialog(FAILED_SAVE_CALIBRATION);;
				}
			});
		}
		public void handleMessage(Message message) {
			
			switch(message.what) {
			case CALIBRATION_POSITIVE:
				mAbot.calibratePositive();
				break;
			case CALIBRATION_NEGATIVE:
				mAbot.calibrateNegative();
				break;
			case CALIBRATION_SAVE:
				// Ensure calibration data is saved
				if(mAbot.saveCalibration()) {
					// On saving calibration the Activity can end.
					exitActivityInUiThread();
				} else {
					// Inform UI thread about this
					notifyFailInUiThread();
				}

				break;
			case MOTOR_RUN:
				mAbot.forward();
				break;
			case MOTOR_STOP:
				mAbot.stop();
				break;
			default:
					
				break;
			}
		}
	}
	
	/** Listener for calibration control buttons */
	View.OnClickListener mCalibrationControlListener = new View.OnClickListener() {
		
		public void onClick(View view) {
			
			if(view.equals(mCalibrationPositiveButton)) {
				mControlHandler.sendEmptyMessage(ControlHandler.CALIBRATION_POSITIVE);
			} else if(view.equals(mCalibrationNegativeButton)) {
				mControlHandler.sendEmptyMessage(ControlHandler.CALIBRATION_NEGATIVE);
			} else if(view.equals(mCalibrationSaveButton)) {
				mControlHandler.sendEmptyMessage(ControlHandler.CALIBRATION_SAVE);
			} else if(view.equals(mMotorRunButton)) {
				mControlHandler.sendEmptyMessage(ControlHandler.MOTOR_RUN);
			} else if(view.equals(mMotorStopButton)) {
				mControlHandler.sendEmptyMessage(ControlHandler.MOTOR_STOP);
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.motor_calibration);
		
		// Get Buttons
		mCalibrationPositiveButton = (ImageButton) findViewById(R.id.motor_calibration_positive_button);
		mCalibrationNegativeButton = (ImageButton) findViewById(R.id.motor_calibration_negative_button);
		mMotorRunButton = (Button) findViewById(R.id.motor_calibration_run_button);
		mMotorStopButton = (Button) findViewById(R.id.motor_calibration_stop_button);
		mCalibrationSaveButton = (Button) findViewById(R.id.motor_calibration_save_button);
		
		// Set onClickListener for all buttons
		mCalibrationPositiveButton.setOnClickListener(mCalibrationControlListener);
		mCalibrationNegativeButton.setOnClickListener(mCalibrationControlListener);
		mMotorRunButton.setOnClickListener(mCalibrationControlListener);
		mMotorStopButton.setOnClickListener(mCalibrationControlListener);
		mCalibrationSaveButton.setOnClickListener(mCalibrationControlListener);
		
		// Create control thread
		HandlerThread mControlThread = new HandlerThread("ControlThread");
		mControlThread.start();
		mControlHandler = new ControlHandler(mControlThread.getLooper());
	}
	
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		// Get Abot here
		mAbot = mAbotManager.getSelectedAbot();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		showDialog(PROMPT_SAVE_CALIBRATION);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		Log.d("MotorCalibrationActivity","onCreateDialog, Dialog ID - " + id);
		switch(id) {
		case PROMPT_SAVE_CALIBRATION:
			dialog = new AlertDialog.Builder(this)
							.setTitle(R.string.alert_dialog_save_motor_calibration_title)
							.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									mControlHandler.sendEmptyMessage(ControlHandler.CALIBRATION_SAVE);
								}
							})
							.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// Just exit the activity
									dialog.dismiss();
									finish();
								}
							})
							.create();
			break;
		case FAILED_SAVE_CALIBRATION:
			dialog = new AlertDialog.Builder(this)
							.setTitle(R.string.alert_dialog_failed_save_calibration_title)
							.setPositiveButton(R.string.alert_dialog_try_again, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									mControlHandler.sendEmptyMessage(ControlHandler.CALIBRATION_SAVE);
								}
							})
							.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// Just exit though the calibration data is not saved
									dialog.dismiss();
									finish();
								}
							})
							.create();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}
	
	@Override
	public void onDisconnect() {
		// Do nothing
	}
	
}
