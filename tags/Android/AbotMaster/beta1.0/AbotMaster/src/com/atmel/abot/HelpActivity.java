package com.atmel.abot;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class HelpActivity extends Activity {
	
	private TextView mHelpText;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        

        setContentView(R.layout.help);
        mHelpText = (TextView) findViewById(R.id.help_text);
        mHelpText.setMovementMethod(new ScrollingMovementMethod());
        
        /* Code to get Help from a text file placed in assets */
//        AssetManager assetManager = getAssets();
//        InputStream input;
//        String helpText = null;
//        
//        try {
//			input = assetManager.open("help.txt");
//			int size = input.available();
//			
//			byte[] buffer = new byte[size];
//			input.read(buffer);
//			input.close();
//			
//			helpText = new String(buffer);
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//        
//        mHelpText.setText(helpText);
    }
}
