package com.atmel.abot;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.util.Log;

/* Preference Activity to manage User Preferences */
public class AppPreferenceActivity extends PreferenceActivity implements
		OnSharedPreferenceChangeListener {

	ListPreference mNetworkPreference;
	EditTextPreference mIpAddressPreference;
	EditTextPreference mPortNumberPreference;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Preference", "Reached onCreate AbotPreferenceActivity");

		// Load preference from preference.xml file
		addPreferencesFromResource(R.xml.preferences);

		mNetworkPreference = (ListPreference) getPreferenceScreen()
				.findPreference(AppPreferences.KEY_NETWORK_PREFERENCE);
		mIpAddressPreference = (EditTextPreference) getPreferenceScreen()
				.findPreference(AppPreferences.KEY_IP_ADDRESS_PREFERENCE);
		mPortNumberPreference = (EditTextPreference) getPreferenceScreen()
				.findPreference(AppPreferences.KEY_PORT_NUMBER_PREFERENCE);
	}

	@Override
	public void onResume() {
		super.onResume();

		Log.d("Preference", "Reached onResume AbotPreferenceActivity");
		Log.d("Preference", "Values: "
				+ getPreferenceScreen().getSharedPreferences().getAll()
						.toString());

		SharedPreferences preferences = getPreferenceScreen()
				.getSharedPreferences();

		// Set Initial values
		if (Integer.parseInt(preferences.getString(
				AppPreferences.KEY_NETWORK_PREFERENCE, null)) == AppPreferences.OTHER_NETWORK) {
			// Other network selected
			mNetworkPreference.setSummary(R.string.other_network);
			Log.d("Preference", "Initial is \"Other network\"");
		} else {
			mNetworkPreference.setSummary(R.string.abot_virtual_router_ssid);
			Log.d("Preference", "Initial is Abot Virtual Router");
		}
			
		String ipAddress = preferences.getString(
				AppPreferences.KEY_IP_ADDRESS_PREFERENCE, null);
		if (ipAddress != null) {
			mIpAddressPreference.setSummary(ipAddress);
		}
		String portNumber = preferences.getString(
				AppPreferences.KEY_PORT_NUMBER_PREFERENCE, null);
		if (portNumber != null) {
			mPortNumberPreference.setSummary(portNumber);
		}

		// Register shared preference change listener 
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);

	}

	@Override
	public void onPause() {
		super.onPause();
		// Unregister shared preference change listener 
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(AppPreferences.KEY_NETWORK_PREFERENCE)) {
			Log.d("Preference", "Network preference changed");
			if (Integer.parseInt(sharedPreferences.getString(
					AppPreferences.KEY_NETWORK_PREFERENCE, null)) == AppPreferences.OTHER_NETWORK) {
				// Other network selected
				mNetworkPreference.setSummary(R.string.other_network);
				Log.d("Preference", "Other network selected");
			} else {
				mNetworkPreference.setSummary(R.string.abot_virtual_router_ssid);
				Log.d("Preference", "Abot Virtual Router selected");
			}
		} else if (key.equals(AppPreferences.KEY_IP_ADDRESS_PREFERENCE)) {
			String ipAddress = sharedPreferences.getString(
					AppPreferences.KEY_IP_ADDRESS_PREFERENCE, null);
			if (ipAddress != null) {
				mIpAddressPreference.setSummary(ipAddress);
			}
		} else if (key.equals(AppPreferences.KEY_PORT_NUMBER_PREFERENCE)) {
			String portNumber = sharedPreferences.getString(
					AppPreferences.KEY_PORT_NUMBER_PREFERENCE, null);
			if (portNumber != null) {
				mPortNumberPreference.setSummary(portNumber);
			}
		}

	}

}
