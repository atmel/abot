package com.atmel.abot;

import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

public class CustomNetworkManager {
	
	/** Members to hold connection managers */
	private WifiManager mWifiManager;
	private ConnectivityManager mConnectivityManager;
	
	/** Abot Virtual Router Info */
	private static final String ABOT_ROUTER_SSID = "Abot Virtual Router";
	private static final String ABOT_ROUTER_KEY  = "12345678";
	
	/** Constructor which requires context */
	public CustomNetworkManager(Context context) {
		mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
	}
	/* */
	public boolean isWifiEnabled() {
		return mWifiManager.isWifiEnabled();
	}
	
	public boolean enableWifi() {
		return mWifiManager.setWifiEnabled(true);
	}
	
	public boolean isNetworkConnected() {
		NetworkInfo networkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return networkInfo.isConnected();
	}
	
	/** Finds whether the connected Wifi network is "Abot Virtual Router" */
	public boolean isAbotRouterConnected() {
		if(isNetworkConnected()) {
			WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
			return (wifiInfo.getSSID() != null && wifiInfo.getSSID().equals(ABOT_ROUTER_SSID));
		} else {
			Log.d("CustomNetworkManager","Network not connected");
			return false;
		}
		
	}
	
	public NetworkInfo getNetworkInfo() {
		return mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}
	
	public WifiInfo getWifiInfo() {
		return mWifiManager.getConnectionInfo();
	}
	
	/** Provides gateway IP address from the last successful DHCP request, if the network has used Automatic Private Addressing 
	 * then this will return null */
	public String getGatewayIpAddress() {
		Log.d("CustomNetworkManager",mWifiManager.getDhcpInfo().toString());
		String ipAddress = Formatter.formatIpAddress(mWifiManager.getDhcpInfo().gateway);
		if(ipAddress != "0.0.0.0") {
			return ipAddress;
		}
		return null;
	}
	public boolean configureAbotRouter() {
		// Check whether Abot Virtual Router configuration is added or not
		List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
		for(WifiConfiguration config : configs) {
			if(config.SSID.equals("\"" + ABOT_ROUTER_SSID + "\"")) {
				// Abot Virtual Router found in configured networks
				return true;
			}
		}
		// Abot Virtual Router not found in configured networks
		// Add configuration now
		return mWifiManager.addNetwork(getAbotNetworkConfiguration())==-1?false:true;		
	}
	
	private WifiConfiguration getAbotNetworkConfiguration() {
		WifiConfiguration config = new WifiConfiguration();
		config.SSID = "\"" + ABOT_ROUTER_SSID + "\"";
		config.preSharedKey = "\"" + ABOT_ROUTER_KEY + "\"";
		config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
		config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
		config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
		return config;
		
	}

}