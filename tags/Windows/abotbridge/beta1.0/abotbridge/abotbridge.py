# To get help execute "python abotbridege.py -h"

import socket
import sys
import atexit
import platform
import subprocess
from optparse import OptionParser
import threading
import thread
import os
import signal
import time
import binascii
import serial
import Queue
import SocketServer

from abot_protocol_parser import AbotUsbStick, FAILURE, SUCCESS

# Initialize all Globals
PROGRAM_NAME = "abotbridge"

# Create thread safe FIFO Queue to handle commands
globalQ = Queue.Queue();


class AbotBridgeHandler(SocketServer.BaseRequestHandler):
    """
    AbotBridgeHandler handles data sent from Android device through TCP socket
    The data sent from Android device is just put into a global thread safe queue.
    Response received in the response queue will be sent back to android device.
    """
    
    def handle(self):
        global gloablQ
        # Create a thread safe local queue to receive response
        self.respQ = Queue.Queue(5)
        print "Connection established with %s" % str(self.client_address)

        # Start infinte loop to handle commands continuously
        while True:
            self.data = self.request.recv(1024).strip()
            if self.data != '':
                globalQ.put((self.data,self.respQ))

                # Wait for response from 'response queue'
                response = self.respQ.get()
                if self.request.sendall(response) != None:
                    print "Response sending failed"
                    

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    """
    Multithreaded TCP Socket server
    """
    pass

class AbotControlServer(threading.Thread):
    """
    Server thread to process the commands available in the globla queue.
    It also starts the multi-threaded TCP socket server for the android devices to establish
    connections.
    """
    def __init__(self,serial,port=None,virtual_router=True):
        threading.Thread.__init__(self)
        #Daemonize thread
        self.setDaemon(True)
        self.serialHandle = serial
        
        #print socket.gethostbyname_ex(socket.gethostname())
        if virtual_router:
            self.serverIP = socket.gethostbyname_ex(socket.gethostname())[2][-1]
        else:
            #self.serverIP = socket.gethostbyname_ex(socket.gethostname())[2][0]
            self.serverIP = self.getWlanIpAddress()
            if self.serverIP == None:
                raise Exception("WiFi network not found !")
        if not port:
            port = 8000
        self.serverPort = int(port)

        # Invoke multi-threaded server
        self.server = ThreadedTCPServer((self.serverIP,self.serverPort),AbotBridgeHandler)

        #Provide serial object to AbotUsbStick()
        self.abotUsbStick = AbotUsbStick(self.serialHandle)
        if(SUCCESS != self.abotUsbStick.enterAndroidMode()):
            raise Exception("Contacting USB Stick failed")

    def getWlanIpAddress(self):
        """
        Obtains IP Address corresponding to the active wifi network.
        If no active network is found, 'None' is returned
        """
        import wmi
        c = wmi.WMI()
        for nic in c.Win32_NetworkAdapter():
            if nic.NetConnectionID and nic.NetConnectionID.startswith("Wireless Network Connection"):
		for config in nic.associators(wmi_result_class="Win32_NetworkAdapterConfiguration"):
			if config.IPAddress !=None:
                            return str(config.IPAddress[0])
                            
        
        return None

    def commandHandler(self):
        """
        Handler which processes the commands available in the Global queue.
        The response for the command is sent back to the thread using the
        response queue.  Response queue will be unique for each connection.
        """
        global globalQ
        while True:
            try:
                command,respQ = globalQ.get()
                response = self.abotUsbStick.handleCommand(command)
                #print "Response: " + binascii.hexlify(response)
                respQ.put(response)
            except serial.SerialException, err:
                if self.serialHandle != None:
                    self.serialHandle.close()
                print "\n\nConnection with USB Stick failed !"
                print "SerialException: ",str(err)
                thread.interrupt_main()
            except Exception,err:
                print "\n\nException: ",str(err)
                #self.abotUsbStick.exitAndroidMode()
                if self.serialHandle != None:
                    self.serialHandle.close()
                thread.interrupt_main()
            
    def run(self):
        # Create seperate thread for command handler
        self.handlerThread = threading.Thread(target=self.commandHandler)
        self.handlerThread.daemon = True
        self.handlerThread.start()
        
        print "Command Handler started in a thread\n"
        
        # Start serving connection requests from Android devices
        print "Server listening on %s:%s" %(self.serverIP,self.serverPort)
        print "Press Ctrl+C to break"
        self.server.serve_forever()

    def exit(self):
        try:
            self.abotUsbStick.exitAndroidMode()
        except Exception,err:
            return
        
    def __del__(self):
        #self.server.close()
        pass

class VirtualRouter(object):
    """Class which uses "netsh.exe" to establish Virtual WiFi Access Point"""
    NWK_KEY  = "12345678"
    NWK_SSID = "Abot Virtual Router"
    MIN_KEY_LENGTH = 8
    ADMIN = "administrator privilege"

    def __init__(self,verbose=False):
        self.verbose = verbose
        #Check whether the system supports virtual wifi router
        #if not raise exception

        if(platform.system().lower() != "windows" or
           platform.release().lower() < "7"):
            raise Exception("Platform not supported")
        process = subprocess.Popen(args = ["netsh","wlan","show","networks"],
                                   shell = False,
                                   stdout = subprocess.PIPE,
                                   stderr = subprocess.PIPE )
        output = process.communicate()
        returncode = process.poll()
        if returncode <> 0:
            if  self.ADMIN in output[0]:
                raise Exception(output[0])
            raise Exception("WLAN not found/supported")

        #Set configure status to false
        self.configured = False
        self.started = False

    def configure(self,in_ssid=NWK_SSID,key=NWK_KEY):
        """
        Configures the Virtual Wifi Access Point with the given SSID and Network Key
        """
        if(len(key) < self.MIN_KEY_LENGTH):
            raise Exception("Key should be minimum of 8 characters")
        process = subprocess.Popen(args = ["netsh", "wlan", "set", "hostednetwork", "mode=allow",
                                           "ssid=%s" % in_ssid,
                                           "key=%s" % key],
                                   shell = False,
                                   stdout = subprocess.PIPE,
                                   stderr = subprocess.PIPE )
        output = process.communicate()
        returncode = process.poll()
        if returncode <> 0:
            if self.verbose:
                print "Stdout:",output[0]
                print "Stderr:",output[1]
            if  self.ADMIN in output[0]:
                raise Exception(output[0])
            raise Exception("Error configuring network")
        self.configured = True

    def start(self):
        """
        Starts the Virutal Router; if the router is not configured, it will be configured with default SSID and KEY
        """
        if(not self.configured):
            self.configure()
        process = subprocess.Popen(args = ["netsh", "wlan", "start", "hostednetwork"],
                                   shell = False,
                                   stdout = subprocess.PIPE,
                                   stderr = subprocess.PIPE )
        output = process.communicate()
        returncode = process.poll()
        if returncode <> 0 :
            if self.verbose:
                print "Stdout:",output[0]
                print "Stderr:",output[1]
            if  self.ADMIN in output[0]:
                raise Exception(output[0])
            raise Exception("Failed to start network")
        self.started = True

    def stop(self):
        """
        Stops Virtual Router.
        NB: If the system is allowed to sleep, then the virtual router will be stopped by windows automatically
        """
        process = subprocess.Popen(args = ["netsh", "wlan", "stop", "hostednetwork"],
                                   shell = False,
                                   stdout = subprocess.PIPE,
                                   stderr = subprocess.PIPE )
        output = process.communicate()
        returncode = process.poll()
        if returncode <> 0:
            if  self.ADMIN in output[0]:
                raise Exception(output[0])
            if(self.started):
                if self.verbose:
                    print "Stdout:",output[0]
                    print "Stderr:",output[1]
                raise Exception("Failed to stop network")
        self.started = False
        
    def __del__(self):
        self.stop()
        

def main():
    USAGE = "Usage: %prog [OPTIONS]"
    EXAMPLE = """Example: %s -c COM46 """ % PROGRAM_NAME
    parser = OptionParser(usage = USAGE, epilog = EXAMPLE)

    parser.add_option("-c","--com-port",dest="com_port",metavar="<COM port>",
                      help="COM port of RZUSBSTICK"
                      )
    parser.add_option("-p","--tcp-port",dest="tcp_port",metavar="<server port>",
                      help="Server port to listen on"
                      )
    parser.add_option("-v","--virtual-router",action="store_true",dest="virtual_router",
                      default = False, help = "Create virtual router (Use this option only when there is no WiFi network available)"
                      )

    # Parse the given command line options 
    (options,args) = parser.parse_args()

    # Handle invalid options
    if not (options.com_port or options.tcp_port or options.virtual_router):
        parser.print_help()
        sys.exit(1)

    if (not options.com_port):
        print "No COM port provided"
        sys.exit(1)

    virtual_router = options.virtual_router

    #Try opening com port
    try:
        serial_port = serial.Serial(options.com_port)
    except serial.SerialException, err:
        print "Could not open %s" % options.com_port
        sys.exit(1)
    except Exception, err:                                   #Exception can be clubbed if required
        print "Error opening %s" % options.com_port
        sys.exit(1)

    if(virtual_router):
        #Try establishing Virtual Router
        try:
            router = VirtualRouter()
            router.configure()
            router.start()
        except Exception, err:                              
            print err
            serial_port.close()
            sys.exit(1)


    #Try creating control server
    try:
        if(virtual_router):
        # This sleep period is required for the IP Address to get reflected in the API call after the
        # virtual router is established
            time.sleep(4)
        
        if options.tcp_port:
            server = AbotControlServer(serial_port,options.tcp_port,virtual_router=virtual_router)
        else:
            server = AbotControlServer(serial_port,virtual_router=virtual_router)
        # Start the server
        server.start()
        # Stay in infinite loop to hold the server threads alive
        while(True):
            time.sleep(0.1)
            
    except KeyboardInterrupt, err:
        print err
        server.exit()
        serial_port.close()
        # Kill process
        sys.exit(1)
    except serial.SerialException, err:
        print "Could not connect to %s" % options.com_port
        serial_port.close()
        sys.exit(1)
    except Exception, err:
        serial_port.close()
        print err
        print "Error in running server"
        sys.exit(1)
        
    
if __name__ == "__main__":
    main()
                
                        
        
