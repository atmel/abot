import os,sys,re
import datetime, time
import binascii


import serial, array

FAILURE                 =   (0x00)
SUCCESS                 =   (0x01)

SOH                     =   (0x01)
SOH_POS                 =   (0x00)
EOT                     =   (0x04)
CMD_POS                 =   (0x02)
PAYLOD_POS              =   (0x03)
PINGRADIO               =   (0x37)
ERROR                   =   (0x40) # Payload contain error number

#Error definitions
NULL                    =   (0x00)
NO_SOH			=   (0x01) # SOH expected 
NO_EOT			=   (0x02) # EOT expected 
A_TIMEOUT		=   (0x03) # Timeout while waiting for RX char 
P_TO_LONG		=   (0x04) # Payload longer than MAX_PAYLOAD
BUF_EMPTY		=   (0x05) # rx_buf empty when trying to get char


CMD_ENTER_ANDROID_MODE  =    [0x0D] #Ctrl+m
CMD_EXIT_ANDROID_MODE   =    [0x03] #Ctrl+c; command = 0x03

CMD_PING                =   [0x01, 0x42, 0x42, 0x00,0x10,0x04]
CMD_NWK_QUERY           =   [0x01, 0x42, 0x42, 0x00,0x30,0x04]
CMD_ENQ                 =   [0x01, 0x42, 0x42, 0x00,0x05,0x04]
CMD_MOVE_UP             =   [0x01, 0x42, 0x42, 0x00,0x41,0x04]
CMD_MOVE_DOWN           =   [0x01, 0x42, 0x42, 0x00,0x42,0x04]
CMD_MOVE_LEFT           =   [0x01, 0x42, 0x42, 0x00,0x44,0x04]
CMD_MOVE_RIGHT          =   [0x01, 0x42, 0x42, 0x00,0x43,0x04]
CMD_MOVE_STOP           =   [0x01, 0x42, 0x42, 0x00,0x13,0x04]
CMD_PING_RESP           =   "Entering mobile application mode"
CMD_TEST                =   [0x01, 0x42, 0x42, 0x00,0x45,0x04]

ERROR_ABOT_BRIDGE       =   [0x01, 0x89, 0x42, 0x01,ERROR,0x00, EOT]

CMD_TEST_ERR            =   [0x01, 0x42, 0x42, 0x01,0x45,0x04]


def abot_error_string(error):
    ERROR_ABOT_BRIDGE[5] = error
    return ERROR_ABOT_BRIDGE
    
def Abot_protocol_parser(data):
    error = NULL
    if (data[SOH_POS] != chr(SOH)):
        error = NO_SOH
    #Frame is |SOH_POS(1byte)|src_id(1byte)|dst_id(1byte)|payload len(1byte)|cmd(1byte)|<payload-nbyte>|
    payload_size = ord (data[PAYLOD_POS])
    #Check for EOT!
    if (data[ 5 + payload_size ] != chr(EOT)):
        error = NO_EOT
        
    if(error == NULL):
        #parsing success
        return SUCCESS,0
    else:
        #parsing failed
        return FAILURE, abot_error_string(error)
    

class AbotUsbStick():
    """"
    ABOT to USBstick communication class
    """
    def __init__(self, ser_handle):
        self.ser = ser_handle
        self.ser.timeout =  5
        self.ser.writeTimeout =  5
        self.in_mobile_app_mode = 0x00 

    def _readResponse(self):
        """"Read response for commands from USBstick"""

        resp_string = self.ser.read()
        if resp_string == '':
            raise Exception("No response from USB Stick !")
        resp_SOH = binascii.hexlify(resp_string)
        
        resp_string = self.ser.read()
        if resp_string == '':
            raise Exception("Invalid response from USB Stick ! 1")
        resp_src = binascii.hexlify(resp_string)

        resp_string = self.ser.read()
        if resp_string == '':
            raise Exception("Invalid response from USB Stick ! 2")
        resp_dst = binascii.hexlify(resp_string)

        resp_string = self.ser.read()
        if resp_string == '':
            raise Exception("Invalid response from USB Stick ! 3")
        resp_payload_length = binascii.hexlify(resp_string)

        resp_string = self.ser.read()
        if resp_string == '':
            raise Exception("Invalid response from USB Stick ! 4")
        resp_cmd_byte = binascii.hexlify(resp_string)

        resp_string = self.ser.read(size = int(resp_payload_length))
        if resp_string == '' and int(resp_payload_length) != 0:
            raise Exception("Invalid response from USB Stick ! 5")
        pay_load_data = binascii.hexlify(resp_string)

        resp_string = self.ser.read()
        if resp_string == '':
            raise Exception("Invalid response from USB Stick !") 
        resp_EOT = binascii.hexlify(resp_string)
 
        protocol_data = (resp_SOH + resp_src + resp_dst + resp_payload_length + resp_cmd_byte + pay_load_data+resp_EOT)
        print "Response: ", protocol_data
        return binascii.unhexlify(protocol_data)
        
    def enterAndroidMode(self):
        """"ping USBstick and verify response"""        
        command_string = array.array('B', CMD_ENTER_ANDROID_MODE).tostring()
        self.ser.write(command_string)
        ping_response = self.ser.readline()
        print ping_response.strip()
        if(CMD_PING_RESP in ping_response.strip()):
            self.ser.readline() # Read the next line printed out.
            self.in_mobile_app_mode = 0x01
            return SUCCESS
        else:
            return FAILURE
                
    def exitAndroidMode(self):
        command_string = array.array('B', CMD_EXIT_ANDROID_MODE).tostring()
        self.ser.write(command_string)
        print self.ser.readline()
        self.in_mobile_app_mode = 0x00
        return       

    def handleCommand(self,command):
        """"ping USBstick and get response"""                
        command_string = array.array('B', command).tostring()
        print "Command: " + binascii.hexlify(command_string)
        status, message = Abot_protocol_parser(command_string)
        if (status == SUCCESS):
            pass
        else:
            return message        
        self.ser.write(command_string)
        abot_response = self._readResponse()
        return abot_response

    def exitComm(self):
        self.ser.close()
                                      

if __name__ == "__main__":
    # Test code
    ser_handle = serial.Serial("COM7")
    obj = AbotUsbStick(ser_handle)
    if(obj.in_mobile_app_mode):
        pass
    else:
        obj.enterAndroidMode()      
    obj.handleCommand(CMD_NWK_QUERY)
    obj.exitComm()


    
    
    
    
    
    
    
    
