/**
 * \file  MOTOR_SERVO_360.c
 *
 * \brief The ABOT servo motor component
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

//include HW related driver FW
#include <boards/board.h>
#if (BOARD==RAVEN)
	#include "board_RAVEN.h"
	#include <drivers/drv_1284_TC1_PWM.h>
#else
#error TARGET_BOARD must be defined somewhere
#endif

//Abot common includes
#include <services/ABOT_protocol.h>
#include "MOTOR_SERVO_360.h"


// Set motor speed i.a. set PWM pulse width 
void set_motor_speed(void)
{
//verify within max limits
		if (ABOT_PW_A > 1900) ABOT_PW_A= 1900;  // max 1900uS
		if (ABOT_PW_A < 1100) ABOT_PW_A= 1100;  // min 1100uS
		if (ABOT_PW_B > 1900) ABOT_PW_B= 1900;  // max 1900uS
		if (ABOT_PW_B < 1100) ABOT_PW_B= 1100;  // min 1100uS

		PWM_set_pulse_width_A(ABOT_PW_A);
		PWM_set_pulse_width_B(ABOT_PW_B);
}

/*
Writes calibration data 'ABOT_CAL_A' and 'ABOT_CAL_B' in mega1284 internal EEPROM
*/
inline void write_calibration(void)
{
	eeprom_write_word((uint16_t *)ABOT_CAL_A_ADDR,ABOT_CAL_A);
	eeprom_write_word((uint16_t *)ABOT_CAL_B_ADDR,ABOT_CAL_B);
	eeprom_write_word((uint16_t *)ABOT_CAL_SPEED_ADDR,ABOT_CAL_SPEED);
}

/*
Reads calibration data into 'ABOT_CAL_A' and 'ABOT_CAL_B' from EEPROM 
*/
inline void read_calibration(void)
{
	ABOT_CAL_A = eeprom_read_word((const uint16_t *)ABOT_CAL_A_ADDR);
	ABOT_CAL_B = eeprom_read_word((const uint16_t *)ABOT_CAL_B_ADDR);
	ABOT_CAL_SPEED = eeprom_read_word((const uint16_t *)ABOT_CAL_SPEED_ADDR);
}
/*
Motor reversed, ABOT turned, and motor in forward again
*/
void ABOT_reverse_turn_forward(void)
{
	extern unsigned char ABOT_int_flag;
	
	if ((!ABOT_STOPPED) && (ABOT_SWITCH_ENABLED)) // To avoid switch trig motor start when stopped and verify if function enabled
	{
	/* reverse direction & turn assuming ABOT going forward */
		ABOT_motor_speed_ctrl(REVERSE, 255);
		_delay_ms(2000);
		ABOT_motor_speed_ctrl(RIGHT_REVERSE, 255);
		_delay_ms(2000);
	/* Go forward again */
		ABOT_motor_speed_ctrl(FORWARD, 255);
	}		
	ABOT_int_flag=0x00;	// clr interrupt flag
}

/* Execute automatic commands triggered by any sensor on board
*/
void ABOT_auto(void)
{
	if (ABOT_AUTO_ENABLED)
	{
		ABOT_command(STOP);
	}
}

/*
Initialize ABOT motor
*/
void Motor_init (void)
{
	PWM_init ();	

	ABOT_PW_A= 1500; //Pulse Width A, 1500uS pulse width i.e. neutral
	ABOT_PW_B= 1500; //Pulse Width B, 1500uS pulse width i.e. neutral
	ABOT_STOPPED = 1;//Set STOP mode active 
	ABOT_SWITCH_ENABLED=0; //ABOT external switch action disabled
	ABOT_AUTO_ENABLED=0; ////ABOT automatic control enabled
	
	// Motor calibration, 
	read_calibration();
}

/* ABOT motor control
Pulse Width Control 20mS period
Direction: Clockwise/Pulse Traveling 1500 to 1900usec

Output A Right- starboard - motor
Output B Left - port side - motor
*/
void ABOT_motor_ctrl(unsigned char position)
{
	ABOT_STOPPED = 0;  //Clr stop mode
		
	switch (position) // Decode motor control command 
		{
			case KEY_STATE_UP:			// increase speed, go forward if stopped
				
				ABOT_PW_A= ABOT_PW_A+1;
				ABOT_PW_B= ABOT_PW_B-1;
				
				if (ABOT_PW_A == 1501){  //Go to stable forward speed from neutral
					ABOT_PW_A= 1600;
					ABOT_PW_B= 1400 - ABOT_CAL_B;
				}
				
			break;
			 
			case KEY_STATE_DOWN:		// decrease speed, go backwards if stopped
				
				ABOT_PW_A= ABOT_PW_A-1;
				ABOT_PW_B= ABOT_PW_B+1;
				
				if (ABOT_PW_A == 1499)  //Go to stable backward speed from neutral
				{
					ABOT_PW_A= 1400 - ABOT_CAL_A;
					ABOT_PW_B= 1600;
				}
						
			break; 
			
			case KEY_STATE_RIGHT: //Turn right, decrease speed on starboard  motor	
				if (ABOT_PW_A>1500) // if moving forward
				{
					ABOT_PW_A= ABOT_PW_A-10; 
				} else
				{
					ABOT_PW_A= ABOT_PW_A+10; 
				}										
			break;			
	 
			case KEY_STATE_LEFT: //Turn left, decrease speed on port side  motor
			
				if (ABOT_PW_B>1500) // if moving forward
				{
					ABOT_PW_B= ABOT_PW_B-10;
				} else				 
				{
					ABOT_PW_B= ABOT_PW_B+10;
				}	
			break;	
				
		}//Switch 
		set_motor_speed();
}

// Execute ABOT commands
void ABOT_command(unsigned char cmd)
{
	switch (cmd) // Decode motor control command 
	{

		case STOP: //stop motor
			ABOT_PW_A= 1500; //Pulse Width A, 1500uS pulse width i.e. neutral
			ABOT_PW_B= 1500; //Pulse Width B, 1500uS pulse width i.e. neutral
			ABOT_STOPPED = 1;  //ABOT i STOP mode
			set_motor_speed();		
		break;
			
		case CALIBRATE_P:
			ABOT_CAL_B = ABOT_CAL_B + 1;
			ABOT_CAL_A = ABOT_CAL_A + 1;
			//Forward speed during calibration, assuming motors close in offset
			ABOT_PW_A = 1650;
			ABOT_PW_B = 1350 - ABOT_CAL_B;
			set_motor_speed();
		break;	
			
		case CALIBRATE_N:
			ABOT_CAL_B = ABOT_CAL_B - 1;
			ABOT_CAL_A = ABOT_CAL_A - 1;	
			//Forward speed during calibration, assuming motors close in offset
			ABOT_PW_A = 1650;
			ABOT_PW_B = 1350 - ABOT_CAL_B;
			set_motor_speed();
		break;	
		
		case CALIBRATE_SPEED:
			ABOT_CAL_SPEED = ABOT_CAL_SPEED+10;
			ABOT_motor_speed_ctrl(FORWARD,255);
		break;
			
		case CAL_SAVE:
			write_calibration();
			// Stop Abot once calibration is done
			ABOT_PW_A= 1500; //Pulse Width A, 1500uS pulse width i.e. neutral
			ABOT_PW_B= 1500; //Pulse Width B, 1500uS pulse width i.e. neutral
			ABOT_STOPPED = 1;  //ABOT iN STOP mode
			set_motor_speed();	
		break;
		
		case ENABLE_SWITCH: ABOT_SWITCH_ENABLED=1; break; //ABOT external switch action enabled
		
		case DISABLE_SWITCH: ABOT_SWITCH_ENABLED=0; break; //ABOT external switch action disabled
		
		case ENABLE_AUTO: ABOT_AUTO_ENABLED=1; break; //ABOT automatic action enabled
		
		case DISABLE_AUTO: ABOT_AUTO_ENABLED=0; break; //ABOT automatic action disabled
			
	}//Switch 	
}		

/* ABOT motor control 
Pulse Width Control 20mS period
Direction: Clockwise/Pulse Traveling 1500 to 1900usec

Output A Right- starboard - motor
Output B Left - port side - motor
*/
void ABOT_motor_speed_ctrl(unsigned char cmd, unsigned char val)
{
	// NB: used '1640' and '1360' as initial value for motors to accommodate only 255 (8-bit value) levels in the control range i.e. 1640+255 = 1895 (near to 1900).
	
	ABOT_STOPPED = 0;  //Clr stop mode
	
	switch(cmd)	// Decode command
	{
		case FORWARD:
			//val = val - ABOT_CAL_SPEED;  // possible to adjust several Abots to same speed forward
			ABOT_PW_A = 1640 + val;
			ABOT_PW_B = 1360 - ABOT_CAL_B - val;
		break;
		
		case REVERSE:
			ABOT_PW_A = 1360 - ABOT_CAL_A - val;
			ABOT_PW_B = 1640 + val;
		break;
		
		case LEFT_FORWARD:
			if((val*2) > 300) // Set the turn to maximum angle (kind of saturation)
			{
				ABOT_PW_A = 1900;
				if((val*2) >= 400) 
				{
					ABOT_PW_B = 1500;
				} 
				else
				{
					ABOT_PW_B = 1400 - ABOT_CAL_B + ((val*2) - 300);
				}
				
			}
			else 
			{
				if((ABOT_PW_A - 1600) > (2*val))
				{
					ABOT_PW_B = 1400 - ABOT_CAL_B - (ABOT_PW_A - 1600 - 2*val);
				}
				else
				{
					ABOT_PW_B = 1400 - ABOT_CAL_B;
					ABOT_PW_A = 1600 + (2*val);
				}
				
			}
		break;
		
		case LEFT_REVERSE:
			if((val*2) > 300)
			{
				ABOT_PW_A = 1100;
				if((val*2) >= 400)
				{
					ABOT_PW_B = 1500;
				}
				else
				{
					ABOT_PW_B = 1600 - ((val*2) - 300);
				}
				
			}
			else
			{
				if((1400 - ABOT_CAL_A - ABOT_PW_A) > (2*val))
				{
					ABOT_PW_B = 1600 + (1400 - ABOT_PW_A - 2*val);
				}
				else
				{
					ABOT_PW_B = 1600;
					ABOT_PW_A = 1400 - ABOT_CAL_A - (2*val);
				}
			}
		break;
		
		case RIGHT_FORWARD:
			if((2*val) > 300)  // Set turning angle to maximum (kind of saturation)
			{
				ABOT_PW_B = 1100;
				if((val*2) >= 400)
				{
					ABOT_PW_A = 1500;
				}
				else
				{
					ABOT_PW_A = 1600 - ((val*2) - 300);
				}
			}
			else
			{
				if((1400 - ABOT_CAL_B - ABOT_PW_B) > (2*val))
				{
					ABOT_PW_A = 1600 + (1400 - ABOT_PW_B - (2*val));
				}
				else
				{
					ABOT_PW_A = 1600;
					ABOT_PW_B = 1400 - ABOT_CAL_B - (2*val);
				}
			}
		break;
		
		case RIGHT_REVERSE:
			if((2*val) > 300)  // Set turning angle to maximum (kind of saturation)
			{
				ABOT_PW_B = 1900;
				if((val*2) >= 400)
				{
					ABOT_PW_A = 1500;
				}
				else
				{
					ABOT_PW_A = 1400 - ABOT_CAL_A + ((val*2) - 300);
				}
				
			}
			else
			{
				if((ABOT_PW_B - 1600) > (2*val))
				{
					ABOT_PW_A = 1400 - ABOT_CAL_A - (ABOT_PW_B - 1600 - (2*val));
				}
				else
				{
					ABOT_PW_A = 1400 - ABOT_CAL_A;
					ABOT_PW_B = 1600 + (2*val);
				}
			}
		break;
		
		default:
			// Unknown command
		break;
	} // switch
	set_motor_speed();
}