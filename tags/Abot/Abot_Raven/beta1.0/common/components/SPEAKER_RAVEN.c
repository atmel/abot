/**
 * \file SPEAKER_RAVEN.c
 *
 * \brief 
 *
 * \author    
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#include <components/SPEAKER_RAVEN.h>
#include <avr/io.h>
#include <avr/interrupt.h>  // sei()
#include <stdbool.h>
#include <drivers/drv_3290_timer.h>
#include <avr/pgmspace.h>

									
const tone_t james_bond[] PROGMEM	=	{{tone_5G, 300}, {tone_5G, 100}, {tone_5Gh, 300}, {tone_5Gh, 100}, {tone_5A,300}, {tone_5A, 100}, {tone_5Gh, 300}, {tone_5Gh, 100},\
								{tone_5G, 300}, {tone_5G, 100}, {tone_5Gh, 300}, {tone_5Gh, 100}, {tone_5A,300}, {tone_5A, 100}, {tone_5Gh, 300}, {tone_5Gh, 100}}; //length = 16
									
const tone_t anthem[]		PROGMEM	=	{{tone_5C, 400}, {tone_5D, 400}, {tone_5E, 400}, {tone_5E, 400}, {tone_5E, 800}, {tone_5E, 400}, {tone_5E, 400},\
								{tone_5E, 800}, {tone_5E, 400}, {tone_5E, 400}, {tone_5D, 400}, {tone_5E, 400}, {tone_5F, 800},\
								{tone_5E, 800}, {tone_5E, 400}, {tone_5E, 400}, {tone_5D, 800}, {tone_5D, 400}, {tone_5C, 400}, {tone_4B, 400}, {tone_5D, 100}, {tone_5C, 400}};//length=22 
									
const tone_t marry[]		PROGMEM	=	{{tone_5B, 200}, {tone_5C, 200}, {tone_5B, 200}, {tone_5G, 400}, {tone_5E, 200}, {tone_5C, 400},\
								{tone_5F, 200}, {tone_5E, 400}, {tone_5C, 200}, {tone_4A, 400}, {tone_4B, 600}, {tone_5C, 600}\
								};	 //length = 8				


uint16_t nxt_tone;
bool tone_playing = false;
tune_t current_music_playing;


/*Play tone by generating PWM
*
*/
uint8_t play_tone( uint16_t tone)
{
	if(tone_playing)  {
		//tone is already playing
		return 0;
	}
	tone_playing = true;
	generate_pwm(VOLUME, tone);
	return 1;
}


/*Play the music completely in callback
*If all tones are played, set the music play status to false and return
*If new tune starts, set the music playing status to true
*Play current tone, updated tone index
*/
void play_music_callback(void)
{

	tone_t current_tone;
	//stop present tone
	stop_pwm();
	tone_playing = false;
	//Check if end of the music reached, change music_palying status to false
	//reset the current tone index to zero
	if (current_music_playing.current_tone_index >= current_music_playing.length )  {
		//current_music_playing.current_tone_index = 0;
		current_music_playing.music_playing = false;
		return;
	}	
	
	//Start a new music, change music_playing to trueD
	if (current_music_playing.current_tone_index == 0)	{
			//Music already playing. return
		if (current_music_playing.music_playing == true)	{
			return;
		}
		current_music_playing.music_playing = true;
	}	
	//play the current tone
	//register function in delay for playing next tone
	current_tone.tone = pgm_read_word(& (current_music_playing.tune[current_music_playing.current_tone_index].tone));
	current_tone.duration = pgm_read_word(& (current_music_playing.tune[current_music_playing.current_tone_index].duration));
	play_tone(current_tone.tone);			
	register_timer(play_music_callback, (current_tone.duration) );
	current_music_playing.current_tone_index++;	
}

/*	Stop music playing
*  \brief:  stop current tone and end the music play back by setting current tone index to end tone
*/
void stop_music(void)
{
	stop_pwm();
	current_music_playing.current_tone_index = current_music_playing.length;	
}

/*Play music using callback function
*brief: Pass pointer to the tune array and tune length
*Check if any tune is playing.
*Update current tune playing with new tune.
*/
void play_music(const tone_t *tune, uint8_t tune_length)
{
	if(current_music_playing.music_playing)  {
		//music play is going on
		return;
	}
	//initialize current tune
	current_music_playing.current_tone_index	= 0;
	current_music_playing.length				= tune_length;
	current_music_playing.tune					= tune;
	
	//play current music in callback mode
	//end of music play is indicated by current_music_playing.music_playing = false;
	play_music_callback();
}

void play_beep_callback(void)
{
	//stop play
	stop_pwm();
	tone_playing = false;
}
/*Play tone
*Play beep sound, get duration in "ms"
*/
void play_beep(uint16_t duration)
{
	generate_pwm(VOLUME, BEEP_TONE);
	register_timer(play_beep_callback, duration);	
}

void test_callback(void)
{
	PINB &= ~(1 << PINB7);
	uint16_t temp = 0;
}

void init_speaker (void)
{
	init_pwm_timer();
	init_generic_timer();
}


void test_speaker(void)
{		
	volatile int a = 0;
	sei();
	init_generic_timer();
	init_pwm_timer();
	play_music(anthem,22);
	//while(1)  {
		////compiler optimization workaround
		////register_timer(test_callback,100);
		//a++;
	//}	
}