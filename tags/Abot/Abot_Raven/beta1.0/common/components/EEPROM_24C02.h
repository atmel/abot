/*
 * EEPROM_24C02.h
 *
 * Created: 20/11/2011 10:35:33
 *  Author: TB
 */ 


#ifndef EEPROM_24C02_H_
#define EEPROM_24C02_H_

#define EEPROM_ADDRESS	(0x50)	//Raven


/*! \brief This address map is used when accessing variables stored in EEPROM 
 *       
 */
#define EE_MAGIC            0x42

#define EE_MAGIC_ADR        0
#define EE_MAGIC_SIZE       1

#define EE_PRODDAY_ADR      (EE_MAGIC_ADR + EE_MAGIC_SIZE)
#define EE_PRODDAY_SIZE     1

#define EE_PRODMONTH_ADR    (EE_PRODDAY_ADR + EE_PRODDAY_SIZE)
#define EE_PRODMONTH_SIZE   1

#define EE_PRODYEAR_ADR     (EE_PRODMONTH_ADR + EE_PRODMONTH_SIZE)
#define EE_PRODYEAR_SIZE    1

#define EE_MAC_ADR          (EE_PRODYEAR_ADR + EE_PRODYEAR_SIZE)
#define EE_MAC_SIZE         8

#define EE_CAPARRAY_ADR     (EE_MAC_ADR + EE_MAC_SIZE)
#define EE_CAPARRAY_SIZE    1

#define EE_1V1_CAL_VALUE_ADR   (EE_MAC_ADR + EE_MAC_SIZE)
#define EE_1V1_CAL_VALUE_SIZE  2

#define EE_HWREV_ADR        (EE_1V1_CAL_VALUE_ADR + EE_1V1_CAL_VALUE_SIZE)
#define EE_HWREV_SIZE       1

#define EE_BOOT_MAGIC_ADR (EE_HWREV_ADR + EE_HWREV_SIZE)
#define EE_BOOT_MAGIC_SIZE (1)
#define EE_BOOT_MAGIC_VALUE (53)

#define EE_LAST_ADR         (EE_BOOT_MAGIC_ADR + EE_BOOT_MAGIC_SIZE -1)

/*typedefs*/

// EEPROM data byte with status
typedef struct
{
	uint8_t data;
	uint8_t status;
} EEPROM_byte;


// Write one byte
uint8_t write_EEPROM_byte(uint8_t b_data, uint8_t b_address);

//Read one byte
EEPROM_byte read_EEPROM_byte(uint8_t b_address);

// read IEEE MAC address from EEPROM
uint64_t get_ieee_address(void);

#endif /* EEPROM_24C02_H_ */