/**
 * \file  Compass_AK8975.c
 *
 * \brief The Compass component
 *
 * \author    TB
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>


//include HW related driver FW
#include <boards/board.h>
#if (BOARD==RAVEN)
	#include <drivers/drv_1284_TWI.h>     //Sensor interface
	#include <drivers/drv_1284_PCINT0.h>  //DRDY interrupt handler
	#include <drivers/drv_AK8975_RAVEN.h> //HW related config for AK8975 on RAVEN
	#include "board_RAVEN.h"
#elif (BOARD==MEGA1284P_XPLAINED_BC)
	#include <drivers/drv_1284_TWI.h>
	#include <drivers/drv_1284_PCINT0.h>  //DRDY interrupt handler
#else
   #error BOARD must be defined somewhere
#endif

#include "Compass_AK8975.h"



/*
 DRDY pin indicates sensor measurement end and data is ready to be read. 
 
 Compass connected to TWI bus
 
*/
void Compass_init(void){
	
	extern unsigned char compass_int_flag;

	compass_int_flag = 0;  // clr interrupt flag 
	
	TWI_master_init(TWI_SPEED, FOSC); //initialize TWI

	AK8975_init();  //initialize HW related 
	
	DRDY_INT_enable();  //enable interrupt
	
}

/*! \brief  Compass sensor mode
 *
 * \param mode  mode to enter
 *
 
 AK8975/B has following four operation modes:
(1) Power-down mode
(2) Single measurement mode
(3) Self-test mode
(4) Fuse ROM access mode
The mode is selected by setting CNTL register MODE[3:0] bits.
After power-down mode is set, at least 100?s(Twat) is needed before setting another mode
 */
uint8_t set_compass_mode(uint8_t mode){

	uint8_t TWI_data_buff[TWI_MAX_DATA];	// TWI data buffer
	uint8_t *data_ptr;
	uint8_t bus_status;
	
	TWI_data_buff[0]= 0x0A;			// Address of CNTL register 
	TWI_data_buff[1]= mode;			// CNTL data, i.e. mode select bit's
	data_ptr=&TWI_data_buff[0];		// point to first byte to transmit
	
	bus_status = TWI_master_transmit(COMPASS_ADDRESS,2,data_ptr,'Y');		//Set compass mode

	return bus_status;
}

/*! \brief Get measurement raw data from Compass sensor
 *
 * \param field  raw data with status
 *
 */
compass_field get_compass_raw_data(void){
	
	uint8_t TWI_data_buff[TWI_MAX_DATA];	//TWI data buffer
	uint8_t *data_ptr;
	uint8_t bus_status;
	compass_field field;
	uint16_t t;
	
	TWI_data_buff[0]= 0x02;			// Address of first register to read from compass 
	data_ptr=&TWI_data_buff[0];		//point to first byte to transmit
	
	// Perform a READ from register 0x02 to 0x09, i.e. read measurement data with status
	
	bus_status = TWI_master_transmit(COMPASS_ADDRESS,1,data_ptr,'N');		//Set compass internal address counter to 0x02
	
	if (bus_status == 0x00){
		data_ptr=&TWI_data_buff[0]; //point to first byte to receive
		bus_status = TWI_master_receive(COMPASS_ADDRESS,8,data_ptr);	//Read compass register 0x02 to 0x09, 8 byte
	
		t = (TWI_data_buff[2]<<8);
		t = t | TWI_data_buff[1];
		field.X_axis= (float)t;
		//field.X_axis= (TWI_data_buff[2]<<8) | TWI_data_buff[1]; //X axis high and low byte combined to 16bit int
		field.Y_axis= (TWI_data_buff[4]<<8) | TWI_data_buff[3]; //Y axis high and low byte combined to 16bit int
		
		//t = (TWI_data_buff[4]<<8) | TWI_data_buff[3]; //Y axis high and low byte combined to 16bit int
		//field.Y_axis= (float)t;
		field.Z_axis= (TWI_data_buff[6]<<8) | TWI_data_buff[5]; //Z axis high and low byte combined to 16bit int
		field.sensor_status = TWI_data_buff[0] | TWI_data_buff[7]; //Status 1 and status 2 byte combined
	}	
	field.bus_status = bus_status;
	
/*
Measuremnet data is stored in two�s complement and Little Endian format. Measurement range of each axis is from -4096 to +4095 in decimal.

0FFF	4095	1229uT
0001	1		0.3uT
0000	0		0
FFFF	-1		-0.3uT
F000	-4096	-1229uT

*/	
	field.X_axis= field.X_axis * 0.3; //X axis raw to uT
	field.Y_axis= field.Y_axis * 0.3; //Y axis raw to uT
	field.Z_axis= field.Z_axis * 0.3; //Z axis raw to uT
	
	return field;
}


/*
// Compass heading data
typedef struct
{
	uint16_t	direction;		// Degrees from magnetic North (0-360)
	int8_t		inclination;	// Degrees from horizontal (-90 to +90) 
	int16_t		strength;		// Field strength in microtesla

} compass_heading;

*/
compass_heading calculate_heading (compass_field field){
	compass_heading heading;
	float t;
	
	//Calculate  strength (field magnitude).
	
	t = (field.X_axis*field.X_axis + field.Y_axis*field.Y_axis + field.Z_axis*field.Z_axis);
	t = sqrt(t);
	heading.strength = (int16_t)t;
	
	//Scale
	field.X_axis = field.X_axis/t;
	field.Y_axis = field.Y_axis/t;
	field.Z_axis = field.Z_axis/t;
	
	/* Calculate the direction angle (degrees) assuming no mounting tilt.
	 * "Wraparound" negative results to a positive heading.  The angle is
	 * calculated relative to +Y axis, so atan2() arguments are (x,y) instead
	 * of (y,x).  Result is inverted (* -1) so positive values reflect
	 * clockwise rotation.
	 */
	t = degrees(-1 * atan2(field.X_axis, field.Y_axis));

	if (t < 0) {
		t += 360;
	}
	heading.direction= (uint16_t)t;
	
	/* Calculate the inclination angle degrees (assuming no mounting tilt).
	 * Downward angle is indicated by a positive inclination value.
	 */
	heading.inclination = (int8_t)degrees(-1 * asin(field.Z_axis));
	
	return heading;
}
