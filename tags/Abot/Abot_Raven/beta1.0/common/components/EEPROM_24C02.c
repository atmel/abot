/*
 * EEPROM_24C02.c
 *
 * Created: 20/11/2011 10:34:52
 *  Author: TB
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <drivers/drv_1284_TWI.h>
#include "EEPROM_24C02.h"

// Write one byte
uint8_t write_EEPROM_byte(uint8_t b_data, uint8_t b_address)
{
	
	uint8_t TWI_data_buff[2];	//!< TWI data buffer
	uint8_t *data_ptr;
	uint8_t bus_status;
	
	TWI_data_buff[0]= b_address;		// byte memory location 
	TWI_data_buff[1]= b_data;			// data
	data_ptr=&TWI_data_buff[0];			//point to first byte to transmit
	
	bus_status = TWI_master_transmit(EEPROM_ADDRESS,2,data_ptr,'Y');	
	
	return bus_status;	
}

//Read one byte
EEPROM_byte read_EEPROM_byte(uint8_t b_address)
{
	
	uint8_t TWI_data_buff[2];	//!< TWI data buffer
	uint8_t *data_ptr;
	uint8_t bus_status;
	EEPROM_byte databyte;
	
	TWI_data_buff[0]= b_address;		// byte memory location 
	data_ptr=&TWI_data_buff[0];			//point to first byte to transmit
	
	bus_status = TWI_master_transmit(EEPROM_ADDRESS,1,data_ptr,'N');	// Sett address - no STOP
	
	if (bus_status == 0x00){
		data_ptr=&TWI_data_buff[0]; //point to first byte to receive
		bus_status = TWI_master_receive(EEPROM_ADDRESS,1,data_ptr);	//Read EEPROM byte
	}	
	
	databyte.data = TWI_data_buff[0];
	databyte.status = bus_status;	
	return databyte;	
}



uint64_t get_ieee_address(void) {
    
	EEPROM_byte e_byte;
	unsigned char t;
	/* Union used to convert from byte read EEPROM address to number. */
    union {
        uint64_t nmbr;
        uint8_t array[sizeof(uint64_t)];
    } address_conv;
    
    address_conv.nmbr = 0;
    
	for (t=0;t<8;t++)
	{
		e_byte = read_EEPROM_byte(EE_MAC_ADR + t);
		address_conv.array[7-t]= e_byte.data;
	}		
		
       // (bool)at24cxx_read_byte(EE_MAC_ADR + 0, &(address_conv.array[7]));

      //  (bool)at24cxx_read_byte(EE_MAC_ADR + 7, &(address_conv.array[0]));
   
    return address_conv.nmbr;
}

