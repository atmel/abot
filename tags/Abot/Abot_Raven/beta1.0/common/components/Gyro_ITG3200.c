/**
 * \file  Gyro_ITG3200.c
 *
 * \brief The Gyro component
 *
 * \author    SH
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

//include HW related driver FW
#include <boards/board.h>
#if (BOARD==RAVEN)
	#include "board_RAVEN.h"
	#include <drivers/drv_1284_TWI.h>     //Sensor interface
	#include <drivers/drv_1284_PCINT0.h>  // Interrupt handler
	#include <drivers/drv_ITG3200_RAVEN.h> //HW related config for BMA150 on RAVEN
#elif (BOARD==MEGA1284P_XPLAINED_BC)
	#include <drivers/drv_1284_TWI.h>
	#include <drivers/drv_1284_PCINT0.h>  // Interrupt handler
#else
   #error BOARD must be defined somewhere
#endif

#include "Gyro_ITG3200.h"

// Calibration values used to compensate for static bias
int32_t calibration_values_gyro_x = 0;
int32_t calibration_values_gyro_y = 0;
int32_t calibration_values_gyro_z = 0;

/**
 *   \brief This function will init the ITG3200 gyroscope 
 *   
 *   This function will setup the ITG3200 gyroscope itself and initialize the application reading gyro data.
 *    The gyroscope is connected to the TWI bus
 * 
 *
 * \returns Status code (0x00 if OK)
 *
*/
uint8_t gyro_init(void){
	
    extern uint8_t gyro_int_flag;
    uint8_t chip_id;
    uint8_t tmp;
    gyro_data_t gyro_data;
    
    // clr interrupt flag 
    gyro_int_flag = 0;  
	
	//initialize TWI
	TWI_master_init(TWI_SPEED, FOSC);
    
    //initialize HW related 
    itg3200_init(); 
	        
    // Check communication by reading the WHO_AM_I register
    if (gyro_read_registers(ITG3200_REG_WHO_AM_I, 1, &chip_id)){
            return ITG3200_STATUS_TWI_ERROR;
    }
    
    if ((chip_id & ITG3200_CHIP_ID_MASK) != ITG3200_CHIP_ID){
        return ITG3200_STATUS_INVALID_CHIP_ID;
    }

    // Set Sampling rate and low pass filter bandwidth, in addition to full scale range (must be set to 3 always)
    
    // Set Full scale range (must always be set to 3)
    tmp = 0x18;

    // Set low pass filter bandwidth and sampling rate
    tmp |= ITG3200_DLPF_CFG_1KHZ_20HZ;

    // Write the value to the DLPF register
    if (gyro_write_register(ITG3200_REG_DLPF, tmp)){
        // TWI error
        return ITG3200_STATUS_TWI_ERROR;
    }
    
    // Set the sample rate divider (Fsample = Finternal/(divider + 1), Finternal is given by the DLPF_CFG value set above, 1 kHz or 8 kHz)
    if (gyro_write_register(ITG3200_REG_SMPLRT_DIV, 99)){
        // TWI error
        return ITG3200_STATUS_TWI_ERROR;
    }
    
    // Enable data ready interrupt and set interrupt pin to active low
    tmp &= (1 << ITG3200_INT_CFG_BIT_ACTL) | (1 << ITG3200_INT_CFG_BIT_RAW_RDY_EN);
    
    // Write the value to the Interrupt Configuration register
    if (gyro_write_register(ITG3200_REG_INT_CFG, tmp)){
        // TWI error
        return ITG3200_STATUS_TWI_ERROR;
    }
	
    // Enable the gyro interrupt
    gyro_INT_enable();
    
    // TODO Switch clock to gyro MEMS oscillators. Remember to check that they are ready first (ITG_RDY, in Interrupt status register)

    // Calibrate gyroscope. Assuming accelerometer is not moving (i.e. robot standing still) 
    // we measure some gyro data which will be subtracted from the normal readings
    // Take 8 measurements
    for (uint8_t i = 0; i < 8; i++){
        uint8_t timeout;
		
		timeout = 100;

        // wait for next sample, note that interrupt pin is sampled directly in case interrupts are not enabled yet
        while ((PINA & (1 << PINA4)) && timeout){
            timeout--;
            _delay_ms(10);
        }

        if (timeout){
            gyro_get_raw_data(&gyro_data);
            
            calibration_values_gyro_x = calibration_values_gyro_x + gyro_data.x_axis;
            calibration_values_gyro_y = calibration_values_gyro_y + gyro_data.y_axis;
            calibration_values_gyro_z = calibration_values_gyro_z + gyro_data.z_axis;
        }
        else {
            return ITG3200_STATUS_TIMEOUT;
        }
    }
	calibration_values_gyro_x /= (int32_t) 8;
	calibration_values_gyro_y /= (int32_t) 8;
	calibration_values_gyro_z /= (int32_t) 8;
    return ITG3200_STATUS_OK;		
	
}

/*! \brief Write ITG3200 register
 *
 * \param address  Address of register to write
 * \param data Data to write to register
 *
 * \return Status code (0x00 if OK)
 */
uint8_t gyro_write_register(uint8_t address, uint8_t data){
    // TWI data buffer
    uint8_t TWI_data_buffer[TWI_MAX_DATA];
    
    TWI_data_buffer[0]= address;
    TWI_data_buffer[1]= data;
	
    if (TWI_master_transmit(ITG3200_ADDRESS,2,(uint8_t *) &TWI_data_buffer[0],'Y')){
        return ITG3200_STATUS_TWI_ERROR;
    }
    else {   
        return ITG3200_STATUS_OK;
    }
}

/*! \brief Read ITG3200 registers
 *
 * \param address  Address of first register to read
 * \param no_of_bytes Number of bytes to read (must be <= TWI_MAX_DATA)
 * \param *data Pointer to data buffer big enough to contain received data
 *
 * \return Status code (0x00 if OK)
 *
 */
uint8_t gyro_read_registers(uint8_t address, uint8_t no_of_bytes, uint8_t *data){
	// TWI data buffer
    uint8_t TWI_data_buffer[TWI_MAX_DATA];
    
    // First write address
    TWI_data_buffer[0]= address;
	
    if (TWI_master_transmit(ITG3200_ADDRESS, 1, (uint8_t *) &TWI_data_buffer[0],'Y')){
        // TWI error, just abort
        return ITG3200_STATUS_TWI_ERROR;
    }

    // Then do the read
    if (TWI_master_receive(ITG3200_ADDRESS, no_of_bytes, data)){
        // TWI error
        return ITG3200_STATUS_TWI_ERROR;
    }
    else{
        return ITG3200_STATUS_OK;
    }
}


/**
 *   \brief This function reads the raw gyro measurements data from the ITG3200 gyroscope
 *   
 * \param gyro_data Pointer to sturct where data will be stored
 * 
 * \returns Status code (0x00 if OK)
 *
*/
uint8_t gyro_get_raw_data(gyro_data_t *gyro_data){
    uint8_t data[6];
    uint8_t status;

    // Read  the data from the accelerometer, 6 consecutive bytes starting with high byte of x-axis
    status = gyro_read_registers (ITG3200_REG_GYRO_DATA_X_H, 6, &data[0]);
    
    if (status == ITG3200_STATUS_OK){
        // Populate the data struct
        (*gyro_data).x_axis = (int) ((data[0] << 8) & 0xFF00) | ((data[1] & 0xFF) >> 6);
        (*gyro_data).y_axis = (int) ((data[2] << 8) & 0xFF00) | ((data[3] & 0xFF) >> 6);
        (*gyro_data).z_axis = (int) ((data[4] << 8) & 0xFF00) | ((data[5] & 0xFF) >> 6); 
        return ITG3200_STATUS_OK;
    }
    else {
        // An error happened while reading data
        return status;
    }
}

/**
 * \brief Converts gyro data to deg/sec
 *
 * Gyro data is given with a resolution of 14.375 LSBs per deg/sec
 * Range is +/- 2000 deg/sec
 * Note some accuracy is lost due to use of integer values for the result but is should be 
 * sufficient given the inaccuracy of the gyroscope
 * This function will also subtract the static bias values
 *
 * \param gyro_data Gyro raw data from accelerometer
 *
 * \returns Gyro data as deg/sec
 *
 */
gyro_data_t gyro_convert_data(gyro_data_t gyro_data_raw){
    gyro_data_t gyro_data_converted;
    
    // Subtract calibration values, note converting to 32 bit to avoid overflow
    gyro_data_converted.x_axis = gyro_data_raw.x_axis - calibration_values_gyro_x;
    gyro_data_converted.y_axis = gyro_data_raw.y_axis - calibration_values_gyro_y;
    gyro_data_converted.z_axis = gyro_data_raw.z_axis - calibration_values_gyro_z;
    
    // Convert to deg/sec by dividing by 14 (simplified, should have been 14.375)
    gyro_data_converted.x_axis /= 14;
    gyro_data_converted.y_axis /= 14;
    gyro_data_converted.z_axis /= 14;

    return gyro_data_converted;
}
