/**
 * \file temperature_sensor_raven.c
 *
 * \brief 
 *
 * \author    
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>
#include <drivers/drv_3290_adc.h>
#include "temp_sensor_raven.h"
#include <services/USART_ABOT_cmd.h>
#include <avr/pgmspace.h>

#define TEMP_ZERO_OFFSET_CELCIUS     -30

/* Celsius temperatures (ADC-value) from -30 to 60 degrees
  Look up table is created with using R = R0 ln(B*(1/T-1/T0)), Temperature(T) in kelvin, constant B for NCP18WF104J03RB is 4250
  and ADC = 1024*Vavcc/vin , vin = Vavcc*R/(R+100) , R in kohm 
*/
static const uint16_t temp_table_celsius[] PROGMEM = {
	985,982,979,976,973,969,966,962,958,953,949,944,939,934,929,
    923,917,911,904,898,891,883,876,868,860,851,843,834,825,815,
    806,796,786,775,765,754,743,732,720,709,697,685,673,661,649,
    636,624,611,599,586,574,562,549,537,524,512,500,488,476,464,
    452,440,429,418,406,396,385,374,364,354,344,334,324,315,306,
    297,288,279,271,263,255,247,240,233,225,219,212,205,199,193,
    187,181,
};


/* \brief Enable temperature sensor
*			Temperature power pin PF6 is set
*			JTAG is disabled as the jtag pin PF4 is shared with ADC channel4
*			disble_temp_sensor function should be called before exiting the program so as to enable the JTAG interface
*/
void enable_temp_sensor(void)
{
	//configure temp sensor pwr pin
    DDRF |= (1 << DDF6);
    //Temperature sensor is enabled by PF6 port pin
    PORTF |= (1 << PF6);
	//disable JTAG interface as ADC channel4 is multiplexed with JTAG
	MCUCR |= (1 << JTD);
	MCUCR |= (1 << JTD); //this register bit has to be written twice
}

/* \brief disable temperature sensor
*			Temperature power pin PF6 is cleared
*			JTAG is enabled as the jtag pin PF4 is shared with ADC channel4
*/
void disable_temp_sensor(void)
{
    //Temperature sensor is disabled by driving low PF6
    PORTF &= ~(1 << PF6);
	//disable JTAG interface as ADC channel4 is multiplexed with JTAG
	MCUCR &= ~(1 << JTD);
	MCUCR &= ~(1 << JTD); //this register has to be written twice
}


/*	\brief	Init temperature sensor
*			Initialize temperature power control PIN
*			<Not used now. Config is integrated in enable sensor>
*/
void init_temp_sensor(void)
{
    //configure temp sensor pwr pin
    DDRF |= (1 << DDF6);   
}

/*	\brief	get temperature in degree Celsius from ADC value
 *	\param	adc_val		ADC value to search for in the temperature lookup table
 *	\return	temp_deg_C	temperature in degree Celsius
 */
int16_t read_lookup_degC_ADC(uint16_t adc_val)
{
	int16_t temp_deg_C;
	uint16_t adc_lookup_val;
	uint8_t index = 0;
	adc_lookup_val = pgm_read_word(& temp_table_celsius[index]);
	
	if ((adc_val > 985) || (adc_val < 187))  {
		//adc value out of limit -30degC/60degC
		return 0xBAD;
	}		
	while (adc_val <= adc_lookup_val)  {
		index++;		
		adc_lookup_val = pgm_read_word(& temp_table_celsius[index]);	
	}	
	if((pgm_read_word(&temp_table_celsius[index-1]) - adc_val) < (adc_val- adc_lookup_val))  {
		//adc value is more close to previous lookup value
		index--;
	}
	temp_deg_C = (int16_t) index + TEMP_ZERO_OFFSET_CELCIUS;
	return temp_deg_C;	
}

/*	\brief	Convert temperature in degC to Fahrenheit
*	\param	temp_C	temperature in degC
*	\return	temp_fh	temperature in Fahrenheit
*/

int16_t get_temp_fh(int16_t temp_c)
{
	int16_t temp_fh;
	//degree centigrade to Fahrenheit conversion
	temp_fh = ((temp_c * 9)/5) + 32;
	return temp_fh;	
}

/*	\brief	read current temperature and return value in degC or Fahrenheit
*	\param	unit	temperature reading in degC or Fahrenheit
*	\return	temp_reading_degC/FH	temperature reading in degree Centigrade or Fahrenheit
*/

uint16_t get_temperature_reading(uint8_t unit)
{
	uint16_t temp_adc_val;
	int16_t temp_reading_degC;
	uint16_t temp_reading_FH;
	
	//initialize temperature sensor
	enable_temp_sensor();//power on the temperature sensor
	adc_simple_init(ADC_CHAN_ADC4);	
	
	//get result for ADC channel4	
	temp_adc_val = adc_read();
	disable_temp_sensor(); //power off the temperature sensor
	//get degC temperature value from look up table using adc reading	
	temp_reading_degC = read_lookup_degC_ADC(temp_adc_val) ;
	if (unit == TEMP_DEG_C)  {
		//return temperature in degree centigrade
		return temp_reading_degC;
	}
	else if(unit == TEMP_FAHREN)  {
		//return temperature in Fahrenheit
		temp_reading_FH = get_temp_fh(temp_reading_degC);
		return temp_reading_FH;
	}	
	else return 0;
}


void test_temp(void)
{
	
	volatile int16_t temp;	
	volatile int16_t temp_fh;	
	init_temp_sensor();
	//temp = read_lookup_degC_ADC(187);
	//temp_fh = get_temp_fh(temp);
	temp = get_temperature_reading(TEMP_DEG_C);
	temp_fh = get_temperature_reading(TEMP_FAHREN);
	//USART_printf("temperature");
}