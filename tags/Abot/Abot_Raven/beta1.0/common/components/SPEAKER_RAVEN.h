
/**
 * \file SPEAKER_RAVEN.h
 *
 * \brief 
 *
 * \author    
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <stdbool.h>
#include <stdint.h>


//Octave5
#define			tone_5C				1911
#define			tone_5Ch			1804
#define			tone_5D				1703
#define			tone_5Dh			1607
#define			tone_5E				1517
#define			tone_5F				1432
#define			tone_5Fh			1351
#define			tone_5G				1276
#define			tone_5Gh			1204
#define			tone_5A				1136
#define			tone_5Ah			1073
#define			tone_5B				1012
//Octave4
#define			tone_4C				3822 
#define			tone_4Ch			3608
#define			tone_4D				3405
#define			tone_4Dh			3214
#define			tone_4E				3034
#define			tone_4F				2863
#define			tone_4Fh			2703
#define			tone_4G				2551
#define			tone_4Gh			2408
#define			tone_4A				2273
#define			tone_4Ah			2145
#define			tone_4B				2025

#define			tone_NIL			10

#define			VOLUME				10
#define			BEEP_TONE			tone_5Gh

typedef enum {
    DUTY_50PC = 625,
    DUTY_25PC = 312,
} pwm_duty_t;

typedef enum {
    FREQ_900hz= 1250,    
} pwm_freq_t;

typedef struct {
	uint16_t tone;
	uint16_t duration;
}tone_t;

typedef struct  {
	tone_t *tune;
	uint8_t current_tone_index;
	uint8_t length;	
	bool music_playing;
}tune_t;


void init_speaker(void);
void test_speaker(void);//test function
void play_music(const tone_t *tune, uint8_t tune_length); //Play music using callback function
void stop_music(void);
void play_beep(uint16_t duration);//Play beep , duration in ms