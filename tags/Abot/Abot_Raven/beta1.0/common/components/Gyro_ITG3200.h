/**
 * \file  Gyro_ITG3200.h
 *
 * \brief The Gyro component
 *
 * \author    SH
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */



#ifndef GYRO_ITG3200_H_
#define GYRO_ITG3200_H_

//Gyro data (raw data from gyroscope)
typedef struct
{
	
    //Gyro data is given by 16 bit values with an resolution of 14.375 LSB per deg/sec
	// Range is +/- 2000 deg/sec
    int32_t x_axis;
    int32_t y_axis;
    int32_t z_axis;
        
} gyro_data_t;


/*
 * Definitions for ITG3200 gyroscope chip.
*/


/*! \name ITG3200 register address
\anchor ITG3200_REG
Defines a register address of the ITG3200.*/
/*! @{*/
#define ITG3200_REG_WHO_AM_I               0x00
#define ITG3200_REG_SMPLRT_DIV             0x15
#define ITG3200_REG_DLPF                   0x16
#define ITG3200_REG_INT_CFG                0x17
#define ITG3200_REG_GYRO_DATA_X_H          0x1D
/*! @}*/

/*! \name ITG3200 Interrupt Configuration bit numbers
\anchor ITG3200_INT_CFG_BIT
Defines a bit number of the Interrupt Configuration register.*/
/*! @{*/
#define	ITG3200_INT_CFG_BIT_RAW_RDY_EN 0
#define ITG3200_INT_CFG_BIT_ACTL       7
/*! @}*/

/*! \name ITG3200 DLPF_CFG values
\anchor ITG3200_DLPF_CFG
Defines possible values for the ITG3200 sampling rate and internal low pass filter.*/
/*! @{*/
#define ITG3200_DLPF_CFG_1KHZ_20HZ 4
#define ITG3200_DLPF_CFG_1KHZ_10HZ 5
#define ITG3200_DLPF_CFG_1KHZ_5HZ  6
/*! @}*/

// ITG3200 chip id mask
#define ITG3200_CHIP_ID_MASK 0x7E

// ITG3200 chip id 
#define ITG3200_CHIP_ID 0x68

// Gyro I2C slave address
#define ITG3200_ADDRESS	(0x68)

// Globals
/*Interrupt flags defined*/
uint8_t gyro_int_flag;

/*! \name Gyro status codes
\anchor ITG3200_STATUS
Defines return statuses for the ITG3200 functions.*/
/*! @{*/
#define ITG3200_STATUS_OK 0x00
#define ITG3200_STATUS_TWI_ERROR 0x01
#define ITG3200_STATUS_INVALID_CHIP_ID 0x02
#define ITG3200_STATUS_TIMEOUT 0x03
/*! @}*/

/* Gyro API */
uint8_t gyro_init(void);
uint8_t gyro_write_register(uint8_t address, uint8_t data);
uint8_t gyro_read_registers(uint8_t address, uint8_t no_of_bytes, uint8_t *data);
uint8_t gyro_get_raw_data(gyro_data_t *gyro_data);
gyro_data_t gyro_convert_data(gyro_data_t gyro_data_raw);
	
#endif /* GYRO_ITG3200_H_ */
