/*
 * drv_1284_SPI.h
 *
 * Created: 15/01/2012 23:25:21
 *  Author: TB
 */ 


#ifndef DRV_1284_SPI_H_
#define DRV_1284_SPI_H_


void SPI_SS_low(void); // Set SPI select low
void SPI_SS_high(void); // Set SPI select high 
void SPI_MasterTransmit(unsigned char cData);
unsigned char SPI_Receive(void);
void SPI_MasterInit(void);

#endif /* DRV_1284_SPI_H_ */