/**
 * \file drv_mega_USART0.c
 *
 * \brief main loop for Munin (mega1284) robot application running on the RAVEN kit
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>

//Abot common includes
#include <services/USART_ABOT_cmd.h>
#include "drv_mega_USART0.h"

#include "board_RAVEN.h"  //Should not be needed but Interrupt vectors badly defined for 3290, should be like the other mega USART0 def

/* USART Transmit Complete interrupt 
ISR(USART0_TX_vect)
{
	extern unsigned char TX0_int_flag;

		TX0_int_flag = 1;
}
*/

/* USART Receive Complete interrupt */
ISR(USART0_RX_vect)
{
	extern unsigned char RX_int_flag;
	extern rx_buf rxbuf;
	
		RX_int_flag = 1;	//Set Rx flag
		
		/* Wait for data to be received */
		while ( !(UCSR0A & (1<<RXC0)) )
		;
		
		rxbuf.buf[rxbuf.tail]=UDR0; // Store received byte in receive buffer
		
		rxbuf.tail++;
		
		if (rxbuf.tail== BUFSIZE) rxbuf.tail=0; // End of buffer reached ?
}



/*! \brief Initialization of mega UART0
 *
 * The USART has to be initialized before any communication can take place. The initialization process
 * normally consists of setting the baud rate, setting frame format and enabling the
 * Transmitter or the Receiver depending on the usage. For interrupt driven USART operation, the
 * Global Interrupt Flag should be cleared (and interrupts globally disabled) when doing the
 * initialization
 *
 * \param   baudrate    the baudrate in baud
 * \param   CPU_MHz     the CPU frequency in kHz
 *
 */
void USART_init(uint16_t baudrate, uint32_t CPU_MHz)
{
	extern unsigned char TX_int_flag;
	extern unsigned char RX_int_flag;
	extern rx_buf rxbuf;
	
	//unsigned int ubrr;
	uint32_t ubrr;
	
	/* For Mega3290P, enable the uart peripheral 
	PRR � Power Reduction Register
	Bit 1 - PRUSART: Power Reduction USART
		Writing logic one to this bit shuts down the USART by stopping the clock to the module. When
		waking up the USART again, the USART should be re-initialized to ensure proper operation.
	*/
   // PRR &= ~(1 << PRUSART0);
   

	/* Set baud rate 
	Table 19-3. Examples of UBRRn Settings for Commonly Used Oscillator Frequencies
	Asynchronous Normal mode (U2Xn = 0) UBRR0 = (fosc/(16*BAUD))-1
	*/
	ubrr=BAUD;
	ubrr = 16*ubrr;
	ubrr = CPU_MHz/ubrr; 
	//ubrr = (int)rint(CPU_MHz/ubrr);  
	ubrr = ubrr-1;
	//ubrr = baudrate;  //6=>9600 1= 28800
	UBRR0H = (unsigned char)(ubrr>>8); //High byte
	UBRR0L = (unsigned char)ubrr;      //Low byte
	
	/* Set frame format: 8data, 1stop bit, no parity 
	UCSRnC � USART Control and Status Register n C = 0x06 0000 0110
	Bit 7 = 0
	Bit 6 USART Mode Select UMSELn This bit selects between asynchronous (0) and synchronous (1) mode of operation.
	Bit 5:4 Parity Mode UPMn1:0 = 00 (Disabled)
	Bit 3 Stop Bit Select USBSn = 0 (1 stop bit)
	Bit 2:1 Character Size UCSZn1:0 = 11 (8-bit)
		UCSZn2 bit in UCSRnB = 0 (8-bit)
	Bit 0 � UCPOLn: Clock Polarity = 0 (This bit is used for synchronous mode only. Write this bit to zero when asynchronous mode is used.
	*/
	UCSR0C = (1<<UCSZ01)|(1 << UCSZ00);
	
	/* Enable receiver and transmitter and RX interrupt on USART and TX interrupt on USART
   
	UCSRnB � USART Control and Status Register n B
	Bit 7 � RXCIEn: RX Complete Interrupt Enable
		Writing this bit to one enables interrupt on the RXCn Flag. A USART Receive Complete interrupt
		will be generated only if the RXCIEn bit is written to one, the Global Interrupt Flag in SREG is
		written to one and the RXCn bit in UCSRnA is set.
	Bit 6 � TXCIEn: TX Complete Interrupt Enable
		Writing this bit to one enables interrupt on the TXCn Flag. A USART Transmit Complete interrupt
		will be generated only if the TXCIEn bit is written to one, the Global Interrupt Flag in SREG is
		written to one and the TXCn bit in UCSRnA is set.
	Bit 5 � UDRIEn: USART Data Register Empty Interrupt Enable
		Writing this bit to one enables interrupt on the UDREn Flag. A Data Register Empty interrupt will
		be generated only if the UDRIEn bit is written to one, the Global Interrupt Flag in SREG is written
		to one and the UDREn bit in UCSRnA is set.
	Bit 4 � RXEN0: Receiver Enable
		Writing this bit to one enables the USART Receiver. The Receiver will override normal port operation
		for the RxD pin when enabled. Disabling the Receiver will flush the receive buffer
		invalidating the FEn, DORn, and UPEn Flags.
	Bit 3 � TXENn: Transmitter Enable
		Writing this bit to one enables the USART Transmitter. The Transmitter will override normal port
		operation for the TxD pin when enabled. The disabling of the Transmitter (writing TXENn to
		zero) will not become effective until ongoing and pending transmissions are completed, i.e.,
		when the Transmit Shift Register and Transmit Buffer Register do not contain data to be transmitted.
		When disabled, the Transmitter will no longer override the TxD port.
	*/
   // UCSR0B = (1 << RXEN0)|(1 << TXEN0)|(1 << RXCIE0)|(1 << TXCIE0);  //TX & RX int enabled
	
	UCSR0B = (1 << RXEN0)|(1 << TXEN0)|(1 << RXCIE0); // TX int disabled
	
	TX_int_flag = 0; //clr TX interrupt flag
	RX_int_flag = 0; //clr RX interrupt flag
	
	rxbuf.head=rxbuf.tail=0; //clr rx buffer
}

/*---------------------------------------------------------------------------*/
void USART_tx_char(unsigned char c)
{
	extern unsigned char TX_int_flag;
	
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) )
	;
	/* Put data into buffer, sends the data */
	UDR0 = c;

}

#define F_CPU 1000000UL		// 1 MHz CPU clk, used in delay
#include <util/delay.h>		//_delay_us()

unsigned char USART_get_char(void)
{
	unsigned char c;
	extern unsigned char RX_int_flag;
	extern rx_buf rxbuf;
	char t;

		t=10; // # times to look for new bytes in buffer
		while((rxbuf.head==rxbuf.tail) & (t>0)) //rxbuf empty
		{
			_delay_us(100); // wait for buffer to be filed up
			t--;
		}
		
		if (t>0)
		{
			c = rxbuf.buf[rxbuf.head];
			rxbuf.head++;
			if (rxbuf.head==BUFSIZE) rxbuf.head=0;
		} else c=0x00; //c=NUL	
		
		return c;
}

