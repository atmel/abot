/**
 * \file  drv_1284_TC3_counter.c
 *
 * \brief  16-bit Timer/Counter3 used as counter in mega1284P
 *
 * \author   SH
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>

/**
 * \brief Initialize counter 
 * 
 * \param clk_div Clock division, 0 = Counter off, 1 = No prescale, 2 = clk / 8, 3 = clk / 64, 4 = clk / 256, 5 = clk / 1024
*/
void counter_init (uint8_t clk_div)
{
    // The only thing that needs to be done to use the Timer/Counter as a 
    // counter is to select the correct clock setting. 
    // Clock Select bits are lowest three bits of TCCR3B. 
    TCCR3B |= (clk_div & 0x07);

}

/**
 * \brief Reset the counter value
 *
 *
 */
void counter_reset(void)
{
    // Clear the counter value
    TCNT3 = 0x0000;

    // Clear the Overflow flag
    TIFR3 |= (1 << TOV3);
}

/**
 * \brief Get current counter value
 *
 * \return Current counter value. In case of a overflow 0xFFFF will be returned
 */
uint16_t counter_get_value(void)
{
    // Check for overflow, if so return 0xFFFF (max counter value)
    if (TIFR3 & (1 << TOV3))
    {
        return 0xFFFF;
    }
    else
    {
        return TCNT3;
    }
}
