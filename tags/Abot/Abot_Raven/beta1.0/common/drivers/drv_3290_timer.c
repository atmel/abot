/**
 * \file drv_3290_timer.c
 *
 * \brief mega3290 timer
 *
 * \author    
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#include <avr/io.h>
#include <avr/interrupt.h>  // sei()
#include <stdbool.h>
#include <drivers/drv_3290_timer.h>


void (*timer_callback) (void);
delay_timer_t timer1 = {0,0,false}; //timer running status false
uint16_t sys_time_256ms;
uint16_t sys_time;
bool generic_timer_running = false;
uint16_t current_timer_expire;

//system clock frequency tracking - TODO
//16 bit register access <cli(); sei()> - TODO
 

/*
*Initialize timer1 in PWM mode
*brief: Timer1 is used in Fast PWM mode
*Timer clock is configured as system clock
*Base frequency of the PWM is adjusted with TOP value reading from ICR1 register
*Duty cycle of the PWM is controlled by OCR1A value
*/

void init_pwm_timer(void)
{
	//clock is configured when its required to start pwm
			
    //setup OC1A pin of timer1 as output    
    DDRB |= (1 << DDB5);
	
	//configure the OC1A pin to set when TCNT1 reach BOTTOM - non inverting mode 
	TCCR1A |= (1 << COM1A1);
	TCCR1A &= ~(1 << COM1A0);
           
    //configure Waveform generation mode, fast PWM with TOP value in ICR1 WGM13:0=1110
	TCCR1A &= ~(1 << WGM10);
    TCCR1A |= (1 << WGM11);	
	TCCR1B |= (1 << WGM13)|(1 << WGM12);	
	
	//enable timer1A overflow interrupt
	TIMSK1 |= (1 << TOIE1);	
	//enable timer1A compare match interrupt
	TIMSK1 |= (1 << OCIE1A);	
	//sei();	
}

/*Enable TIMER1 module
*brief: TIMER1 is started by configuring the module clock
*/

void start_pwm(void)
{
	//set up the clock, IO clock = 1M, timer clk = 1M/1	
    TCCR1B |= (1 << CS10);
    TCCR1B &= ~((1 << CS11)|(1 << CS12));	
}

/*Disable TIMER1 module
*brief: TIMER1 is stopped by configuring the module clock to none
*/

void stop_pwm(void)
{
	//set up the clock to none
    TCCR1B &= ~((1 << CS10)|(1 << CS11)|(1 << CS12));	
}

/*
*Start timer in PWM mode
*accept input frequency and duty
*brief: Timer1 is used in Fast PWM mode
*/

void generate_pwm(uint16_t duty, uint16_t freq)
{
	//PWM frequency control TOP - in ICR1 , timer clock is 1M
	ICR1 = freq;
	//set OCR1A for fast PWM TOP (define the frequency) , atomic access!    
    OCR1A = duty;
	//start pwm
	start_pwm();
}


/*
*TIMER1 overflow isr
*brief: 
*/
ISR(TIMER1_OVF_vect)
{	
	//unused
}

/*
*TIMER1 compare match isr
*brief: 
*/
ISR(TIMER1_COMPA_vect)
{
	//unused
}

/*
*Generic timer initialization
*brief: Timer is run in Fast PWM mode with overflow interrupt and compare match interrupt enabled.
*on overflow, system time is incremented by 256ms
*Compare match is used to implement callback function when requested delay reaches
*/


void init_generic_timer(void)
{	
	
	//set clock for the Timer2 module, system clock, 1M/1024
	TCCR2A |= (1 << CS22) | (1 << CS21) | (1 << CS20);
	//Set mode to fast PWM mode, TOP = 0xFF
	TCCR2A &= ~((1 << WGM21) | (1 << WGM20));	
	//set up COM2A for test
	DDRB |= (1<<DDB7);
	PORTB &= ~(1<< PORTB7);
	//TCCR2A |= (1 << COM2A1) | (1 << COM2A0);
	
				
	//enable overflow interrupt
	TIMSK2 |= (1 << TOIE2);
	//sei();
		
}

/*register a new timer
*brief: callback is registered, and shall be called when the requested delay is reached
*coarse delay is calculated considering that overflow interrupt gets triggered in 256ms
*remaining delay time is completed when compare match timer is called
*/


uint8_t register_timer(void (*timer_callback_register) (void), uint16_t delay)
{
	
	uint16_t current_system_time;
	if(generic_timer_running)  {
		return 0; //timer service busy
	}
	//test code
	PORTB |=(1 << PORTB7);
	current_system_time = sys_time + TCNT2;
	//register delay callback to func. pointer in compare match int.
	timer_callback = timer_callback_register;
	//current timer expire at this time,
	current_timer_expire = current_system_time + delay ;
	generic_timer_running = true;
	OCR2A = current_timer_expire & (0xFF);	
	if ((current_timer_expire >> 8) == (sys_time >> 8))  {
		//timer will expire before the next overflow interrupt
		//reset the timer; OCR2A is update only when TCNT2 is zero
		TIFR2 |= (1 << OCF2A); //clear any pending compare interrupt
		TIMSK2 |= (1 << OCIE2A); //enable compare match interrupt
	}
	return 1;
}

/*Timer2 overflow ISR
*brief: update the system_time by 256 ms
*/

ISR(TIMER2_OVF_vect)
{
	//increment system timer	
	sys_time += 256;	
	if(generic_timer_running && ((current_timer_expire >> 8) == (sys_time >> 8)))  {
		//clear any pending compare interrupt
		TIFR2 |= (1 << OCF2A);
		//current timer expires before the next overflow, execute callback in overflow
		TIMSK2 |= (1 << OCIE2A); //enable compare match interrupt
	}		
}

/*Timer2 Compare match ISR
*brief: Compare match indicates delay has expired, call the delay_callback function
*/
ISR(TIMER2_COMP_vect)
{
	//disable Timer2 compare match interrupt
	TIMSK2 &= ~(1 << OCIE2A);
	//completed execution of registered timer
	generic_timer_running = false;
	//test code
	PORTB &= ~(1 << PORTB7);
	//delay over, invoke callback function			
	timer_callback();
}

/*get system timer in ms
*brief: monitors system time from init_generic_timer is called
*maximum value is, 65535
*/
uint16_t get_system_time(void)
{	
	return ((sys_time ) + TCNT2);	
}

/*for testing
*/

void timer_callback_test(void)
{
	uint8_t temp = 0;	
}

/*for testing
*/
void test_generic_timer(void)
{	
	init_generic_timer();
	register_timer(timer_callback_test, 1000);
		
}
