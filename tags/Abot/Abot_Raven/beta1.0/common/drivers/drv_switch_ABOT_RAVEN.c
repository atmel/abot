/**
 * \file  drv_switch_ABOT.c
 *
 * \brief The ABOT switch driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>

//Abot common includes
#include "drv_switch_ABOT_RAVEN.h"

void switch_init(void)
{
	
	// Initialize ABOT switches
	// PA0 and PA1 connected to switch, interrupt enabled
	/*
	PORTA � Port A Data Register 
	If PORTxn is written logic one when the pin is configured as an input pin, the pull-up resistor is
	activated.
	DDRA � Port A Data Direction Register 
	"0" => input
	*/	
	DDRA &= !(1<<DDA0);  //PA0 as input

	//DDRA |= (1<<DDA1);  //PA1 as output to drive distance sensor pwr

	PORTA |= (1<<PORTA0); //PA0  pull-up activated
	
	//PORTA |= (1<<PORTA1);  //PA1 output high

	/*
	PCINT7:0, Pin Change Interrupt source 7:0 The PA7:0 pins can serve as external interrupt sources.

	PCMSK0 � Pin Change Mask Register 0
	Bit 7:0 � PCINT7:0: Pin Change Enable Mask 7..0
	Each PCINT7:0 bit selects whether pin change interrupt is enabled on the corresponding I/O pin.
	If PCINT7:0 is set and the PCIE0 bit in PCICR is set, pin change interrupt is enabled on the corresponding I/O pin. 
	If PCINT7:0 is cleared, pin change interrupt on the corresponding I/O pin is disabled.
	*/
	PCMSK0 |= (1<<PCINT0); // PCINT0 enabled
	
		
}