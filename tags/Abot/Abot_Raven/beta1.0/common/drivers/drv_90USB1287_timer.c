/*
 * drv_3290_timer.c
 *
 * Created: 
 *  Author: 
 */ 



#include <avr/io.h>
#include <avr/interrupt.h>  // sei()
#include <stdbool.h>
#include <drivers/drv_90USB1287_timer.h>


void (*timer_callback) (void);
delay_timer_t timer1 = {0,0,false}; //timer running status false
uint16_t sys_time_256ms;
uint16_t sys_time;
bool generic_timer_running = false;
uint16_t current_timer_expire;

//system clock frequency tracking - TODO
//16 bit register access <cli(); sei()> - TODO
 

/*
*Generic timer initialization
*brief: Timer is run in Fast PWM mode with overflow interrupt and compare match interrupt enabled.
*on overflow, system time is incremented by 256ms
*Compare match is used to implement callback function when requested delay reaches
*/


void init_generic_timer(void)
{	
	
	//set clock for the Timer2 module, system clock, F_CPU = 8M/1024
	TCCR0B |= (1 << CS22)| (1 << CS20);
	TCCR0B &= ~(1 << CS21);
	//Set mode to fast PWM mode, TOP = 0xFF
	TCCR0A &= ~((1 << WGM01) | (1 << WGM00));	
	TCCR0B &= ~(1 << WGM02);
	//set up COM2A for test
	//DDRB |= (1<<DDB7);
	//PORTB &= ~(1<< PORTB7);
						
	//enable overflow interrupt
	TIMSK0 |= (1 << TOIE0);
	//sei();
		
}

/*register a new timer
*brief: callback is registered, and shall be called when the requested delay is reached
*coarse delay is calculated considering that overflow interrupt gets triggered in 256ms
*remaining delay time is completed when compare match timer is called
*/


uint8_t register_timer(void (*timer_callback_register) (void), uint16_t delay)
{
	
	uint16_t current_system_time;
	if(generic_timer_running)  {
		return 0; //timer service busy
	}
	//test code
	//PORTB |=(1 << PORTB7);
	current_system_time = sys_time + TCNT0;
	//register delay callback to func. pointer in compare match int.
	timer_callback = timer_callback_register;
	//current timer expire at this time,
	current_timer_expire = current_system_time + delay ;
	generic_timer_running = true;
	OCR0A = current_timer_expire & (0xFF);	
	if ((current_timer_expire >> 8) == (sys_time >> 8))  {
		//timer will expire before the next overflow interrupt
		//reset the timer; OCR0A is update only when TCNT2 is zero
		TIFR0 |= (1 << OCF0A); //clear any pending compare interrupt
		TIMSK0 |= (1 << OCIE0A); //enable compare match interrupt
	}
	return 1;
}

/*Timer2 overflow ISR
*brief: update the system_time by 256 ms
*/

ISR(TIMER0_OVF_vect)
{
	//increment system timer	
	sys_time += 256;	
	if(generic_timer_running && ((current_timer_expire >> 8) == (sys_time >> 8)))  {
		//clear any pending compare interrupt
		TIFR0 |= (1 << OCF0A);
		//current timer expires before the next overflow, execute callback in overflow
		TIMSK0 |= (1 << OCIE0A); //enable compare match interrupt
	}		
}

/*Timer2 Compare match ISR
*brief: Compare match indicates delay has expired, call the delay_callback function
*/
ISR(TIMER0_COMPA_vect)
{
	//disable Timer2 compare match interrupt
	TIMSK0 &= ~(1 << OCIE0A);
	//completed execution of registered timer
	generic_timer_running = false;
	//test code
	//PORTB &= ~(1 << PORTB7);
	//delay over, invoke callback function			
	timer_callback();
}

/*get system timer in ms
*brief: monitors system time from init_generic_timer is called
*maximum value is, 65535
*/
uint16_t get_system_time(void)
{	
	return ((sys_time_256ms << 8) + TCNT0);	
}

/*for testing


void timer_callback_test(void)
{
	uint8_t temp = 0;	
}



void test_generic_timer(void)
{	
	
	register_timer(timer_callback_test, 1000);
		
}
*/
