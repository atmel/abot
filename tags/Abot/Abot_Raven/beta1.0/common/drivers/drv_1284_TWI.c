/**
 * \file drv_1284_TWI.c
 *
 * \brief mega1284 TWI driver
  The 2-wire Serial Interface (TWI) is ideally suited for typical microcontroller applications. The
  TWI protocol allows the systems designer to interconnect up to 128 different devices using only
  two bi-directional bus lines, one for clock (SCL) and one for data (SDA). The only external hardware
  needed to implement the bus is a single pull-up resistor for each of the TWI bus lines. All
  devices connected to the bus have individual addresses, and mechanisms for resolving bus
  contention are inherent in the TWI protocol.

  PC1 (SDA/PCINT17)
  PC0 (SCL/PCINT16)
  
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>
//#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "drv_1284_TWI.h"

/** \brief initialize the TWI as master
 *
 *  \param speed        The desired TWI bus speed STANDARD or FAST.
 *  \param cpu_clk_frq  The CPU clock frequency.
 *
 */
void TWI_master_init(uint32_t speed, uint32_t cpu_clk_frq)
{
	uint16_t x;
/*	
	TWBR  TWI Bit Rate Register
	TWBR selects the division factor for the bit rate generator. The bit rate generator is a frequency
	divider which generates the SCL clock frequency in the Master modes.
	
	SCL frequency = CPU Clock frequency/ (16 + 2(TWBR) ? 4^TWPS)
	
	
	TWSR  TWI Status Register
	Bits 1:0  TWPS: TWI Prescaler Bits
	These bits can be read and written, and control the bit rate prescaler.
	TWPS1 TWPS0 Prescaler Value
	0		0		1
	0		1		4
	1		0		16
	1		1		64
	
	
	CPU Clock frequency < 20 MHz on mega1284
	
*/
	
	x = cpu_clk_frq/speed;
	
	if (x < 16){
		TWBR = 0;
	}
	else {
		TWBR = x/8-2;
	}		
	

	TWSR &= 0xFC; //Prescaler value = 1, reasonable value with CPU speeds up to 20mHz
	
/*
TWCR  TWI Control Register	
	Bit 2  TWEN: TWI Enable Bit
	The TWEN bit enables TWI operation and activates the TWI interface. When TWEN is written to
	one, the TWI takes control over the I/O pins connected to the SCL and SDA pins, enabling the
	slew-rate limiters and spike filters. If this bit is written to zero, the TWI is switched off and all TWI
	transmissions are terminated, regardless of any ongoing operation.
	Bit 1  Res: Reserved Bit
	This bit is a reserved bit and will always read as zero.
	Bit 0  TWIE: TWI Interrupt Enable
	When this bit is written to one, and the I-bit in SREG is set, the TWI interrupt request will be activated
	for as long as the TWINT Flag is high.	
*/

TWCR &= !(1<<TWIE);	// Clr TWIE - disable interrupt
}



/*! \internal transmit START condition

The first step in a TWI transmission is to transmit a START condition. This is done by
writing a specific value into TWCR, instructing the TWI hardware to transmit a START
condition. Which value to write is described later on. However, it is important that the
TWINT bit is set in the value written. Writing a one to TWINT clears the flag. The TWI will

*/
void TWI_transmit_START_condition(void)
{
	
/*
TWCR  TWI Control Register	

	Bit 7  TWINT: TWI Interrupt Flag
	This bit is set by hardware when the TWI has finished its current job and expects application
	software response. If the I-bit in SREG and TWIE in TWCR are set, the MCU will jump to the
	TWI Interrupt Vector. While the TWINT Flag is set, the SCL low period is stretched. The TWINT
	Flag must be cleared by software by writing a logic one to it. Note that this flag is not automatically
	cleared by hardware when executing the interrupt routine. Also note that clearing this flag
	starts the operation of the TWI, so all accesses to the TWI Address Register (TWAR), TWI Status
	Register (TWSR), and TWI Data Register (TWDR) must be complete before clearing this flag.
	
	Bit 6  TWEA: TWI Enable Acknowledge Bit
	The TWEA bit controls the generation of the acknowledge pulse. If the TWEA bit is written to
	one, the ACK pulse is generated on the TWI bus if the following conditions are met:
	1. The devices own slave address has been received.
	2. A general call has been received, while the TWGCE bit in the TWAR is set.
	3. A data byte has been received in Master Receiver or Slave Receiver mode.
	By writing the TWEA bit to zero, the device can be virtually disconnected from the 2-wire Serial
	Bus temporarily. Address recognition can then be resumed by writing the TWEA bit to one
	again

	Bit 5  TWSTA: TWI START Condition Bit
	The application writes the TWSTA bit to one when it desires to become a Master on the 2-wire
	Serial Bus. The TWI hardware checks if the bus is available, and generates a START condition
	on the bus if it is free. However, if the bus is not free, the TWI waits until a STOP condition is
	detected, and then generates a new START condition to claim the bus Master status. TWSTA
	must be cleared by software when the START condition has been transmitted.
	
	Bit 2  TWEN: TWI Enable Bit
	The TWEN bit enables TWI operation and activates the TWI interface. When TWEN is written to
	one, the TWI takes control over the I/O pins connected to the SCL and SDA pins, enabling the
	slew-rate limiters and spike filters. If this bit is written to zero, the TWI is switched off and all TWI
	transmissions are terminated, regardless of any ongoing operation.
*/	
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN); // Clr interrupt flag, enable TWI, and activate START condition, ACK disabled (bit6=0)
	
}

/*! \internal TWI transmit SLA_W
 *
 * \param SLA		address of Slave to transmit data to
 
 TWDR  TWI Data Register
	In Transmit mode, TWDR contains the next byte to be transmitted. 
	It is writable while the TWI is not in the process of shifting a byte.
	This occurs when the TWI Interrupt Flag (TWINT) is set by hardware. Note that the Data Register
	cannot be initialized by the user before the first interrupt occurs. The data in TWDR remains
	stable as long as TWINT is set. While data is shifted out, data on the bus is simultaneously
	shifted in. TWDR always contains the last byte present on the bus, except after a wake up from
	a sleep mode by the TWI interrupt. In this case, the contents of TWDR is undefined.
	In the case of a lost bus arbitration, no data is lost in the transition from Master to Slave. Handling of the
	ACK bit is controlled automatically by the TWI logic, the CPU cannot access the ACK bit directly.
	
*/
void TWI_transmit_SLA_W(uint8_t SLA)
{
	TWDR = (SLA<<1);				 //  shift in 0 to bit 0 and Load Slave address into Data register
	TWCR = (1<<TWINT) | (1<<TWEN);  // Clr interrupt flag, TWI still enabled, other bit = 0
}

void TWI_transmit_SLA_R(uint8_t SLA)
{
	TWDR = (SLA<<1)| 0x01;			 // Shift Slave address left and fill with 1 (bit 0 = 1) into Data register
	TWCR = (1<<TWINT) | (1<<TWEN);  // Clr interrupt flag, TWI still enabled, other bit = 0
}

/*! \internal Transmit TWI STOP condition
 
TWCR  TWI Control Register	

	Bit 7  TWINT: TWI Interrupt Flag
	This bit is set by hardware when the TWI has finished its current job and expects application
	software response. If the I-bit in SREG and TWIE in TWCR are set, the MCU will jump to the
	TWI Interrupt Vector. While the TWINT Flag is set, the SCL low period is stretched. The TWINT
	Flag must be cleared by software by writing a logic one to it. Note that this flag is not automatically
	cleared by hardware when executing the interrupt routine. Also note that clearing this flag
	starts the operation of the TWI, so all accesses to the TWI Address Register (TWAR), TWI Status
	Register (TWSR), and TWI Data Register (TWDR) must be complete before clearing this flag.
	
	Bit 4  TWSTO: TWI STOP Condition Bit
	Writing the TWSTO bit to one in Master mode will generate a STOP condition on the 2-wire
	Serial Bus. When the STOP condition is executed on the bus, the TWSTO bit is cleared automatically.
	In Slave mode, setting the TWSTO bit can be used to recover from an error condition.
	This will not generate a STOP condition, but the TWI returns to a well-defined unaddressed
	Slave mode and releases the SCL and SDA lines to a high impedance state.
	
	Bit 2  TWEN: TWI Enable Bit
	The TWEN bit enables TWI operation and activates the TWI interface. When TWEN is written to
	one, the TWI takes control over the I/O pins connected to the SCL and SDA pins, enabling the
	slew-rate limiters and spike filters. If this bit is written to zero, the TWI is switched off and all TWI
	transmissions are terminated, regardless of any ongoing operation.
*/	

void TWI_transmit_STOP_condition(void)
{

	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN); // Clr interrupt flag, enable TWI, and activate STOP condition
	
}



/*! \brief TWI master receive data
 * 
 * \param slave_address		 address of Slave to transmit data to
 * \param no_of_byte		 number of bytes to receive from slave
 * \param TWI_data_received  Pointer to where data is located
 *
 * \return  status			0x00 if receive ok - otherwise  TWI erroneous status code

TWCR  TWI Control Register	
 Bit 6  TWEA: TWI Enable Acknowledge Bit
	The TWEA bit controls the generation of the acknowledge pulse. If the TWEA bit is written to
	one, the ACK pulse is generated on the TWI bus if the following conditions are met:
	1. The devices own slave address has been received.
	2. A general call has been received, while the TWGCE bit in the TWAR is set.
	3. A data byte has been received in Master Receiver or Slave Receiver mode.
	By writing the TWEA bit to zero, the device can be virtually disconnected from the 2-wire Serial
	Bus temporarily. Address recognition can then be resumed by writing the TWEA bit to one
	again
	
 */
uint8_t TWI_master_receive(uint8_t slave_address, uint8_t no_of_byte, uint8_t *TWI_data_received)
{
	
	uint8_t status;				// receive status
	
	if (no_of_byte <= TWI_MAX_DATA){  // Can not receive more data than max buffer size
	
		TWI_transmit_START_condition(); 
	 
		while (!(TWCR & (1<<TWINT)));				// Wait Wait for TWINT Flag set, i.e. Start condition transmitted
		
		//TODO add timeout in case bus not ready
		
		status = TWSR & 0xF8;						// Get Bits 7:3  TWS: TWI Status
	
		if ((status == 0x08) || (status == 0x10)) {	//A START condition or repeated start has been transmitted
		
			TWI_transmit_SLA_R(slave_address);
			while (!(TWCR & (1<<TWINT)));			// Wait Wait for TWINT Flag set, i.e. Start condition transmitted
			status = TWSR & 0xF8;					// Get Bits 7:3  TWS: TWI Status
		
			if (status == 0x40){					// SLA+R has been transmitted; ACK has been received
			
				if (no_of_byte!=0x01){
					
					do{		
						TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);  // Clr interrupt flag, TWI enabled, ACK enabled, other bit = 0
						while (!(TWCR & (1<<TWINT)));	// Wait Wait for TWINT Flag set, i.e. 
						status = TWSR & 0xF8;			// Get Bits 7:3  TWS: TWI Status
						*TWI_data_received = TWDR;		// if status = 0x58 data has also been received but no ack
						if (status== 0x50){				//Data byte has been received; ACK has been returned  
							TWI_data_received++;		//next buffer
							no_of_byte--;
							if (no_of_byte == 1) status=0x02;	// specified number of Data received ok
						
						} //if (status== 0x50)
					} while (status == 0x50); //Data byte has been received; ACK has been returned
				} else status=0x02; //if (no_of_byte!=0x01)	
							
				//last byte without ACK
				if (status== 0x02){				//Data byte has been received; ACK has been returned  
					//TWI_data_received++;		//next buffer
					TWCR = (1<<TWINT) | (1<<TWEN);  // Clr interrupt flag, TWI enabled, ACK disabled, other bit = 0
					while (!(TWCR & (1<<TWINT)));	// Wait Wait for TWINT Flag set, i.e. 
					status = TWSR & 0xF8;			// Get Bits 7:3  TWS: TWI Status
					*TWI_data_received = TWDR;	
					if (status== 0x58) status=0x00;	//Data byte has been received; No ACK has been returned
						
				} //if (status== 0x02)
				
			} //SLA_R			
		} //START
	
		TWI_transmit_STOP_condition();
	
	} else status = TWI_STATUS_BUFF_OVERFLOW;
	
	return status; // 0x00 if ok, otherwise the erroneous status
}	

/*! \brief TWI master transmit data
 *
 * \param slave_address		address of Slave to transmit data to
 * \param no_of_byte		Number of bytes to transmit
 * \param data_to_transmit  Pointer to where data is located, NULL terminated
 * \param end_with_STOP		Transmit STOP at end if ='Y'
 *
 * \return  status			0x00 if transmit ok - otherwise  TWI erroneous status code
 
 TWSR  TWI Status Register
	Bits 7:3  TWS: TWI Status
	These 5 bits reflect the status of the TWI logic and the 2-wire Serial Bus. The different status
	codes are described Transmission Modes on page 219. Note that the value read from TWSR
	contains both the 5-bit status value and the 2-bit prescaler value. The application designer
	should mask the prescaler bits to zero when checking the Status bits. This makes status checking
	independent of prescaler setting. 
 
 */
uint8_t TWI_master_transmit(uint8_t slave_address, uint8_t no_of_byte, uint8_t *data_to_transmit, char end_with_STOP)
{
	uint8_t status;					// transmit status
	
	if (no_of_byte <= TWI_MAX_DATA){  // Can not receive more data than max buffer size
		
		TWI_transmit_START_condition(); 
	 
		while (!(TWCR & (1<<TWINT)));	// Wait Wait for TWINT Flag set, i.e. Start condition transmitted
		//TODO add timeout in case bus not ready
		status = TWSR & 0xF8;			// Get Bits 7:3  TWS: TWI Status
	
		if (status == 0x08){			//A START condition has been transmitted
		
			TWI_transmit_SLA_W(slave_address);
			while (!(TWCR & (1<<TWINT)));	// Wait Wait for TWINT Flag set, i.e. Start condition transmitted
			status = TWSR & 0xF8;			// Get Bits 7:3  TWS: TWI Status
		
			if (status == 0x18){			// SLA+W has been transmitted; ACK has been received
			
				do{
					TWDR = *data_to_transmit;		// Write byte to transmit into TWI Data Register
					data_to_transmit++;
							
					TWCR = (1<<TWINT) | (1<<TWEN);  // Clr interrupt flag, TWI still enabled, other bit = 0
					while (!(TWCR & (1<<TWINT)));	// Wait Wait for TWINT Flag set, i.e. Start condition transmitted
					status = TWSR & 0xF8;			// Get Bits 7:3  TWS: TWI Status
				
					if (status== 0x28){				//Data byte has been transmitted; ACK has been received
						no_of_byte--;
						if (no_of_byte == 0) status=0x00;	// specified number of Data received ok
					}//if (status== 0x28)
				} while (status == 0x28); //Data byte has been transmitted; ACK has been received
			} //if (status == 0x18)			
		} //if (status == 0x08)
	
		if (end_with_STOP== 'Y') TWI_transmit_STOP_condition();
		
	} else status = TWI_STATUS_BUFF_OVERFLOW;
	
	//if (status)
	//	__asm__("BREAK");
		
	return status; // 0x00 if ok, otherwise the erroneous status
}	