/**
 * \file drv_1284_SPI.c
 *
 * \brief mega1284 SPI driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>
#include "drv_1284_SPI.h"
 
// SEL (SS) = PB4 (SPI select; active low)
void SPI_SS_low(void)
{
	PORTB &= ~(1<<PINB4); // SS = 0
}	

// SEL (SS) = PB4 (SPI select; active low)
void SPI_SS_high(void)
{
	PORTB|= (1<<PINB4); // SS = 1
}	

void SPI_MasterTransmit(unsigned char cData)
{
/* Start transmission */
/*
SPDR  SPI Data Register
The SPI Data Register is a read/write register used for data transfer between the Register File
and the SPI Shift Register. Writing to the register initiates data transmission. Reading the register
causes the Shift Register Receive buffer to be read.
*/
SPDR = cData;

/* Wait for transmission complete */
/*
SPSR  SPI Status Register
 Bit 7  SPIF: SPI Interrupt Flag
When a serial transfer is complete, the SPIF Flag is set. An interrupt is generated if SPIE in
SPCR is set and global interrupts are enabled. If SS is an input and is driven low when the SPI is
in Master mode, this will also set the SPIF Flag. 
SPIF is cleared by hardware when executing the corresponding interrupt handling vector. 
Alternatively, the SPIF bit is cleared by first reading the SPI Status Register with SPIF set, 
then accessing the SPI Data Register (SPDR).
*/

while(!(SPSR & (1<<SPIF)))
;
}

unsigned char SPI_Receive(void)
{
/* Wait for reception complete */
//while(!(SPSR & (1<<SPIF)));

/* Return Data Register */
return SPDR;
}


/* 
SEL (SS)	= PB4 (SPI select; active low)
MOSI		= PB5
MISO		= PB6
SCLK (SCK)	= PB7
 */
void SPI_MasterInit(void)
{
/* 	The Power Reduction SPI bit, PRSPI, in PRR  Power Reduction Register on page 49 on page
50 must be written to zero to enable SPI module.*/
	
/* Set MOSI, SCK, and SEL as output */

//DDRB  Port B Data Direction Register

DDRB |= (1<<DDB4)|(1<<DDB5)|(1<<DDB7);

/* Enable SPI, Master, set clock rate fck/16 */
/*
SPCR  SPI Control Register
 Bit 7  SPIE: SPI Interrupt Enable
This bit causes the SPI interrupt to be executed if SPIF bit in the SPSR Register is set and the if
the Global Interrupt Enable bit in SREG is set.
 Bit 6  SPE: SPI Enable
When the SPE bit is written to one, the SPI is enabled. This bit must be set to enable any SPI
operations.
 Bit 5  DORD: Data Order (MSB first required)
When the DORD bit is written to one, the LSB of the data word is transmitted first.
When the DORD bit is written to zero, the MSB of the data word is transmitted first.
 Bit 4  MSTR: Master/Slave Select
This bit selects Master SPI mode when written to one, and Slave SPI mode when written logic
zero. If SS is configured as an input and is driven low while MSTR is set, MSTR will be cleared,
and SPIF in SPSR will become set. The user will then have to set MSTR to re-enable SPI Master
mode.
 Bit 3  CPOL: Clock Polarity
When this bit is written to one, SCK is high when idle. When CPOL is written to zero, SCK is low
when idle
 Bit 2  CPHA: Clock Phase
The settings of the Clock Phase bit (CPHA) determine if data is sampled on the leading (first) or
trailing (last) edge of SCK.
 Bits 1:0  SPR1, SPR0: SPI Clock Rate Select 1 and 0
These two bits control the SCK rate of the device configured as a Master. SPR1 and SPR0 have
no effect on the Slave.
*/

SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);

/* Set CLK high
 PORTB  Port B Data Register
 */
 PORTB|= (1<<PINB7); // CLK = 1
 
 SPI_SS_high();
}

