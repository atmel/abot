/**
 * \file ABOT_protocol.h
 *
 * \brief The ABOT protocol definitions
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */




/*
 *
 * The ABOT protocol definitions details are specified in /Doc/ABOT_protocol.doc
 *
 *
 * Frame definition:
 * | SOH | Destination ABOT | Source ABOT | frame_length | cmd |  payload | EOT
 *
 * Default short address allocation:
 * Broad cast address: 0x2A2A
 * Coordinator:        0x4242
 * Abot node           0x4201 - x42FE
 *
 */ 


#ifndef ABOT_PROTOCOL_H_
#define ABOT_PROTOCOL_H_

/*Application modes*/
#define	TERMINAL_MODE	(0x00)
#define	MOBILE_MODE		(0x01)

/*Constants defined */
#define MAX_PAYLOAD		(0x2F)	// Maximum number of characters in payload 

/* UART baudrate common to more than one project*/
//#define BAUD			(0x01)	// 1=>28800 6=>9600 Baudrate USART0 #define FOSC 1000000 CPU Clock Speed in Hz needs to be specified in main	TODO: make general i.e 9600/28800
#define BAUD			28800	// 9600/19200/28800 Baudrate USART0 #define FOSC 1000000 CPU Clock Speed in Hz needs to be specified in board file1
#define BUFSIZE			(0x60)	// Maximum number of byte in receive buffer for data received from Hugin via UART0

/*ASCII characters defined */
#define SOH				(0x01)  // Start-of-frame character.
#define EOT				(0x04)  // End-of-frame character. 
#define NUL				(0x00)	// Null character

/*----------- Command definitions -----------------*/

/* ASCII based from 0x00 to 0x1F */
#define ACK             (0x06)  //Acknowledge, ASCII ctrl F
#define NAC             (0x15)  //Negative Acknowledge, ASCII ctrl U
#define BEL             (0x07)  //Bell, ASCII ctrl G
#define ENQ             (0x05)  //inquiry, ASCII ctrl E

/*Communication Commands from 0x20 to 0x3F */
#define SET_RF_CH       (0x20) // Change network channel
#define SET_RF_PWR      (0x21) // Set RF output power
#define SET_RF_PAN_ID   (0x22) // Change network ID
#define SET_RF_SHORT    (0x23) // Provide short address to node
#define CLR_RF_SHORT    (0x24) // From Coordinator to node - clear short address to node and request new
#define REQ_RF_SHORT    (0x25) // Node: request coordinator for new short address. Coordinator: request node current short address
#define RF_LINK_DATA    (0x26) // Link data bundle
/*Link data codes*/
	#define LINK_REQUEST        0x01
	#define LINK_STATUS         0x02	
#define ROUTING_TABLE   (0x27) // From Coordinator: Short address routing table - To Coordinator: request current routing table
//#define PINGRADIO_RSP	(0x3E) // Test radio connection reply for PINGRADIO
#define	PING_A   	    (0x3F) // Test radio connection, required reply payload = 0x2A
#define NWK_QUERY		(0x30) // network_query


/*Debug Commands from 0x40 to 0x4F  */
#define ERROR           (0x40) // Payload contain error number
#define VERSION         (0x41) // Request version, reply with DISPLAY ver= Date + time + app
#define GETCRTR         (0x42) // Return Creator
#define FOURHEX			(0x43) // Display a 4 digit hex number
//#define PINGRAVEN		(0x44) // Test connection between Hugin and Munin, request = 0x4C - reply = 0x2A
#define TEST			(0x45) // Test command
/* TEST commands*/
	#define TEST1				0x01  //test #1
	#define TEST2				0x02  //test #2
#define GETSTATE		(0x46) // Request RADIO state	
#define GETRSSI			(0x47) // Request RADIO RSSI
#define GETSHORT		(0x48) // Request nodes short address

/*General Commands from 0x50 to 0xFF  */
#define JOYSTICK		(0x50) // Direction commands to ABOT
/*Valid Key States for JOYSTICK command. */
	#define KEY_STATE_UP        0x01
	#define KEY_STATE_DOWN      0x02
	#define KEY_STATE_LEFT      0x04
	#define KEY_STATE_RIGHT     0x08

#define ABOT			(0x51) // Motor command to ABOT
/* ABOT commands*/
	#define CALIBRATE_P			0x02  //Calibrate motor - add speed
	#define CALIBRATE_N			0x03  //Calibrate motor - subtract speed
	#define CAL_SAVE			0x04  //Save Calibration
	#define STOP				0x05  //STOP motor
	#define CALIBRATE_SPEED	    0x06  //Calibrate motor - speed offset forward
	#define ENABLE_SWITCH       0x07  //Distance sensor enable
	#define DISABLE_SWITCH      0x08  //Distance sensor disable
	#define ENABLE_AUTO         0x09  //Auto action enable
	#define DISABLE_AUTO        0x0A  //Auto action disable
	
#define ABOT_MOTOR_DIRECTION	(0x60)			
	/* Direct motor control sub commands*/
	#define FORWARD				0x06
	#define REVERSE				0x07
	#define LEFT_FORWARD	    0x08
	#define RIGHT_FORWARD		0x09
	#define LEFT_REVERSE        0x0A
	#define RIGHT_REVERSE       0x0B
	
#define DISPLAY			(0x52) // Display text 

#define GET_ABOT_SENSOR (0x53) // Request ABOT sensor value(s)
#define ABOT_SENSOR     (0x54) // ABOT sensor value
/* ABOT sensor codes */
	#define ALL_SENSORS         0x01 // Request all available sensors
	#define SUPPLY_VOLTAGE      0x02 // ABOT supply voltage (battery voltage) in mV
	#define VOLTAGE_VCC		    0x03 // ABOT the uC voltage 
	#define VOLTAGE_EXT  		0x04 // ABOT any external voltage reading 
	#define TEMP_SENSOR		    0x05 // ABOT temperature in degree C x 10
	#define LIGHT_SENSOR        0x06 // ABOT Light sensor 
	
#define ABOT_UIO        (0x55)
/* User I/O detail commands*/
	#define UIO_CONFIGURE       0x01  // config I/O
	#define UIO_STATUS          0x02  // reply from I/O with pin status
	#define UIO_REQUEST         0x03  // Ask for I/O pin status
	#define UIO_SET             0x04  // Set output high or low	

#define ABOT_AUDIO      (0x56)
/* Sound detail commands*/
	#define AUDIO_RECORDING     0x01  // Recorded sound
	#define AUDIO_STOP          0x02  // Stop record - Stop tune
	#define AUDIO_TUNE_PLY      0x03  // Play selected predefined tune

/*Error definitions*/
#define  NO_SOH			(0x01) // SOH expected 
#define  NO_EOT			(0x02) // EOT expected 
#define  A_TIMEOUT		(0x03) // Timeout while waiting for RX char 
#define  P_TO_LONG		(0x04) // Payload longer than MAX_PAYLOAD
#define  BUF_EMPTY		(0x05) // rx_buf empty when trying to get char


// ABOT protocol frame,  SOH | frame_length | cmd | payload | EOT
typedef struct
{
	unsigned char error;
	unsigned char length;
	unsigned char cmd;
	unsigned char payload[MAX_PAYLOAD];
} ABOT_frame;


#endif /* ABOT_PROTOCOL_H_ */
