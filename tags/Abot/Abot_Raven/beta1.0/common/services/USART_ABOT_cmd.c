/**
 * \file USART_ABOT_cmd.c
 *
 * \brief The ABOT protocol implemented for a UART point to point communication
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <string.h>

//Abot common includes
#include <services/ABOT_protocol.h>

#include "USART_ABOT_cmd.h"

//include board related drivers
#include <boards/board.h>
#if (BOARD==RAVEN)
	#include "board_RAVEN.h"
	#include <drivers/drv_mega_USART0.h>
#else
	#error TARGET_BOARD must be defined somewhere
#endif

/* UART init */
void USART_ABOT_init(void){
	
	USART_init(BAUD, FOSC);
}	

/* Transmit one frame on USART0*/
void USART0_tx_frame(ABOT_frame f)
{
  unsigned char t;
  
  USART_tx_char(SOH);                                       // frame start
  USART_tx_char(f.length);                                  // frame length
  USART_tx_char(f.cmd);                                     // frame command
  for (t=0; t<f.length-4; t++) USART_tx_char(f.payload[t]);	// frame payload
  USART_tx_char(EOT);                                       // frame end
}


/*
	Get frame from USART0
*/
ABOT_frame USART_Get_frame(void)
{
	unsigned char	error;
	unsigned char	t;
	unsigned char	p_lengt;	// Payload length
	ABOT_frame		f;			// The received frame
	
	error = NUL;
	t=0;
	
	// Start of frame
	while ((t!=SOH) & (error==0))
	{
		t=USART_get_char(); // Get start of frame
		if (t==NUL) error=NO_SOH; //Could not find SOH in buffer, BUF_EMPTY;
	}
	
	//Frame length
	if (error==NUL) 
	{
		p_lengt = USART_get_char();
		if (p_lengt==NUL)  error = A_TIMEOUT;
		
		if (p_lengt>MAX_PAYLOAD-4)  error = P_TO_LONG;	
		f.length = p_lengt; // generally not used - but good to have when debugging	
		p_lengt=p_lengt-4;  // payload length = Frame length - 4 byte : SOH | frame_length | cmd | payload | EOT
	}		
	
	// Command
	if (error==NUL) 
	{
		f.cmd= USART_get_char(); //Timeout at this stage => f.cmd=NUL as well
		if (f.cmd==NUL) error = A_TIMEOUT;
	} else f.cmd=NUL;		
	
	//Payload (Payload can be NULL i.e. no TIMEOUT error detection
	if (error==NUL) 
	{
		for (t=0; t<p_lengt; t++)
		{
			f.payload[t] = USART_get_char();		
		} 		
	} else f.payload[0] = NUL;
		
	//End of frame
	if (error==NUL)
	{
		if (USART_get_char()!=EOT)  error = NO_EOT; 
	}
	
	f.error= error;
	return(f);
}


/* ================== commands =========================== */


/* Transmit a DISPLAY frame 
*/
void USART_printf(char txt[MAX_PAYLOAD])
{
  ABOT_frame pf; // protocol frame
  unsigned char t=0;
  
  pf.cmd=DISPLAY;
  while ((txt[t]!='\0') & (t<MAX_PAYLOAD))
  {
		pf.payload[t]=txt[t];
		t++;
  }
  pf.payload[t]='\0';
  
  t=strnlen((const char *)pf.payload,MAX_PAYLOAD)+5;	// Payload length + nul char + SOH + Length + cmd + EOT
  //returns the length of the string s in bytes if this length is smaller than maxlen bytes. Otherwise it returns maxlen, 
  
  pf.length=t;
  USART0_tx_frame(pf);    
}

/* Transmit a 4 digit hex NUMBER frame 
*/
void USART_print4hex(unsigned int num)
{
  ABOT_frame pf; // protocol frame
  unsigned int t=0;
  
	pf.cmd=FOURHEX;
	t = num & 0x0F;
	pf.payload[0]=(char)t; // first hex digit (4bit)
	num= num>>4;
	t = num & 0x0F;
	pf.payload[1]=(char)t;
	num= num>>4;
	t = num & 0x0F;
	pf.payload[2]=(char)t;
	num= num>>4;
	t = num & 0x0F;
	pf.payload[3]=(char)t;
	pf.length=8;
  
  USART0_tx_frame(pf);  
}


/* ============================================================= */

/* Transmit joystick command*/
void USART_joy(unsigned char position)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=JOYSTICK;
	pf.payload[0]= position; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

/* Transmit ABOT command*/
void USART_ABOT(unsigned char command)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=ABOT;
	pf.payload[0]= command; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

/* Transmit TEST command */
void USART_TEST(uint8_t command)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=TEST;
	pf.payload[0]= command; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

/* Transmit a RADIO PING command */
void USART_RADIOPING(unsigned char pingpay)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=PING_A;
	pf.payload[0]= pingpay; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

/* Transmit a GET RADIO STATE command */
void USART_GET_STATE(void)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=GETSTATE;
	pf.payload[0]= 0x21; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

/* Transmit a GET RADIO RSSI command */
void USART_GET_RSSI(void)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=GETRSSI;
	pf.payload[0]= 0x22; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

/* Transmit a GET RADIO RSSI command */
void USART_GET_SHORT(void)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=GETSHORT;
	pf.payload[0]= 0x22; 
	pf.length=5;
  
  USART0_tx_frame(pf); 
}

void USART_GET_SENSOR(unsigned char sensor)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=GET_ABOT_SENSOR;
	pf.payload[0]= sensor; 
	pf.length=5;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
  
  USART0_tx_frame(pf); 
}


void USART_SENSOR(unsigned char sensor, uint16_t svalue)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd=ABOT_SENSOR;
	pf.payload[0]= sensor; // Sensor type
	pf.payload[1]= (unsigned char) svalue;   // value Low byte
	svalue = svalue >>8;
	pf.payload[2]= (unsigned char)svalue;  // value high byte
	pf.length=7;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
  
  USART0_tx_frame(pf); 
}

void USART_AUDIO(unsigned char cmd, uint16_t length, char sound[MAX_PAYLOAD])
{
	ABOT_frame pf; // protocol frame
	//unsigned char t=0;
	
	if (cmd == AUDIO_TUNE_PLY)
	{
		pf.cmd=ABOT_AUDIO;
		pf.payload[0]= cmd; // Sensor type
		pf.payload[1]= (unsigned char)length; // sound length low byte
		length = length >>8;
		pf.payload[2]= (unsigned char)length; // sound length high byte
	
	/*
		while ((sound[t]!='\0') & (t<MAX_PAYLOAD))
		{
			pf.payload[t+4]=sound[t];
			t++;
		}

		pf.length=7+t;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
		*/
		pf.length=7;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
		USART0_tx_frame(pf);
	}
}


/*! \brief User I/O handling
 *
    #define UIO_CONFIGURE       0x01  // config I/O
	#define UIO_STATUS          0x02  // reply from I/O with pin status
	#define UIO_REQUEST         0x03  // Ask for I/O pin status
	#define UIO_SET             0x04  // Set output high or low
 *
 * \param  cmd     The user I/O action to perform
 *
 * \param  IOdata  The user I/O value, 
 *                 if UIO_CONFIGURE: pin direction input ("0") and output ("1") 
 *
 * \param  GPIO    UIO_SET: select pin (s) to set
 *                 UIO_CONFIGURE: "1" fro pin with interrupt capability
 *
 */
void USART_USERIO(unsigned char cmd, uint16_t IOdata, uint16_t GPIO)
{
  ABOT_frame pf; // protocol frame
  
	pf.cmd = ABOT_UIO;
	pf.payload[0]= cmd;
	switch (cmd)
	{
		case UIO_CONFIGURE:
		case UIO_SET:
			pf.payload[1]= (unsigned char)IOdata;  //Low byte
			IOdata = IOdata >> 8;
			pf.payload[2]= (unsigned char)IOdata;  //High byte
			pf.payload[3]= (unsigned char)GPIO;
			GPIO = GPIO >> 8;
			pf.payload[4]= (unsigned char)GPIO;
			pf.length=9;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
		break;
		case UIO_STATUS:
			pf.payload[1]= (unsigned char)IOdata;  //Low byte
			IOdata = IOdata >> 8;
			pf.payload[2]= (unsigned char)IOdata;  //High byte
			pf.length=7;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
		break;
		case UIO_REQUEST:
			pf.length=5;  // SOH + FRAME LENGTH + CMD + PAYLOAD + EOT
		break;
	}	
	USART0_tx_frame(pf); 
}	


