**Abot**, the Atmel Robot.


**A gadget designed to inspire people to play with Atmel development kits and have fun.** [View on YouTube](http://youtu.be/HAuAM8x_vGM)

![img.jpg](https://bitbucket.org/repo/Edxbq9/images/2328372399-img.jpg)

This project conatins a library (/common), board related projects (/projects), Windows related projects (/windows), and Android related projects (/android).


The projects initially contain a library and projects for the RAVEN kit, and a general Android control interface.


The basic kit is [available at the Atmel Store](http://www.atmel.com/tools/mrabot.aspx) (order code: ATABOT) and consists of:

- Basic acrylic chassis (easy to add stuff making additional holes or use glue)
- Two low power Micro Gear motors with
- Wheels
- Battery case
- Ball Caster (rear wheel)
- Distance Sensor, 10cm
- Cable kit



This kit can be combined with a number of Atmel kits to form a basic robot.

It is easy to add sensors and other stuff to the basic Abot to form more advanced robots.

A common protocol enables communication between all future Abot combinations.

There is more work to do on the library and an endless amount of possible projects - if you want to contribute please read the Wiki pages for prerequisites to join the Abot project.


More information on the Wiki pages - major documents available in Docs