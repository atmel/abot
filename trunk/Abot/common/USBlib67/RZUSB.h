/*
 * RZUSB.h
 *
 * Created: 29/12/2011 12:41:11
 *  Author: TB
 */ 


//! This module contains low level hardware abstraction layer for RZUSB stick

#ifndef RZUSB_H_
#define RZUSB_H_


//_____ I N C L U D E S ____________________________________________________

#include "config.h"
//#include <USBlib67/config.h>

#if (TARGET_BOARD==RZUSB)

//_____ M A C R O S ________________________________________________________


//! Macros to manage LEDs
//! The led 0 correspond at led LED1  PD7 (VBUS) 1=on
//! The led 1 correspond at led LED2  PD5
//! The led 2 correspond at led LED3  PE7
//! The led 3 correspond at led LED4  PE6
// LEDs are connected to Vcc, i.e. ON = "0"
      
#define  LED_PORT             PORTD
#define  LED_DDR              DDRD
#define  LED_PIN              PIND
#define  LED0_BIT             PIND7
#define  LED1_BIT             PIND5
#define  LED2_BIT             PINE7
#define  LED3_BIT             PINE6
                      
#define  Leds_init()          ((LED_DDR |= (1<<LED0_BIT) | (1<<LED1_BIT)),(DDRE |= (1<<LED2_BIT) | (1<<LED3_BIT)))

//#define  Leds_on()            (LED_PORT |= (1<<LED0_BIT) | (1<<LED1_BIT) | (1<<LED2_BIT) | (1<<LED3_BIT))
//#define  Leds_off()           (LED_PORT &= ~((1<<LED0_BIT) | (1<<LED1_BIT) | (1<<LED2_BIT) | (1<<LED3_BIT)))
//#define  Leds_set_val(c)      (Leds_off(),LED_PORT |= (c<<4)&((1<<LED0_BIT) | (1<<LED1_BIT) | (1<<LED2_BIT) | (1<<LED3_BIT)))
//#define  Leds_get_val()       (LED_PORT>>4)

#define  Led0_on()            (LED_PORT |=  (1<<LED0_BIT))
#define  Led1_off()            (LED_PORT |=  (1<<LED1_BIT))
#define  Led2_off()            (PORTE |=  (1<<LED2_BIT))
#define  Led3_off()            (PORTE |=  (1<<LED3_BIT))
#define  Led0_off()           (LED_PORT &= ~(1<<LED0_BIT))
#define  Led1_on()           (LED_PORT &= ~(1<<LED1_BIT))
#define  Led2_on()           (PORTE &= ~(1<<LED2_BIT))
#define  Led3_on()           (PORTE &= ~(1<<LED3_BIT))
#define  Led0_toggle()        (LED_PIN  |=  (1<<LED0_BIT))
#define  Led1_toggle()        (LED_PIN  |=  (1<<LED1_BIT))
#define  Led2_toggle()        (PINE  |=  (1<<LED2_BIT))
#define  Led3_toggle()        (PINE  |=  (1<<LED3_BIT))
//#define  Is_led0_on()         (LED_PIN  &   (1<<LED0_BIT) ? TRUE : FALSE)
//#define  Is_led1_on()         (LED_PIN  &   (1<<LED1_BIT) ? TRUE : FALSE)
//#define  Is_led2_on()         (PINE  &   (1<<LED2_BIT) ? TRUE : FALSE)
//#define  Is_led3_on()         (PINE  &   (1<<LED3_BIT) ? TRUE : FALSE)


#endif   // TARGET_BOARD==RZUSB
#endif /* RZUSB_H_ */