/*
 * drv_Xmega256A3U_SPI.h
 *
 * Created: 02.10.2013 10:27:38
 *  Author: tbjoernvold
 */ 


#ifndef DRV_XMEGA256A3U_SPI_H_
#define DRV_XMEGA256A3U_SPI_H_

#define AT86RFX_SPI                  &SPIC

#define AT86RFX_SPI_CS               IOPORT_CREATE_PIN(PORTC, 4)
#define AT86RFX_SPI_MOSI             IOPORT_CREATE_PIN(PORTC, 5)
#define AT86RFX_SPI_MISO             IOPORT_CREATE_PIN(PORTC, 6)
#define AT86RFX_SPI_SCK              IOPORT_CREATE_PIN(PORTC, 7)

#define AT86RFX_SPI_BAUDRATE            3000000

void SPI_SS_low(void); // Set SPI select low
void SPI_SS_high(void); // Set SPI select high
void SPI_MasterTransmit(unsigned char cData);
unsigned char SPI_Receive(void);
void SPI_MasterInit(void);


#endif /* DRV_XMEGA256A3U_SPI_H_ */