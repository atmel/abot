/*
 * drv_RF233_XZigBit.h
 *
 * Created: 02.10.2013 10:18:06
 *  Author: tbjoernvold
 */ 


#ifndef DRV_RF233_XZIGBIT_H_
#define DRV_RF233_XZIGBIT_H_

#define AT86RFX_RST_PIN              IOPORT_CREATE_PIN(PORTC, 0)
#define AT86RFX_IRQ_PIN              IOPORT_CREATE_PIN(PORTC, 2)
#define AT86RFX_SLP_PIN              IOPORT_CREATE_PIN(PORTC, 3)

#define AT86RFX_INTC_INIT()          ioport_configure_pin(AT86RFX_IRQ_PIN, IOPORT_DIR_INPUT); \
									 PORTC.PIN2CTRL = PORT_ISC0_bm; \
									 PORTC.INT0MASK = PIN2_bm; \
									 PORTC.INTFLAGS = PORT_INT0IF_bm;

#define AT86RFX_ISR()                ISR(PORTC_INT0_vect)

/** Enables the transceiver main interrupt. */
#define ENABLE_TRX_IRQ()                (PORTC.INTCTRL |= PORT_INT0LVL_gm)

/** Disables the transceiver main interrupt. */
#define DISABLE_TRX_IRQ()               (PORTC.INTCTRL &= ~PORT_INT0LVL_gm)

/** Clears the transceiver main interrupt. */
#define CLEAR_TRX_IRQ()                 (PORTC.INTFLAGS = PORT_INT0IF_bm)

/*
 * This macro saves the trx interrupt status and disables the trx interrupt.
 */
#define ENTER_TRX_REGION()   { uint8_t irq_mask = PORTC.INTCTRL; PORTC.INTCTRL &= ~PORT_INT0LVL_gm

/*
 *  This macro restores the transceiver interrupt status
 */
#define LEAVE_TRX_REGION()   PORTC.INTCTRL = irq_mask; }


void rf230_TST_low(void);     // (Diables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void);    // (Enables Continuous Transmission Test Mode; active high)
void rf230_RST_low(void);     // (Chip reset; active low)
void rf230_RST_high(void);    // (Chip reset; active low)
void rf230_SLP_TR_low(void);  // (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_high(void); // (Controls sleep, transmit start and receive states; active high)
void rf230_drv_init(void);



#endif /* DRV_RF233_XZIGBIT_H_ */