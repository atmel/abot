/*
 * drv_Xmega256A3U_SPI.c
 *
 * Created: 02.10.2013 10:28:02
 *  Author: tbjoernvold
 */ 

/*
 * drv_1284_SPI.c
 *
 * Created: 15/01/2012 23:24:46
 *  Author: TB
 */ 
#include "drv_Xmega256A3U_SPI.h"

#include <avr/io.h>
#include "xmega_gpio/xmega_gpio.h"
//#include "spi.h"
#include "xmega_spi/spi_master.h"

struct spi_device SPI_AT86RFX_DEVICE = {
	/* ! Board specific select id */
	.id = AT86RFX_SPI_CS
};
 
// SEL (SS) = PC4 (SPI select; active low)
void SPI_SS_low(void)
{
	gpio_set_pin_low(AT86RFX_SPI_CS); // SS = 0
}	

// SEL (SS) = PC4 (SPI select; active low)
void SPI_SS_high(void)
{
	gpio_set_pin_high(AT86RFX_SPI_CS); // SS = 1
}	

void SPI_MasterTransmit(unsigned char cData)
{
		SPI_t *spi;
		spi = AT86RFX_SPI;
		
/* Start transmission */
/*
SPDR  SPI Data Register
The SPI Data Register is a read/write register used for data transfer between the Register File
and the SPI Shift Register. Writing to the register initiates data transmission. Reading the register
causes the Shift Register Receive buffer to be read.
*/
////////SPDR = cData;
spi->DATA = cData;

/* Wait for transmission complete */
/*
SPSR  SPI Status Register
 Bit 7  SPIF: SPI Interrupt Flag
When a serial transfer is complete, the SPIF Flag is set. An interrupt is generated if SPIE in
SPCR is set and global interrupts are enabled. If SS is an input and is driven low when the SPI is
in Master mode, this will also set the SPIF Flag. 
SPIF is cleared by hardware when executing the corresponding interrupt handling vector. 
Alternatively, the SPIF bit is cleared by first reading the SPI Status Register with SPIF set, 
then accessing the SPI Data Register (SPDR).
*/

////////while(!(SPSR & (1<<SPIF)))
while (!spi_is_rx_full(spi)) {
}

}

unsigned char SPI_Receive(void)
{
	SPI_t *spi;
	spi = AT86RFX_SPI;
/* Wait for reception complete */
//while(!(SPSR & (1<<SPIF)));

/* Return Data Register */
////////return SPDR;
	while (!spi_is_rx_full(spi)) {
	}
	return spi->DATA;

}


/* 
SEL (SS)	= PC4 (SPI select; active low)
MOSI		= PC5
MISO		= PC6
SCLK (SCK)	= PC7
 */
void SPI_MasterInit(void)
{
/* 	The Power Reduction SPI bit, PRSPI, in PRR  Power Reduction Register on page 49 on page
50 must be written to zero to enable SPI module.*/
	

/* Enable SPI, Master, set clock rate fck/16 */
/*
SPCR  SPI Control Register
 Bit 7  SPIE: SPI Interrupt Enable
This bit causes the SPI interrupt to be executed if SPIF bit in the SPSR Register is set and the if
the Global Interrupt Enable bit in SREG is set.
 Bit 6  SPE: SPI Enable
When the SPE bit is written to one, the SPI is enabled. This bit must be set to enable any SPI
operations.
 Bit 5  DORD: Data Order (MSB first required)
When the DORD bit is written to one, the LSB of the data word is transmitted first.
When the DORD bit is written to zero, the MSB of the data word is transmitted first.
 Bit 4  MSTR: Master/Slave Select
This bit selects Master SPI mode when written to one, and Slave SPI mode when written logic
zero. If SS is configured as an input and is driven low while MSTR is set, MSTR will be cleared,
and SPIF in SPSR will become set. The user will then have to set MSTR to re-enable SPI Master
mode.
 Bit 3  CPOL: Clock Polarity
When this bit is written to one, SCK is high when idle. When CPOL is written to zero, SCK is low
when idle
 Bit 2  CPHA: Clock Phase
The settings of the Clock Phase bit (CPHA) determine if data is sampled on the leading (first) or
trailing (last) edge of SCK.
 Bits 1:0  SPR1, SPR0: SPI Clock Rate Select 1 and 0
These two bits control the SCK rate of the device configured as a Master. SPR1 and SPR0 have
no effect on the Slave.
*/

////////SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0)|(1<<SPR1); //fck/128

/* Set CLK high
 PORTB  Port B Data Register
 */
 ////////PORTB|= (1<<PINB1); // CLK = 1
 
 /* Set MOSI, SCK, and SEL as output, MISO as input */
 ioport_configure_pin(AT86RFX_SPI_SCK, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
 ioport_configure_pin(AT86RFX_SPI_MOSI, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
 ioport_configure_pin(AT86RFX_SPI_MISO, IOPORT_DIR_INPUT);
 ioport_configure_pin(AT86RFX_SPI_CS, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);


/* Initialize SPI in master mode to access the transceiver */
spi_master_init(AT86RFX_SPI);
spi_master_setup_device(AT86RFX_SPI, &SPI_AT86RFX_DEVICE, SPI_MODE_0,AT86RFX_SPI_BAUDRATE, 0);
spi_enable(AT86RFX_SPI);

 // Set CLK high??
 
 SPI_SS_high();
}

