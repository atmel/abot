/**
 * \file drv_RF230_RAVEN.c
 *
 * \brief RF230 driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h> 


/* RF230 IRQ interrupt connected to PD6, PCINT30 

By reading the register after an interrupt is signaled at IRQ pin, the reason for the
interrupt can be identified.
A detailed description of the individual interrupts can be found in Table 6-8.

*/
ISR(PCINT3_vect)
{
	extern unsigned char RF230_int_flag;

		RF230_int_flag=0x01;	
}

// TST = PB0 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_low(void)
{
	PORTB &= ~(1<<PINB0); // TST = 0
}	

// TST = PB0 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void)
{
	PORTB|= (1<<PINB0); // TST = 1
}	

// RST = PB1 (Chip reset; active low)
void rf230_RST_low(void)
{
	PORTB &= ~(1<<PINB1); // RST = 0
}	

// RST = PB1 (Chip reset; active low)
void rf230_RST_high(void)
{
	PORTB|= (1<<PINB1); // RST = 1
}

// SLP_TR= PB3 (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_low(void)
{
	PORTB &= ~(1<<PINB3); // SLP_TR = 0
}	

// SLP_TR= PB3 (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_high(void)
{
	PORTB|= (1<<PINB3); // SLP_TR = 1
}


/*
TST		= PB0 (Enables Continuous Transmission Test Mode; active high)
RST		= PB1 (Chip reset; active low)
SLP_TR	= PB3 (Controls sleep, transmit start and receive states; active high)
IRQ		= PD6, PCINT30

A hardware reset is initiated by pulling the RST line low. 6 ?s later the same line is pulled high again and the hardware reset finished. 
All registers will have their default values and the state machine will be in P_ON or TRX_OFF state.
*/
void RF_drv_init(void)
{
	extern unsigned char RF230_int_flag;
	
	RF230_int_flag=0;
	
	/* Set SLP_TR, RST, and TST as output */

	//DDRB � Port B Data Direction Register "1" = output

	DDRB |= (1<<DDB0)|(1<<DDB1)|(1<<DDB3);
	
	/* Enable Interrupt from RF230
		
	PCMSK3 � Pin Change Mask Register 3
	Bit 7:0 � PCINT31:24: Pin Change Enable Mask 31:24
		Each PCINT31:24-bit selects whether pin change interrupt is enabled on the corresponding I/O
		pin. If PCINT31:24 is set and the PCIE3 bit in PCICR is set, pin change interrupt is enabled on
		the corresponding I/O pin. If PCINT31..24 is cleared, pin change interrupt on the corresponding
		I/O pin is disabled.
	
	*/
	PCMSK3 |= (1<<PCINT30);  //Pin change bit =1
	
	/*	
	PCICR � Pin Change Interrupt Control Register
	Bit 3 � PCIE3: Pin Change Interrupt Enable 3
		When the PCIE3 bit is set (one) and the I-bit in the Status Register (SREG) is set (one), pin
		change interrupt 3 is enabled. Any change on any enabled PCINT31..24 pin will cause an interrupt.
		The corresponding interrupt of Pin Change Interrupt Request is executed from the PCI3
		Interrupt Vector. PCINT31..24 pins are enabled individually by the PCMSK3 Register.
	*/
	PCICR |= (1<<PCIE3);
	
}