/*
 * userpage.h
 *
 * Created: 18.10.2013 14:12:35
 *  Author: tbjoernvold
 */ 


#ifndef USERPAGE_H_
#define USERPAGE_H_


typedef struct // Wireless board info stored in user page
{
	uint16_t   mib_revision;
	uint64_t   mac_ieee_address_64bit;
	uint8_t    board_name[30];	       //PCBA name
	uint8_t    board_serial[10];	   //PCBA production serial number
	uint8_t    board_id[8];	           //PLM PCBA A# ( A09-1845)
	uint8_t    board_revision;	       //PCBA revision in PLM
	uint8_t    rf_power_class;
	uint8_t    rf_feature;
	uint8_t    rf_ant_rangeinfo;
	uint8_t    cal_16mhz;
	uint8_t    ant_gain;
	uint8_t    ant_range1_start;
	uint8_t    ant_range1_stop;
	uint8_t    ant_range2_start;
	uint8_t    ant_range2_stop;
	uint8_t    ant_range3_start;
	uint8_t    ant_range3_stop;
	uint8_t    tx_pwr_default;
	uint8_t    pa_gain;
	uint8_t    bypass_gain;
	uint8_t    lna_gain;
	uint16_t   mib_crc;
} board_info;

board_info userpage(void);



#endif /* USERPAGE_H_ */