/**
 * \file   drv_256RFR2_LED_EXT.c
 *
 * \brief LED control
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>

/*! \brief Initialize LED's
 * 
 * Define IO ports as output
 * PE2 YELLOW
 * PD6 RED
 * PG2 GREEN
 * PB4 BLUE
 */
void _LED_init(void)
{
	/*
	DDRE � Port E Data Direction Register "0" => input
	*/	
	
	DDRE|= (1<<DDRE2); //PE2 as output (=1)
	DDRD|= (1<<DDRD6); //PD6 as output (=1)
	DDRG|= (1<<DDRG2); //PG2 as output (=1)
	DDRB|= (1<<DDRB4); //PB4 as output (=1)
	
	//LED off
	
	PORTD |= (1<<PORTD6); //=1
	PORTG |= (1<<PORTG2); //=1
	PORTE |= (1<<PORTE2); //=1
	PORTB &= !(1<<PORTB4); //=0
	
}


/*! \brief Turn LED on
 *
 * 
 *
 * \param   color LED color
 *
 */
void _LED_off(unsigned char color)
{
	/*
	If PORTBn is written logic one when the pin is configured as an output pin, the port pin is driven high (one). If
	PORTBn is written logic zero when the pin is configured as an output pin, the port pin is
	driven low (zero).
	*/
	switch (color)
	{
		case 0xA1: //PD6 RED
			PORTD |= (1<<PORTD6); //=1
		break;
		case 0xA2: //PG2 GREEN
			PORTG |= (1<<PORTG2); //=1
		break;
		case 0xA3: //PE2 YELLOW
			PORTE |= (1<<PORTE2); //=1
		break;
		case 0xA4: //PB4 BLUE
			PORTB &= !(1<<PORTB4); //=0
		break;
	}
	
}

/*! \brief Turn LED off
 *
 * 
 *
 * \param   color LED color
 *
 */
void _LED_on(unsigned char color)
{

	/*
	If PORTBn is written logic one when the pin is configured as an output pin, the port pin is driven high (one). If
	PORTBn is written logic zero when the pin is configured as an output pin, the port pin is
	driven low (zero).
	*/
	switch (color)
	{
		case 0xA1: //PD6 RED
			PORTD &= !(1<<PORTD6); //=0
		break;
		case 0xA2: //PG2 GREEN
			PORTG &= !(1<<PORTG2); //=0
		break;
		case 0xA3: //PE2 YELLOW
			PORTE &= !(1<<PORTE2); //=0
		break;
		case 0xA4: //PB4 BLUE
			PORTB |= (1<<PORTB4); //=1
		break;
	}
}

/*! \brief Toggle LED
 *
 * 
 *
 * \param   color LED color
 *
 */
void _LED_toggle(unsigned char color)
{
	/*
	PINB � Port B Input Pins Address
	This register allows access to the PORTB pins independent of the setting of the Data
	Direction bit DDBn. The port pin can be read through the PINBn Register bit, and
	writing a logic one to PINBn toggles the value of PORTBn.
	*/
	switch (color)
	{
		case 0xA1: //PD6 RED
			PIND |= (1<<PIND6); //=1
		break;
		case 0xA2: //PG2 GREEN
			PING |= (1<<PING2); //=1
		break;
		case 0xA3: //PE2 YELLOW
			PINE |= (1<<PINE2); //=1
		break;
		case 0xA4: //PB4 BLUE
			PINB |= (1<<PINB4); //=1
		break;
	}
}