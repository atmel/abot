/*
 * drv_RF230.h
 *
 * Created: 25/02/2012 15:44:48
 *  Author: TB
 */ 


#ifndef DRV_RF230_H_
#define DRV_RF230_H_


void rf230_TST_low(void);     // (Diables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void);    // (Enables Continuous Transmission Test Mode; active high)
void rf230_RST_low(void);     // (Chip reset; active low)
void rf230_RST_high(void);    // (Chip reset; active low)
void rf230_SLP_TR_low(void);  // (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_high(void); // (Controls sleep, transmit start and receive states; active high)
void RF_drv_init(void);

#endif /* DRV_RF230_H_ */