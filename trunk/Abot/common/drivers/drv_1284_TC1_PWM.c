
/**
 * \file  drv_1284_TC1_PWM.c
 *
 * \brief 16-bit Timer/Counter1 with PWM in mega1284P
 *
 * \author   TB
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>

/*
PD5 OC1A (Timer/Counter1 Output Compare Match A Output)
PD4 OC1B (Timer/Counter1 Output Compare Match B Output)

PD5 OC1A Right- starboard - motor
PD4 OC1B Left - port side - motor

*/
void PWM_init (void)
{


/*
The general I/O port function is overridden by the Output Compare (OCnx) from the Waveform
Generator if either of the COMnx1:0 bits are set. However, the OCnx pin direction (input or output)
is still controlled by the Data Direction Register (DDR) for the port pin. The Data Direction
Register bit for the OCnx pin (DDR_OCnx) must be set as output before the OCnx value is visible
on the pin.
DDRD  Port D Data Direction Register "1" => output

OC1B  Port D, Bit 4
OC1B, Output Compare Match B output: The PD4 pin can serve as an external output for the
Timer/Counter1 Output Compare B. The pin has to be configured as an output (DDD4 set (one))
to serve this function. The OC1B pin is also the output pin for the PWM mode timer function.

OC1A  Port D, Bit 5
OC1A, Output Compare Match A output: The PD5 pin can serve as an external output for the
Timer/Counter1 Output Compare A. The pin has to be configured as an output (DDD5 set (one))
to serve this function. The OC1A pin is also the output pin for the PWM mode timer function.

*/	

DDRD |= ((1<<DDD5) | (1<<DDD4));

/*
The Timer/Counter can be clocked by an internal or an external clock source. The clock source
is selected by the Clock Select logic which is controlled by the Clock Select (CSn2:0) bits
located in the Timer/Counter control Register B (TCCRnB).

the TOP value defines the period time for waveforms generated by the Waveform Generator

The OCRnx Register is double buffered when using any of the twelve Pulse Width Modulation
(PWM) modes. The double buffering synchronizes the update of the OCRnx Compare
Register to either TOP or BOTTOM of the counting sequence. The synchronization prevents the occurrence of odd-length, 
non-symmetrical PWM pulses, thereby making the output glitch-free.

Phase and Frequency Correct PWM Mode
The counter counts repeatedly from BOTTOM
(0x0000) to TOP and then from TOP to BOTTOM. In non-inverting Compare Output mode, the
Output Compare (OCnx) is cleared on the compare match between TCNTn and OCRnx while
upcounting, and set on the compare match while downcounting. In inverting Compare Output
mode, the operation is inverted

The dual-slope operation gives a lower maximum operation frequency
compared to the single-slope operation. However, due to the symmetric feature of the
dual-slope PWM modes, these modes are preferred for motor control applications.

*/


/*
TCCR1A  Timer/Counter1 Control Register A 1111 0000
Bit 7:6  COMnA1:0: Compare Output Mode for Channel A
Bit 5:4  COMnB1:0: Compare Output Mode for Channel B
Set OCnA/OCnB on Compare Match when upcounting.Clear OCnA/OCnB on Compare Match when downcounting.
Bit 1:0  WGMn1:0: Waveform Generation Mode 8 (00)
Combined with the WGMn3:2 bits found in the TCCRnB Register, these bits control the counting
sequence of the counter, the source for maximum (TOP) counter value, and what type of waveform
generation to be used.

TCCR1B  Timer/Counter1 Control Register B  0001 0001
Bit 4:3  WGMn3:2: Waveform Generation Mode 10
Bit 2:0  CSn2:0: Clock Select = clkI/O/1 (No prescaling) i.e. CLKio = 1MHz

Mode 8: PWM, Phase and Frequency Correct
TOP = ICRn
Update of OCRnx at BOTTOM
TOVn Flag Set on BOTTOM
*/
TCCR1A = 0xF0;
TCCR1B = 0x11;


//TCCR1C  Timer/Counter1 Control Register C - not used in PWM mode - should be set to 0x00
TCCR1C=0x00;

/*
The ICRn Register can only be written when using a Waveform Generation mode that utilizes
the ICRn Register for defining the counters TOP value. In these cases the Waveform Generation mode (WGMn3:0) bits 
must be set before the TOP value can be written to the ICRn Register. 
*/
ICR1 = 10000;  // CLKio = 1MHz and 20mS periode, TOP


//OCR1AH and OCR1AL  Output Compare Register 1 A The Output Compare Registers contain a 16-bit value that is continuously compared with the counter value (TCNTn).
OCR1A = 0x0; //No pulse at start - motor not moving

//OCR1BH and OCR1BL  Output Compare Register 1 B
OCR1B = 0x0; // No pulse at start - motor not moving


//TIMSK1  Timer/Counter1 Interrupt Mask Register, no interrupts enabled
TIMSK1 = 0x00;

	
}

/* set the period in uS */
void PWM_set_period (unsigned int period)
{
	/*
	The ICRn Register can only be written when using a Waveform Generation mode that utilizes
	the ICRn Register for defining the counters TOP value. In these cases the Waveform Generation mode (WGMn3:0) bits 
	must be set before the TOP value can be written to the ICRn Register. 
	*/
	ICR1 = period/2;  // TOP value equals half the period, CLKio = 1MHz
	
}

/* set the pulse width in uS for output OCnA (Output Compare) */
void PWM_set_pulse_width_A (unsigned int width)
{
	/*Phase and Frequency Correct PWM Mode
		The counter counts repeatedly from BOTTOM (0x0000) to TOP and then from TOP to BOTTOM. 
		In non-inverting Compare Output mode, 
			the Output Compare (OCnx) is cleared on the compare match between TCNTn and OCRnx while upcounting, 
			and set on the compare match while downcounting. 
		In inverting Compare Output mode, the operation is inverted*/
	
	//OCR1AH and OCR1AL  Output Compare Register 1 A The Output Compare Registers contain a 16-bit value that is continuously compared with the counter value (TCNTn).
	OCR1A = ICR1-width/2; // TOP value - half the pulse width. Only valid if CLKio = 1MHz 
	
}

/* set the pulse width in uS for output OCnB (Output Compare) */
void PWM_set_pulse_width_B (unsigned int width)
{
	/*Phase and Frequency Correct PWM Mode
		The counter counts repeatedly from BOTTOM (0x0000) to TOP and then from TOP to BOTTOM. 
		In non-inverting Compare Output mode, 
			the Output Compare (OCnx) is cleared on the compare match between TCNTn and OCRnx while upcounting, 
			and set on the compare match while downcounting. 
		In inverting Compare Output mode, the operation is inverted*/
	
	//OCR1AH and OCR1AL  Output Compare Register 1 A The Output Compare Registers contain a 16-bit value that is continuously compared with the counter value (TCNTn).
	OCR1B = ICR1-width/2; // TOP value - half the pulse width. Only valid if CLKio = 1MHz
	
}
