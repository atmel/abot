/**
 * \file drv_256RFR2_switch_Abot.c
 *
 * \brief The ABOT switch driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

//Abot common includes
#include "drv_256RFR2_switch_ABOT.h"

/*! \brief Initialization of Abot switch interrupt
 *
 *  PE7 - INT7 is connected to the Distance sensor
 *
 *  PE0 - Pin Change Interrupt8 is connected to the USER Switch
 *
 *  I/O port and interrupt configured 
 */
void switch_init(void)
{
	extern unsigned char ABOT_int_flag;
	extern unsigned char USR_SWITCH_int_flag;
	/*
	PORTE � Port E Data Register 
	If PORTxn is written logic one when the pin is configured as an input pin, the pull-up resistor is
	activated.
	DDRE � Port E Data Direction Register "0" => input
	*/	
	
	DDRE &= !(1<<DDRE7);  //PE7 as input
	PORTE |= (1<<PORTE7); //PE7 pull-up activated
	
	DDRE &= !(1<<DDRE0);  //PE0 as input
	//PORTE |= (1<<PORTE0); //PE0 pull-up activated


	/*
	EICRB � External Interrupt Control Register B
		The level and edges on the external pins that activate the interrupts are defined
		
		Bit 7:6 � ISC71:70 - External Interrupt 7 Sense Control Bit
		= 0x00 => The low level of INTn generates an interrupt request.
		= 0x02 => The falling edge of INTn generates asynchronously an interrupt request.
		= 0x03 =>The rising edge of INTn generates asynchronously an interrupt request.
	*/	
	//EICRB = 0x80;  //INT7 Falling edge
	EICRB |= (1<<ISC71); //ISC71=1
	//EICRB &= !(1<<ISC70); //ISC70=0
	
	ABOT_int_flag=0x00;	// Clear interrupt flag 
	USR_SWITCH_int_flag=0x00;
}

void switch_INT_enable(void){

	/*
	EIMSK � External Interrupt Mask Register
		When an INT7:0 bit is written to one and the I-bit in the Status Register (SREG) is set
		(one), the corresponding external pin interrupt is enabled
	*/
	EIMSK |= (1<<INT7); // INT7 enabled
	
	
	/*
	PCICR � Pin Change Interrupt Control Register
		� Bit 1 � PCIE1 - Pin Change Interrupt Enable 1
		When the PCIE1 bit is set (one) and the I-bit in the Status Register (SREG) is set (one),
		pin change interrupt 1 is enabled. Any change on any enabled PCINT15:8 pin will
		cause an interrupt. The corresponding interrupt of Pin Change Interrupt Request is
		executed from the PCI1 Interrupt Vector. PCINT15:8 pins are enabled individually by
		the PCMSK1 Register. Note that the I/O ports corresponding to PCINT15:9 are not
		implemented.
	*/
	PCICR |= (1<<PCIE1); //Pin Change Interrupt Enable 1
	
	/*
	PCMSK1 � Pin Change Mask Register 1
		Bit PCINT8 selects whether the pin change interrupt is enabled on the corresponding
		I/O pin. If PCINT8 is set and the PCIE1 bit in PCICR is set, the pin change interrupt is
		enabled on the corresponding I/O pin. If PCINT8 is cleared, the pin change interrupt on
		the corresponding I/O pin is disabled.
	*/
	PCMSK1 |= (1<<PCINT8); //PCINT8 enabled
	
}

/*
When an edge or logic change on the INT7:0 pin triggers an interrupt request, INTF7:0
becomes set (one). If the I-bit in SREG and the corresponding interrupt enable bit
INT7:0 in EIMSK are set (one), the MCU will jump to the interrupt vector. The flag is
cleared when the interrupt routine is executed.
*/
ISR(INT7_vect)
{
	extern unsigned char ABOT_int_flag;

	ABOT_int_flag=0x01;	//Interrupt from Switch received

}


/*
When an edge or logic change on the INT7:0 pin triggers an interrupt request, INTF7:0
becomes set (one). If the I-bit in SREG and the corresponding interrupt enable bit
INT7:0 in EIMSK are set (one), the MCU will jump to the interrupt vector. The flag is
cleared when the interrupt routine is executed.
*/
ISR(PCINT1_vect)
{
	extern unsigned char USR_SWITCH_int_flag;

	USR_SWITCH_int_flag=0x01;	//Interrupt from Switch received

}