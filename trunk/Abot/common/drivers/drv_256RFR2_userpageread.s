
/*
 * userpageread.s
 *
 * Created: 01.03.2013 13:44:45
 *  Author: Are.Halvorsen
 */ 

 .global read_user_page

read_user_page:	
	PUSH	R30	
	PUSH	R31
	PUSH	R19	
	MOVW	R30,R24  // Load address variable into Z-pointer.
	LDI		R19,0x21	 // Load (1<<SIGRD) | (1<<SPMEN) to R19
	OUT		0x37,R19 // Write to SPMCSR
	LPM		R24,Z	 // Read from UserPage
	POP		R19
	POP		R31
	POP		R30
	ret