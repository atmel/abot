/*
 * drv_RF233_XZigBit.c
 *
 * Created: 02.10.2013 10:17:44
 *  Author: tbjoernvold
 */ 


/*
 * drv_RF230.c
 *
 * Created: 25/02/2012 15:44:24
 *  Author: TB
 */ 

#include <avr/io.h>
#include <avr/interrupt.h> 
#include "xmega_gpio/xmega_gpio.h"
//#include "xmega_rf233_zigbit/xmega_rf233_zigbit.h"

#include "drv_RF233_XZigBit.h"

/* RF233 IRQ interrupt connected to PC2, TIMER1 CAPT vector 17

ICFn is automatically cleared when the Input Capture Interrupt Vector is executed. 

*/
ISR(PORTC_INT0_vect)
{
	extern unsigned char RF_int_flag;

		RF_int_flag = 0x01;	
}

// TST = PB7 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_low(void)
{
	// TST = 0
}	

// TST = PB7 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void)
{
	 // TST = 1
}	

// RST = PC0 (Chip reset; active low)
void rf230_RST_low(void)
{
	gpio_set_pin_low(AT86RFX_RST_PIN); // RST = 0
}	

// RST = PC0 (Chip reset; active low)
void rf230_RST_high(void)
{
	 gpio_set_pin_high(AT86RFX_RST_PIN); // RST = 1
}

// SLP_TR= PC3 (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_low(void)
{ 
	gpio_set_pin_low(AT86RFX_SLP_PIN);  // SLP_TR = 0
}	

// SLP_TR= PC3 (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_high(void)
{
	gpio_set_pin_high(AT86RFX_SLP_PIN); // SLP_TR = 1
}


/*
TST		= NA (Enables Continuous Transmission Test Mode; active high)
RST		= PC0 (Chip reset; active low)
SLP_TR	= PC3 (Controls sleep, transmit start and receive states; active high)
IRQ		= PC2, 

A hardware reset is initiated by pulling the RST line low. 6 ?s later the same line is pulled high again and the hardware reset finished. 
All registers will have their default values and the state machine will be in P_ON or TRX_OFF state.
*/
void rf230_drv_init(void)
{
		
	/* Set SLP_TR, RST, and TST as output */
	
	/* Initialize TRX_RST and SLP_TR as GPIO. */
	ioport_configure_pin(AT86RFX_RST_PIN, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
	ioport_configure_pin(AT86RFX_SLP_PIN, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
	
	
	AT86RFX_INTC_INIT();
	ENABLE_TRX_IRQ();
}

