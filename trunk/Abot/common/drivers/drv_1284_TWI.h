/**
 * \file drv_1284_TWI.h
 *
 * \brief mega1284 TWI driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef drv_1284_TWI_H_
#define drv_1284_TWI_H_

/*
TWI application dependent constants
*/
#define TWI_MAX_DATA		10			//!< maximum number of bytes to transmit or receive

//uint8_t TWI_data_buff[TWI_MAX_DATA];	//!< TWI data buffer, /0 charcter included


// TWI status codes which indicate an erroneous situation
#define TWI_STATUS_BUFF_OVERFLOW	(0x01)	//Number of received or transmitted bytes higher than TWI_MAX_DATA - using lowest status bit not used by TWI

//Status codes for Master Transmitter Mode
//0x20 SLA+W has been transmitted; NOT ACK has been received
//0x30 Data byte has been transmitted; NOT ACK has been received
//0x38 Arbitration lost in SLA+W or data bytes

//Status codes for Master Receiver Mode
//0x38 Arbitration lost in SLA+R or NOT ACK bit
//0x48 SLA+R has been transmitted; NOT ACK has been received
//0x58 Data byte has been received; NOT ACK has been returned

/** \brief initialize the TWI as master
 *
 *  \param speed        The desired TWI bus speed STANDARD or FAST.
 *  \param cpu_clk_frq  The CPU clock frequency.
 *
 */
void TWI_master_init(uint32_t speed, uint32_t cpu_clk_frq);


/*! \brief TWI master receive data
 * 
 * \param slave_address		 address of Slave to transmit data to
 * \param no_of_byte		 number of bytes to receive from slave
 * \param TWI_data_received  Pointer to where data is located
 *
 * \return  status			0x00 if receive ok - otherwise  TWI erroneous status code
 */
uint8_t TWI_master_receive(uint8_t slave_address, uint8_t no_of_byte, uint8_t *TWI_data_received);

/*! \brief TWI master transmit data
 *
 * \param slave_address		address of Slave to transmit data to
 * \param data_to_transmit  Pointer to where data is located, NULL terminated
 * \param end_with_STOP		Transmit STOP at end if ='Y'
 *
 * \return  status			0x00 if transmit ok - otherwise  TWI erroneous status code
 */
uint8_t TWI_master_transmit(uint8_t slave_address, uint8_t no_of_byte, uint8_t *data_to_transmit, char end_with_STOP);


#endif /* TWI_1284_TB_H_ */