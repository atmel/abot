
/**
 * \file drv_256RFR2_RF.h
 *
 * \brief RF230 driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef DRV_256RFR2_RF_H_
#define DRV_256RFR2_RF_H_

//Abot common includes
#include <services/RADIO_ABOT_cmd.h>

// RF configuration

#define RF_CCA_MODE                 0x02 //CCA Mode 2, Carrier sense only, register PHY_CC_CCA

//Transceiver states
/*
0x00 P_ON
0x01 BUSY_RX
0x02 BUSY_TX
0x06 RX_ON
0x08 TRX_OFF (Clock State)
0x09 PLL_ON (TX_ON)
0x0F SLEEP
0x11 BUSY_RX_AACK
0x12 BUSY_TX_ARET
0x16 RX_AACK_ON
0x19 TX_ARET_ON
0x1C RX_ON_NOCLK
0x1D RX_AACK_ON_NOCLK
0x1E BUSY_RX_AACK_NOCLK
0x1F STATE_TRANSITION_IN_PROGRESS */

#define RF_RX_ON_STATE       0x06
#define RF_PLL_ON_STATE      0x09
#define RF_RX_AACK_ON_STATE  0x16


//State Control Commands, Register Bits TRX_CMD
#define RF230_TRX_CMD_TX_START		(0x02)
#define RF230_TRX_CMD_FORCE_TRX_OFF	(0x03)
#define RF230_TRX_CMD_RX_ON			(0x06)
#define RF230_TRX_CMD_TRX_OFF		(0x08)
#define RF230_TRX_CMD_PLL_ON		(0x09)
#define RF230_TRX_CMD_RX_AACK_ON	(0x16)
#define RF230_TRX_CMD_TX_ARET_ON	(0x19)


/* RF230 API */
void rf230_TST_low(void);     // (Diables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void);    // (Enables Continuous Transmission Test Mode; active high)
void rf230_RST_low(void);     // (Chip reset; active low)
void rf230_RST_high(void);    // (Chip reset; active low)
void rf230_SLP_TR_low(void);  // (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_high(void); // (Controls sleep, transmit start and receive states; active high)
void RF_init(void);       // RF230 init

unsigned char RX_enable_drv(void);

void drv_set_RF_output_pwr(char pwr);
void drv_set_RF_channel(unsigned char channel);
void drv_set_RF_PAN_ID(uint16_t panid);
void drv_set_RF_SHORT(uint16_t shortad);

void RADIO_tx_frame(RADIO_frame Rf);
RADIO_frame drv_get_RADIO_rx_frame(void);

unsigned char drv_get_IRQ_status(void);
unsigned char drv_get_RADIO_state(void);
unsigned char drv_get_RADIO_RSSI(void);
unsigned char drv_get_RF_channel(void);
unsigned char drv_get_RF_output_pwr(void);
unsigned char drv_get_RADIO_TX_status(void);
uint16_t drv_get_RADIO_SHORT_filter(void);
uint16_t drv_get_RADIO_PANID_filter(void);
uint16_t drv_get_MAC_SHORT(void);
board_data drv_get_board_id(void);
#endif /* DRV_RF230_RAVEN_H_ */