/**
 * \file drv_256RFR2_RF.c
 *
 * \brief RF230 driver
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h> 
#include <stdio.h>

//Abot common includes
#include "drv_256RFR2_RF.h"

//Project includes
#include <drivers/drv_256RFR2_userpage.h>

/*! \brief RF Interrupt service routine 
 *
 * A pending IRQ is cleared automatically if an Interrupt service routine is called.
 */
ISR(TRX24_PLL_UNLOCK_vect)
{
	extern unsigned char RF_int_flag; //General RF IRQ

	RF_int_flag=0x01;	
}

/*! \brief RF Interrupt service routine 
 *
 * A pending IRQ is cleared automatically if an Interrupt service routine is called.
 */
ISR(TRX24_TX_END_vect )
{
	extern unsigned char RF_TX_END_int_flag; // Indicates the completion of a frame transmission

	RF_TX_END_int_flag=0x01;	
}

/*! \brief RF Interrupt service routine 
 *
 * A pending IRQ is cleared automatically if an Interrupt service routine is called.
 */
ISR(TRX24_RX_END_vect )
{
	extern unsigned char RF_RX_END_int_flag; // Indicates the completion of a frame reception

	RF_RX_END_int_flag=0x01;	
}


/*
Transceiver Pin Register TRXPR on page 191
The Transceiver Pin Register TRXPR is located in the Controller clock domain and is
accessible even if the transceiver is in sleep state. This register provides access to the
pin functionality, known from the Atmel standalone transceiver devices (two chip
solution).
*/

// TST = PB0 (Enables Continuous Transmission Test Mode; active high)

void rf230_TST_low(void)
{
	//TRXPR &= ~(1<<TRXTST); // TST = 0
}	

// TST = PB0 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void)
{
	//TRXPR|= (1<<TRXTST); // TST = 1
}	

// RST = PB1 (Chip reset; active low)
//The register (TRXRST) can be used to reset the transceiver without resetting the
//controller. After the reset bit was set, it is cleared immediately

/*! \brief set RST low
Bit 0 � TRXRST - Force Transceiver Reset
The RESET state is used to set back the state machine and to reset all registers of the
transceiver (except IRQ_MASK) to their default values. A reset forces the radio
transceiver into the TRX_OFF state and resets all transceiver register to their default
values. A reset is initiated with bit TRXRST = H. The bit is cleared automatically. During
transceiver reset the microcontroller has to set the radio transceiver control bit SLPTR
to the default value. = L ??
 */
void rf230_RST_low(void)
{
	TRXPR &= ~(1<<TRXRST); // RST = 0
}	

/*! \brief set RST high  Force Transceiver Reset
*/
void rf230_RST_high(void)
{
	TRXPR|= (1<<TRXRST); // RST = 1
}


/*! \brief set SLPTR low
 * Controls sleep, transmit start and receive states; active high
 * A second configuration bit (SLPTR) is used to control frame transmission or sleep and
 * wakeup of the transceiver. This bit is not cleared automatically.
 * The function of the SLPTR bit relates to the current state of the transceiver module and
 * is summarized in Table 9-1
 *
 */
void rf230_SLP_TR_low(void)
{
	TRXPR &= ~(1<<SLPTR); // SLP_TR = 0
}	

/*! \brief set SLPTR high
 * Controls sleep, transmit start and receive states; active high
 * A second configuration bit (SLPTR) is used to control frame transmission or sleep and
 * wakeup of the transceiver. This bit is not cleared automatically.
 * The function of the SLPTR bit relates to the current state of the transceiver module and
 * is summarized in Table 9-1
 *
 */
void rf230_SLP_TR_high(void)
{
	TRXPR|= (1 << SLPTR); // SLP_TR = 1
}




/*! \brief Set RF output power
 *
 The  RF output power can be set via register bits TX_PWR in register 0x05 
(PHY_TX_PWR). The maximum output power of the transmitter is typical +3 dBm. The 
selectable output power range of the transmitter is 20 dB.

The PHY_TX_PWR register sets the transmit power and controls the FCS algorithm for 
TX operation. 

The register bits TX_PWR sets the TX output power of the AT86RF230. The available 
power settings are summarized in Table 9-1.
0x0 = +3dBm
0xF = -17.2dBm

Bit [7:4] � Reserved 
Bit [3:0] � TX_PWR 

 *
 * \param  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */

void drv_set_RF_output_pwr(char pwr)
{
	PHY_TX_PWR =  pwr;
}


/*! \brief RF Channel Selection 
 *
The PLL is designed to support 16 channels in the IEEE 802.15.4 � 2.4 GHz band with 
a channel spacing of 5 MHz. The center frequency FCH of these channels is defined as 
follows: 
FCH = 2405 + 5 (k � 11) [MHz], for k = 11, 12, ..., 26 
where k is the channel number. 
The channel k is selected by register bits CHANNEL (register 0x08, PHY_CC_CA).

Both calibration routines are initiated  automatically when the PLL is turned on. 
Additionally, the center frequency calibration is running when the PLL is programmed to 
a different channel (register bits CHANNEL in register 0x08).

The PHY_CC_CCA register contains register bits to initiate and control the CCA 
measurement as well as to set the channel center frequency.

Bit 7 � CCA_REQUEST 
Refer to section 8.6.4.
A manual CCA measurement is initiated with setting CCA_REQUEST = 1
Clear Channel Assessment (CCA) 
Note, it is not recommended to initiate manually a CCA measurement when using the Extended Operating Mode
�  Bit [6:5] � CCA_MODE 
Refer to section 8.6.4. 
The CCA mode can be selected using register bits CCA_MODE
�  Bit [4:0] � CHANNEL 
The register bits CHANNEL define the RX/TX channel. The channel assignment is 
according to IEEE 802.15.

0x0B => channel 11
0x1A => channel 26

 * \param  channel     The IEEE 802.15.4 channel. 0x0B => channel 11, 0x1A => channel 26
 *
 */
void drv_set_RF_channel(unsigned char channel)
{
	//channel = channel | (RF_CCA_MODE << 5);  // bit 5 & 6 configures CCA mode
	//channel = (channel | 0x40); 
	//PHY_CC_CCA = channel;
	PHY_CC_CCA = 0x4E;
}


/*! \brief Set PAN ID
 *
 * The 16 bit identifier of the PAN on which the device is operating. If
 * this value is 0xffff, the device is not associated.
 *
 * \param  panid     The IEEE 802.15.4 PAN ID (network id)
 *
 */
void drv_set_RF_PAN_ID(uint16_t panid)
{
	unsigned char t;
	
	/* Register 0x22 (PAN_ID_0)  
	This register contains bits [7:0] of the 16 bit PAN ID for address filtering. */
	t = (char)panid;
	PAN_ID_0 = t;
	
	/* Register 0x23 (PAN_ID_1)  
	This register contains bits [15:8] of the 16 bit PAN ID for address filtering. */
	t = (panid >> 8);
	PAN_ID_1 = t;
}

/*! \brief Set board (my) SHORT address
 *
 * The 16 bit address that the device uses to communicate in the PAN.
 *
 * If the device is a PAN coordinator, this value shall be chosen before a PAN is started. 
 * Otherwise, the address is allocated by a coordinator during association.
 *
 * A value of 0xfffe indicates that the device has associated but has not
 * been allocated an address. 
 *
 * \param  shortad     The IEEE 802.15.4 short address (device id in PAN)
 *
 */
void drv_set_RF_SHORT(uint16_t shortad)
{
	unsigned char t;

		/* Register 0x22 (PAN_ID_0)  
		This register contains bits [7:0] of the 16 bit PAN ID for address filtering. */
		//t = shortad & 0x0F;
		t = (char)shortad;
		SHORT_ADDR_0 = t;
	
		/* Register 0x23 (PAN_ID_1)  
		This register contains bits [15:8] of the 16 bit PAN ID for address filtering. */
		t = (shortad >> 8);
		SHORT_ADDR_1 = t;
}


/* ====================== STATE CHANGE COMMANDS ============================================================== */


/*
Entering the PLL_ON state from TRX_OFF state enables the analog voltage regulator 
(AVREG) first. After the voltage regulator has been settled, the PLL frequency 
synthesizer is enabled. When the PLL has been settled at the receive frequency, a 
successful PLL lock is indicated by issuing a PLL_LOCK interrupt. 
If an RX_ON command is issued in PLL_ON state, the receiver is immediately enabled. 
If the PLL has not been settled before, actual frame reception can only happen once the 
PLL has locked. 
The PLL_ON state corresponds to the TX_ON state in IEEE 802.15.4. 

Return 0x01 if ok
0x02 if did not enter PLL_ON state
0x03 if PLL did not lock
*/
char enter_PLL_ON_state(void)
{
	char status;

	/*If TRX_STATUS = 0x1F (STATE_TRANSITION_IN_PROGRESS) the AT86RF230 is on a state transition. 
	Do not try to initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS.*/
	status = TRX_STATUS & 0x1F;  // bit 4:0
	while (status== 0x1F)
	{
		status = TRX_STATUS & 0x1F;
	}
		
	//Issue PLL_ON cmd
	TRX_STATE = RF230_TRX_CMD_PLL_ON;
	
	//A successful state transition shall be confirmed by reading the radio transceiver status from register TRX_STATUS
	status = TRX_STATUS & 0x1F;  // bit 4:0
	while (status== 0x1F)
	{
		status = TRX_STATUS & 0x1F;
	}
	if (status!= 0x09) 
	{
		return 0x02; // PLL_ON = 0x09, did not enter state
	}
	else return 0x01; // state change ok	
}

/*! \brief Change state from PLL_ON to RX_AACK_ON
 *
   
In the RX_AACK_ON state, the radio transceiver listens for incoming frames. After 
detecting a frame start (SFD), the radio transceiver state changes to BUSY_RX_AACK 
(register 0x01) and the TRAC_STATUS bits (register 0x02) are set to INVALID. The 
AT86RF230 starts to parse the MAC header (MHR) and a filtering procedure as 
described in IEEE 802.15.4-2003 section 7.5.6.2. (third level filter rules) is applied 
accordingly. 
 
The state RX_AACK_ON is entered by writing the command RX_AACK_ON to the 
register bits TRX_CMD in register 0x02 (TRX_STATE). The state change shall be 
confirmed by reading register 0x01 (TRX_STATUS) that changes to RX_AACK_ON or 
BUSY_RX_AACK on success. 

 * \return  0x01 if state change ok
 */
char from_PLL_ON_to_RX_AACK_ON_state (void)
{
	char status;
	/*If TRX_STATUS = 0x1F (STATE_TRANSITION_IN_PROGRESS) the AT86RF230 is on a state transition. 
		Do not try to initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS.*/
		status = TRX_STATUS & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = TRX_STATUS & 0x1F;
		}
		
		//Issue RX_AACK_ON cmd
		TRX_STATE = RF230_TRX_CMD_RX_AACK_ON;
		//_delay_us(1); // PLL_ON to RX_AACK_ON state delay
		
		//A successful state transition shall be confirmed by reading the radio transceiver status from register 0x01 (TRX_STATUS), 
		status = TRX_STATUS & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = TRX_STATUS & 0x1F;
		}
		if (status== 0x16) 
		{
			return 0x01; // RX_AACK_ON state ok
		}
		else
		{
			 return 0x00; //did not enter RX_AACK_ON state	 			
		}		

}	


/*! \brief Enter RX state, i.e. ready to receive 
 *
 * This routine enters the mode enabling the radio to listen for 
 * incoming frames
 * 
 * \return  error code, 0x01 if OK
 */
unsigned char RX_enable_drv(void)
{
	unsigned char error = 0x01;
	
		error = enter_PLL_ON_state(); // Enter PLL_ON state
		error = from_PLL_ON_to_RX_AACK_ON_state(); //Enter RX_AACK_ON
	return error;
}

/* ====================== END STATE CHANGE COMMANDS ============================================================== */


/*
BUSY_TX � Transmit State 
A transmission can only be started in state PLL_ON. There are two ways to start a 
transmission: 
� Rising edge of SLP_TR 
� TX_START command to register 0x02 (TRX_STATE). 
Either of these causes the AT86RF230 to enter the BUSY_TX state. 
During the transition to BUSY_TX state, the PLL frequency shifts to the transmit 
frequency. Transmission of the first data chip of the preamble starts after 16 �s to allow 
PLL settling and PA ramping, see Figure 7-2. After transmission of the preamble and 
the SFD, the Frame Buffer content is transmitted. 
The last two bytes to be transmitted are the FCS (see Figure 8-2). The radio transceiver 
can  be  configured to autonomously compute the FCS bytes and append it to the 
transmit data. The register bit TX_AUTO_CRC_ON in register 0x05 (PHY_TX_PWR) 
needs to be set to 1 to enable this feature. For further details refer to section 8.2.When 
the frame transmission is completed, the radio transceiver automatically turns off the 
power amplifier, generates a TRX_END interrupt and returns to PLL_ON state. 
Note that in case the PHR indicates a frame length of zero, the transmission is aborted.

The SHR (except the SFD used to generate the last octet of the SHR) can generally not
be read by the application software.
The PHR and the PSDU need to be stored in the Frame Buffer for frame transmission.
The PHR byte is the first byte in the Frame Buffer (address 0x180) and must be
calculated based on the PHR and the PSDU. The maximum frame size supported by
the radio transceiver is 128 bytes. If the TX_AUTO_CRC_ON bit is set in register
PHY_TX_PWR, the FCS field of the PSDU is replaced by the automatically calculated
FCS during frame transmission. There is no need to write the FCS field when using the
automatic FCS generation.


*/
void RADIO_tx_frame(RADIO_frame Rf)
{
  extern uint16_t MAC_PAN_ID; // current MAC PAN ID
  extern uint16_t MAC_PAN_ID_dest; // destination net MAC PAN ID
  extern uint16_t MAC_SHORT; // current MAC short address
  extern uint16_t MAC_SHORT_dest; // MAC short address destination
  extern unsigned char MAC_seq_no; //MAC header sequence number
  extern unsigned char ABOT_address;// This Abot Address
  extern unsigned char ABOT_destination; //Abot LAN destination address

 // char debugs[0x50];
  unsigned char c;
  uint8_t *reg_adr;
  uint8_t t;
  uint16_t a;
  unsigned char MPDU[MAX_PAYLOAD+14]; //MAC protocol data unit, MAC header = 12byte with short address
  
	
		//Add MAC Header (MHR)
		//FCS added automatically when PHY_TX_PWR.TX_AUTO_CRC_ON sub register is set to 1. Which is default
		
		// Generate MAC Frame Control field and add to header
		MPDU[0] = 0x01;  // Intra PAN disabled,   No ACK,  001 Data Frame type(001)
		if ((MAC_SHORT_dest != 0xFFFF) && (MAC_SHORT_dest != 0x422A))
		{
			//MPDU[0] |= 0x21; // Intra PAN disabled,  ACK,  001 Data Frame type(001)
			MPDU[0] |= 0x01; // Intra PAN disabled,   no ACK,  001 Data Frame type(001)
		}
		MPDU[1] = 0x88;  // 1000 1000 Addressing mode 16-bit short
	
		
		//Generate MAC sequence number and add to header
		if (MAC_seq_no<0xfe) MAC_seq_no++; else MAC_seq_no=0;
		MPDU[2] = MAC_seq_no;
		
		//Destination PAN ID
		MPDU[3] = (unsigned char)MAC_PAN_ID_dest; //low byte
		a = (MAC_PAN_ID_dest >>8); 
		MPDU[4] = (unsigned char)a; // high byte
		
		//Destination address
		MPDU[5] = (unsigned char)MAC_SHORT_dest; //low byte
		a = (MAC_SHORT_dest >>8); 
		MPDU[6] = (unsigned char)a; // high byte
			
		//Source PAN ID
		MPDU[7] = (unsigned char)MAC_PAN_ID; //low byte
		a = (MAC_PAN_ID >>8); 
		MPDU[8] = (unsigned char)a; // high byte
		
		//Source address
		MPDU[9] = (unsigned char)MAC_SHORT; //low byte
		a = (MAC_SHORT >>8); 
		MPDU[10] = (unsigned char)a; // high byte
		
			
		//Set header size for use when adding payload
		c = 11;
		// MAC Header Footer (MFR) added automatically
	
	// add ABOT source and destination
	MPDU[c] = ABOT_destination; //destination address
	c++;
	MPDU[c] = ABOT_address; //ABOT source address
	c++;
	 
	// Set PHY header frame length PHR
	Rf.PHR = Rf.PHR + c;
	for (t=c; t< Rf.PHR; t++) // add payload
	{
		 MPDU[t] = Rf.PSDU[t-c]; 
	}
	
  
	// Change state from RX to PLL_ON, i.e disable receive
	c=enter_PLL_ON_state(); 
	
	if (c==0x01) //PLL_ON state entered ok
	{
	
	//USART_printf_trm("PLL_ON state entered ok");
    // Write to Frame buffer
	// The first byte of the Frame Buffer can be accessed with the symbolical address TRXFBST
		
		TRXFBST = Rf.PHR+2;  // Write Frame length, number of PSDU (payload data) incl. FCS (2byte)
		reg_adr = &TRXFBST;
		for (t=0; t< Rf.PHR; t++)
		{
			reg_adr++;
			*reg_adr = MPDU[t];  // Write Frame PSDU (payload data) to Frame buffer
			//sprintf(debugs,"MPDU[0x%X]= 0x%X",t,MPDU[t]);
			//USART_printf_trm(debugs);
		}
			

		// Transmit frame

		TRX_STATE = RF230_TRX_CMD_TX_ARET_ON; //Enter TX_ARET_ON state
		
		/*The register bits TRX_STATUS signal the current radio transceiver status. Do not try to
		initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS state.*/
		c = TRX_STATUS & 0x1F;  // State bit 4:0
		while (c == 0x1F)
		{
			c = TRX_STATUS & 0x1F;
		}
		//if (c == 0x19) USART_printf_trm("TX_ARET_ON state entered");
		//USART_printf_trm("TX_START to be issued");
		TRX_STATE = RF230_TRX_CMD_TX_START; //Enter BUSY_TX_ARET state, issue TX_START cmd,
		
		
	}//if (c==0x01) //PLL_ON state entered ok
	
	// Change state to RX done in main when transmit interrupt received 
	
}

RADIO_frame drv_get_RADIO_rx_frame(void)
{
	RADIO_frame Rf;
	uint8_t *reg_adr;
	uint16_t u;
	unsigned char c,t;
	unsigned char MPDU[MAX_PAYLOAD+23]; //MAC protocol data unit, max header = 23byte
	extern unsigned char ABOT_address;// This Abot Address


	//On received frames, the frame length byte is not stored in the Frame Buffer, but can be  accessed over the TST_FRAME_LENGTH register.
		Rf.PHR = TST_RX_LENGTH;// Frame length, number of PSDU (payload data)
		reg_adr = &TRXFBST;
		for (t=0; t< Rf.PHR; t++)
		{
			MPDU[t] = *reg_adr; //Frame PSDU (payload data) The Transceiver has removed the Frame control field first byte = Sequence number
			reg_adr++;
		}
		
		//During frame receive, the Link Quality Indication (LQI) value is appended to the frame data in the Frame Buffer.
		//reg_adr++; 
		Rf.LQI = *reg_adr; // Link Quality Indication (LQI)


		// get source address from received frame
		Rf. SHORT_source_adr = MPDU[9];
		u = MPDU[10];
		u = u<<8;
		Rf. SHORT_source_adr = Rf. SHORT_source_adr | u;

		//Remove MAC Header (MHR) - 11 byte 
		c=11;
		// MAC Header Footer (MFR) removed automatically by Transceiver
		
		// decode and remove ABOT LAN address information
		if ((MPDU[c] == ABOT_address) || (MPDU[c] == 0x2A)||(MPDU[c] == 0xFF))  // For me or broadcast?
		{
			Rf.ForMe = 0x01; //TRUE
		}else
		{
			Rf.ForMe=0;// Not for me
		}
		c++; //next byte is source
		Rf.source_adr = MPDU[c]; // ABOT source address
		c++; //next byte is CMD
		for (t=c; t< Rf.PHR; t++) // extract Abot cmd + payload
		{
			Rf.PSDU[t-c] = MPDU[t];
		}
	
	return(Rf);
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------


/*! \brief Get IRQ status 

	This register contains the status of the pending interrupt requests. An interrupt is
	pending if the associated bit has a value of one. Such a pending interrupts can be
	manually cleared by writing a 1 to that register bit. Interrupts are automatically cleared
	when the corresponding interrupt service routine is being executed.
	� Bit 7 � AWAKE - Awake Interrupt Status
	� Bit 6 � TX_END - TX_END Interrupt Status
	� Bit 5 � AMI - Address Match Interrupt Status
	� Bit 4 � CCA_ED_DONE - End of ED Measurement Interrupt Status
	� Bit 3 � RX_END - RX_END Interrupt Status
	� Bit 2 � RX_START - RX_START Interrupt Status
	� Bit 1 � PLL_UNLOCK - PLL Unlock Interrupt Status
	� Bit 0 � PLL_LOCK - PLL Lock Interrupt Status

* \return  pending interrupt requests  
 *
 */
unsigned char drv_get_IRQ_status(void)
{
	unsigned char irq_status;
	
	irq_status = IRQ_STATUS;
	
	return irq_status;
}

/*reading the radio transceiver state from register 0x01 (TRX_STATUS), bit 4:0
0x00 P_ON 
0x01 BUSY_RX 
0x02 BUSY_TX 
0x06 RX_ON 
0x08 TRX_OFF (Clock State) 
0x09 PLL_ON (TX_ON) 
0x0F SLEEP 
0x11 BUSY_RX_AACK 
0x12 BUSY_TX_ARET 
0x16 RX_AACK_ON 
0x19 TX_ARET_ON 
0x1C RX_ON_NOCLK 
0x1D RX_AACK_ON_NOCLK 
0x1E BUSY_RX_AACK_NOCLK 
0x1F STATE_TRANSITION_IN_PROGRESS */
unsigned char drv_get_RADIO_state(void)
{
	unsigned char state;
	
	state = TRX_STATUS & 0x1F;
	
	return state;
}

/*! \brief Get RSSI 

	The PHY_RSSI register is a multi purpose register to indicate the current received 
signal strength (RSSI) and the FCS validity of a received frame.
Bit [4:0] � RSSI 
The register bits RSSI contain the result of the automated RSSI measurement. The 
value is updated every 2 ?s in receive states. 
The read value is a number between 0 and 28 indicating the received signal strength as 
a linear curve on a logarithmic input power scale (dBm) with a resolution of 3 dB. An 
RSSI value of 0 indicates an RF input power of < -91 dBm (see parameter 11.7.17), a 
value of 28 a power of ? -10 dBm (see parameter 11.7.18).
	
* \return  received signal strength in dBm  
 *
 */
unsigned char drv_get_RADIO_RSSI(void)
{
	unsigned char rssi;
	
	rssi = PHY_RSSI & 0x1F;
	
	return rssi;
}


/*! \brief Get RF Channel 
 *
The PLL is designed to support 16 channels in the IEEE 802.15.4 � 2.4 GHz band with 
a channel spacing of 5 MHz. The center frequency FCH of these channels is defined as 
follows: 
FCH = 2405 + 5 (k � 11) [MHz], for k = 11, 12, ..., 26 
where k is the channel number. 
The channel k is selected by register bits CHANNEL (register 0x08, PHY_CC_CA).

Both calibration routines are initiated  automatically when the PLL is turned on. 
Additionally, the center frequency calibration is running when the PLL is programmed to 
a different channel (register bits CHANNEL in register 0x08).

The PHY_CC_CCA register contains register bits to initiate and control the CCA 
measurement as well as to set the channel center frequency.

Bit 7 � CCA_REQUEST 
Refer to section 8.6.4.
A manual CCA measurement is initiated with setting CCA_REQUEST = 1
Clear Channel Assessment (CCA) 
Note, it is not recommended to initiate manually a CCA measurement when using the Extended Operating Mode
�  Bit [6:5] � CCA_MODE 
Refer to section 8.6.4. 
The CCA mode can be selected using register bits CCA_MODE
�  Bit [4:0] � CHANNEL 
The register bits CHANNEL define the RX/TX channel. The channel assignment is 
according to IEEE 802.15.

0x0B => channel 11
0x1A => channel 26

 * \return  channel     The IEEE 802.15.4 channel. 0x0B => channel 11, 0x1A => channel 26
 *
 */
unsigned char drv_get_RF_channel(void)
{
	unsigned char channel;
	channel = PHY_CC_CCA & 0x1F;
	return channel;
}


/*! \brief Get current RF output power
 *
 The  RF output power can be set via register bits TX_PWR in register 0x05 
(PHY_TX_PWR). The maximum output power of the transmitter is typical +3 dBm. The 
selectable output power range of the transmitter is 20 dB.

The PHY_TX_PWR register sets the transmit power and controls the FCS algorithm for 
TX operation. 

The register bits TX_PWR sets the TX output power of the AT86RF230. The available 
power settings are summarized in Table 9-1.
0x0 = +3dBm
0xF = -17.2dBm

Bit 7 � TX_AUTO_CRC_ON Refer to sections 7.2.6 and 8.2. AUTO CRC always on
Bit [6:4] � Reserved 
Bit [3:0] � TX_PWR 

 *
 * \return  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */

unsigned char drv_get_RF_output_pwr(void)
{
	unsigned char pwr;

	pwr = PHY_TX_PWR & 0x0F;
	return pwr;
}

/*! \brief read status of last transmission 
 *
 * Bit [7:5] � TRAC_STATUS 
 * The status of the TX_ARET algorithm is indicated by register bits TRAC_STATUS. 
 *
		0 SUCCESS 
		1 SUCCESS_DATA_PENDING 
		2 SUCCESS_WAIT_FOR_ACK (RX_AACK)
		3 CHANNEL_ACCESS_FAILURE 
		5 NO_ACK 
		7 INVALID 
 * 
 * \return  TRAC_STATUS 
 */
unsigned char drv_get_RADIO_TX_status(void)
{
	unsigned char status;
	
		status = TRX_STATE & 0xE0;
		status = status >> 5;
	return status;
}

/*! \brief address used for filtering messages to unit 
 *
 * Register 0x20 (SHORT_ADDR_0)
 * This register contains bits [7:0] of the 16 bit short address for address filtering
 *
 * Register 0x21 (SHORT_ADDR_1)
 * This register contains bits [15:8] of the 16 bit short address for address filtering.
 * 
 * \return  address used for filtering messages to unit 
 */
uint16_t drv_get_RADIO_SHORT_filter(void)
{
	uint16_t address, t;
	
		address = SHORT_ADDR_0;
		t = SHORT_ADDR_1;
		t=t<<8;
		address = address | t;
	return address;
}

uint16_t drv_get_RADIO_PANID_filter(void)
{
	uint16_t address, t;
	
	address = PAN_ID_0;
	t = PAN_ID_1;
	t=t<<8;
	address = address | t;
	return address;
}

/*! \brief Get preprogrammed MAC address 
 *
 * Retrieve MAC address last two byte from user page
 * 
 * \return  MAC address used for filtering messages to unit 
 */
uint16_t drv_get_MAC_SHORT(void)
{
	uint16_t address;
	board_info boardI;
	
		boardI = userpage();
		address = (uint16_t)boardI.mac_ieee_address_64bit;
		
	return address;
}

/*! \brief Get board data
 *
 * 
 * 
 * \return  board data
 */
board_data drv_get_board_id(void)
{
	board_data boardId;
	board_info boardInfo;
	unsigned char t;
	
		boardInfo = userpage();
		
		for (t = 0; t < 8; t++)
		{
			boardId.board_id[t] = boardInfo.board_id[t];
		}
		for (t = 0; t < 10; t++)
		{
			boardId.board_serial[t] = boardInfo.board_serial[t];
		}
		for (t = 0; t < 30; t++)
		{
			boardId.board_name[t] = boardInfo.board_name[t];
		}
		
		boardId.board_revision = boardInfo.board_revision;
		
	return boardId;
}

/*
TST		= PB0 (Enables Continuous Transmission Test Mode; active high)
RST		= PB1 (Chip reset; active low)
SLP_TR	= PB3 (Controls sleep, transmit start and receive states; active high)
IRQ		= PD6, PCINT30

A hardware reset is initiated by pulling the RST line low. 6 ?s later the same line is pulled high again and the hardware reset finished. 
All registers will have their default values and the state machine will be in P_ON or TRX_OFF state.
*/
void RF_init(void)
{
	extern unsigned char RF_TX_END_int_flag; // Indicates the completion of a frame transmission
	extern unsigned char RF_RX_END_int_flag; // Indicates the completion of a frame reception
	extern unsigned char RF_int_flag; //General RF IRQ 
	extern unsigned char rand_seed; // random number for MAC short address request
	
	
	RF_int_flag = 0;
	RF_RX_END_int_flag = 0;
	RF_TX_END_int_flag = 0;
	
	
/*	The transceiver module differentiates between eight interrupt events. Internally all
pending interrupts are stored in a separate bit of the interrupt status register
(IRQ_STATUS). Each interrupt is enabled by setting the corresponding bit in the
interrupt mask register (IRQ_MASK). If an IRQ is enabled an interrupt service routine
must be defined to handle the IRQ. A pending IRQ is cleared automatically if an
Interrupt service routine is called. It is also possible to handle IRQs manually by polling
the IRQ_STATUS register. If an IRQ occurred, the appropriate IRQ_STATUS register
bit is set. The IRQ can be cleared by writing �1� to the register bit. It is recommended to
clear the corresponding status bit before enabling an interrupt.
*/

	//clear the IRQ status bits
	IRQ_STATUS = 0xFF;
	
	//Enable interrupts
	IRQ_MASK = (1<<RX_END_EN)|(1<<TX_END_EN)|(1<<PLL_UNLOCK_EN);
	//IRQ_MASK = 0x48;
	

/*
The RESET state is used to set back the state machine and to reset all registers of the
transceiver (except IRQ_MASK) to their default values. A reset forces the radio
transceiver into the TRX_OFF state and resets all transceiver register to their default
values. A reset is initiated with bit TRXRST = H. The bit is cleared automatically. During
transceiver reset the microcontroller has to set the radio transceiver control bit SLPTR
to the default value. = L ??
 */

	rf230_SLP_TR_low(); //Disable sleep, transmit start and receive states;
	//_delay_us(6);
	rf230_RST_high(); //Reset the state machine and reset all RF registers;
	
	
	//  set the TX output power of the AT86RF230RF to +3dBm, max 
	drv_set_RF_output_pwr(0x00); 

/*
TRX_CTRL_1 � Transceiver Control Register 1

The TRX_CTRL_1 register is a multi purpose register to control various operating
modes and settings of the radio transceiver.
� Bit 7 � PA_EXT_EN - External PA support enable
This register bit enables pin DIG3 and pin DIG4 to indicate the transmit state of the
radio transceiver. The control of the external RF front-end is disabled when this bit is 0.
Both pins DIG3 and DIG4 are then defined by the register of I/O ports F and G (PORTF,
DDRF, PORTG, DDRG). The control of the external front-end is enabled when this bit is
1. DIG3 and DIG4 then indicate the state of the radio transceiver. Pin DIG3 is high and
pin DIG4 is low in the state TX_BUSY. In all other states pin DIG3 is low and pin DIG4
is high. It is recommended to set PA_EXT_EN=1 only in receive or transmit states to
reduce the power consumption or avoid leakage current of external RF switches or
other building blocks especially during SLEEP state.
� Bit 6 � IRQ_2_EXT_EN - Connect Frame Start IRQ to TC1
When this bit is set to one the capture input of Timer/Counter 1 is connected to the RX
frame start signal and pin DIG2 becomes an output, driving the RX frame start signal.
Antenna Diversity RF switch control (ANT_EXT_SW_EN=1) shall not be used at the
same time, because it shares the same device pin. The function IRQ_2_EXT_EN is
available for alternate frame time stamping using Timer/Counter 1. In general the
preferred method for frame time stamping is using the symbol counter.
� Bit 5 � TX_AUTO_CRC_ON - Enable Automatic CRC Calculation
This register bit controls the automatic FCS generation for TX operations. The
automatic FCS algorithm is performed autonomously by the radio transceiver if register
bit TX_AUTO_CRC_ON=1.
� Bit 4 � PLL_TX_FLT - Enable PLL TX Filter
PLL TX filtering controls the output spectrum of the transmitted signal to decrease
sidelobes. This is required for systems with an external power amplifier. TX filtering
influences the signal quality. The EVM of the transmitted signal slightly degrades.
� Bit 3:0 � Res3:0 - Reserved
*/

TRX_CTRL_1 = 0x20; //Enable Automatic CRC Calculation
	
/*
VREG_CTRL � Voltage Regulator Control and Status Register
This register controls the use of the voltage regulators and indicates their status.
*/
	
VREG_CTRL = 0x00;

/*	
BATMON � Battery Monitor Control and Status Register - defaul used
	
*/


/*
XOSC_CTRL � Crystal Oscillator Control Register	
This register controls the operation of the 16MHz crystal oscillator.

Bit 7:4 � XTAL_MODE3:0 - Crystal Oscillator Operating Mode
These register bits set the operating mode of the 16 MHz crystal oscillator. For normal
operation the default value is set to XTAL_MODE = 0xF after reset. For use with an
external clock source it is recommended to set XTAL_MODE = 0x4.
Bit 3:0 � XTAL_TRIM3:0 - Crystal Oscillator Load Capacitance Trimming
These register bits control two internal capacitance arrays connected to pins XTAL1
and XTAL2. A capacitance value in the range from 0 pF to 4.5 pF is selectable with a
resolution of 0.3 pF.
*/

XOSC_CTRL = 0x40; //external clock source 


	
/*		
Note: The following is important to ensure correct operation of the radio transceiver: 
	1. MAX_FRAME_RETRIES must always equal zero. A frame will not be retried 
	after the CSMA-CA algorithm has failed. This must be handled in software. 
	2. The CSMA_SEED parameter should not be altered from its reset value, since 
	this will corrupt the operation of the random number generator. 

		Register 0x2C (XAH_CTRL)  
		The XAH_CTRL register controls the TX_ARET transaction in the Extended Operating 
		Mode of the radio transceiver.
		
		Bit [7:4] � MAX_FRAME_RETRIES 
		MAX_FRAME_RETRIES specifies the maximum number of frame retransmission in 
		TX_ARET transaction. 
		
		Bit [3:1] � MAX_CSMA_RETRIES 
		MAX_CSMA_RETRIES specifies the maximum number of retries in TX_ARET 
		transaction to repeat the random back-off/CCA procedure before the transaction gets 
		canceled. Even though the valid range of MAX_CSMA_RETRIES is [0, 1 � 5] 
		according to IEEE 802.15.4-2003, the AT86RF230 can be configured up to a maximum 
		value of 7 retries. 
		Bit 0 � Reserved   
*/
	XAH_CTRL_0 = 0x0A;//MAX_CSMA_RETRIES = 5 & MAX_FRAME_RETRIES = 1, SLOTTED_OPERATION disabled 0

//CSMA_SEED_0 � Transceiver CSMA-CA Random Number Generator Seed Register	
		rand_seed = PHY_RSSI; //Random number from the bits RND_VALUE of register PHY_RSSI for later use
		CSMA_SEED_0 = PHY_RSSI; // Random number from the bits RND_VALUE of register PHY_RSSI
		
//CSMA_SEED_1 � Transceiver Acknowledgment Frame Control Register 2		
	//Acknowledge frames independent of frame version number, The frame control field of the MAC header (MHR) contains a frame version subfield.
	// Bit 7:6 � AACK_FVN_MODE1:0 - Acknowledgment Frame Filter Mode ACK enabled
	CSMA_SEED_1 =  0xE0;       //Bit 5 � AACK_SET_PD - Set Frame Pending Sub-field 

        
		/*
		TRX_CTRL_2 � Transceiver Control Register 2
		Default RX_SAFE_MODE disable frame protection
		*/
		
	TRX_STATE = RF230_TRX_CMD_TRX_OFF;	//go to TRX_OFF state
		
}
