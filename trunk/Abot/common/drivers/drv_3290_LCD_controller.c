/**
 * \file drv_3290_LCD_controller.c
 *
 * \brief mega3290 LCD controller driver
 *
 * \author    TB
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>

#include "drv_3290_LCD_controller.h"
	
/*
* Prior to enabling the LCD some initialization must be preformed. The initialization process normally
* consists of setting the frame rate, duty, bias and port mask. LCD contrast is set initially, but
* can also be adjusted during operation.

By keeping the percentage of the time the LCD drivers are turned on at a minimum, the power
consumption of the LCD driver can be minimized. This can be achieved by using the lowest
acceptable frame rate, and using low power waveform if possible. The drive time should be kept
at the lowest setting that achieves satisfactory contrast for a particular display, while allowing
some headroom for production variations between individual LCD drivers and displays. Note
that some of the highest LCD voltage settings may result in high power consumption when VCC
is below 2.0V. The recommended maximum LCD voltage is 2*(VCC - 0.2V).

Low Power Waveform
To reduce toggle activity and hence power consumption a low power waveform can be selected
by writing LCDAB to one. Low power waveform requires two subsequent frames with the same
display data to obtain zero DC voltage. Consequently data latching and Interrupt Flag is only set
every second frame.


* Before a re-initialization is done, the LCD controller/driver should be disabled
*/
void LCD_ctrl_init(void)
{
	/* LCDCRB � LCD Control and Status Register B 
	*  1/3 bias is optimal for LCD displays with four common terminals (1/4 duty)
	*  Use 32kHz crystal oscillator LCDCS=1 
	*  1/3 Bias LCD2B=0
	*  1/4 duty LCDMUX1:0= 11
	*  LCD Port Mask LCDPM3:0 = 1111 (160 segment display, 4 common and 40 segment drivers)
	*/
	LCDCRB = (1<<LCDCS) | (1<<LCDMUX1)| (1<<LCDMUX0)|(1<<LCDPM3)|(1<<LCDPM2)|(1<<LCDPM1)|(1<<LCDPM0);
	
	/* LCDFRR � LCD Frame Rate Register
	When using more than one common pin, the maximum period the LCD drivers can be turned on
	for each voltage transition on the LCD pins is 50% of the prescaled LCD clock period, clkLCD_PS.
	To avoid flickering, it is recommended to keep the framerate above 30Hz, thus giving a maxi-
	mum drive time of approximately 2ms when using 1/2 or 1/4 duty, and approximately 2.7ms
	when using 1/3 duty. To achieve satisfactory contrast, all segments on the LCD display must
	therefore be able to be fully charged/discharged within 2 or 2.7ms, depending on the number of
	common pins.
	
	 
     * Configuring LCD with Extern clock (TOSC, 32.768kHz) and 1/4 duty
     *                      32786 Hz          32786 Hz
     *  frame_rate = ------------------ = ------------- = 32 Hz
     *               8 * .prescl * .div     8 * 16 * 8
     
	 
	 LCD Prescaler Select LCDPS2:0 = 000 (16)
	 LCD Clock Divide 2, 1, and 0 LCDCD2:0 = 111 (8)
	 
	* Frame Rate = Number of times the LCD segments is energized per second */
	LCDFRR = (1<<LCDCD2) | (1<<LCDCD1) | (1<<LCDCD0);
	
	/* LCDCCR � LCD Contrast Control Register
	LCDDC2:0 determine the amount of time the LCD drivers are turned on for each voltage
	transition on segment and common pins. A short drive time will lead to lower power
	consumption, but displays with high internal resistance may need longer drive time to achieve
	satisfactory contrast.
	
	 LDC Display Configuration LCDDC2:0 = 011 (450 ?s) Nominal drive time
	 LCD Contrast Control LCDCC3:0 = 1110 (3.3 V) determine the maximum voltage VLCD on segment and common pins
	*/
	LCDCCR = (1<<LCDDC1) | (1<<LCDDC0) | (1<<LCDCC3) | (1<<LCDCC2) | (1<<LCDCC1);
	
	/* Enable LCD, default waveform and no interrupt enabled */
	LCDCRA = (1<<LCDEN);
}

/*
Disabling the LCD
In some application it may be necessary to disable the LCD. This is the case if the MCU enters
Power-down mode where no clock source is present.
The LCD should be completely discharged before being disabled. No DC voltage should be left
across any segment. The best way to achieve this is to use the LCD Blanking feature that drives
all segment pins and common pins to GND.
When the LCD is disabled, port function is activated again. Therefore, the user must check that
port pins connected to a LCD terminal are either tri-state or output low (sink).
*/
void LCD_ctrl_disable(void)
{
	/* Wait until a new frame is started. */
	while ( !(LCDCRA & (1<<LCDIF)) )
	;
	/* Set LCD Blanking and clear interrupt flag */
	/* by writing a logical one to the flag. */
	LCDCRA = (1<<LCDEN)|(1<<LCDIF)|(1<<LCDBL);
	
	/* Wait until LCD Blanking is effective. */
	while ( !(LCDCRA & (1<<LCDIF)) )
	;
	/* Disable LCD */
	LCDCRA = (0<<LCDEN);
}
