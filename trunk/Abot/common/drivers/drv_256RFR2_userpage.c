/*
 * userpage.c
 *
 * Created: 01.03.2013 13:43:33
 *  Author: Are.Halvorsen
 */ 


#include <avr/io.h>
#include "drv_256RFR2_userpage.h"

extern uint8_t read_user_page(uint16_t);

board_info userpage(void)
{
	
	board_info board;
	uint8_t *board_p = (uint8_t *) &board;
	static uint8_t i;
	
	/*
	NEMCR � Flash Extended-Mode Control-Register
	
	Bit 6 � ENEAM - Enable Extended Address Mode for Extra Rows
	When active high, the extended address mode of the extra rows is enabled. The
	address is decoded from bits AEAM1:0 of this register.
	
	Bit 5:4 � AEAM1:0 - Address for Extended Address Mode of Extra Rows
	These bits are only used when bit ENEAM of this register is set high. Then AEAM1:0
	are used to decode the addresses of the extra rows. A value of 0 decodes the default
	factory row that is also accessible when the extended address mode is deactivated.
	*/
	NEMCR = 0x50; // bit 6,5,4 = 101, User Row 1
	
	for( i = 0; i<75; i++){
		board_p[i] = read_user_page(0x100+i);
	}
	// Reset to factory row or not able to program again without erasing chip first
	NEMCR = 0x40; // bit 6,5,4 = 100, Factory Row 
	
	return board;

}