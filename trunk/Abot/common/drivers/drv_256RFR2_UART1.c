
/**
 * \file drv_mega256RFR2_UART1.c
 *
 * \brief ATmega256RFR2 UART1 driver
 *
 * \author    Torgeir Bjornvold
 *
 * Created: 27.01.2013 08:38:49
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
//#include <string.h>

//Abot common includes

#include "drv_256RFR2_UART1.h"
#include <services/USART_ABOT_trm.h>
#define NAC             (0x15)  //Negative Acknowledge, ASCII ctrl U

/* USART1 Receive Complete interrupt */
ISR(USART1_RX_vect)
{
	extern unsigned char RX1_int_flag;
	extern unsigned char trm_cmd_int_flag;
	unsigned char rx_char;
	extern rx_buf rxbuf_trm;
	
		RX1_int_flag = 1;	//Set Rx flag
		
		/* Wait for data to be received */
		while ( !(UCSR1A & (1<<RXC1)) )
		;
		rx_char=UDR1;
		rxbuf_trm.buf[rxbuf_trm.tail]=rx_char; // Store received byte in receive buffer

		//if (rxbuf_trm.tail== BUFSIZE_TRM) rxbuf_trm.tail=0; // End of buffer reached ?
		
		if (rx_char == '\r' || rx_char == '\n')
		{
			trm_cmd_int_flag = 1;  //command received
			rxbuf_trm.buf[rxbuf_trm.tail]='\0';
		}
		rxbuf_trm.tail++;
		
}

void USART1_tx_char(unsigned char c)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR1A & (1<<UDRE1)) )
	;
	/* Put data into buffer, sends the data */
	UDR1 = c;
}

void USART1_clr_buffer(void)
{
	extern rx_buf rxbuf_trm;
	
	rxbuf_trm.head=0;
	rxbuf_trm.tail=0; //clr rx buffer
}

unsigned char USART1_get_char(void)
{
	unsigned char c;
	extern rx_buf rxbuf_trm;
	
	if (rxbuf_trm.head != rxbuf_trm.tail) //not empty
	{
		c = rxbuf_trm.buf[rxbuf_trm.head];
		rxbuf_trm.head++;
		if (rxbuf_trm.head==BUFSIZE_TRM) USART1_clr_buffer(); //End of buffer
	} else c = NAC; 
	return c;
}


/*! \brief Initialization of mega UART1
 *
 * The USART has to be initialized before any communication can take place. The initialization process
 * normally consists of setting the baud rate, setting frame format and enabling the
 * Transmitter or the Receiver depending on the usage. For interrupt driven USART operation, the
 * Global Interrupt Flag should be cleared (and interrupts globally disabled) when doing the
 * initialization
 *
 * \param   baudrate    the baudrate in baud
 * \param   CPU_MHz     the CPU frequency in kHz
 *
 */
void USART1_init(uint16_t baudrate, uint32_t CPU_MHz)
{
	extern unsigned char TX1_int_flag;
	extern unsigned char RX1_int_flag;
	extern unsigned char trm_cmd_int_flag;
	extern rx_buf rxbuf_trm;
	
	//unsigned int ubrr;
	//uint32_t ubrr;
	
	/* For Mega3290P, enable the uart peripheral 
	PRR � Power Reduction Register
	Bit 1 - PRUSART: Power Reduction USART
		Writing logic one to this bit shuts down the USART by stopping the clock to the module. When
		waking up the USART again, the USART should be re-initialized to ensure proper operation.
	*/
   // PRR &= ~(1 << PRUSART0);
   

	/* Set baud rate 
	Table 19-3. Examples of UBRRn Settings for Commonly Used Oscillator Frequencies
	Asynchronous Normal mode (U2Xn = 0) UBRR0 = (fosc/(16*BAUD))-1
	*/
	/*ubrr=BAUD;
	ubrr = 16*ubrr;
	ubrr = CPU_MHz/ubrr; 
	//ubrr = (int)rint(CPU_MHz/ubrr);  
	ubrr = ubrr-1;
	//ubrr = baudrate;  //6=>9600 1= 28800
	UBRR0H = (unsigned char)(ubrr>>8); //High byte
	UBRR0L = (unsigned char)ubrr;      //Low byte
	*/

	
	// 9600baud at 8MHz
	
		UBRR1 = 51;
		
	// 9600baud at 16MHz
		
		//UBRR1 = 103;
		
	/*
	UCSR0B � USART0 Control and Status Register B
	Bit 7 � RXCIE0 - RX Complete Interrupt Enable
	Writing this bit to one enables interrupt on the RXC0 Flag. A USART Receive Complete
	interrupt will be generated only if the RXCIE0 bit is written to one, the Global Interrupt
	Flag in SREG is written to one and the RXC0 bit in UCSR0A is set.
	� Bit 6 � TXCIE0 - TX Complete Interrupt Enable
	Writing this bit to one enables interrupt on the TXC0 Flag. A USART Transmit Complete
	interrupt will be generated only if the TXCIE0 bit is written to one, the Global Interrupt
	Flag in SREG is written to one and the TXC0 bit in UCSR0A is set.
	� Bit 5 � UDRIE0 - USART Data Register Empty Interrupt Enable
	Writing this bit to one enables interrupt on the UDRE0 Flag. A Data Register Empty
	interrupt will be generated only if the UDRIE0 bit is written to one, the Global Interrupt
	Flag in SREG is written to one and the UDRE0 bit in UCSR0A is set.
	� Bit 4 � RXEN0 - Receiver Enable
	Writing this bit to one enables the USART Receiver. The Receiver will override normal
	port operation for the RxD0 pin when enabled. Disabling the Receiver will flush the
	receive buffer invalidating the FE0, DOR0 and UPE0 Flags.
	� Bit 3 � TXEN0 - Transmitter Enable
	Writing this bit to one enables the USART Transmitter. The Transmitter will override
	normal port operation for the TxD0 pin when enabled. The disabling of the Transmitter
	(writing TXEN0 to zero) will not become effective until ongoing and pending
	transmissions are completed, i.e., when the Transmit Shift Register and Transmit Buffer
	Register do not contain data to be transmitted. When disabled, the Transmitter will no
	longer override the TxD0 port.
	� Bit 2 � UCSZ02 - Character Size
	The UCSZ02 bits combined with the UCSZ01:0 bit in UCSR0C sets the number of data
	bits (Character Size) in the frame that the Receiver and Transmitter use.
	� Bit 1 � RXB80 - Receive Data Bit 8
	RXB80 is the 9th data bit of the received character when operating with serial frames
	with nine data bits. The bit must be read before reading the lower 8 bits from UDR0.
	� Bit 0 � TXB80 - Transmit Data Bit 8
	TXB80 is the 9th data bit in the character to be transmitted when operating with serial
	frames with nine data bits. The bit must be written before writing the lower 8 bits to
	UDR0.
	*/		
		UCSR1B = (1 << RXCIE1) | (0 << TXCIE1) | (1 << RXEN1) | (1 << TXEN1); //RX int enabled

	/* Set frame format: 8data, 1stop bit, no parity 
	UCSRnC � USART Control and Status Register n C = 0000 0110
	Bit 7:6 USART Mode Select UMSELn This bit selects between asynchronous (0) and synchronous (1) mode of operation.
	Bit 5:4 Parity Mode UPMn1:0 = 00 (Disabled)
	Bit 3 Stop Bit Select USBSn = 0 (1 stop bit)
	Bit 2:1 Character Size UCSZn1:0 = 11 (8-bit)
		UCSZn2 bit in UCSRnB = 0 (8-bit)
	Bit 0 � UCPOLn: Clock Polarity = 0 (This bit is used for synchronous mode only. Write this bit to zero when asynchronous mode is used.
	*/	
		UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);

	TX1_int_flag = 0; //clr TX interrupt flag
	RX1_int_flag = 0; //clr RX interrupt flag
	trm_cmd_int_flag=0;
	
	USART1_clr_buffer(); //clr rx buffer
}
