/*
 * drv_3290_timer.h
 *
 * Created: 
 *  Author: 
 */ 

#include <stdbool.h>

typedef struct {
	uint16_t delay_coarse;
	uint16_t delay_fine;
	bool timer_running_status;	
}delay_timer_t;

void test_generic_timer(void);
uint16_t get_system_time(void);
void generate_pwm(uint16_t duty, uint16_t freq);
void stop_pwm(void);
void init_pwm_timer(void);
void init_generic_timer(void);
uint8_t register_timer(void (*timer_callback_register) (void), uint16_t delay);

