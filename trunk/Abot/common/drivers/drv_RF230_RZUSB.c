/*
 * drv_RF230.c
 *
 * Created: 25/02/2012 15:44:24
 *  Author: TB
 */ 

#include <avr/io.h>
#include <avr/interrupt.h> 
//#include <drivers/drv_90USB1287_SPI.h>

/* RF230 IRQ interrupt connected to PD4, TIMER1 CAPT vector 17

ICFn is automatically cleared when the Input Capture Interrupt Vector is executed. 

*/
ISR(TIMER1_CAPT_vect)
{
	extern unsigned char RF230_int_flag;

		RF230_int_flag=0x01;	
}

// TST = PB7 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_low(void)
{
	PORTB &= ~(1<<PINB7); // TST = 0
}	

// TST = PB7 (Enables Continuous Transmission Test Mode; active high)
void rf230_TST_high(void)
{
	PORTB|= (1<<PINB7); // TST = 1
}	

// RST = PB5 (Chip reset; active low)
void rf230_RST_low(void)
{
	PORTB &= ~(1<<PINB5); // RST = 0
}	

// RST = PB5 (Chip reset; active low)
void rf230_RST_high(void)
{
	PORTB|= (1<<PINB5); // RST = 1
}

// SLP_TR= PB4 (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_low(void)
{
	PORTB &= ~(1<<PINB4); // SLP_TR = 0
}	

// SLP_TR= PB4 (Controls sleep, transmit start and receive states; active high)
void rf230_SLP_TR_high(void)
{
	PORTB|= (1<<PINB4); // SLP_TR = 1
}


/*
TST		= PB7 (Enables Continuous Transmission Test Mode; active high)
RST		= PB5 (Chip reset; active low)
SLP_TR	= PB4 (Controls sleep, transmit start and receive states; active high)
IRQ		= PD4, ICP1 (Timer/Counter1 Input Capture Trigger)Timer/Counter1 Capture Event

A hardware reset is initiated by pulling the RST line low. 6 ?s later the same line is pulled high again and the hardware reset finished. 
All registers will have their default values and the state machine will be in P_ON or TRX_OFF state.
*/
void RF_drv_init(void)
{
		
	/* Set SLP_TR, RST, and TST as output */

	//DDRB – Port B Data Direction Register "1" = output

	DDRB |= (1<<DDB4)|(1<<DDB5)|(1<<DDB7);
	
	/* Enable Interrupt from RF230
	Input Capture Unit timer 1 used
	
	TIMSK1 - Timer/Counter1 Interrupt Mask Register 	
		Bit 5 – ICIEn: Timer/Countern, Input Capture Interrupt Enable
		When this bit is written to one, and the I-flag in the Status Register is set (interrupts globally
		enabled), the Timer/Countern Input Capture interrupt is enabled. The corresponding Interrupt
		Vector (See “Interrupts” on page 68.) is executed when the ICFn Flag, located in TIFRn, is set.
	
	*/
	TIMSK1 |= (1<<ICIE1);  // Timer/Countern, Input Capture Interrupt Enable bit =1
	
	/*	
	TCCR1B Timer/Counter1 Control Register B – 
	
		Bit 7 – ICNCn: Input Capture Noise Canceler
		Setting this bit (to one) activates the Input Capture Noise Canceler. When the Noise Canceler is
		activated, the input from the Input Capture Pin (ICPn) is filtered. The filter function requires four
		successive equal valued samples of the ICPn pin for changing its output. The input capture is
		therefore delayed by four Oscillator cycles when the noise canceler is enabled.
		
		Bit 6 – ICESn: Input Capture Edge Select
		This bit selects which edge on the Input Capture Pin (ICPn) that is used to trigger a capture
		event. When the ICESn bit is written to zero, a falling (negative) edge is used as trigger, and
		when the ICESn bit is written to one, a rising (positive) edge will trigger the capture.
		When a capture is triggered according to the ICESn setting, the counter value is copied into the
		Input Capture Register (ICRn). The event will also set the Input Capture Flag (ICFn), and this
		can be used to cause an Input Capture Interrupt, if this interrupt is enabled.
		When the ICRn is used as TOP value (see description of the WGMn3:0 bits located in the
		TCCRnA and the TCCRnB Register), the ICPn is disconnected and consequently the input cap-
		ture function is disabled.
	
		IRQ active high 
	*/
	TCCR1B |= (1<<ICES1);  //rising (positive) edge, bit 6 = 1
	TCCR1B &= ~(1<<ICNC1);	// noise canceler disabled, bit 7 = 0
	

	
}

