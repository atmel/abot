/**
 * \file drv_1284_PCINT0.c
 *
 * \brief The 1284 PCINT0 interrupt handler
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

//Abot common includes
#include "drv_1284_PCINT0.h"

/*
When a logic change on any PCINT7..0 pin triggers an interrupt request, PCIF0 becomes set
(one). If the I-bit in SREG and the PCIE0 bit in EIMSK are set (one), the MCU will jump to the
corresponding Interrupt Vector. The flag is cleared when the interrupt routine is executed. Alternatively,
the flag can be cleared by writing a logical one to it.
*/	
ISR(PCINT0_vect)
{
	extern unsigned char ABOT_int_flag;
	extern unsigned char compass_int_flag;
	extern uint8_t accelerometer_int_flag;
	extern uint8_t gyro_int_flag;
	uint8_t int_source;

	//Get which pin generated interrupt.
	
	int_source = PINA;  // Get value on PA0 to PA7 pins
		
	if ((int_source & 0x04) !=0) compass_int_flag=0x01; // PA2 connected to DRDY, DRDY pin =1, DRDY pin cleared when data register read
	
	if ((int_source & 0x01) ==0) ABOT_int_flag=0x01;	//PA0 connected to switch, int when 0
	
	if ((int_source & 0x08) !=0) accelerometer_int_flag=0x01;	// PA3 connected to BMA150 interrupt line
	
	if ((int_source & 0x10) == 0) gyro_int_flag = 0x01; // PA4 connected to ITG3200 interrupt line	
}

void PCINT0_enable(void){

/*
	PCICR � Pin Change Interrupt Control Register
	Bit 0 � PCIE0: Pin Change Interrupt Enable 0
	When the PCIE0 bit is set (one) and the I-bit in the Status Register (SREG) is set (one), pin
	change interrupt 0 is enabled. Any change on any enabled PCINT7..0 pin will cause an interrupt.
	The corresponding interrupt of Pin Change Interrupt Request is executed from the PCI0 Interrupt
	Vector. PCINT7..0 pins are enabled individually by the PCMSK0 Register.
	*/
	
	// Set Interrupt Sense Control to generate interrupt on any edge
	EICRA |= (1 << ISC00);
	
	// set bit0 = 1, enable PCINT7..0
	PCICR |= (1<<PCIE0); 
	
}

void DRDY_INT_enable(void){

	PCINT0_enable();
}	
	
void switch_INT_enable(void){

	PCINT0_enable();	
}

void accelerometer_INT_enable(void){

	PCINT0_enable();	
}

void gyro_INT_enable(void){

	PCINT0_enable();	
}