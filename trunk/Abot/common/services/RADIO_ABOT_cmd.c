
/**
 * \file RADIO_ABOT_cmd.c
 *
 * \brief The Abot protocol implemented on IEEE 802.15.4-2003 Frame Format
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//Abot common includes
#include <services/ABOT_protocol.h>
#include "RADIO_ABOT_cmd.h"


#include <boards/board.h>
#if (BOARD==RAVEN)
	#include <drivers/drv_1284_SPI.h>
	#include <drivers/drv_RF230_RAVEN.h>
	//#include <components/EEPROM_24C02.h> //MAC address
	#include "board_RAVEN.h"
#elif (BOARD==RZUSB)
	#include <drivers/drv_90USB1287_SPI.h>
	#include <drivers/drv_RF230_RZUSB.h>
	#include <components/RADIO_RF230_MAC_d.h>
	#include "board_RZUSB.h"
#elif (BOARD==AT256RFR2_XPLD_PRO)
	#include "board_256RFR2_XPLD_PRO.h"
	#include <drivers/drv_256RFR2_RF.h>
#elif (BOARD==AT256RFR2_EXT)
	#include "board_256RFR2_EXT.h"
	#include <drivers/drv_256RFR2_RF.h>	
#elif (BOARD==XMEGA_RF233_ZIGBIT)
	#include <components/RADIO_RF233_MAC.h>
	#include "xmega_rf233_zigbit/xmega_rf233_zigbit.h"
#else
   #error TARGET_BOARD must be defined somewhere
#endif



/*! \brief Enter RX state, i.e. ready to receive 
 *
 * This routine enters the mode enabling the radio to listen for 
 * incoming frames
 * 
 * \return  error code, 0x01 if OK
 */
unsigned char RX_enable(void)
{
	unsigned char error = 0x01;
	
	error = RX_enable_drv();
	return error;
}


/*! \brief Set SHORT address destination
 *
 * The 16 bit address of the receiving node
 *
 * \param  shortad     The IEEE 802.15.4 short address (device id in PAN)
 *
 */
void set_RF_SHORT_DESTINATION(uint16_t shortad)
{
	extern uint16_t MAC_SHORT_dest; //  MAC short address
	
	MAC_SHORT_dest = shortad; // set destination MAC short address
	
}

/*! \brief get new SHORT address from routing table
 *
 * The coordinator keeps a routing table with addresses for nodes in its ABOT network
 *
 * RTG
 *
 * \param  clr_r if 0x01 the table is cleared
 *
 * \return new short address from routing table 
 *
 */
uint16_t get_RF_SHORT(unsigned char clr_r)
{
	extern node_info RTG_table[ROUTING_TABLE_SIZE]; // routing table
	uint16_t shortad;
	unsigned char p; // pointer
	
		if (clr_r == 0x01) clr_routing_table();  // Clear routing table
		p= RTG_table[0].node_short_adr+1; // next cell in table
		RTG_table[0].node_short_adr=p; // update header with last valid cell in table
		shortad = 0x4200 + p; // new short address
		RTG_table[p].node_short_adr= shortad;
		RTG_table[p].live = 0x01; //TRUE
		strncpy (RTG_table[p].node_family_name,"Family Name",10 );
	
	return shortad;
	
}



/*! \brief Set RF output power
 *

 *
 * \param  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */

void set_RF_output_pwr(char pwr)
{
	drv_set_RF_output_pwr(pwr);
}


/*! \brief RF Channel Selection 
 *
 * \param  channel     The IEEE 802.15.4 channel. channel 11 to channel 26
 *
 */
void set_RF_channel(unsigned char channel)
{
	drv_set_RF_channel(channel);
}

/*! \brief Set PAN ID
 *
 * The 16 bit identifier of the PAN on which the device is operating. If
 * this value is 0xffff, the device is not associated.
 *
 * \param  panid     The IEEE 802.15.4 PAN ID (network id)
 *
 */
void set_RF_PAN_ID(uint16_t panid)
{
	extern uint16_t MAC_PAN_ID; // current MAC PAN ID
	extern uint16_t MAC_PAN_ID_dest; // destination net MAC PAN ID
	
	MAC_PAN_ID = panid; // set current MAC PAN ID
	MAC_PAN_ID_dest = panid; // TODO add support for gateway function to another PAN
	
	drv_set_RF_PAN_ID(panid);
}



/*! \brief Set board (my) SHORT address
 *
 * The 16 bit address that the device uses to communicate in the PAN.
 *
 * If the device is a PAN coordinator, this value shall be chosen before a PAN is started. 
 * Otherwise, the address is allocated by a coordinator during association.
 *
 * A value of 0xfffe indicates that the device has associated but has not
 * been allocated an address. 
 *
 * \param  shortad     The IEEE 802.15.4 short address (device id in PAN)
 *
 */
void set_RF_SHORT(uint16_t shortad)
{
	extern uint16_t MAC_SHORT; // current MAC short address
	
	MAC_SHORT = shortad; // set current MAC short address
	
	drv_set_RF_SHORT(shortad);
}


// ======================================================================================================================================

/*! \brief Get IRQ status 

	This register contains the status of the pending interrupt requests. An interrupt is
	pending if the associated bit has a value of one. Such a pending interrupts can be
	manually cleared by writing a 1 to that register bit. Interrupts are automatically cleared
	when the corresponding interrupt service routine is being executed.
	� Bit 7 � AWAKE - Awake Interrupt Status
	� Bit 6 � TX_END - TX_END Interrupt Status
	� Bit 5 � AMI - Address Match Interrupt Status
	� Bit 4 � CCA_ED_DONE - End of ED Measurement Interrupt Status
	� Bit 3 � RX_END - RX_END Interrupt Status
	� Bit 2 � RX_START - RX_START Interrupt Status
	� Bit 1 � PLL_UNLOCK - PLL Unlock Interrupt Status
	� Bit 0 � PLL_LOCK - PLL Lock Interrupt Status

* \return  pending interrupt requests  
 *
 */
unsigned char get_IRQ_status(void)
{
	unsigned char irq_status;
	
	irq_status = drv_get_IRQ_status();
	
	return irq_status;
}

/*reading the radio transceiver state from register 0x01 (TRX_STATUS), bit 4:0
0x00 P_ON 
0x01 BUSY_RX 
0x02 BUSY_TX 
0x06 RX_ON 
0x08 TRX_OFF (Clock State) 
0x09 PLL_ON (TX_ON) 
0x0F SLEEP 
0x11 BUSY_RX_AACK 
0x12 BUSY_TX_ARET 
0x16 RX_AACK_ON 
0x19 TX_ARET_ON 
0x1C RX_ON_NOCLK 
0x1D RX_AACK_ON_NOCLK 
0x1E BUSY_RX_AACK_NOCLK 
0x1F STATE_TRANSITION_IN_PROGRESS */
unsigned char get_RADIO_state(void)
{
	unsigned char state;
	
	state = drv_get_RADIO_state();
	
	return state;
}

/*! \brief Get RSSI 

	The read value is a number between 0 and 28 indicating the received signal strength as 
	a linear curve on a logarithmic input power scale (dBm) with a resolution of 3 dB.
	
* \return  received signal strength in dBm  
 *
 */
unsigned char get_RADIO_RSSI(void)
{
	unsigned char rssi;
	
	rssi = drv_get_RADIO_RSSI();
	
	return rssi;
}


/*! \brief Get RF Channel 
 *
	0x0B => channel 11
	0x1A => channel 26

 * \return  channel     The IEEE 802.15.4 channel. 0x0B => channel 11, 0x1A => channel 26
 *
 */
unsigned char get_RF_channel(void)
{
	unsigned char channel;
	channel = drv_get_RF_channel();
	return channel;
}


/*! \brief Get current RF output power
 * 
	The register bits TX_PWR sets the TX output power of the AT86RF230. The available 
	power settings are summarized in Table 9-1.
	0x0 = +3dBm
	0xF = -17.2dBm

 *
 * \return  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */

unsigned char get_RF_output_pwr(void)
{
	unsigned char pwr;

	pwr=drv_get_RF_output_pwr();
	return pwr;
}

/*! \brief read status of last transmission 
 *
 *
		0 SUCCESS 
		1 SUCCESS_DATA_PENDING 
		SUCCESS_WAIT_FOR_ACK (RX_AACK)
		3 CHANNEL_ACCESS_FAILURE 
		5 NO_ACK 
		7 INVALID 
 * 
 * \return  TRAC_STATUS 
 */
unsigned char get_RADIO_TX_status(void)
{
	unsigned char status;
	
		status = drv_get_RADIO_TX_status();
		
	return status;
}

/*! \brief get current SHORT address destination
 *
 * The 16 bit address of the receiving node
 *
 * \param  shortad     The IEEE 802.15.4 short address (device id in PAN)
 *
 */
uint16_t get_RF_SHORT_DESTINATION(void)
{
	extern uint16_t MAC_SHORT_dest; //  MAC short address
	
	return MAC_SHORT_dest; // get destination MAC short address
	
}

RADIO_frame get_RADIO_rx_frame(void)
{
	RADIO_frame Rf_a;
	
	Rf_a = drv_get_RADIO_rx_frame();
	return(Rf_a);
}

/*! \brief read address used for filtering messages to unit 
 *
 * 
 * \return  address used for filtering messages to unit 
 */
uint16_t get_RADIO_SHORT_filter(void)
{
	uint16_t address;
	
		address = drv_get_RADIO_SHORT_filter();
		
	return address;
}

/*! \brief read address used for filtering messages to unit 
 *
 * 
 * \return  address used for filtering messages to unit 
 */
uint16_t get_RADIO_PANID_filter(void)
{
	uint16_t address;
	
		address = drv_get_RADIO_PANID_filter();
		
	return address;
}

/*! \brief Get preprogrammed MAC address 
 *
 * 
 * 
 * \return  MAC address used for filtering messages to unit 
 */
uint16_t get_MAC_SHORT(void)
{	
	uint16_t short_address;

	short_address = drv_get_MAC_SHORT();
	if (short_address == 0xFFFF) //No MAC available
	{
		short_address = MAC_SHORT_DEFAULT;
	}
	return short_address;
}

/*! \brief Get board data
 *
 * 
 * 
 * \return  board data
 */
board_data get_board_id(void)
{	
	
	return drv_get_board_id();
}


// ======================================================================================================================================


/* Typically used for debug - display integer as 4 hex values*/
void transmit_RADIO_print4hex(unsigned int num)
{
  RADIO_frame Rf; // RADIO frame
  unsigned int t=0;
  
	Rf.PSDU[0]=FOURHEX;
	t = num & 0x0F;
	Rf.PSDU[1]=(char)t; // first hex digit (4bit)
	num= num>>4;
	t = num & 0x0F;
	Rf.PSDU[2]=(char)t;
	num= num>>4;
	t = num & 0x0F;
	Rf.PSDU[3]=(char)t;
	num= num>>4;
	t = num & 0x0F;
	Rf.PSDU[4]=(char)t;		 
	Rf.PHR=5;
  
	RADIO_tx_frame(Rf); 
}


void transmit_RADIO_joy(unsigned char position)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=JOYSTICK;
	Rf.PSDU[1]=position; 
	Rf.PHR=0x02;
  
	RADIO_tx_frame(Rf); 
}

void transmit_RADIO_ABOT(unsigned char command)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=ABOT;
	Rf.PSDU[1]=command; 
	Rf.PHR=0x02;
  
	RADIO_tx_frame(Rf); 
}

void transmit_RADIO_ABOT_motor(unsigned char command, unsigned char val)
{
	RADIO_frame Rf;  // RADIO frame
	 
	 Rf.PSDU[0] = ABOT_MOTOR_DIRECTION;
	 Rf.PSDU[1] = command;
	 Rf.PSDU[2] = val;
	 Rf.PHR = 0x03;
	 
	 RADIO_tx_frame(Rf);
}
void transmit_RADIO_TEST(uint8_t command)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=TEST;
	Rf.PSDU[1]=command; 
	Rf.PHR=0x02;
  
	RADIO_tx_frame(Rf); 
}

void transmit_RADIO_PING(unsigned char pingpay)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=PING_A;
	Rf.PSDU[1]=pingpay; 
	Rf.PHR=0x02;

	RADIO_tx_frame(Rf); 
}

/*! \brief Transmit a PING with family name as payload
 *
 * Typically used for providing a readable name to the robot
 *
 */
void transmit_RADIO_PING_name(void)
{
	RADIO_frame Rf; // RADIO frame
	char txt[11] = "";
	unsigned char t = 0;
	
		Rf.PSDU[0]=PING_A;
		Rf.PSDU[1]=0xAB;

		strncpy (txt,FAMILY_NAME,10 ); // Mr. Abot's family name
		
		while (t<10)
		{
			Rf.PSDU[t+2]=txt[t];
			t++;
		}
		
		Rf.PHR=t+2;
	
		RADIO_tx_frame(Rf);
}

/*! \brief Transmit a text on RF
 *
 * Typically used for status reporting - debug printf
 *
 * Automatically routed to Coordinator 0x4242 (currently disabled)
 *
 * \param  txt - the text to display
 *
 */

void transmit_RADIO_DISPLAY(char txt[MAX_PAYLOAD])
{
  RADIO_frame Rf; // RADIO frame
  unsigned char t=0;
  extern uint16_t MAC_SHORT; // current MAC short address
  extern uint16_t MAC_SHORT_dest; // MAC short address destination
  //uint16_t MAC_SHORT_temp; // Backup of destination address
   
	Rf.PSDU[0]=DISPLAY;
	
	while ((txt[t]!='\0') & (t<MAX_PAYLOAD))
	{
		Rf.PSDU[t+1]=txt[t];
		t++;
	}
	Rf.PSDU[t+1]='\0';
  
  	Rf.PHR=t+2;
  
	//MAC_SHORT_temp = MAC_SHORT_dest;  // current destination address  
	//if (MAC_SHORT != 0x4242) MAC_SHORT_dest = 0x4242; // if I am not Odin Set destination to Abot net coordinator (Odin) 
	RADIO_tx_frame(Rf);
	//MAC_SHORT_dest = MAC_SHORT_temp; // Restore destination address 
}

void transmit_RADIO_AUDIO(unsigned char cmd, uint16_t length, char sound[MAX_PAYLOAD])
{
	RADIO_frame Rf; // RADIO frame
	unsigned char t=0;
	
	Rf.PSDU[0]=ABOT_AUDIO;
	Rf.PSDU[1]=cmd;
	Rf.PSDU[2]= (unsigned char)length; // sound length low byte
	length = length >> 8;
	Rf.PSDU[3]= (unsigned char)length; // sound length high byte
		
	while ((sound[t]!='\0') & (t<MAX_PAYLOAD))
	{
		Rf.PSDU[t+4]=sound[t];
		t++;
	}

	Rf.PHR=t+5;
	
	RADIO_tx_frame(Rf);
}



void transmit_RADIO_GETVERSION(void)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=VERSION;
	Rf.PHR=0x01;
  
	RADIO_tx_frame(Rf); 
}

/*
__DATE__
    This macro expands to a string constant that describes the date on which the preprocessor is being run. 
	The string constant contains eleven characters and looks like "Feb 12 1996". 
	If the day of the month is less than 10, it is padded with a space on the left.
    If GCC cannot determine the current date, it will emit a warning message (once per compilation) and __DATE__ will expand to "??? ?? ????".
__TIME__
    This macro expands to a string constant that describes the time at which the preprocessor is being run. 
	The string constant contains eight characters and looks like "23:59:01".
    If GCC cannot determine the current time, it will emit a warning message (once per compilation) and __TIME__ will expand to "??:??:??". 
	
	BOARD defined as symbol in GCC (-D)
*/

void transmit_RADIO_VERSION(void)
{
  char v_txt[32] = "";
  char c_txt[11] = "";
  
	strncpy (v_txt, __DATE__,11 ); // compile date
	strncpy (c_txt, __TIME__,8 ); // compile time
	v_txt[11]=' ';// space between date and time
	strcat(v_txt, c_txt); //ver= DATE + TIME
	v_txt[20]=' ';// space before application
	strncpy (c_txt,VERSION_APP,10 ); // Application name
	strcat(v_txt, c_txt);//ver= DATE + TIME + App name
    v_txt[31]='\0';
	transmit_RADIO_DISPLAY(v_txt);
}

void transmit_RADIO_GETCRTR(void)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=GETCRTR;
	Rf.PHR=0x01;
  
	RADIO_tx_frame(Rf); 
}

void transmit_RADIO_CRTR(void)
{
	transmit_RADIO_DISPLAY("ABOT creator is Torgeir Bjornvold:-)");
}
	
	
void transmit_RADIO_GETSENSOR(unsigned char sensor)	
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=GET_ABOT_SENSOR;
	Rf.PSDU[1]= sensor;
	Rf.PHR=0x02; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	


void transmit_RADIO_A_SENSOR(unsigned char sensor, uint16_t svalue)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=ABOT_SENSOR;
	Rf.PSDU[1]= sensor; // Sensor type
	Rf.PSDU[2]= (unsigned char)svalue; // sensor value low byte
	svalue = svalue >> 8;
	Rf.PSDU[3]= (unsigned char)svalue; // sensor value high byte
	Rf.PHR=0x04; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	

/*! \brief RF Channel Selection on all nodes in network
 *
 * The PLL is designed to support 16 channels in the IEEE 802.15.4 � 2.4 GHz band with 
 * a channel spacing of 5 MHz. The center frequency FCH of these channels is defined as 
 * follows: 
 * FCH = 2405 + 5 (k � 11) [MHz], for k = 11, 12, ..., 26 where k is the channel number. 
 *
 * \param  channel     The IEEE 802.15.4 channel. 0x0B => channel 11, 0x1A => channel 26
 *
 */
void transmit_RADIO_SETCH(unsigned char channel)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=SET_RF_CH;
	Rf.PSDU[1]= channel;
	Rf.PHR=0x02; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	

/*! \brief Set RF output power on destination node
 *
 * The maximum output power of the transmitter is typical +3 dBm. The 
 * selectable output power range of the transmitter is 20 dB.
 *
 * \param  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */
void transmit_RADIO_SETPWR(unsigned char pwr)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=SET_RF_PWR;
	Rf.PSDU[1]= pwr;
	Rf.PHR=0x02; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	


/*! \brief Set SHORT address on destination node
 *
 * The 16 bit address that the device uses to communicate in the PAN.
 *
 * If the device is a PAN coordinator, this value shall be chosen before a PAN is started. 
 * Otherwise, the address is allocated by a coordinator during association.
 *
 * A value of 0xfffe indicates that the device has associated but has not
 * been allocated an address. 
 *
 * A value of 0xffff indicates that the device does not have a short address.
 *
 * \param  short     The IEEE 802.15.4 short address (device id in PAN)
 *
 */
void transmit_RADIO_SETSHORT(uint16_t shortad, unsigned short random1, unsigned short random2)
{
  RADIO_frame Rf; // RADIO frame
  
	Rf.PSDU[0]=SET_RF_SHORT;
	Rf.PSDU[1]= random1;
	Rf.PSDU[2]= random2;
	Rf.PSDU[3]= (unsigned char)shortad;
	shortad = shortad >> 8;
	Rf.PSDU[4]= (unsigned char)shortad;
	Rf.PHR=0x05; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	


/*! \brief request SHORT address 
 *
 * The 16 bit address that the device uses to communicate in the PAN.
 *
 *  the address is allocated by a coordinator during association.
 *
 */
void transmit_RADIO_REQSHORT(uint8_t random_seed)
{
  extern unsigned char MAC_RANDOM1;
  extern unsigned char MAC_RANDOM2;
  RADIO_frame Rf; // RADIO frame
  uint16_t r;  //random number   
  
  	Rf.PSDU[0]=REQ_RF_SHORT;
	srandom(random_seed); //get random seed; from radio ???
	r =(uint16_t)random();  // get random number
		
	Rf.PSDU[1]= (unsigned char)r;
	MAC_RANDOM1 = Rf.PSDU[1];
	r = r >> 8;
	Rf.PSDU[2]= (unsigned char)r;
	MAC_RANDOM2=Rf.PSDU[2]; 
	Rf.PHR=0x03; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	

/*! \brief sen CLR address to all ABOT network nodes 
 *
 * 
 *
 */
void transmit_RADIO_CLRSHORT(void)
{
  
  RADIO_frame Rf; // RADIO frame
  
  
	Rf.PSDU[0]= CLR_RF_SHORT;
	
	Rf.PHR=0x01; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	

/*! \brief Set PAN ID on all nodes in PAN
 *
 * The 16 bit identifier of the PAN on which the device is operating. If
 * this value is 0xffff, the device is not associated.
 *
 * \param  panid     The IEEE 802.15.4 PAN ID (network id)
 *
 */
void transmit_RADIO_SETPANID(uint16_t panid)
{
  RADIO_frame Rf; // RADIO frame
  uint16_t t;
  
	Rf.PSDU[0]=SET_RF_PAN_ID;
	t = panid & 0x0F;
	Rf.PSDU[1]= (unsigned char)t;
	t = panid >> 8;
	Rf.PSDU[2]= (unsigned char)t;
	Rf.PHR=0x03; // # of Payload byte
  
	RADIO_tx_frame(Rf); 
}	


/*! \brief User I/O handling
 *
    #define UIO_CONFIGURE       0x01  // config I/O
	#define UIO_STATUS          0x02  // reply from I/O with pin status
	#define UIO_REQUEST         0x03  // Ask for I/O pin status
	#define UIO_SET             0x04  // Set output high or low
 *
 * \param  cmd     The user I/O action to perform
 *
 * \param  IOdata  The user I/O value, 
 *                 if UIO_CONFIGURE: pin direction input ("0") and output ("1") 
 *
 * \param  GPIO    UIO_SET: select pin (s) to set
 *                 UIO_CONFIGURE: "1" fro pin with interrupt capability
 *
 */
void transmit_RADIO_USERIO(unsigned char cmd, uint16_t IOdata, uint16_t GPIO)
{
  RADIO_frame Rf; // RADIO frame
  uint16_t t;
  
	Rf.PSDU[0]= ABOT_UIO;
	Rf.PSDU[1]= cmd;
	switch (cmd)
	{
		case UIO_CONFIGURE:
		case UIO_SET:
			Rf.PSDU[2]= (unsigned char)IOdata;
			t = IOdata >> 8;
			Rf.PSDU[3]= (unsigned char)t;
			Rf.PSDU[4]= (unsigned char)GPIO;
			t = GPIO >> 8;
			Rf.PSDU[5]= (unsigned char)t;
			Rf.PHR=0x06; // # of Payload byte
		break;
		case UIO_STATUS:
			Rf.PSDU[2]= (unsigned char)IOdata;
			t = IOdata >> 8;
			Rf.PSDU[3]= (unsigned char)t;
			Rf.PHR=0x04; // # of Payload byte
		break;
		case UIO_REQUEST:
			Rf.PHR=0x02; // # of Payload byte
		break;
	}	
	RADIO_tx_frame(Rf); 
}	



/* \brief clear routing table
*
*
*/
void clr_routing_table(void)
{
	extern node_info RTG_table[ROUTING_TABLE_SIZE]; // // Coordinators Routing Table for short addresses, first byte contains last address
	
	RTG_table[0].node_short_adr=0;
}


/* \brief clear routing table live flag
*
*
*/
void clr_routing_table_live_flag(void)
{
	extern node_info RTG_table[ROUTING_TABLE_SIZE]; // // Coordinators Routing Table for short addresses, first byte contains last address
	unsigned char t;
	
	for (t = 1; t < RTG_table[0].node_short_adr+1; t++)
	{
		RTG_table[t].live = 0x0; //FALSE
	}
}

/* \brief Updates Abot routing table with new node
*	Set live flag if already in table
*
*/
void update_routing_table(RADIO_frame cmd)
{
	extern node_info RTG_table[ROUTING_TABLE_SIZE]; // routing table
	unsigned char t,i; // loop counter
	unsigned char new = 1; // new node, default TRUE
	
	//already in table? set active flag
	for (t = 1; t < RTG_table[0].node_short_adr+1; t++)
	{
		if (RTG_table[t].node_short_adr ==cmd.SHORT_source_adr)
		{
			RTG_table[t].live = 0x01; //TRUE
			new = 0; //FALSE
		}
	}
	
	//if new update table
	if (new)
	{
		t = RTG_table[0].node_short_adr + 1; //new node in table
		RTG_table[0].node_short_adr = t; //update number of nodes in table
		RTG_table[t].live = 1; //TRUE
		RTG_table[t].node_short_adr = cmd.SHORT_source_adr;
		for (i = 0; i < 10; i++)
		{
			RTG_table[t].node_family_name[i] = cmd.PSDU[i+2];
		}
	}//new

}


/* \brief Updates Abot routing table with active Abots in the network
*	PING each registered address in routing table and remove inactive nodes
*   Nodes not responding will have live flag = FALSE
*/
void update_active_abot_nodes(void)
{
	extern node_info RTG_table[ROUTING_TABLE_SIZE];// routing table
	unsigned char t; // loop counter
	uint16_t l; //loop length
	uint16_t i,b;
	uint16_t MAC_SHORT_temp; // Backup of destination address
	
	MAC_SHORT_temp = get_RF_SHORT_DESTINATION();
	
	clr_routing_table_live_flag();
	
	l = RTG_table[0].node_short_adr+1; //Number of allocated addresses in table
	for (t = 1; t < l; t++)
	{
		set_RF_SHORT_DESTINATION(RTG_table[t].node_short_adr);// Only PING this node
		transmit_RADIO_PING(0xAB); //Get Abot name
		for (i = 0; i < 0xffff; i++)//delay loop allows Abot to respond and get registered
		{
			b=i;//do something in loop
		}
	}
	set_RF_SHORT_DESTINATION(MAC_SHORT_temp); // Reset SHORT destination address

}



void RADIO_init(void)
{
	extern unsigned char MAC_seq_no; //MAC header sequence number
	extern unsigned char RF_int_flag;
	extern unsigned char ABOT_address;// This Abot Address
	extern unsigned char ABOT_destination; //Abot LAN destination address
	extern unsigned char ABOT_seq_no; //Abot msg header sequence number
	
	RF_int_flag=0;
	MAC_seq_no = 0; //MAC header sequence number
	//RF_drv_init(); //initialise HW related config, PIN & INT
	RF_init(); //initialise HW related config, PIN & INT
	
	set_RF_channel(RF_DEFAULT_CH);  
	set_RF_SHORT(MAC_SHORT_DEFAULT);// A value of 0xffff indicates that the device does not have a short address.
	set_RF_SHORT_DESTINATION(MAC_SHORT_DEFAULT_DEST);
	set_RF_PAN_ID(RF_DEFAULT_PAN_ID); //The 16 bit identifier of the PAN on which the device is operating
	clr_routing_table();// clr routing table
	
	ABOT_address = (unsigned char)MAC_SHORT_DEFAULT; //my address, to be obtained from LAN admin
	
	ABOT_destination = 0xFF; //Broadcast
	
	ABOT_seq_no = 0;

   
}

