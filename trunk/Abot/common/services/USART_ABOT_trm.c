
/**
 * \file USART_ABOT_trm.c
 *
 * \brief 
 *
 * \author    Torgeir Bjornvold
 * Created: 27.01.2013 09:46:07
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <string.h>
#include <stdio.h> //sprintf

//Abot common includes
#include "USART_ABOT_trm.h"
#include <services/ABOT_protocol.h>

//include HW related driver FW
#include <boards/board.h>
#if (BOARD==RAVEN)
	#include "board_RAVEN.h"
	#include <drivers/drv_mega_USART0.h>
#elif (BOARD==AT256RFR2_XPLD_PRO)
	#include "board_256RFR2_XPLD_PRO.h"
	#include <drivers/drv_256RFR2_UART1.h>
#elif (BOARD==AT256RFR2_EXT)
	#include "board_256RFR2_EXT.h"
	#include <drivers/drv_256RFR2_UART1.h>
#else
#error TARGET_BOARD must be defined somewhere
#endif

/* UART init */
void USART1_ABOT_init(void)
{
	USART1_init(9600, FOSC); //From board def
}	



/*! \brief get command typed on terminal
 *
 *
 *
 * \return  unsigned char predefined command number
 *
 */
unsigned char USART_Get_trm_cmd(void)
{
	unsigned char	trm_cmd=0;
	unsigned char	t=0;
	unsigned char	c=0;
	char	command[10];
	//char debugs[0x50];

	t = 0;
	while (c != NAC)
	{
		c = USART1_get_char();
		command[t] = c;
		
		//sprintf(debugs, "0x%X,0x%X ",t,c);
		//USART_printf_trm(debugs);
		
		t++;
	}
	USART1_clr_buffer();
//	USART_printf_trm("cmd:");
	USART_printf_trm(command);
	
    if (!strcmp(command, "help\0"))
    {
	    trm_cmd = HELP;
    }
    if(!strcmp (command, "?\0"))
    {
		trm_cmd = HELP;
    }
    if (!strcmp(command, "HELP\0"))
    {
		trm_cmd = HELP;
    }
	if(!strcmp (command, "h\0"))
	{
		trm_cmd = HELP;
	}
	if (!strcmp(command, "ping\0"))
	{
		trm_cmd = PING_A;
	}  
	if (!strcmp(command, "test\0"))
	{
		trm_cmd = TEST;
	}			
	if (!strcmp(command, "ver\0"))
	{
		trm_cmd = VERSION;
	}
	if (!strcmp(command, "hei\0"))
	{
		trm_cmd = HEI;
	}
	if (!strcmp(command, "rfi\0"))
	{
		trm_cmd = RFINFO;
	}
	return(trm_cmd);
}


/*! \brief transmit a text string to terminal
 *
 *
 *
 * \param   txt[0x2F] Text to transmit
 *
 */
void USART_printf_trm(char txt[0x2F])
{
  unsigned char t=0;
  
  USART1_tx_char('>');
  while ((txt[t]!='\0') & (t<0x2F))
  {
	USART1_tx_char(txt[t]);
	t++;
  }
	USART1_tx_char('\r');
	USART1_tx_char('\n');   
}


void USART_print_VERSION(void)
{
	char v_txt[32] = "";
	char c_txt[11] = "";
	
	strncpy (v_txt, __DATE__,11 ); // compile date
	strncpy (c_txt, __TIME__,8 ); // compile time
	v_txt[11]=' ';// space between date and time
	strcat(v_txt, c_txt); //ver= DATE + TIME
	v_txt[20]=' ';// space before application
	strncpy (c_txt,VERSION_APP,10 ); // Application name
	strcat(v_txt, c_txt);//ver= DATE + TIME + App name
	v_txt[31]='\0';
	USART_printf_trm(v_txt);
	
	strncpy (c_txt,FAMILY_NAME,10 ); // Application name
	strncpy (v_txt,"Mr. Abot \0",11 );
	strcat(v_txt, c_txt);//Mr. Abot + family name
	USART_printf_trm(v_txt);

}