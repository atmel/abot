
/**
 * \file USART_ABOT_trm.h
 *
 * \brief UART terminal interface
 *
 * \author    Torgeir Bjornvold
 * Created: 27.01.2013 09:41:29
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef USART_ABOT_TRM_H_
#define USART_ABOT_TRM_H_

#define BUFSIZE_TRM			(0x30)	// Maximum number of byte in receive buffer for data received from terminal

/* Receive Circular buffer structure */
typedef struct
{
	unsigned char head;				//Index to first available character in buffer.
	unsigned char tail;				//Index to last available character in buffer.
	unsigned char buf[BUFSIZE_TRM];	//The actual buffer used for storing characters.
} rx_buf;

rx_buf rxbuf_trm; //receive buffer

/*Global Interrupt flags defined*/

unsigned char TX1_int_flag;  //USART transmit interrupt flag
unsigned char RX1_int_flag;  //USART receive interrupt flag
unsigned char trm_cmd_int_flag;

//Terminal command definitions
#define HELP 0xE1  //List available commands
#define RFINFO 0xE2 //List RF details
#define HEI 0xE3


/* USART protocol API */
unsigned char USART_Get_trm_cmd(void); //Get command from terminal
void USART1_ABOT_init(void); // Initialize USART
void USART_printf_trm(char txt[0x2F]);	// send text to USART
void USART_print_VERSION(void); //Print SW version to terminal

#endif /* USART_ABOT_TRM_H_ */