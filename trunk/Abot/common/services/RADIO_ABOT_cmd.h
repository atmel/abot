/**
 * \file RADIO_ABOT_cmd.h
 *
 * \brief The Abot protocol implemented on IEEE 802.15.4-2003 Frame Format
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#ifndef RADIO_ABOT_CMD_H_
#define RADIO_ABOT_CMD_H_

#include <services/ABOT_protocol.h>


/*global typedefs*/

#define RF_DEFAULT_CH               0x0E //Default channel during initialization
#define RF_DEFAULT_PAN_ID           0xAB07 //Default PAN ID during initialization

unsigned char ABOT_address;// This Abot Address
unsigned char ABOT_destination; //Abot LAN destination address

unsigned char MAC_seq_no; //MAC header sequence number
uint16_t MAC_PAN_ID; // current MAC PAN ID
uint16_t MAC_PAN_ID_dest; // destination net MAC PAN ID
uint16_t MAC_SHORT; // current MAC short address
uint16_t MAC_SHORT_dest; // MAC short address destination

unsigned char MAC_RANDOM1; // Random byte number 1 - used when requesting new short address
unsigned char MAC_RANDOM2; // Random byte number 2 - used when requesting new short address
unsigned char rand_seed; // random number for MAC short address request
unsigned char last_LQI;	// Link Quality Indication (LQI) of last frame received

unsigned char ABOT_seq_no; //Abot msg header sequence number

#define ROUTING_TABLE_SIZE 50 //Number of cells in routing table
// Node info in routing table
typedef struct
{
	unsigned char live;         //node was alive when pinged, TRUE (0x01)
	uint16_t node_short_adr;    // node short address
	char node_family_name[10];   // Mr. Abots family name
} node_info;

node_info RTG_table[ROUTING_TABLE_SIZE]; // Coordinators Routing Table for Abot nodes, RTG_table[0] contains last address

typedef struct // Wireless board info stored in user page
{
	uint8_t    board_name[30];	       //PCBA name
	uint8_t    board_serial[10];	   //PCBA production serial number
	uint8_t    board_id[8];	           //PLM PCBA A# ( A09-1845)
	uint8_t    board_revision;	       //PCBA revision in PLM
} board_data;



// Radio frame global
typedef struct
{
	unsigned char LQI;					// Link Quality Indication (LQI)
	unsigned char PHR;					// # of Payload byte
	unsigned char PSDU[MAX_PAYLOAD];	// Payload data
	unsigned char source_adr;           // Source Short address from Abot payload
	uint16_t SHORT_source_adr;          // MAC SHORT Source address
	unsigned char ForMe;				// TRUE (0x01) if message to this ABOT (=ABOT_address) or broadcast 
} RADIO_frame;

/* Global Interrupt flags defined*/

unsigned char RF_int_flag; //General RF IRQ 
unsigned char RF_TX_END_int_flag; // Indicates the completion of a frame transmission
unsigned char RF_RX_END_int_flag; // Indicates the completion of a frame reception
unsigned char RF230_int_flag;


/* RADIO API, global functions */

unsigned char RX_enable(void); //Enter receive state

void set_RF_channel(unsigned char channel); //11 to 26
void set_RF_output_pwr(char pwr); //1 (max) to 15 (min)
void set_RF_PAN_ID(uint16_t panid);
void set_RF_SHORT(uint16_t shortad);
void set_RF_SHORT_DESTINATION(uint16_t shortad);

unsigned char get_RADIO_state(void);
unsigned char get_RADIO_RSSI(void);
unsigned char get_RF_channel(void);
unsigned char get_RF_output_pwr(void);
unsigned char get_IRQ_status(void);
unsigned char get_RADIO_TX_status(void);
uint16_t get_RF_SHORT(unsigned char clr_r); //get new SHORT address from routing table
uint16_t get_RF_SHORT_DESTINATION(void);// get current SHORT destination address
RADIO_frame get_RADIO_rx_frame(void);
uint16_t get_RADIO_SHORT_filter(void);
uint16_t get_RADIO_PANID_filter(void);
uint16_t get_MAC_SHORT(void);
board_data get_board_id(void);

void transmit_RADIO_PING(unsigned char pingpay);
void transmit_RADIO_PING_name(void);
void transmit_RADIO_joy(unsigned char position);
void transmit_RADIO_ABOT(unsigned char command);
void transmit_RADIO_ABOT_motor(unsigned char command, unsigned char val);
void transmit_RADIO_print4hex(unsigned int num);
void transmit_RADIO_TEST(uint8_t command);
void transmit_RADIO_VERSION(void);
void transmit_RADIO_GETVERSION(void);
void transmit_RADIO_CRTR(void);
void transmit_RADIO_GETCRTR(void);
void transmit_RADIO_DISPLAY(char txt[MAX_PAYLOAD]);
void transmit_RADIO_AUDIO(unsigned char cmd, uint16_t length, char sound[MAX_PAYLOAD]);
void transmit_RADIO_GETSENSOR(unsigned char sensor);
void transmit_RADIO_A_SENSOR(unsigned char sensor, uint16_t svalue);
void transmit_RADIO_SETCH(unsigned char channel);
void transmit_RADIO_SETPWR(unsigned char pwr);
void transmit_RADIO_SETSHORT(uint16_t shortad, unsigned short random1, unsigned short random2);
void transmit_RADIO_REQSHORT(uint8_t random_seed);
void transmit_RADIO_CLRSHORT(void);
void transmit_RADIO_SETPANID(uint16_t panid);
void transmit_RADIO_USERIO(unsigned char cmd, uint16_t IOdata, uint16_t GPIO);

void clr_routing_table(void);
void update_active_abot_nodes(void); // remove not live (responding) nodes
void update_routing_table(RADIO_frame cmd); //update routing table with new node

void RADIO_init(void);

#endif /* RADIO_TB_H_ */