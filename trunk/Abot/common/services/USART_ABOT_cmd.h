/**
 * \file USART_ABOT_cmd.h
 *
 * \brief The ABOT protocol implemented for a UART point to point communication
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#ifndef USART_ABOT_CMD_MUNIN_H_
#define USART_ABOT_CMD_MUNIN_H_

#include <services/ABOT_protocol.h>  //MAX_PAYLOAD

//#include <board.h> //BUFSIZE

/* Receive Circular buffer structure */
typedef struct 
{
    unsigned char head;				//Index to first available character in buffer. 
    unsigned char tail;				//Index to last available character in buffer.
    unsigned char buf[BUFSIZE];		//The actual buffer used for storing characters. 
} rx_buf;

rx_buf rxbuf; //receive buffer

/*Global Interrupt flags defined*/

unsigned char TX_int_flag;  //USART transmit interrupt flag
unsigned char RX_int_flag;  //USART receive interrupt flag



/* USART protocol API */
ABOT_frame USART_Get_frame(void); //Get received frame from USART

void USART_ABOT_init(void); // Initialize USART

void USART_printf(char txt[MAX_PAYLOAD]);	// send text to USART
void USART_print4hex(unsigned int num); // send 4 hex digit to USART
void USART_RADIOPING(unsigned char pingpay); //transmit RADIO PING cmd
void USART_GET_STATE(void); //transmit Get RADIO state cmd
void USART_GET_RSSI(void); // transmit Get RADIO RSSI cmd
void USART_GET_SHORT(void); // transmit Get RADIO SHORT cmd
void USART_joy(unsigned char position); //Transmit joystick cmd
void USART_ABOT(unsigned char command); //Transmit ABOT command 
void USART_TEST(uint8_t command); //Transmit TEST cmd
void USART_GET_SENSOR(unsigned char sensor); // Ask for sensor value
void USART_SENSOR(unsigned char sensor, uint16_t svalue); // Provide sensor value
void USART_AUDIO(unsigned char cmd, uint16_t length, char sound[MAX_PAYLOAD]);
void USART_USERIO(unsigned char cmd, uint16_t IOdata, uint16_t GPIO);

#endif /* USART_ABOT_CMD_H_ */