

#include <avr/io.h>

#include <services/ABOT_protocol.h>
#include <services/USART_ABOT_cmd.h>

#include <components/LCD_RAVEN.h>
#include <components/Joystick_RAVEN.h>
#include <components/voltage_raven.h>
#include <components/temp_sensor_raven.h>
#include <components/SPEAKER_RAVEN.h>

#include <drivers/drv_3290_timer.h>
#include "Hugin_sensor.h"

//extern tone_t james_bond[] ;
//functions

/*
* \brief initialize hugin sensors
*/

void hugin_sensor_init(void)
{
	//init voltage sensor is inbuilt in the get_supply_voltage
	
	//init temperature sensor
	init_temp_sensor();
	//init Speaker
	init_speaker();	
}

/*read sensor value function
* param sensor sensor code
* return sensor value in 16bit hex
*/

uint16_t read_sensor_value(uint8_t sensor)
{
	
	uint16_t sensor_reading;
	
	switch(sensor)
	{
		case ALL_SENSORS:
			//TODO: call all sensors
			sensor_reading = 0x2BAD;
		break;
		
		case SUPPLY_VOLTAGE:
			sensor_reading = get_supply_voltage();//voltage sensor reading in mV										
		break;
		
		case VOLTAGE_VCC:
			sensor_reading = read_external_voltage(EXT_VOLT_0_VCC);//voltage sensor reading in mV										
		break;
		
		case VOLTAGE_EXT:
			sensor_reading = read_external_voltage(EXT_VOLT_0_5VCC);//voltage sensor reading in mV										
		break;
		
		case TEMP_SENSOR:
			sensor_reading = get_temperature_reading(TEMP_DEG_C);//temperature sensor reading in degC*10
		break;
				
		case LIGHT_SENSOR:
			sensor_reading = 0x2BAD;	
		break;
		
		default:
		//wrong sensor code
		break;
	}
	
	return sensor_reading;			 
}