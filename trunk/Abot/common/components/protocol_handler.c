/**
 * \file  protocol_handler.c
 *
 * \brief Android interface
 *
 * \author   
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#include <avr/io.h>
#include <services/ABOT_protocol.h>
#include <stdio.h>
#include <components/protocol_handler.h>
#include <drivers/drv_90USB1287_timer.h>
#include <USBlib67/compiler.h>
#include <USBlib67/uart_usb_lib.h>
#include <components/RADIO_RF230_MAC.h>

extern unsigned char application_mode; //terminal mode for 0 and mobile mode for 1

typedef struct
{
	unsigned char length; //length of the command
	unsigned char src_id; //command originator ID
	unsigned char dst_id; //command destination ID
	unsigned char cmd;	  //command
	unsigned char payload[MAX_PAYLOAD]; //payload
	unsigned char error; //error status of the valid command
	unsigned char command_pending; //says whether the command is yet to be processed
} app_frame;

typedef struct
{
	unsigned char src_id; //command originator ID
	unsigned char cmd;	  //command
	unsigned char *payload; //payload
	unsigned char payload_length; //length of the command
	unsigned char response_pending; //says whether the command is yet to be processed
} app_resp_data;

//Address table for the nodes present
typedef struct
{
	uint8_t abot_count; //no. of nodes in the network
	uint8_t Adress_table[10]; //address table, maximum ABOTs 10 !
	unsigned char nw_search_on;
}active_addr_table_t;

app_frame app_data_frame;
app_resp_data app_response;

unsigned char ack_req = 0x0;	// state variable used to request to send ACK/NACK after transmission of frame.
uint8_t app_response_string[MAX_PAYLOAD];
active_addr_table_t active_abot_table = {0,{0},0}; //initializing active abot table - empty list


/* \brief print byte stream on usbstick if
*	
*/
void print_byte_stream(uint8_t *array, uint8_t length)
{
	uint8_t i;
	for(i=0; i<length; i++)  {
		uart_usb_putchar(array[i]);
	}	
}

/* \brief send back responses packed in Abot protocol header
*	
*/
void send_app_response(uint8_t app_cmd, uint8_t source, uint8_t app_payload_len, uint8_t *app_payload)
{
	uint8_t frame_length;
	uint8_t t;
	//construct the application frame with |SOH|src addr|dst addr|CMD|payload len|payload|EOT|
	app_response_string[0] = SOH;
	app_response_string[1] = 0x89; //destination address is 0x89 for the mobile
	app_response_string[2] = source;
	app_response_string[3] = app_payload_len;
	app_response_string[4] = app_cmd;
	for( t=0; t<app_payload_len ; t++)  {
		app_response_string[5+t] = app_payload[t];
	}
	app_response_string[5+t] = EOT;
	frame_length = 6+t;
	print_byte_stream( app_response_string, frame_length);
}

void abot_search_callback(void)
{
	//timer callback - abot_table_received
	//pack address list and send back to the USBstick
	send_app_response(NWK_QUERY, 0x42, active_abot_table.abot_count, active_abot_table.Adress_table);
}

/* \brief Updates table with active Abots in the network
*	PING each registered address in routing table

*/
void ping_abot_network(void)
{
	extern uint16_t RTG_table[100]; // routing table
	unsigned char t; // loop counter
	uint16_t l; //loop length
	uint16_t i,b;
		
	//clear active_abot_table
	active_abot_table.abot_count = 0;	
	//enable network switch
	active_abot_table.nw_search_on = 0x01;	
	//start timer with call back
	register_timer(abot_search_callback, 10000);
	
	l = RTG_table[0]+1; //Number of allocated addresses in table
	for (t = 1; t < l; t++)
	{
		set_RF_SHORT_DESTINATION(RTG_table[t]);// Only PING this node
		transmit_RADIO_PING(0x2F);	
		for (i = 0; i < 0xffff; i++)//delay loop allows Abot to respond and get registered
		{
			b=i;
		}
	}
	set_RF_SHORT_DESTINATION(0xFFFF); // Reset to Broadcast
	
	//send_app_response(NWK_QUERY, 0x42, active_abot_table.abot_count, active_abot_table.Adress_table);
}	
		

/* \brief parses the commands received from ABOT_bridge PC application
*	command string is ABOT protocol frame

*/
void application_parser(void)
{
	//get the protocol string
	unsigned char uart_rcv_char;
	unsigned char count;	

	
	//if (!app_data_frame.command_pending)  //no pending command
	//{		
		if (uart_usb_test_hit()) //data received?
		{	
			uart_rcv_char = uart_usb_getchar();
			
			if ( uart_rcv_char == SOH) //Abot message
			{
				uart_rcv_char = uart_usb_getchar();	//2nd byte is ABOT destination ID
				app_data_frame.dst_id = uart_rcv_char; 
				
				uart_rcv_char = uart_usb_getchar();	//3rd byte is ABOT source ID
				app_data_frame.src_id = uart_rcv_char; 
			
				uart_rcv_char = uart_usb_getchar();//4th byte is Frame length
				app_data_frame.length = uart_rcv_char;

				uart_rcv_char = uart_usb_getchar();//5th byte is ABOT data frame
				app_data_frame.cmd = uart_rcv_char;
	
				for( count = 0; count < app_data_frame.length ; count++)  
				{
					uart_rcv_char = uart_usb_getchar();//receive payload bytes
					app_data_frame.payload[count] = uart_rcv_char;
				}
			
				uart_rcv_char = uart_usb_getchar();
				if(uart_rcv_char != EOT)  
				{
					app_data_frame.error = NO_EOT;
				}else  //valid command to process
				{
					app_data_frame.error = NUL;
					app_data_frame.command_pending	= 0x01;  //valid command to process
					application_task();
				}

			} else //No message
			{
				app_data_frame.command_pending	= 0x0;  //no valid command to process
				if (uart_rcv_char == 0x03) //Ctrl+C to exit mobile application mode
				{
					application_mode	=	TERMINAL_MODE; //escape mobile mode
					printf("Exiting mobile application mode \r\n");
				}
				if (uart_rcv_char == 0x0D){ //Ctrl+M to enter mobile application mode
					printf("Entering mobile application mode \r\n");
					//application_mode	=	MOBILE_MODE;
				}				
			}			
		} //if (uart_usb_test_hit())
	//} //if((!app_data_frame.command_pending)
}	


/*! \brief Decodes commands from the Android app and takes action
*
*/
void application_task(void)
{
	uint8_t str_array[] =  {0xA1, 0xA2, 0xA3};
	
	//if (app_data_frame.command_pending) //New command received
	//{

		// Set destination MAC short address
		if(app_data_frame.dst_id==0x2A || app_data_frame.dst_id==0x42) // whether destination is broadcast or 'RZUSBSTICK'
		{
			set_RF_SHORT_DESTINATION(0xFFFF);// Set destination MAC short address for broadcast
		} else 
		{
			set_RF_SHORT_DESTINATION(0x4200+app_data_frame.dst_id);
		}
		
		switch (app_data_frame.cmd)  //Decode command
		{		
			case PING_A: //Reply to Androide with ACK TODO: reply with PING				
				send_app_response(ACK,0x42,0,0);
			break;
			
			case JOYSTICK:
				transmit_RADIO_joy(app_data_frame.payload[0]);
				ack_req = 0x01;						
			break;		
							
			case ABOT:
				ack_req = 0x01;
				transmit_RADIO_ABOT(app_data_frame.payload[0]);
			break;
				
			case ABOT_MOTOR_DIRECTION:
					ack_req = 0x01;
					transmit_RADIO_ABOT_motor(app_data_frame.payload[0],app_data_frame.payload[1]);				
			break;
								
			
			/*case 0x13:  //Ctrl-s STOP ABOT
				//send_app_response(STOP,0x42,0,0);
				ack_req = 0x01;
				transmit_RADIO_ABOT(STOP);
			break;
			*/
			case ENQ: //for checking USBstick state from PC application
				send_app_response(ACK,0x42,0,0);
			break;
			
			case NWK_QUERY: //Query active nodes in Abot net
				ping_abot_network();
			break;
			
			case TEST: //Test
				send_app_response(TEST, 0x42, 3,str_array );
			break;
			
			case GET_ABOT_SENSOR:
				transmit_RADIO_GETSENSOR(app_data_frame.payload[0]);
			break;
			
			case 0x03: //exit android mode, Ctrl-C
				printf("Exit mobile mode \r\n");
				application_mode = TERMINAL_MODE;
			break;
		}
		app_data_frame.command_pending = 0x0;
	//} //if (app_data_frame.command_pending)
}	

	
/*! \brief receives RF frame and relay to ABOT_Bridge PC application when applicable
*/
/* RF230 interrupts enabled:
	IRQ_3: TRX_END 
	IRQ 1: PLL unlock
	IRQ_0: PLL_LOCK  
*/ 
void application_RF_task(void)
{
	extern unsigned char RF230_int_flag;
	RADIO_frame RF_f;
	unsigned char state;
	unsigned char status;
	unsigned char t;
	uint16_t v1;
	
	if (RF230_int_flag){ // interrupt from RF230 detected

		RF230_int_flag= 0x0; // interrupt handled			
		status=get_IRQ_status();	// Read interrupt status register 0x0F (IRQ_STATUS), reg cleared
		state= get_RADIO_state();	// Get RADIO current state
			
		if ((status & 0x02) != 0) //IRQ 1: PLL unlock
		{
			printf("RZ>PLL ERR state= %x \r\n", state);
		}
			
		if ((status & 0x08) != 0)  //IRQ_3: TRX_END 
		{
				
			/*IRQ_3: TRX_END	RX:  Indicates the completion of a frame reception. 
								TX:  Indicates the completion of a frame transmission. */
				
			if ((state == RF_RX_ON_STATE) || (state == RF_RX_AACK_ON_STATE) ) //  frame received, decode command
			{
					RF_f = get_RADIO_rx_frame(); // get received frame
				 
					switch (RF_f.PSDU[0])  //command byte
					{		
						case PING_A: //call update table
							if(active_abot_table.nw_search_on)  
							{
								//update_active_abot_table
								active_abot_table.Adress_table[active_abot_table.abot_count] = RF_f.source_adr;
								active_abot_table.abot_count ++;
							}						
						break;	
						
						case REQ_RF_SHORT:  //Assign Abot net address when requested by node
							v1 = get_RF_SHORT(0);
							transmit_RADIO_SETSHORT(v1,RF_f.PSDU[1],RF_f.PSDU[2]);											
						break;
						
						case ABOT_SENSOR: // Transmit received sensor values
							app_response_string[0] = SOH;
							app_response_string[1] = 0x89; //destination address is 0x89 for the mobile
							app_response_string[2] = RF_f.source_adr;
							app_response_string[3] = 0x04; //payload length
							app_response_string[4] = ABOT_SENSOR;
							app_response_string[5] = RF_f.PSDU[1]; //Sensor type
							app_response_string[6] = RF_f.PSDU[2]; //Sensor value low byte
							app_response_string[7] = RF_f.PSDU[3]; //Sensor value high byte
							app_response_string[8] = EOT;
							print_byte_stream( app_response_string, 9);
						break;
					} //switch (RF_f.PSDU[0])
			} else // frame transmitted
			{
				// Only in extended mode
				if (RF_OPERATION_MODE == RF_EXT_OP_MODE) //Extended Operation Mode
				{
					t = get_RADIO_TX_status();
					if (t != 0)
					{
						send_app_response(NAC,0x42,0,0);
						
					} else //TX success
					{
						if(ack_req) 
						{
							send_app_response(ACK,0x42,0,0);
							ack_req = 0x0;
						}
					}
				}									
				RX_enable(); // Start listen for incoming frames
			}
		} //if ((status & 0x08) != 0)  //IRQ_3: TRX_END 
	}	//if (RF230_int_flag)					
} // task_RF(void)
