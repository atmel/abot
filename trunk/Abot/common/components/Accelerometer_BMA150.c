/**
 * \file  Accelerometer_BMA150.c
 *
 * \brief The Accelerometer component
 *
 * \author    SH
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

//include HW related driver FW
#include <boards/board.h>
#if (BOARD==RAVEN)
	#include "board_RAVEN.h"
	#include <drivers/drv_1284_TWI.h>     //Sensor interface
	#include <drivers/drv_1284_PCINT0.h>  // Interrupt handler
	#include <drivers/drv_BMA150_RAVEN.h> //HW related config for BMA150 on RAVEN
#elif (BOARD==MEGA1284P_XPLAINED_BC)
	#include <drivers/drv_1284_TWI.h>
	#include <drivers/drv_1284_PCINT0.h>  // Interrupt handler
#else
   #error BOARD must be defined somewhere
#endif

#include "Accelerometer_BMA150.h"

// Calibration values used for relative acceleration measurements
int32_t calibration_values_acc_x = 0;
int32_t calibration_values_acc_y = 0;
int32_t calibration_values_acc_z = 0;

/**
 *   \brief This function will init the RAVEN for the BMA150 accelerometer and the accelerometer itself.
 *   
 *   This function will setup the BMA150 accelerometer and configure it for the given mode.
 *    Accelerotmeter is connected to TWI bus
 * 
 * \param mode Configuration mode for the accelerometer
 *
 * \returns Status code (0x00 if OK)
 *
*/
uint8_t accelerometer_init(uint8_t mode){
	
    extern uint8_t accelerometer_int_flag;
    uint8_t chip_id;
	uint8_t tmp;
	accelerometer_data_t accelerometer_data = {0, 0, 0, 0};
	int32_t acc_x, acc_y, acc_z;
    
    // clr interrupt flag 
    accelerometer_int_flag = 0;  
	
	//initialize TWI
	TWI_master_init(TWI_SPEED, FOSC);
    
    //initialize HW related 
    bma150_init(mode); 
	        
    // Check communication by reading chip_id register
    if (accelerometer_read_registers(BMA150_REG_CHIP_ID, 1, &chip_id)){
            return BMA150_STATUS_TWI_ERROR;
    }
    
    if ((chip_id & BMA150_CHIP_ID_MASK) != BMA150_CHIP_ID){
        return BMA150_STATUS_INVALID_CHIP_ID;
    }

	// Set Accelerometer chip bandwidth to 25 Hz and range to +/- 2g 
	// NOTE the bandwith/range register contains critical calibration data so a read/modify/write is required
	if (accelerometer_read_registers(BMA150_REG_BANDWIDTH_RANGE_CONTROL, 1, &tmp)){
        // TWI error, just abort	
        return BMA150_STATUS_TWI_ERROR;
    }
	
	// Mask out bandwidth bits
	tmp &= ~BMA150_BANDWIDTH_MASK;
	
	// Modify register value by setting bandwidth to 25 Hz
	tmp |= BMA150_BANDWIDTH_25_HZ;

    // Mask out range bits
    tmp &= ~BMA150_RANGE_MASK;

    // Set range to +/- 2 g
    tmp |= BMA150_RANGE_2_G;
	
	// Write back the modified register value
	if (accelerometer_write_register(BMA150_REG_BANDWIDTH_RANGE_CONTROL, tmp)){
		// TWI error
		return BMA150_STATUS_TWI_ERROR;
	}

	if (mode == ACCELEROMETER_MODE_MOTION_ALERT){
		// TODO the thresholds for this mode needs to be fine tuned to suit the intended use of the mode. 
		
		/*
		*   From BMA150 datasheet
		*	Any motion criterion is valid if	|acc(t0)-acc(t0+3/(2*bandwidth))|>= any_motion_thres
		*	An interrupt is set if				(any_motion_criterion_x OR any_motion_criterion_y OR any_motion_criterion_z) for any_motion_dur consecutive times
		*	The interrupt is reset if			NOT(any_motion_criterion_x OR any_motion_criterion_y OR any_motion_criterion_z) for any_motion_dur consecutive times
		*/
		
		//enable interrupt
		accelerometer_INT_enable();  
		
		// Set any_motion threshold			
		if (accelerometer_write_register(BMA150_REG_ANY_MOTION_THRES, ACCELEROMETER_ANY_MOTION_THRESHOLD)){
			// TWI error
			return BMA150_STATUS_TWI_ERROR;
		}
		
		// Set any_motion duration
		if (accelerometer_read_registers(BMA150_REG_ANY_MOTION_DUR, 1, &tmp)){
			// TWI error, just abort	
			return BMA150_STATUS_TWI_ERROR;
		}
	
		// Mask out duration bits
		tmp &= ~BMA150_ANY_MOTION_DUR_MASK;
	
		// Modify register value by setting bandwidth to 25 Hz
		tmp |= BMA150_ANY_MOTION_DUR_3;

		// Write back the modified register value
		if (accelerometer_write_register(BMA150_REG_ANY_MOTION_DUR, tmp)){
			// TWI error
			return BMA150_STATUS_TWI_ERROR;
		}	
		
		// Set Accelerometer chip to give interrupt on INT pin when change of acceleration exceeds the given limit
		// First set the enable_adv_INT bit in INT_CONTROL register to enable advanced interrupts. Note no read-modify-write to avoid that any other interrupts are enabled at the same time
		if (accelerometer_write_register(BMA150_REG_INT_CONTROL, (1 << BMA150_INT_CONTROL_ENABLE_ADV_INT))){
			// TWI error, just abort	
			return BMA150_STATUS_TWI_ERROR;
		}
			
		// Then set the any_motion bit in MODE_CONTRO to enble the any_motion interrupt mode. Note no read-modify-write to avoid that any other interrupts are enabled at the same time
		if (accelerometer_write_register(BMA150_REG_MODE_CONTROL, (1 << BMA150_MODE_CONTROL_ANY_MOTION))){
			// TWI error, just abort	
			return BMA150_STATUS_TWI_ERROR;
		}
		
	
	}
	else if (mode == ACCELEROMETER_MODE_POSITION){
		// Calibrate accelerometer. This is necessary to compensate for gravitation. Assuming accelerometer
		// is not moving (i.e. robot standing still) we measure an average acceleration which will be subtracted from the normal readings
		// Take 8 measurements, sampling every 20 ms due to 25 Hz bandwidth
		// TWI speed of 100k, 85 TWI bits to read acceleration data -> 0.85 ms at least to read one sample
		// avg = avg + meas / 8
		for (uint8_t i = 0; i < 8; i++){
			accelerometer_get_raw_data(&accelerometer_data);
			// Calculate acceleration in um/s^2
			acc_x = accelerometer_convert_acc_data (accelerometer_data.acc_x);
			acc_y = accelerometer_convert_acc_data (accelerometer_data.acc_y);
			acc_z = accelerometer_convert_acc_data (accelerometer_data.acc_z);

			calibration_values_acc_x = calibration_values_acc_x + (acc_x / (int32_t) 8);
			calibration_values_acc_y = calibration_values_acc_y + (acc_y / (int32_t) 8);
			calibration_values_acc_z = calibration_values_acc_z + (acc_z / (int32_t) 8);
			_delay_ms(20);
		}		
	}	
	return BMA150_STATUS_OK;		
	
}

/*! \brief Write BMA150 register
 *
 * \param address  Address of register to write
 * \param data Data to write to BMA150 register
 *
 * \return Status code (0x00 if OK)
 */
uint8_t accelerometer_write_register(uint8_t address, uint8_t data){
    // TWI data buffer
    uint8_t TWI_data_buffer[TWI_MAX_DATA];
    
    TWI_data_buffer[0]= address;
    TWI_data_buffer[1]= data;
	
    if (TWI_master_transmit(BMA150_ADDRESS,2,(uint8_t *) &TWI_data_buffer[0],'Y')){
        return BMA150_STATUS_TWI_ERROR;
    }
    else {   
        return BMA150_STATUS_OK;
    }
}

/*! \brief Read BMA150 registers
 *
 * \param address  Address of first register to read
 * \param no_of_bytes Number of bytes to read (must be <= TWI_MAX_DATA)
 * \param *data Pointer to data buffer big enough to contain received data
 *
 * \return Status code (0x00 if OK)
 *
 */
uint8_t accelerometer_read_registers(uint8_t address, uint8_t no_of_bytes, uint8_t *data){
	// TWI data buffer
    uint8_t TWI_data_buffer[TWI_MAX_DATA];
    
    // First write address
    TWI_data_buffer[0]= address;
	
    if (TWI_master_transmit(BMA150_ADDRESS, 1, (uint8_t *) &TWI_data_buffer[0],'Y')){
        // TWI error, just abort
        return BMA150_STATUS_TWI_ERROR;
    }

    // Then do the read
    if (TWI_master_receive(BMA150_ADDRESS, no_of_bytes, data)){
        // TWI error
        return BMA150_STATUS_TWI_ERROR;
    }
    else{
        return BMA150_STATUS_OK;
    }
}


/**
 *   \brief This function reads the raw accelerometer measurements data from the BMA150 accelerometer
 *   
 * \param accelerometer_data Pointer to sturct where data will be stored
 * 
 * \returns Status code (0x00 if OK)
 *
*/
uint8_t accelerometer_get_raw_data(accelerometer_data_t *accelerometer_data){
    uint8_t data[6];
    uint8_t status;

    // Read  the data from the accelerometer
    status = accelerometer_read_registers (BMA150_REG_ACC_DATA_X_0, 6, &data[0]);
    
    if (status == BMA150_STATUS_OK){
        // Populate the data struct
        // The acceleration data for each axis contains 10 bits, one register byte contains all bits from 2 to 9 and bit 0 and 1 is stored in the upper two bits of a different register byte.
        (*accelerometer_data).acc_x = (data[1] << 2) | ((data[0] & 0xC0) >> 6);
        (*accelerometer_data).acc_y = (data[3] << 2) | ((data[2] & 0xC0) >> 6);
        (*accelerometer_data).acc_z = (data[5] << 2) | ((data[4] & 0xC0) >> 6);
        return BMA150_STATUS_OK;
    }
    else {
        // An error happened while reading data
        return status;
    }
}

/**
 * \brief Calculates new relative position from current relative position
 *
 * This function calculates a three dimensional relative position based on the 
 * current relative position and the input acceleration data. Note that using 
 * an accelerometer to calculate position will accumulate a relatively large 
 * error over short time. In addition it requires sampling at at least 25 Hz (lowest possible 
 * bandwidth of the accelerometer). This sampling frequency might be difficult to acheive with
 * the current configuration of the 1284 (CKDIV8 programmed, and normal not fast TWI speed, CPU intencive calculations etc.)
 * In other words this function is not very useful.
 *
 * \param accelerometer_data Sample from the BMA150 accelerometer (raw data)
 * \param current_position Current relative position in three dimensions
 * 
 * \returns New relative position
 */
position_t accelerometer_get_posistion (accelerometer_data_t accelerometer_data, position_t current_position) {
    position_t new_position;
    int32_t acc_x;
    int32_t acc_y;
    int32_t acc_z;
    uint32_t time;
    uint32_t time_2;

    // r = at^2/2 + v_0t + r_0
    // r = relative position
    // a = acceleration (assumed constant since last sample)
    // t = time since last sample
    // v_0 = velocity at last position
    // r_0 = relative position at last position
    //
    // v = at + v_0
    // v = velocity at current position
    // a = acceleration (assumed constant since last sample)
    // t = time since last sample
    // v_0 = velocity at last position
	
	// Calculate acceleration in um/s^2
    acc_x = accelerometer_convert_acc_data (accelerometer_data.acc_x);
    acc_y = accelerometer_convert_acc_data (accelerometer_data.acc_y);
    acc_z = accelerometer_convert_acc_data (accelerometer_data.acc_z);

	// Subtract calibration values
	acc_x = acc_x - calibration_values_acc_x;
	acc_y = acc_y - calibration_values_acc_y;	
	acc_z = acc_z - calibration_values_acc_z;
   
    // Calculate time since last sample in us
    time = ACCELEROMETER_TICK_TIME * accelerometer_data.timestamp;
    // Time squared [ms^2]
    time_2 = (time*time) / 1000000;
    
    // UNITS:
    // um = um/s^2 * ms^2 + um/ms * us + um
    // Note to get correct units the first term needs to be divided by 1000^2 and second term by 1000 
    new_position.pos_x = (int64_t) (acc_x*time_2)/(int64_t) 2000000 + ((int64_t) (current_position.vel_x)) * time / (int64_t) 1000 + current_position.pos_x;
    new_position.pos_y = (int64_t) (acc_y*time_2)/(int64_t) 2000000 + ((int64_t) (current_position.vel_y)) * time / (int64_t) 1000 + current_position.pos_y;
    new_position.pos_z = (int64_t) (acc_z*time_2)/(int64_t) 2000000 + ((int64_t) (current_position.vel_z)) * time / (int64_t) 1000 + current_position.pos_z;
    
    // UNITS:
    // um/ms = um/s^2 * us + um/ms
    // Note to get correct units the first term needs to be divided by 1000^2. Time needs to be divided by 1000 to get ms, however by dividing by 100 first we avoid loosing too much accuracy
    new_position.vel_x = ((acc_x / (int64_t) 1000) * (time/ (int64_t) 100)) / (int64_t) 10000 + current_position.vel_x;
    new_position.vel_y = ((acc_y / (int64_t) 1000) * (time/ (int64_t) 100)) / (int64_t) 10000 + current_position.vel_y;
    new_position.vel_z = ((acc_z / (int64_t) 1000) * (time/ (int64_t) 100)) / (int64_t) 10000 + current_position.vel_z;
       
    return new_position;

}

/**
 * \brief Converts acceleration raw data to um/s^2
 *
 * Accelerometer data is given in g (1 g = 9807 mm/s^2, 0.004g ~ 39000 um/s^2 )
 * For a range setting of +/- 2g the raw values are:
 * 10 0000 0000: -2.000g 
 * 10 0000 0001: -1.996g
 * ...
 * 11 1111 1111: -0.004g
 * 00 0000 0000: 0.000g
 * 00 0000 0001: +0.004g
 * ...
 * 01 1111 1110: +1.992g
 * 01 1111 1111: +1.996g
 *
 * \param acc_data Acceleration raw data from accelerometer
 *
 * \returns Acceleration data as um/s^2
 *
 */
int32_t accelerometer_convert_acc_data (uint16_t acc_data){
    int32_t acceleration;

    if (acc_data >= 0x200){
        // Negative value
        acceleration = ((int32_t) (0x400 - acc_data)) * -39000;
    }
    else {
        acceleration = ((int32_t) acc_data) * 39000;
    }
    return acceleration;
}
    
