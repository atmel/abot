/**
 * \file RADIO_RF230_MAC.h
 *
 * \brief The Abot protocol implemented on IEEE 802.15.4-2003 Frame Format
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#ifndef RADIO_RF233_MAC_H_
#define RADIO_RF233_MAC_H_

//Abot common includes
#include <services/RADIO_ABOT_cmd.h>

// RF configuration



#define RF_CCA_MODE                 0x02 //CCA Mode 2, Carrier sense only, register 0x08 RF230_REG_PHY_CC_CCA

//Transceiver states
/*
0x00 P_ON 
0x01 BUSY_RX 
0x02 BUSY_TX 
0x06 RX_ON 
0x08 TRX_OFF (Clock State) 
0x09 PLL_ON (TX_ON) 
0x0F SLEEP 
0x11 BUSY_RX_AACK 
0x12 BUSY_TX_ARET 
0x16 RX_AACK_ON 
0x19 TX_ARET_ON 
0x1C RX_ON_NOCLK 
0x1D RX_AACK_ON_NOCLK 
0x1E BUSY_RX_AACK_NOCLK 
0x1F STATE_TRANSITION_IN_PROGRESS */

#define RF_RX_ON_STATE       0x06
#define RF_PLL_ON_STATE      0x09
#define RF_RX_AACK_ON_STATE  0x16


//State Control Commands, Register Bits TRX_CMD
#define RF230_TRX_CMD_TX_START		(0x02)
#define RF230_TRX_CMD_FORCE_TRX_OFF	(0x03)
#define RF230_TRX_CMD_RX_ON			(0x06)
#define RF230_TRX_CMD_TRX_OFF		(0x08)
#define RF230_TRX_CMD_PLL_ON		(0x09)
#define RF230_TRX_CMD_RX_AACK_ON	(0x16)
#define RF230_TRX_CMD_TX_ARET_ON	(0x19)

//RF230 registers
#define RF230_REG_TRX_STATUS    (0x01) //signals the current state of the radio transceiver as well as the status of the CCA measurement.
#define RF230_REG_TRX_STATE     (0x02) //controls the radio transceiver states
#define RF230_REG_TRX_CTRL_0	(0x03) // register controls the drive current of the digital output pads and the CLKM clock rate
#define RF230_REG_TRX_CTRL_1	(0x04) //multi-purpose register to control various operating modes and settings of the radio transceiver
#define RF230_REG_PHY_TX_PWR	(0x05) //The PHY_TX_PWR register sets the transmit power and controls the FCS algorithm for TX operation.
#define RF230_REG_PHY_RSSI		(0x06) //The PHY_RSSI register is a multi purpose register to indicate the current received signal strength (RSSI) and the FCS validity of a received frame
#define RF230_REG_PHY_CC_CCA	(0x08) //The PHY_CC_CCA register contains register bits to initiate and control the CCA measurement as well as to set the channel center frequency. 
#define RF230_REG_IRQ_MASK		(0x0E) //interrupt mask register
#define RF230_REG_IRQ_STATUS	(0x0F) //interrupt status register
#define RF230_REG_VREG_CTRL		(0x10) //This register VREG_CTRL controls the use of the voltage regulators and indicates the status of these
#define RF230_REG_XOSC_CTRL		(0x12) //controls the operation of the crystal oscillator.
#define RF230_REG_XAH_CTRL      (0x2C) //controls the TX_ARET transaction in the Extended Operating mode
#define RF230_REG_CSMA_SEED_0   (0x2D) //contains a fraction of the CSMA_SEED value for the CSMA-CA algorithm.
#define RF230_REG_CSMA_SEED_1   (0x2E) // contains a fraction of the CSMA_SEED value
 
#define RF230_REG_SHORT_ADDR_0  (0x20) //This register contains bits [7:0] of the 16 bit short address for address filtering. 
#define RF230_REG_SHORT_ADDR_1  (0x21) //This register contains bits [15:8] of the 16 bit short address for address filtering. 
#define RF230_REG_PAN_ID_0      (0x22) //This register contains bits [7:0] of the 16 bit PAN ID for address filtering. 
#define RF230_REG_PAN_ID_1      (0x23) //This register contains bits [15:8] of the 16 bit PAN ID for address filtering. 
#define RF230_REG_IEEE_ADDR_0   (0x24) //This register contains bits [7:0] of the 64 bit IEEE address for address filtering. 
#define RF230_REG_IEEE_ADDR_1   (0x25) //
#define RF230_REG_IEEE_ADDR_2   (0x26) //
#define RF230_REG_IEEE_ADDR_3   (0x27) //
#define RF230_REG_IEEE_ADDR_4   (0x28) //
#define RF230_REG_IEEE_ADDR_5   (0x29) //
#define RF230_REG_IEEE_ADDR_6   (0x2A) //
#define RF230_REG_IEEE_ADDR_7   (0x2B) //This register contains bits [63:56] of the 64 bit IEEE address for address filtering. 


/* RADIO API, global functions used by RADIO_ABOT_cmd*/

void drv_set_RF_output_pwr(char pwr);//1 (max) to 15 (min)
void drv_set_RF_channel(unsigned char channel); //11 to 26
void drv_set_RF_PAN_ID(uint16_t panid);
void drv_set_RF_SHORT(uint16_t shortad);

//not used by RADIO_ABOT_cmd?
void drv_set_RF_SHORT_DESTINATION(uint16_t shortad);
void drv_set_frame_retries(uint8_t retries);

void RADIO_tx_frame(RADIO_frame Rf);
RADIO_frame drv_get_RADIO_rx_frame(void);

unsigned char drv_get_IRQ_status(void);
unsigned char drv_get_RADIO_state(void);
unsigned char drv_get_RADIO_RSSI(void);
unsigned char drv_get_RF_channel(void);
unsigned char drv_get_RF_output_pwr(void);
unsigned char drv_get_RADIO_TX_status(void);
uint16_t drv_get_RADIO_SHORT_filter(void);
uint16_t drv_get_RADIO_PANID_filter(void);
uint16_t drv_get_MAC_SHORT(void);
board_data drv_get_board_id(void);
void RF_init(void);       // RF230 init
unsigned char RX_enable_drv(void); //Enter receive state

/*


*/

#endif /* RADIO_TB_H_ */