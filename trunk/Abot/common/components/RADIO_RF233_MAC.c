/**
 * \file RADIO_RF230_MAC.c
 *
 * \brief The Abot protocol implemented on IEEE 802.15.4-2003 Frame Format
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//#define FOSC 8000000		// CPU Clock Speed in MHz
#define F_CPU 8000000UL		// 1 MHz CPU clk, used in delay
#include <util/delay.h>		//_delay_us()

//Abot common includes
#include "RADIO_RF233_MAC.h"
#include <drivers/drv_Xmega256A3U_SPI.h>
#include <drivers/drv_RF233_XZigBit.h>




/*
Each transfer sequence starts with transferring a command byte from SPI master via
MOSI  This command byte defines the access mode and additional mode-dependent information.
*/
char rf230_register_read(char address) 
{
	char register_value;

   /*Add the register read command to the register address. */
   
   // Register Access Mode � Read Access 
    address |= 0x80; // Register address [5:0], bit 7=1 and bit 6=0
    
    SPI_SS_low();
    
    /*Send Register address and read register content.*/
	SPI_MasterTransmit(address);
	address = SPI_Receive();
	register_value = 0;
	SPI_MasterTransmit(register_value);
	register_value = SPI_Receive();
	
	SPI_SS_high();
	
    return register_value;
}

/*
Each transfer sequence starts with transferring a command byte from SPI master via
MOSI  This command byte defines the access mode and additional mode-dependent information.
*/
void rf230_register_write(char address, char value) 
{
    /* Add the Register Write command to the address. */
	
	// Register Access Mode �  Write Access 
    address |= 0xC0; // Register address [5:0], bit 7=1 and bit 6=1
    
     SPI_SS_low();
    
    /*Send Register address and write register content.*/
	SPI_MasterTransmit(address);
	address = SPI_Receive();
	SPI_MasterTransmit(value);
	value = SPI_Receive();
	
	SPI_SS_high();

}

/*************** drv API ********************************************************************************/

/*! \brief Set RF output power
 *
 The  RF output power can be set via register bits TX_PWR in register 0x05 
(PHY_TX_PWR). The maximum output power of the transmitter is typical +3 dBm. The 
selectable output power range of the transmitter is 20 dB.

The PHY_TX_PWR register sets the transmit power and controls the FCS algorithm for 
TX operation. 

The register bits TX_PWR sets the TX output power of the AT86RF230. The available 
power settings are summarized in Table 9-1.
0x0 = +3dBm
0xF = -17.2dBm

Bit 7 � TX_AUTO_CRC_ON Refer to sections 7.2.6 and 8.2. AUTO CRC always on
Bit [6:4] � Reserved 
Bit [3:0] � TX_PWR 

 *
 * \param  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */

void drv_set_RF_output_pwr(char pwr)
{
	pwr = pwr | 0x80; //bit 7 = 1, AUTO CRC always on
	rf230_register_write(RF230_REG_PHY_TX_PWR, pwr);
}


/*! \brief RF Channel Selection 
 *
The PLL is designed to support 16 channels in the IEEE 802.15.4 � 2.4 GHz band with 
a channel spacing of 5 MHz. The center frequency FCH of these channels is defined as 
follows: 
FCH = 2405 + 5 (k � 11) [MHz], for k = 11, 12, ..., 26 
where k is the channel number. 
The channel k is selected by register bits CHANNEL (register 0x08, PHY_CC_CA).

Both calibration routines are initiated  automatically when the PLL is turned on. 
Additionally, the center frequency calibration is running when the PLL is programmed to 
a different channel (register bits CHANNEL in register 0x08).

The PHY_CC_CCA register contains register bits to initiate and control the CCA 
measurement as well as to set the channel center frequency.

Bit 7 � CCA_REQUEST 
Refer to section 8.6.4.
A manual CCA measurement is initiated with setting CCA_REQUEST = 1
Clear Channel Assessment (CCA) 
Note, it is not recommended to initiate manually a CCA measurement when using the Extended Operating Mode
�  Bit [6:5] � CCA_MODE 
Refer to section 8.6.4. 
The CCA mode can be selected using register bits CCA_MODE
�  Bit [4:0] � CHANNEL 
The register bits CHANNEL define the RX/TX channel. The channel assignment is 
according to IEEE 802.15.

0x0B => channel 11
0x1A => channel 26

 * \param  channel     The IEEE 802.15.4 channel. 0x0B => channel 11, 0x1A => channel 26
 *
 */
void drv_set_RF_channel(unsigned char channel)
{
	channel = channel | (RF_CCA_MODE << 5);  // bit 5 & 6 configures CCA mode
	rf230_register_write(RF230_REG_PHY_CC_CCA, channel);
}

/*! \brief Set PAN ID
 *
 * The 16 bit identifier of the PAN on which the device is operating. If
 * this value is 0xffff, the device is not associated.
 *
 * \param  panid     The IEEE 802.15.4 PAN ID (network id)
 *
 */
void drv_set_RF_PAN_ID(uint16_t panid)
{
	unsigned char t;
	
	/* Register 0x22 (PAN_ID_0)  
	This register contains bits [7:0] of the 16 bit PAN ID for address filtering. */
	t = (char)panid;
	rf230_register_write(RF230_REG_PAN_ID_0, (char)t);
	
	/* Register 0x23 (PAN_ID_1)  
	This register contains bits [15:8] of the 16 bit PAN ID for address filtering. */
	t = (panid >> 8);
	rf230_register_write(RF230_REG_PAN_ID_1, (char)t);
	
}

/*! \brief Set board (my) SHORT address
 *
 * The 16 bit address that the device uses to communicate in the PAN.
 *
 * If the device is a PAN coordinator, this value shall be chosen before a PAN is started. 
 * Otherwise, the address is allocated by a coordinator during association.
 *
 * A value of 0xfffe indicates that the device has associated but has not
 * been allocated an address. 
 *
 * \param  shortad     The IEEE 802.15.4 short address (device id in PAN)
 *
 */
void drv_set_RF_SHORT(uint16_t shortad)
{
	unsigned char t;
		
		/* Register 0x22 (PAN_ID_0)  
		This register contains bits [7:0] of the 16 bit PAN ID for address filtering. */
		t = (char)shortad;
		rf230_register_write(RF230_REG_SHORT_ADDR_0, t);
	
		/* Register 0x23 (PAN_ID_1)  
		This register contains bits [15:8] of the 16 bit PAN ID for address filtering. */
		t = (shortad >> 8);
		rf230_register_write(RF230_REG_SHORT_ADDR_1, t);
	
}



void drv_set_frame_retries(uint8_t retries)
{
	rf230_register_write(RF230_REG_XAH_CTRL,(retries<<4)|(rf230_register_read(RF230_REG_XAH_CTRL)));
}

/* ====================== STATE CHANGE COMMANDS ============================================================== */

/*Change state from P_ON to TRX_OFF
A write access to register bits TRX_CMD initiates a radio transceiver state transition
towards the new state.
*/
void send_TRX_OFF_cmd(void)
{
	rf230_register_write(RF230_REG_TRX_STATE,RF230_TRX_CMD_TRX_OFF);
}

/*
Entering the PLL_ON state from TRX_OFF state enables the analog voltage regulator 
(AVREG) first. After the voltage regulator has been settled, the PLL frequency 
synthesizer is enabled. When the PLL has been settled at the receive frequency, a 
successful PLL lock is indicated by issuing a PLL_LOCK interrupt. 
If an RX_ON command is issued in PLL_ON state, the receiver is immediately enabled. 
If the PLL has not been settled before, actual frame reception can only happen once the 
PLL has locked. 
The PLL_ON state corresponds to the TX_ON state in IEEE 802.15.4. 

Return 0x01 if ok
0x02 if did not enter PLL_ON state
0x03 if PLL did not lock
*/
char from_TRX_OFF_to_PLL_ON_state (void)
{
	char status;

		/*If TRX_STATUS = 0x1F (STATE_TRANSITION_IN_PROGRESS) the AT86RF230 is on a state transition. 
		Do not try to initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS.*/
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		
		//Issue PLL_ON cmd
		rf230_register_write(RF230_REG_TRX_STATE,RF230_TRX_CMD_PLL_ON);
		_delay_us(180); //TRX_OFF to PLL_ON state delay
		//A successful state transition shall be confirmed by reading the radio transceiver status from register 0x01 (TRX_STATUS), 
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		if (status!= 0x09) 
		{
			return 0x02; // PLL_ON = 0x09, did not enter state
		}
		else
		{
			/*
			if (RF_OPERATION_MODE != RF_EXT_OP_MODE)
			{
				//wait for PLL lock interrupt IRQ_0: PLL_LOCK Indicates PLL lock 
				while (RF_int_flag==0);
				status=get_IRQ_status() & 0x01;
		
				if ((status == 0x01) || (RF_OPERATION_MODE == RF_EXT_OP_MODE))
				{
					return 0x01; // state change ok	
				} else return 0x03; // PLL_LOCK status in bit 0, PLL did not lock	
			} else return 0x01; // state change ok	
			*/	 
			return 0x01; // state change ok				
		}		

}

/*
RX_ON and BUSY_RX � RX Listen and Receive State 
In RX_ON state the receiver blocks and the PLL frequency synthesizer are enabled. 
The AT86RF230 receive mode is internally divided into RX_ON state and BUSY_RX 
state. There is no difference between these states with respect to the analog radio 
transceiver circuitry, which is always turned on. During RX_ON state, only the preamble 
detection of the digital signal processing is running. When a preamble and a valid SFD 
are detected, also the digital receiver is turned on. The radio transceiver enters the 
BUSY_RX state and a RX_START interrupt is generated. 
During the frame reception frame data are stored continuously in the Frame Buffer until 
the last byte was received. The completion of the frame reception is indicated by a 
TRX_END interrupt and the radio transceiver reenters the state RX_ON. At the same 
time the register bit RX_CRC_VALID (register 0x06) is updated with the result of the 
FCS check (see section 8.2).  
Note, settings of address registers 0x20 to 0x2B do not affect the frame reception in 
Basic Operating Mode. Frame address filtering is only applied when using the Extended 
Operating Mode (see section 7.2).
*/
char from_PLL_ON_to_RX_ON_state (void)
{
	char status;
	/*If TRX_STATUS = 0x1F (STATE_TRANSITION_IN_PROGRESS) the AT86RF230 is on a state transition. 
		Do not try to initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS.*/
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		
		//Issue RX_ON cmd
		rf230_register_write(RF230_REG_TRX_STATE,RF230_TRX_CMD_RX_ON);
		_delay_us(1); // PLL_ON to RX_ON state delay
		//A successful state transition shall be confirmed by reading the radio transceiver status from register 0x01 (TRX_STATUS), 
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		if (status== 0x06) 
		{
			return 0x01; // RX_ON state ok
		}
		else
		{
			 return 0x00; //did not enter RX_ON state	 			
		}		

}	

char from_RX_ON_to_PLL_ON_state(void)
{
	char status;
	/*If TRX_STATUS = 0x1F (STATE_TRANSITION_IN_PROGRESS) the AT86RF230 is on a state transition. 
		Do not try to initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS.*/
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		
		//Issue PLL_ON cmd
		rf230_register_write(RF230_REG_TRX_STATE,RF230_TRX_CMD_PLL_ON);
		_delay_us(1); // PLL_ON to RX_ON state delay
		//A successful state transition shall be confirmed by reading the radio transceiver status from register 0x01 (TRX_STATUS), 
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		if (status== 0x09) 
		{
			return 0x01; // PLL_ON state ok
		}
		else
		{
			 return 0x00; //did not enter PLL_ON state	 			
		}		

}	

/*! \brief Change state from PLL_ON to RX_AACK_ON
 *
   
In the RX_AACK_ON state, the radio transceiver listens for incoming frames. After 
detecting a frame start (SFD), the radio transceiver state changes to BUSY_RX_AACK 
(register 0x01) and the TRAC_STATUS bits (register 0x02) are set to INVALID. The 
AT86RF230 starts to parse the MAC header (MHR) and a filtering procedure as 
described in IEEE 802.15.4-2003 section 7.5.6.2. (third level filter rules) is applied 
accordingly. 
 
The state RX_AACK_ON is entered by writing the command RX_AACK_ON to the 
register bits TRX_CMD in register 0x02 (TRX_STATE). The state change shall be 
confirmed by reading register 0x01 (TRX_STATUS) that changes to RX_AACK_ON or 
BUSY_RX_AACK on success. 

 * \return  0x01 if state change ok
 */
char from_PLL_ON_to_RX_AACK_ON_state (void)
{
	char status;
	/*If TRX_STATUS = 0x1F (STATE_TRANSITION_IN_PROGRESS) the AT86RF230 is on a state transition. 
		Do not try to initiate a further state change while the radio transceiver is in STATE_TRANSITION_IN_PROGRESS.*/
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		
		//Issue RX_AACK_ON cmd
		rf230_register_write(RF230_REG_TRX_STATE,RF230_TRX_CMD_RX_AACK_ON);
		_delay_us(1); // PLL_ON to RX_AACK_ON state delay
		
		//A successful state transition shall be confirmed by reading the radio transceiver status from register 0x01 (TRX_STATUS), 
		status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;  // bit 4:0
		while (status== 0x1F)
		{
			status = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
		}
		if (status== 0x06) 
		{
			return 0x01; // RX_AACK_ON state ok
		}
		else
		{
			 return 0x00; //did not enter RX_AACK_ON state	 			
		}		

}	

/* ====================== END STATE CHANGE COMMANDS ============================================================== */


/*
BUSY_TX � Transmit State 
A transmission can only be started in state PLL_ON. There are two ways to start a 
transmission: 
� Rising edge of SLP_TR 
� TX_START command to register 0x02 (TRX_STATE). 
Either of these causes the AT86RF230 to enter the BUSY_TX state. 
During the transition to BUSY_TX state, the PLL frequency shifts to the transmit 
frequency. Transmission of the first data chip of the preamble starts after 16 �s to allow 
PLL settling and PA ramping, see Figure 7-2. After transmission of the preamble and 
the SFD, the Frame Buffer content is transmitted. 
The last two bytes to be transmitted are the FCS (see Figure 8-2). The radio transceiver 
can  be  configured to autonomously compute the FCS bytes and append it to the 
transmit data. The register bit TX_AUTO_CRC_ON in register 0x05 (PHY_TX_PWR) 
needs to be set to 1 to enable this feature. For further details refer to section 8.2.When 
the frame transmission is completed, the radio transceiver automatically turns off the 
power amplifier, generates a TRX_END interrupt and returns to PLL_ON state. 
Note that in case the PHR indicates a frame length of zero, the transmission is aborted.
*/
void RADIO_tx_frame(RADIO_frame Rf)
{
  extern uint16_t MAC_PAN_ID; // current MAC PAN ID
  extern uint16_t MAC_PAN_ID_dest; // destination net MAC PAN ID
  extern uint16_t MAC_SHORT; // current MAC short address
  extern uint16_t MAC_SHORT_dest; // MAC short address destination
  extern unsigned char MAC_seq_no; //MAC header sequence number
  extern unsigned char ABOT_address;// This Abot Address
  extern unsigned char ABOT_destination; //Abot LAN destination address
  
  unsigned char c;
  uint8_t t;
  uint16_t a;
  unsigned char MPDU[MAX_PAYLOAD+14]; //MAC protocol data unit, MAC header = 12byte with short address
  
		//Add MAC Header (MHR)
		//FCS added automatically when PHY_TX_PWR.TX_AUTO_CRC_ON sub register is set to 1. Which is default
		//MAC_PAN_ID = 0xAB07;
		// Generate MAC Frame Control field and add to header
		MPDU[0] = 0x01;  // Intra PAN disabled,   No ACK,  001 Data Frame type(001)
		if ((MAC_SHORT_dest != 0xFFFF) && (MAC_SHORT_dest != 0x422A))
		{
			//MPDU[0] |= 0x21; // Intra PAN disabled,  ACK,  001 Data Frame type(001)
			MPDU[0] |= 0x01; // Intra PAN disabled,   no ACK,  001 Data Frame type(001)
		}
		MPDU[1] = 0x88;  // 1000 1000 Addressing mode 16-bit short
	
		
		//Generate MAC sequence number and add to header
		if (MAC_seq_no<0xfe) MAC_seq_no++; else MAC_seq_no=0;
		MPDU[2] = MAC_seq_no;
		
		//Destination PAN ID
		MPDU[3] = (unsigned char)MAC_PAN_ID_dest; //low byte
		a = (MAC_PAN_ID_dest >>8); 
		MPDU[4] = (unsigned char)a; // high byte
		
		//Destination address
		MPDU[5] = (unsigned char)MAC_SHORT_dest; //low byte
		a = (MAC_SHORT_dest >>8); 
		MPDU[6] = (unsigned char)a; // high byte
			
		//Source PAN ID
		MPDU[7] = (unsigned char)MAC_PAN_ID; //low byte
		a = (MAC_PAN_ID >>8); 
		MPDU[8] = (unsigned char)a; // high byte
		
		//Source address
		MPDU[9] = (unsigned char)MAC_SHORT; //low byte
		a = (MAC_SHORT >>8); 
		MPDU[10] = (unsigned char)a; // high byte
		
			
		//Set header size for use when adding payload
		c = 11;
		// MAC Header Footer (MFR) added automatically
			
		
	
	
	// add ABOT source and destination in ABOT LAN
	MPDU[c] = ABOT_destination; //destination address
	c++;
	MPDU[c] = ABOT_address; //ABOT source address
	c++;
	 
	// Set PHY header frame length PHR
	Rf.PHR = Rf.PHR + c;
	for (t=c; t< Rf.PHR; t++) // add payload
	{
		 MPDU[t] = Rf.PSDU[t-c]; 
	}
	
  
	// Change state from RX to PLL_ON, i.e disable receive
	c=from_RX_ON_to_PLL_ON_state(); // from RX_ON & RX_AACK_ON 
	
	if (c==0x01) //PLL_ON state entered ok
	{
	
		//upload frame to transmit
		SPI_SS_low();
    
		/*Send Register address and write register content.*/
		SPI_MasterTransmit(0x60); //Frame Buffer Write command, Frame Buffer Access Mode � Write Access bit 7:5 = 011
		c = SPI_Receive();
		SPI_MasterTransmit(Rf.PHR+2);  // Frame length, number of PSDU (payload data) incl. FCS (2byte)
		c = SPI_Receive();
		for (t=0; t< Rf.PHR; t++)
		{
			//SPI_MasterTransmit(Rf.PSDU[t]);  // Frame PSDU (payload data)
			SPI_MasterTransmit(MPDU[t]);  // Frame PSDU (payload data)
			c = SPI_Receive();	
		}
			
		SPI_SS_high();

		// issue TX_START cmd, i.e. transmit frame
	
	
			rf230_register_write(RF230_REG_TRX_STATE,RF230_TRX_CMD_TX_ARET_ON);//Enter TX_ARET_ON
			
			rf230_SLP_TR_low();
			for (a=0;a<0xffff;a++)
			{
				c=0;
			}
			//delay 65nS
			rf230_SLP_TR_high();
		
		
	}
	// Change state from PLL_ON to RX_ON done in main when interrupt received 
	
}

RADIO_frame drv_get_RADIO_rx_frame(void)
{
	RADIO_frame Rf;
	uint16_t u;
	unsigned char c,t;
	unsigned char MPDU[MAX_PAYLOAD+23]; //MAC protocol data unit, max header = 23byte
	extern unsigned char ABOT_address;// This Abot Address
	  
		SPI_SS_low();
    
		/*Send Register address and write register content.*/
		SPI_MasterTransmit(0x20); //Frame Buffer Read command, Frame Buffer Access Mode � Read Access bit 7:5 = 001
		c = SPI_Receive();
		SPI_MasterTransmit(0x00);  // Frame length, number of PSDU (payload data)
		Rf.PHR = SPI_Receive();
		for (t=0; t< Rf.PHR; t++)
		{
			SPI_MasterTransmit(0x00);  // Frame PSDU (payload data) The Transceiver has removed the Frame control field first byte = Sequence number
			MPDU[t] = SPI_Receive();
		}
		SPI_MasterTransmit(0x00);  // Link Quality Indication (LQI)
		Rf.LQI = SPI_Receive();
			
		SPI_SS_high();
		
		// get source address from received frame
		Rf. SHORT_source_adr = MPDU[9];
		u = MPDU[10];
		u = u<<8;
		Rf. SHORT_source_adr = Rf. SHORT_source_adr | u;
		
		//Remove MAC Header (MHR) - 11 byte 
		c=11;
		// MAC Header Footer (MFR) removed automatically by Transceiver

		// decode and remove ABOT address information
		 
		if ((MPDU[c] == ABOT_address) || (MPDU[c] == 0x2A)||(MPDU[c] == 0xFF))  // For me or broadcast?
		{
			Rf.ForMe = 0x01; //TRUE
		}else
		{
			Rf.ForMe=0;// Not for me
		}
		c++; //next byte is source
		Rf.source_adr = MPDU[c]; //low byte ABOT source address
		c++; //next byte is CMD
		for (t=c; t< Rf.PHR; t++) // extract Abot cmd + payload
		{
			Rf.PSDU[t-c] = MPDU[t];
		}
	
	return(Rf);
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------


/*
The IRQ_STATUS register contains the status of the individual interrupts. A read
access to this register resets all interrupt bits.
*/
unsigned char drv_get_IRQ_status(void)
{
	unsigned char irq_status;
	
	irq_status = rf230_register_read(RF230_REG_IRQ_STATUS);
	
	return irq_status;
}

/*reading the radio transceiver state from register 0x01 (TRX_STATUS), bit 4:0
0x00 P_ON 
0x01 BUSY_RX 
0x02 BUSY_TX 
0x06 RX_ON 
0x08 TRX_OFF (Clock State) 
0x09 PLL_ON (TX_ON) 
0x0F SLEEP 
0x11 BUSY_RX_AACK 
0x12 BUSY_TX_ARET 
0x16 RX_AACK_ON 
0x19 TX_ARET_ON 
0x1C RX_ON_NOCLK 
0x1D RX_AACK_ON_NOCLK 
0x1E BUSY_RX_AACK_NOCLK 
0x1F STATE_TRANSITION_IN_PROGRESS */
unsigned char drv_get_RADIO_state(void)
{
	unsigned char state;
	
	state = rf230_register_read(RF230_REG_TRX_STATUS) & 0x1F;
	
	return state;
}

/*
Register 0x06 (PHY_RSSI)  
The PHY_RSSI register is a multi purpose register to indicate the current received 
signal strength (RSSI) and the FCS validity of a received frame.
Bit [4:0] � RSSI 
The register bits RSSI contain the result of the automated RSSI measurement. The 
value is updated every 2 ?s in receive states. 
The read value is a number between 0 and 28 indicating the received signal strength as 
a linear curve on a logarithmic input power scale (dBm) with a resolution of 3 dB. An 
RSSI value of 0 indicates an RF input power of < -91 dBm (see parameter 11.7.17), a 
value of 28 a power of ? -10 dBm (see parameter 11.7.18).
*/
unsigned char drv_get_RADIO_RSSI(void)
{
	unsigned char rssi;
	
	rssi = rf230_register_read(RF230_REG_PHY_RSSI) & 0x1F;
	
	return rssi;
}


/*! \brief Get RF Channel 
 *
The PLL is designed to support 16 channels in the IEEE 802.15.4 � 2.4 GHz band with 
a channel spacing of 5 MHz. The center frequency FCH of these channels is defined as 
follows: 
FCH = 2405 + 5 (k � 11) [MHz], for k = 11, 12, ..., 26 
where k is the channel number. 
The channel k is selected by register bits CHANNEL (register 0x08, PHY_CC_CA).

Both calibration routines are initiated  automatically when the PLL is turned on. 
Additionally, the center frequency calibration is running when the PLL is programmed to 
a different channel (register bits CHANNEL in register 0x08).

The PHY_CC_CCA register contains register bits to initiate and control the CCA 
measurement as well as to set the channel center frequency.

Bit 7 � CCA_REQUEST 
Refer to section 8.6.4.
A manual CCA measurement is initiated with setting CCA_REQUEST = 1
Clear Channel Assessment (CCA) 
Note, it is not recommended to initiate manually a CCA measurement when using the Extended Operating Mode
�  Bit [6:5] � CCA_MODE 
Refer to section 8.6.4. 
The CCA mode can be selected using register bits CCA_MODE
�  Bit [4:0] � CHANNEL 
The register bits CHANNEL define the RX/TX channel. The channel assignment is 
according to IEEE 802.15.

0x0B => channel 11
0x1A => channel 26

 * \return  channel     The IEEE 802.15.4 channel. 0x0B => channel 11, 0x1A => channel 26
 *
 */
unsigned char drv_get_RF_channel(void)
{
	unsigned char channel;
	channel = rf230_register_read(RF230_REG_PHY_CC_CCA) & 0x1F;
	return channel;
}


/*! \brief Get current RF output power
 *
 The  RF output power can be set via register bits TX_PWR in register 0x05 
(PHY_TX_PWR). The maximum output power of the transmitter is typical +3 dBm. The 
selectable output power range of the transmitter is 20 dB.

The PHY_TX_PWR register sets the transmit power and controls the FCS algorithm for 
TX operation. 

The register bits TX_PWR sets the TX output power of the AT86RF230. The available 
power settings are summarized in Table 9-1.
0x0 = +3dBm
0xF = -17.2dBm

Bit 7 � TX_AUTO_CRC_ON Refer to sections 7.2.6 and 8.2. AUTO CRC always on
Bit [6:4] � Reserved 
Bit [3:0] � TX_PWR 

 *
 * \return  pwr     The RF output power level. 0x0 = +3dBm, 0xF = -17.2dBm
 *
 */

unsigned char drv_get_RF_output_pwr(void)
{
	unsigned char pwr;

	pwr=rf230_register_read(RF230_REG_PHY_TX_PWR) & 0x0F;
	return pwr;
}

/*! \brief read status of last transmission 
 *
 * Bit [7:5] � TRAC_STATUS 
 * The status of the TX_ARET algorithm is indicated by register bits TRAC_STATUS. 
 *
		0 SUCCESS 
		1 SUCCESS_DATA_PENDING 
		3 CHANNEL_ACCESS_FAILURE 
		5 NO_ACK 
		7 INVALID 
 * 
 * \return  TRAC_STATUS 
 */
unsigned char drv_get_RADIO_TX_status(void)
{
	unsigned char status;
	
		status = rf230_register_read(RF230_REG_TRX_STATE) & 0xE0;
		status = status >> 5;
	return status;
}

/*! \brief read contents of Short address reg.
 *
 * Register 0x20 (SHORT_ADDR_0)
 * This register contains bits [7:0] of the 16 bit short address for address filtering
 *
 * Register 0x21 (SHORT_ADDR_1)
 * This register contains bits [15:8] of the 16 bit short address for address filtering.
 * 
 * \return  address used for filtering messages to unit 
 */
uint16_t drv_get_RADIO_SHORT_filter(void)
{
	uint16_t address, t;
	
		address = rf230_register_read(RF230_REG_SHORT_ADDR_0);
		t = rf230_register_read(RF230_REG_SHORT_ADDR_1);
		t=t<<8;
		address = address | t;
	return address;
}

uint16_t drv_get_RADIO_PANID_filter(void)
{
	uint16_t address, t;
	
	address = rf230_register_read(RF230_REG_PAN_ID_0);
	t = rf230_register_read(RF230_REG_PAN_ID_1);
	t=t<<8;
	address = address | t;
	return address;
}

/*! \brief Get preprogrammed MAC address 
 *
 * 
 * 
 * \return  MAC address used for filtering messages to unit 
 */
uint16_t drv_get_MAC_SHORT(void)
{
	return 0x4242;
}


/*! \brief Get board data
 *
 * 
 * 
 * \return  board data
 */
board_data drv_get_board_id(void)
{
	board_data boardId;
	//board_info boardInfo;
	//unsigned char t;
	
	/*TODO
		boardInfo = userpage();
		
		for (t = 0; t < 8; t++)
		{
			boardId.board_id[t] = boardInfo.board_id[t];
		}
		for (t = 0; t < 10; t++)
		{
			boardId.board_serial[t] = boardInfo.board_serial[t];
		}
		for (t = 0; t < 30; t++)
		{
			boardId.board_name[t] = boardInfo.board_name[t];
		}
		
		boardId.board_revision = boardInfo.board_revision;
		
		*/
		
	return boardId;
}


/*! \brief Enter RX state, i.e. ready to receive 
 *
 * This routine enters the mode enabling the radio to listen for 
 * incoming frames
 *
 * if RF_OPERATION_MODE == RF_EXT_OP_MODE then Extended Operation mode used
 * 
 * \return  error code, 0x01 if OK
 */
unsigned char RX_enable_drv(void)
{
	unsigned char RF_state;
	unsigned char error = 0x01;
	
	RF_state = get_RADIO_state();
	
	if (RF_state != RF_PLL_ON_STATE) //not in PLL_ON state
	{
		error = from_TRX_OFF_to_PLL_ON_state (); // Enter PLL_ON state
	}
	
	if (error == 0x01) // PLL_ON state entered ok
	{	
			error = from_PLL_ON_to_RX_AACK_ON_state(); //Enter RX_AACK_ON	
	}// PLL_ON state entered ok
	return error;
}



/*
A hardware reset is initiated by pulling the RST line low. 6 ?s later the same line is pulled high again and the hardware reset finished. 
All registers will have their default values and the state machine will be in P_ON or TRX_OFF state.
*/
void RF_init(void)
{


	char c_txt[8];
	
	
	
	rf230_drv_init(); //initialise HW related config, PIN & INT
	SPI_MasterInit(); // init SPI 
	_delay_us(512);
	rf230_TST_low(); // Disable Continuous Transmission Test Mode
	rf230_RST_low(); 
	rf230_SLP_TR_low(); //Disable sleep, transmit start and receive states;
	_delay_us(6);
	rf230_RST_high();
	_delay_us(512);  // If in SLEPP mode a delay of about 1mS is required before entering TRX_OFF
	_delay_us(512);
	send_TRX_OFF_cmd();
	_delay_us(512);
	
	//set the ISR mask
	/*
	If an interrupt will be enabled or disabled, it is recommended to read the interrupt status
	register 0x0F (IRQ_STATUS) first to clear the history.
	*/
	get_IRQ_status();
	
	/*
	The AT86RF230 differentiates between six interrupt events. Each interrupt is enabled 
	or disabled by writing the corresponding bit to the interrupt mask register 0x0E
	1 = enable : 0000 1000
	Bit 7: Indicates a supply voltage below the programmed threshold.
	Bit 6: Indicates a Frame Buffer access violation (under run).
	IRQ_5 (AMI)	Indicates address matching.
	IRQ_4 (CCA_ED_DONE)
			Multi-functional interrupt:
			1. AWAKE_END:
			� Indicates finished transition to TRX_OFF state from P_ON, SLEEP, DEEP_SLEEP, or RESET state.
			2. CCA_ED_DONE:
			� Indicates the end of a CCA or ED measurement.
	Bit 3: RX:  Indicates the completion of a frame reception. TX:  Indicates the completion of a frame transmission. 
	Bit 2: Indicates a SFD detection. The TRX_STATE changes to BUSY_RX. IRQ_2: RX_START
	Bit 1: Indicates PLL unlock
	Bit 0: Indicates PLL lock
	
	IRQ_3: TRX_END 
	IRQ 1: PLL unlock
	IRQ_0: PLL_LOCK   
	*/ 
	rf230_register_write(RF230_REG_IRQ_MASK,0x0B);
	
	/* Enable automatic CRC generation
	The AT86RF230 applies an FCS check on each received frame. The FCS check result 
	is stored to register bit RX_CRC_VALID in register 0x06 (PHY_RSSI). 
	The register bit is updated at the event of the TRX_END interrupt and 
	remains valid until the next TRX_END interrupt caused by a new frame reception.
    On transmit the radio transceiver can be configured to autonomously compute and append the FCS bytes.
	PHY_TX_PWR register (0x05)
	Bit 7 � TX_AUTO_CRC_ON 
		Register bit TX_AUTO_CRC_ON controls the automatic FCS generation for TX 
		operation. The automatic FCS algorithm is performed autonomously by the radio 
		transceiver if register bit TX_AUTO_CRC_ON = 1. 
	*/
		
	//  set the TX output power of the AT86RF230RF to +3dBm, max, 
	set_RF_output_pwr(0x00); //TX_AUTO_CRC_ON = 1 in function
	
	/*
	Use internal voltage regulators
	This register VREG_CTRL controls the use of the voltage regulators and indicates the status of these
	Bit 7 � AVREG_EXT 0= Internal
		The register bit AVREG_EXT defines whether the internal analog voltage regulator or 
		an external regulator is used to supply the  analog low voltage building blocks of the 
		radio transceiver. 
	Bit 3 � DVREG_EXT 0= Internal
		The register bit DVREG_EXT defines whether the internal digital voltage regulator or an 
		external regulator is used to supply the digital low voltage building blocks of the radio transceiver
	
	*/
	rf230_register_write(RF230_REG_VREG_CTRL,0x00);
	
	// Battery Monitor (BATMON) not used
	
	/*CLK configuration. external 16Mhz x-tall
	The crystal oscillator generates the reference frequency for the AT86RF230. All other 
	internally generated frequencies of the radio transceiver are derived from this unique 
	frequency. Therefore, the overall system performance is mainly based on the accuracy 
	of this reference frequency. The external components of the crystal oscillator should be 
	selected carefully and the related board layout should be done meticulously (refer to 
	section 5). 
	The register 0x12 (XOSC_CTRL) provides access to the control signals of the 
	oscillator. Two operating modes are supported. It is recommended to use the integrated 
	oscillator setup as described in Figure 9-4,
	
	Bit [7:4] � XTAL_MODE 0xF Internal crystal oscillator enabled 
		The register bit XTAL_MODE sets the operating mode of the crystal oscillator.
		
	Bit [3:0] � XTAL_TRIM 0x0 0.0 pF, trimming capacitors disconnected 
		The register bits XTAL_TRIM control two internal capacitance arrays connected to pins 
		XTAL1 and XTAL2. A capacitance value in the range from 0 pF to 4.5 pF is selectable 
		with a resolution of 0.3 pF
		
	*/
	rf230_register_write(RF230_REG_XOSC_CTRL,0xF0);
	
	/* CLKM signal configuration
	 Master Clock Signal Output (CLKM) 
		The generated reference clock signal can be fed to a microcontroller using pin 17 
		(CLKM). The internal 16 MHz raw clock can be divided by an internal prescaler. Thus, 
		clock frequencies of 16 MHz, 8 MHz, 4 MHz, 2 MHz or 1 MHz can be supplied by 
		pin CLKM.
	The TRX_CTRL_0 register controls the drive current of the digital output pads and the 
	CLKM clock rate
	
	No clock, pin set to logic low 
	*/
	rf230_register_write(RF230_REG_TRX_CTRL_0,0x00);
	
	
	/* RF Channel Selection 
	The PLL is fully integrated and configurable by registers 0x08 (PHY_CC_CCA), 0x1A (PLL_CF) and 0x1B (PLL_DCU).
	
	The PLL is turned on when entering the state PLL_ON and stays on in all receive and 
	transmit states. The PLL settles to the correct frequency needed for RX or TX operation 
	according to the adjusted channel center frequency in register 0x08 (PHY_CC_CCA). 
	Two calibration loops ensure correct PLL functionality within the specified operating limits
	
	The center frequency control loop ensures a correct center frequency of the VCO for the currently programmed channel.  
	The delay calibration unit compensates the phase errors inherent in fractional-N PLLs. 
	Using this technique, unwanted spurious frequency components beside the RF carrier 
	are suppressed, and the PLL behaves similar to an integer-N PLL. 
	Both calibration routines are initiated automatically when the PLL is turned on. 
	Additionally, the center frequency calibration is running when the PLL is programmed to 
	a different channel (register bits CHANNEL in register 0x08).
	 
	If the PLL operates for a long time on the same channel or the operating temperature 
	changes significantly, the control loops should be initiated manually. The recommended 
	calibration interval is 5 min.  
	Both calibration loops can be initiated manually by setting PLL_CF_START = 1 of 
	register 0x1A (PLL_CF) and register bit PLL_DCU_START = 1 of register 0x1B (PLL_DCU).
	*/
	/*
	Register 0x04 (TRX_CTRL_1):
	The TRX_CTRL_1 register is a multi-purpose register to control various operating modes and settings of the radio transceiver.
	
	Bit 3:2 - SPI_CMD_MODE
	Each SPI transfer returns bytes back to the SPI master. The content of the first byte (PHY_STATUS) can be configured using register bits SPI_CMD_MODE.
	
	*/
	//rf230_register_write(RF230_REG_TRX_CTRL_1,0x0C); //Monitor PHY_RSSI register, SPI_CMD_MODE=2
	
	//set_frame_retries(10);

	
	
		
/*		
Note: The following is important to ensure correct operation of the radio transceiver: 
	1. MAX_FRAME_RETRIES must always equal zero. A frame will not be retried 
	after the CSMA-CA algorithm has failed. This must be handled in software. 
	2. The CSMA_SEED parameter should not be altered from its reset value, since 
	this will corrupt the operation of the random number generator. 

		Register 0x2C (XAH_CTRL)  
		The XAH_CTRL register controls the TX_ARET transaction in the Extended Operating 
		Mode of the radio transceiver.
		
		Bit [7:4] � MAX_FRAME_RETRIES 
		MAX_FRAME_RETRIES specifies the maximum number of frame retransmission in 
		TX_ARET transaction. 
		
		Bit [3:1] � MAX_CSMA_RETRIES 
		MAX_CSMA_RETRIES specifies the maximum number of retries in TX_ARET 
		transaction to repeat the random back-off/CCA procedure before the transaction gets 
		canceled. Even though the valid range of MAX_CSMA_RETRIES is [0, 1 � 5] 
		according to IEEE 802.15.4-2003, the AT86RF230 can be configured up to a maximum 
		value of 7 retries. 
		Bit 0 � Reserved   
*/

		rf230_register_write(RF230_REG_XAH_CTRL,0x1A);//MAX_CSMA_RETRIES = 5 & MAX_FRAME_RETRIES = 1
		
		 
/*		Register 0x2D (CSMA_SEED_0)  
		The CSMA_SEED_0 register contains a fraction of the CSMA_SEED value for the 
		CSMA-CA algorithm.
		
		Bit [7:0] � CSMA_SEED_0 
		The register bits CSMA_SEED_0 contain the lower 8 bits of the CSMA_SEED. The 
		upper 3 bits are stored in register  bits CSMA_SEED_1 of register 0x2E 
		(CSMA_SEED_1). CSMA_SEED is the seed of the random number generator for the 
		CSMA-CA algorithm.  
*/
		strncpy (c_txt, __TIME__,8 ); // compile time
		
		rf230_register_write(RF230_REG_CSMA_SEED_0,c_txt[1]);//let compile time generate random number
		
/*		
		Register 0x2E (CSMA_SEED_1)  1100   0000
		The CSMA_SEED_1 register contains a fraction of the CSMA_SEED value, the back-
		off exponent for the CSMA-CA algorithm and configuration bits for address filtering in 
		RX_AACK operation. 
    
		Bit [7:6] � MIN_BE    set to 11, max
		The register bit MIN_BE defines the minimal back-off exponent used in the CSMA-CA 
		algorithm to generate a pseudo random number for back-off the CCA. For details refer 
		to IEEE 802.15.4-2003 section 7.5.1.3. If set to zero, the start of the CCA algorithm is 
		not delayed. 
		Bit 5 � AACK_SET_PD 0
		This register bit defines the content of the frame pending subfield for acknowledgement 
		frames in RX_AACK mode. If this bit is enabled the frame pending subfield of the 
		acknowledgment frame is set in response to a MAC command data request frame, 
		otherwise not. The register bit has to be set before finishing the SHR transmission of 
		the acknowledgment frame. This is 352 �s (192 �s ACK wait time + 160 �s SHR 
		transmission) after the TRX_END interrupt issued by the frame to be acknowledged. 
		Bit 4 � Reserved  0
		Bit 3 � I_AM_COORD 0
		This register bit has to be set if the node is a PAN coordinator. This register bit is used 
		for address filtering in RX_AACK. If I_AM_COORD = 1 and if only source addressing 
		fields are included in a data or MAC command frame, the frame shall be accepted only 
		if the device is the PAN coordinator and the source PAN identifier matches macPANId, 
		for details refer to IEEE 802.15.4-2003, section 7.5.6.2 (third-level filter rules). 
		Bit [2:0] � CSMA_SEED_1 set to 000
		The register bits CSMA_SEED_1 contain  the upper 3 bits of the CSMA_SEED. The 
		lower part is defined in register 0x2D (CSMA_SEED_0). 
*/
		//rf230_register_write(RF230_REG_CSMA_SEED_1,0xC0); //  AACK_SET_PD = 0, ACK for one frame at the time 
		
		rf230_register_write(RF230_REG_CSMA_SEED_1,0xE0); //  AACK_SET_PD = 1, I_AM_COORD =  0
		
		//TODO set MAC filtering address 
		

        
}
