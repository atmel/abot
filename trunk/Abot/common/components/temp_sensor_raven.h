/*
 * Temperature sensor
 *
 * Created: 
 *  Author: 
 */ 

#define		TEMP_DEG_C		0
#define		TEMP_FAHREN		1

/*	\brief	Init temperature sensor
*			Initialize temperature power control PIN
*/
void init_temp_sensor(void);

/*	\brief read current temperature and return value in degC or Fahrenheit
*	\param	unit	temperature reading in degC or Fahrenheit
*	\return	temp_reading_degC/FH	temperature reading in degree Centigrade or Fahrenheit
*/
uint16_t get_temperature_reading(uint8_t unit);

void test_temp(void);