/**
 * \file  Compass_AK8975.h
 *
 * \brief The Compass component
 *
 * \author    TB
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef COMPASS_AK8975_H_
#define COMPASS_AK8975_H_

#ifndef M_PI
#   define M_PI                 (3.14159265358979323846264338327)
#endif

#define RAD_TO_DEG              (180 / M_PI)    //!< radians to degrees

#define degrees(a)  (RAD_TO_DEG * (a))


/*
 * Definitions for akm8975 compass chip.
*/


/*! \name AK8975 operation mode
 \anchor AK8975_Mode
 Defines an operation mode of the AK8975.*/
/*! @{*/
#define AK8975_MODE_SNG_MEASURE	0x01
#define	AK8975_MODE_SELF_TEST	0x08
#define	AK8975_MODE_FUSE_ACCESS	0x0F
#define	AK8975_MODE_POWERDOWN	0x00
/*! @}*/

/*! \name AK8975 register address
\anchor AK8975_REG
Defines a register address of the AK8975.*/
/*! @{*/
#define AK8975_REG_WIA		0x00
#define AK8975_REG_INFO		0x01
#define AK8975_REG_ST1		0x02
#define AK8975_REG_HXL		0x03
#define AK8975_REG_HXH		0x04
#define AK8975_REG_HYL		0x05
#define AK8975_REG_HYH		0x06
#define AK8975_REG_HZL		0x07
#define AK8975_REG_HZH		0x08
#define AK8975_REG_ST2		0x09
#define AK8975_REG_CNTL		0x0A
#define AK8975_REG_RSV		0x0B
#define AK8975_REG_ASTC		0x0C
#define AK8975_REG_TS1		0x0D
#define AK8975_REG_TS2		0x0E
#define AK8975_REG_I2CDIS	0x0F
/*! @}*/

/*! \name AK8975 fuse-rom address
\anchor AK8975_FUSE
Defines a read-only address of the fuse ROM of the AK8975.*/
/*! @{*/
#define AK8975_FUSE_ASAX	0x10
#define AK8975_FUSE_ASAY	0x11
#define AK8975_FUSE_ASAZ	0x12
/*! @}*/



#define COMPASS_ADDRESS	(0x0C)		// Compass I2C slave address, dependent on HW select signals
//#define COMPASS_INT_VECT _VECTOR(21)		// Compass Interrupt vector, depending on where data ready signal from compass connected

/*Interrupt flags defined*/
unsigned char compass_int_flag;

//Compass filed data
typedef struct
{
	
	float X_axis;
	float Y_axis;
	float Z_axis;
	
	//double X_axis;
	//double Y_axis;
	//double Z_axis;
	//int16_t X_axis;
	//int16_t Y_axis;
	//int16_t Z_axis;
	uint8_t  sensor_status;
	uint8_t  bus_status;
	
} compass_field;


// Compass heading data
typedef struct
{
	uint16_t	direction;		// Degrees from magnetic North (0-360)
	int8_t		inclination;	// Degrees from horizontal (-90 to +90) 
	//float	direction;		// Degrees from magnetic North (0-360)
	//float		inclination;	// Degrees from horizontal (-90 to +90) 
	//double		direction;		// Degrees from magnetic North (0-360)
	//double		inclination;	// Degrees from horizontal (-90 to +90) 
	int16_t		strength;		// Field strength in microtesla

} compass_heading;


/* Compass API */
void Compass_init(void);
uint8_t set_compass_mode(uint8_t mode);
compass_field get_compass_raw_data(void);
compass_heading calculate_heading (compass_field field);

	
#endif /* COMPASS_AK8975_TB_H_ */