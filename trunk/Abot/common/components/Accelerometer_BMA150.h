/**
 * \file  Accelerometer_BMA150.h
 *
 * \brief The Accelerometer component
 *
 * \author    SH
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef ACCELEROMETER_BMA150_H_
#define ACCELEROMETER_BMA150_H_

/*
 * Definitions for BMA150 accelerometer chip.
*/


/*! \name BMA150 register address
\anchor BMA150_REG
Defines a register address of the BMA150.*/
/*! @{*/
#define BMA150_REG_CHIP_ID                 0x00
#define BMA150_REG_AL_ML_VERSION           0x01
#define BMA150_REG_ACC_DATA_X_0            0x02
#define BMA150_REG_MODE_CONTROL			   0x0B
#define BMA150_REG_ANY_MOTION_THRES		   0x10
#define BMA150_REG_ANY_MOTION_DUR		   0x11
#define BMA150_REG_BANDWIDTH_RANGE_CONTROL 0x14
#define BMA150_REG_INT_CONTROL             0x15
/*! @}*/

// BMA150 bandwidth mask for BMA150_REG_BANDWIDTH_RANGE_CONTROL
#define BMA150_BANDWIDTH_MASK 0x07

// BMA150 any_motion duration mask for BMA250_REG_ANY_MOTION register
#define BMA150_ANY_MOTION_DUR_MASK 0xC0

/*! \name BMA150 any_motion duration setting
\anchor BMA150_ANY_MOTION_DUR
Defines a duration setting for the any_motion mode of the BMA150.*/
/*! @{*/
#define BMA150_ANY_MOTION_DUR_1 0x00
#define BMA150_ANY_MOTION_DUR_3 0x40
#define BMA150_ANY_MOTION_DUR_5 0x80
#define BMA150_ANY_MOTION_DUR_7 0xC0
/*! @}*/

/*! \name BMA150 bandwidth setting
\anchor BMA150_BANDWIDTH
Defines a bandwidth setting of the BMA150.*/
/*! @{*/
#define BMA150_BANDWIDTH_25_HZ 0x00
#define BMA150_BANDWIDTH_1500_HZ 0x06
/*! @}*/

// BMA150 range mask for BMA150_REG_BANDWIDTH_RANGE_CONTROL
#define BMA150_RANGE_MASK 0x18

/*! \name BMA150 range setting
\anchor BMA150_RANGE
Defines a range setting of the BMA150.*/
/*! @{*/
#define BMA150_RANGE_2_G 0x00
#define BMA150_RANGE_4_G 0x08
#define BMA150_RANGE_8_G 0x10
/*! @}*/

/*! \name BMA150 INT_CONTROL register bits
\anchor BMA150_INT_CONTROL
Defines bit numbers for the INT_CONTROL register (0x15) in the BMA150 chip*/
/*! @{*/
#define BMA150_INT_CONTROL_NEW_DATA_INT 5
#define BMA150_INT_CONTROL_ENABLE_ADV_INT 6
/*! @}*/

/*! \name BMA150 MODE_CONTROL register bits
\anchor BMA150_MODE_CONTROL
Defines bit numbers for the MODE_CONTROL register (0x0B) in the BMA150 chip*/
/*! @{*/
#define BMA150_MODE_CONTROL_ANY_MOTION 6
/*! @}*/

// BMA150 chip id mask
#define BMA150_CHIP_ID_MASK 0x03

// BMA150 chip id 
#define BMA150_CHIP_ID 0x02

// Accelerometer I2C slave address
#define BMA150_ADDRESS	(0x38)

//Accelerometer data (raw data from accelerometer)
typedef struct
{
	
    // Accelerometer data is given in g (1 g = 9807 mm/s^2, 0.004g ~ 39 mm/s^2 ). For a range of +/- 2g:
    // 10 0000 0000: -2.000g 
    // 10 0000 0001: -1.996g
    // ...
    // 11 1111 1111: -0.004g
    // 00 0000 0000: 0.000g
    // 00 0000 0001: +0.004g
    // ...
    // 01 1111 1110: +1.992g
    // 01 1111 1111: +1.996g

    uint16_t acc_x;
    uint16_t acc_y;
    uint16_t acc_z;
    
    // Timestamp for acceleration data. 
    uint16_t timestamp;
	
} accelerometer_data_t;

// Three dimensional relative position including speed.
typedef struct
{
    // UNITS:
    // position: um
    // velocity: mm/s = um/ms
    // um are used since we are looking at small speeds in small intervals (16 ms interval, speed in the range of cm/s)
    
    // relative position, x-axis
    int64_t pos_x;
    // velocity, x-axis
    int32_t vel_x;
    // relative position, y-axis
    int64_t pos_y;
    // velocity, y-axis
    int32_t vel_y;
    // relative position, z-axis
    int64_t pos_z;
    // velocity, z-axis
    int32_t vel_z;

} position_t;

// Globals
/*Interrupt flags defined*/
uint8_t accelerometer_int_flag;

/*Current position and velocity */
position_t position;

/* Current accelerometer sample */
accelerometer_data_t accelerometer_data;

// Accelerometer modes
// Accelerometer data used to calculate position and speed
#define ACCELEROMETER_MODE_POSITION 0x00
// Accelerometer set up to give interrupt on start and stop
#define ACCELEROMETER_MODE_MOTION_ALERT 0x01

// Threshold for any motion interrupt 
// LSB size corresponds to 15.6 mg for +/- 2g range 
#define ACCELEROMETER_ANY_MOTION_THRESHOLD 0x02

/*! \name Accelerometer status codes
\anchor BMA150_STATUS
Defines return statuses for the BMA150 functions.*/
/*! @{*/
#define BMA150_STATUS_OK 0x00
#define BMA150_STATUS_TWI_ERROR 0x01
#define BMA150_STATUS_INVALID_CHIP_ID 0x02
/*! @}*/

/* Accelerometer API */
uint8_t accelerometer_init(uint8_t mode);
uint8_t accelerometer_write_register(uint8_t address, uint8_t data);
uint8_t accelerometer_read_registers(uint8_t address, uint8_t no_of_bytes, uint8_t *data);
uint8_t accelerometer_get_raw_data(accelerometer_data_t *accelerometer_data);
int32_t accelerometer_convert_acc_data (uint16_t acc_data);
position_t accelerometer_get_posistion (accelerometer_data_t accelerometer_data, position_t current_position);
	
#endif /* ACCELEROMETER_BMA150_TB_H_ */
