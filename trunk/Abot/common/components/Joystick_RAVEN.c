/**
 * \file Joystick_RAVEN.c
 *
 * \brief The Joystick component for Raven
 *
 * \author    TB
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h> 

//Solution includes
#include <drivers/drv_3290_adc.h>

#include "Joystick_RAVEN.h"

/* Pin Change Interrupt Request 2 */
/* Enter is configured as input w/pullup -- CTR is connected to PE2 (PCINT2)*/
ISR(PCINT0_vect)
{
	extern unsigned char PCINT0_int_flag;

		PCINT0_int_flag = 1;
}

/*---------------------------------------------------------------------------*/

/**
 *   \brief This will intialize the joystick and the ADC for button readings.
*/
void Joystick_init(void)
{
	extern unsigned char PCINT0_int_flag;
	extern unsigned char joy_mode;
	
    /* Disable digital input buffer for joystick signal 
		UP-DOWN-LEFT-RIGHT is connected to PF1 (ADC1)*/
	/* Digital Input Disable Register 0
	Bit 7:0 � ADC7D:ADC0D: ADC7:0 Digital Input Disable
		When this bit is written logic one, the digital input buffer on the corresponding ADC pin is disabled.
		The corresponding PIN Register bit will always read as zero when this bit is set. When an
		analog signal is applied to the ADC7:0 pin and the digital input from this pin is not needed, this
		bit should be written logic one to reduce power consumption in the digital input buffer.
	*/
    DIDR0 |= (1 << ADC1D);

    /* Enter is configured as input w/pullup -- CTR is connected to PE2 (PCINT2)*/
	/* 
		PE2 is configured as an input pin. 
		DDRE � Port E Data Direction Register, bit2 DDE2 = 0
		
		If PORTxn is written logic one when the pin is configured as an input pin, the pull-up resistor is activated.
		PORTE � Port E Data Register
	
	*/
	DDRE &= ~(1<<DDE2); // bit2 DDE2 = 0
	PORTE |= (1<<PINE2); // bit 2 in PORTE = 1


    /* Joystick (PF1) is configured as input wo/pullup (all though normal port function is overridden by ADC module when reading) */
	
    DDRF &= ~(1<<DDF1);  // bit 1 DDF1 = 0
    PORTF &= ~(1<<PINF1); // bit 1 in PORTF = 0

    /* Get the ADC ready to use */
    adc_init(ADC_CHAN_ADC1, ADC_TRIG_FREE_RUN, ADC_REF_AVCC, ADC_PS_128);
	
	/* Enable Interrupt on Enter
	EIMSK � External Interrupt Mask Register
	Bit 4 � PCIE0: Pin Change Interrupt Enable 0
		When the PCIE0 bit is set (one) and the I-bit in the Status Register (SREG) is set (one), pin
		change interrupt 0 is enabled. Any change on any enabled PCINT7:0 pin will cause an interrupt.
		The corresponding interrupt of Pin Change Interrupt Request is executed from the PCINT0 Interrupt
		Vector. PCINT7:0 pins are enabled individually by the PCMSK0 Register.
	
	*/
	EIMSK |= (1<<PCIE0); //PCIE0 bit = 1
	
	/*
	PCMSK0 � Pin Change Mask Register 0
	Bit 7:0 � PCINT7:0: Pin Change Enable Mask 7:0
		Each PCINT7:0 bit selects whether pin change interrupt is enabled on the corresponding I/O pin.
		If PCINT7:0 is set and the PCIE0 bit in EIMSK is set, pin change interrupt is enabled on the corresponding
		I/O pin. If PCINT7:0 is cleared, pin change interrupt on the corresponding I/O pin is disabled.
	*/
	PCMSK0 |= (1<<PCINT2);
	
	PCINT0_int_flag = 0;	// Clr interrupt flag
	joy_mode = 0;			//joy mode not active
}



/**
 *    This function will start the ADC conversion and read the current
 *   conversion value to determine the button position.
 *
 *   KEY_ENTER Enter button has been pressed.
 *   KEY_UP Up Button has been pressed.
 *   KEY_RIGHT Right Button has been pressed.
 *   KEY_LEFT Left Button has been pressed.
 *   KEY_DOWN Down Button has been pressed.
 *   KEY_NO_KEY No Button has been pressed.
*/

unsigned char get_key_state(void)
{
    int16_t reading;

    // Start the A/D conversion
    adc_conversion_start();

    // Wait for conversion to finish 
    while ((reading = adc_result_get(ADC_ADJ_RIGHT)) == EOF )
        ;
	
    // Determine which button (if any) is being pressed
    if (!(PINE & (1<<PINE2))){
        return KEY_STATE_ENTER;
    }
    if (reading < 0x00A0){
        return KEY_STATE_UP;
    }
    if (reading < 0x0180){
        return KEY_STATE_RIGHT;
    }
    if (reading < 0x0280){
        return KEY_STATE_LEFT;
    }
    if (reading < 0x0380){
        return KEY_STATE_DOWN;
    }
    return KEY_STATE_NO_KEY;
}




/* =========================================================================================== */



