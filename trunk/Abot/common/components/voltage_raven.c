/**
 * \file voltage_raven.c
 *
 * \brief battery voltage measurement functions
 *
 * \author    
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <drivers/drv_3290_adc.h>
#include <components/VOLTAGE_RAVEN.h>

/*	\brief return adc reading for the voltage input on channel3 (port pin PF3)
*	\return 16bit adc value
*/
uint16_t get_adc_ext_voltage(void)
{
	int16_t adc_val;
	adc_simple_init(ADC_CHAN_ADC3);
	adc_val = adc_read();
	return adc_val;	
}

/*	\brief	read the ADC value for the 1100mV band gap reference. 
*			calculate battery voltage VCC from ADC = ( 1024*1100/VCC)
*	\return supply(battery) voltage in mV
*/
uint16_t get_supply_voltage(void)
{
	uint16_t bandgap_adc_val;
	uint16_t bat_voltage;
	adc_simple_init(ADC_CHAN_ADC14);
	adc_read();//dummy read	
	//read adc reading for supply voltage
	bandgap_adc_val = adc_read();	
	//convert ADC value	to battery voltage
	bat_voltage = ((uint32_t)1100 << 10)/bandgap_adc_val;	
	return bat_voltage;
}

/*	\brief	get external voltage reading on J401.6
*			measure voltage signal connected to 1k-100E resistor network
*	\return supply voltage in mV
*/
uint16_t read_external_voltage(uint8_t mode)
{
	uint16_t vcc_voltage_mv; //supply(battery) voltage in mV
	uint16_t ext_voltage_mv; //external voltage in mV
	uint16_t adc_ext_voltage_reading;
	uint16_t ext_voltage_devided;
	//measure internal Vcc for calibration
	vcc_voltage_mv = get_supply_voltage();	
	//read external voltage ADC channel
	adc_ext_voltage_reading = get_adc_ext_voltage();
	//check the measurement mode
	switch(mode)
	{
		case EXT_VOLT_0_5VCC: //external voltage reading connected to pin J401.5
			//calculate the divided ext. voltage reading in mV,use the calibrated value of AVCC for calculation
			//ADC = (ext_5vcc)*1024/vcc_voltage
			ext_voltage_devided = (((uint32_t)adc_ext_voltage_reading * vcc_voltage_mv) >> 10);
			//conversion VOLT_SIG = (Vext * 100) / 571
			ext_voltage_mv = (uint32_t)ext_voltage_devided * 571/100;
			break;
		
		case EXT_VOLT_0_VCC://external voltage reading connected to pin J401.6
			//calculate the divided ext. voltage reading in mV,use the calibrated value of AVCC for calculation
			//ADC = (ext_5vcc)*1024/vcc_voltage
			ext_voltage_devided = (((uint32_t)adc_ext_voltage_reading * vcc_voltage_mv) >> 10);;
			//conversion VOLT_SIG = (Vext * 100) / 571
			ext_voltage_mv = (uint32_t)ext_voltage_devided * 101/100;
			break;

		default:
			//invalid option
			break;
	}
	return ext_voltage_mv;			
}

//test function
void voltage_test(void)
{
	volatile uint16_t adc_bat_read;
	volatile uint16_t adc_ext5Vcc_read;
	volatile uint16_t adc_extVcc_read;
	adc_bat_read = get_supply_voltage();	
	adc_ext5Vcc_read = read_external_voltage(EXT_VOLT_0_5VCC);	
	adc_extVcc_read = read_external_voltage(EXT_VOLT_0_VCC);	

}