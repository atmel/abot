/**
 * \file  MOTOR_SERVO_360.h
 *
 * \brief The ABOT servo motor component
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef ABOT_TB_H_
#define ABOT_TB_H_

/* Definitions */
#define ABOT_CAL_A_ADDR   (0x0042)          // EEPROM location pointing to the calibration offset
#define ABOT_CAL_B_ADDR   (0x0044)
#define ABOT_CAL_SPEED_ADDR   (0x0046)

/* Global Variables */
unsigned int ABOT_PW_A;            //Pulse Width A, starboard motor
unsigned int ABOT_PW_B;            //Pulse Width B, port motor
unsigned int ABOT_CAL_A;           //Calibration value starboard motor
unsigned int ABOT_CAL_B;           //Calibration value port motor
unsigned int ABOT_CAL_SPEED;       //Calibration value speed offset
unsigned char ABOT_STOPPED;        //ABOT STOP command issued
unsigned char ABOT_SWITCH_ENABLED; //ABOT switch action enabled
unsigned char ABOT_AUTO_ENABLED;   //ABOT automatic control enabled

/* Global functions*/
void Motor_init (void);                          //Initialize motor
void ABOT_motor_ctrl(unsigned char position);    //command motor directions
void ABOT_motor_speed_ctrl(unsigned char cmd, unsigned char val);	// command control motor speed & directions
void ABOT_reverse_turn_forward(void);            //Motor reversed, ABOT turned, and motor in forward again
void ABOT_Trix(char No);
void ABOT_command(unsigned char cmd);            //Execute ABOT motor commands
void ABOT_auto(void);  //Execute automatic commands

#endif /* ABOT_TB_H_ */