/*
 * board.h
 *  for RXUSBSTICK
 * Created: 28/01/2012 09:41:26
 *  Author: TB
 */ 


#ifndef BOARD_RZUSB_H_
#define BOARD_RZUSB_H_

//#define FOSC 8000000		// CPU Clock Speed	- defined in kHz in config.h!
#define F_CPU 8000000UL		// 1 MHz CPU clk, used in delay 
#include <util/delay.h>		//_delay_us()

// Define SW version SW ID (text string 10 characters max)
#define VERSION_APP      "RZUSBSTICK" //10 char

// Define Mr. Abot's family name (text string 10 characters max)
#define FAMILY_NAME      "Bjornvold"

// Default PAN network addresses
#define MAC_SHORT_DEFAULT 0x4242  //Abot network coordinator (Odin)
#define MAC_SHORT_DEFAULT_DEST 0xFFFF //broadcast

//_____ M A C R O S _____TODO: replace macros with functions in component LED.h________________________________________________


//! Macros to manage LEDs
//! The led 0 correspond at led LED1  PD7 (VBUS) 1=on
//! The led 1 correspond at led LED2  PD5
//! The led 2 correspond at led LED3  PE7
//! The led 3 correspond at led LED4  PE6
// LEDs are connected to Vcc, i.e. ON = "0"
      
#define  LED_PORT             PORTD
#define  LED_DDR              DDRD
#define  LED_PIN              PIND
#define  LED0_BIT             PIND7
#define  LED1_BIT             PIND5
#define  LED2_BIT             PINE7
#define  LED3_BIT             PINE6
                      
#define  Leds_init()          ((LED_DDR |= (1<<LED0_BIT) | (1<<LED1_BIT)),(DDRE |= (1<<LED2_BIT) | (1<<LED3_BIT)))

//#define  Leds_on()            (LED_PORT |= (1<<LED0_BIT) | (1<<LED1_BIT) | (1<<LED2_BIT) | (1<<LED3_BIT))
//#define  Leds_off()           (LED_PORT &= ~((1<<LED0_BIT) | (1<<LED1_BIT) | (1<<LED2_BIT) | (1<<LED3_BIT)))
//#define  Leds_set_val(c)      (Leds_off(),LED_PORT |= (c<<4)&((1<<LED0_BIT) | (1<<LED1_BIT) | (1<<LED2_BIT) | (1<<LED3_BIT)))
//#define  Leds_get_val()       (LED_PORT>>4)

#define  Led0_on()            (LED_PORT |=  (1<<LED0_BIT))
#define  Led1_off()            (LED_PORT |=  (1<<LED1_BIT))
#define  Led2_off()            (PORTE |=  (1<<LED2_BIT))
#define  Led3_off()            (PORTE |=  (1<<LED3_BIT))
#define  Led0_off()           (LED_PORT &= ~(1<<LED0_BIT))
#define  Led1_on()           (LED_PORT &= ~(1<<LED1_BIT))
#define  Led2_on()           (PORTE &= ~(1<<LED2_BIT))
#define  Led3_on()           (PORTE &= ~(1<<LED3_BIT))
#define  Led0_toggle()        (LED_PIN  |=  (1<<LED0_BIT))
#define  Led1_toggle()        (LED_PIN  |=  (1<<LED1_BIT))
#define  Led2_toggle()        (PINE  |=  (1<<LED2_BIT))
#define  Led3_toggle()        (PINE  |=  (1<<LED3_BIT))
//#define  Is_led0_on()         (LED_PIN  &   (1<<LED0_BIT) ? TRUE : FALSE)
//#define  Is_led1_on()         (LED_PIN  &   (1<<LED1_BIT) ? TRUE : FALSE)
//#define  Is_led2_on()         (PINE  &   (1<<LED2_BIT) ? TRUE : FALSE)
//#define  Is_led3_on()         (PINE  &   (1<<LED3_BIT) ? TRUE : FALSE)



#endif /* BOARD_H_ */