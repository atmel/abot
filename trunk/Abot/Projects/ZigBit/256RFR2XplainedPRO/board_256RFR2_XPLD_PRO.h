/**
 * \file board_256RFR2_XPLD_PRO.h 
 *
 * \brief board definition file for Munin (mega1284) on the RAVEN kit
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#ifndef BOARD_256RFR2_XPLD_PRO_H_
#define BOARD_256RFR2_XPLD_PRO_H_

#define FOSC 8000000		// CPU Clock Speed in MHz
#define F_CPU 8000000UL		// 1 MHz CPU clk, used in delay 
#include <util/delay.h>		//_delay_us()

// Define SW version ID (text string 10 characters max)
#define VERSION_APP      "256XPRObot" //10 char

// Define Mr. Abot's family name (text string 10 characters max)
#define FAMILY_NAME      "Bjornvold"

// Default PAN network addresses
#define MAC_SHORT_DEFAULT 0x422A
#define MAC_SHORT_DEFAULT_DEST 0x4242  //LAN admin

//TWI speed
#define TWI_SPEED_STANDARD  100000		// standard, 100k bit-per-second
#define	TWI_SPEED_FAST      400000		// fast, 400k bit-per-second
#define	TWI_SPEED           TWI_SPEED_STANDARD //Selected speed

#endif /* BOARD_H_ */