/*
 * _256RFR2XplainedPRO.c
 *
 * Created: 26.01.2013 17:10:46
 *  Author: Torgeir
 */ 

/**
 * \file 256RFR2XplainedPRO.c 
 *
 * \brief main loop for robot application running on the mega256RFR2 Xplained PRO kit
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */



//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

//Abot common includes
#include <services/ABOT_protocol.h>
#include <services/USART_ABOT_trm.h>
#include <services/RADIO_ABOT_cmd.h>
#include <components/MOTOR_SERVO_360.h>
#include <components/LED_ABOT.h>
#include <components/switch_ABOT.h>


//Global variables
extern unsigned char rand_seed; // random number for MAC short address request


/* ============= Set Fuses ======================================================================== */
/*

Fuse Low Byte =0xD8  1101 1000
	CKDIV8[7]  = "1" (disabled)
	CKOUT[6]   = 1 (disabled)
	SUT = "01" Select start-up time
	CKSEL = 16MHz external
Fuse High Byte =0x1F   0001 1111
	 OCDEN[7] = 0 (enabled)
	 JTAG[6]  = 0 (enabled)
	 SPIEN[5] = 0 (enabled)
	 WDTON[4] = 1 (disabled) Watchdog Timer always off
	 EESAVE[3]= 1 (disabled) EEPROM not preserved through the Chip Erase
	 BOOTSZ1:0=11 Boot Size = 512 word Boot Reset Address = 0x1FE00 (table s.491)
 
	 BOOTRST= 1 (disabled) Select Reset Vector disabled, Reset Vector = Application Reset (address 0x0000)
 
 Extended Fuse Byte =0xFF 
	No BOD
 
 Note that the fuses are read as logical zero, �0�, if they are programmed.
 */


#include <avr/fuse.h>
FUSES =
	{
		.low = 0xDE,
		.high = 0x1F,
		.extended = 0xff,
	};
//============= Set Fuses END========================================================================
	

	
/*
If some pins are unused, it is recommended to ensure that these pins have a defined level. Even
though most of the digital inputs are disabled in the deep sleep modes as described above, floating
inputs should be avoided to reduce current consumption in all other modes where the digital
inputs are enabled (Reset, Active mode and Idle mode).
The simplest method to ensure a defined level of an unused pin, is to enable the internal pull-up.
In this case, the pull-up will be disabled during reset. If low power consumption during reset is
important, it is recommended to use an external pull-up or pull-down. Connecting unused pins
directly to VCC or GND is not recommended, since this may cause excessive currents if the pin is
accidentally configured as an output.
*/






void task_USART_rx_trm(void) //Take action on received message on USART from terminal
{
	extern unsigned char RX1_int_flag;
	extern unsigned char trm_cmd_int_flag;
	extern unsigned char last_LQI;
	unsigned char cmd;	
	char debugs[0x50];
	char txts[0x10];
	unsigned char t;
	uint16_t m;
	board_data boardData;
	
	if (trm_cmd_int_flag) // Data from terminal sent by enter
	{		
		cmd=USART_Get_trm_cmd();
		switch (cmd)
		{		
			case HELP:                                 // Respond on a HELP command from terminal - display command menu
				USART_printf_trm("        Help menu");
				USART_printf_trm("ping  : transmit PING msg");
				USART_printf_trm("rfi   : display RF details");
				USART_printf_trm("test  : issue test cmd");
				USART_printf_trm("ver   : display version");
			break;
			
			case PING_A:	
				USART_printf_trm("RF ping transmitted"); 
				transmit_RADIO_PING(0x42);             // Transmit PING on RF on request from terminal					
			break; 
			
			case TEST:                                      
				USART_printf_trm("test");
				transmit_RADIO_DISPLAY("test");
			break;
			
			case VERSION:
				USART_print_VERSION();
				boardData = get_board_id();
				for (t = 0; t < 8; t++)
				{
					txts[t] = boardData.board_id[t];
				}
				txts[8] = NULL;
				//USART_printf_trm(debugs);
				sprintf(debugs,"PCBA %s revision 0x%X",txts,boardData.board_revision);
				USART_printf_trm(debugs);
			break;
			
			case HEI:
				USART_printf_trm("Hei :-)");
				USART_print_VERSION();
			break;
			
			case RFINFO:  //display RF info
				t = get_RADIO_state();	
				switch (t)
				{
					case 0x08: sprintf(debugs, "RF state: 0x%X TRX_OFF",t); break;
					case 0x09: sprintf(debugs, "RF state: 0x%X PLL_ON",t); break;
					case 0x16: sprintf(debugs, "RF state: 0x%X RX_AACK_ON",t); break;
					default:   sprintf(debugs, "RF state: 0x%X ",t); break;
				}//Switch state
				USART_printf_trm(debugs);
				
				t = get_RADIO_RSSI();
				sprintf(debugs, "LQI last frame: 0x%X, RSSI: 0x%X ",last_LQI,t);
				USART_printf_trm(debugs);
				
				t=get_RF_channel();
				sprintf(debugs, "RF channel: 0x%X ",t);
				USART_printf_trm(debugs);		
						
				t= get_RF_output_pwr();
				sprintf(debugs, "RF output pwr: 0x%X (0x0 = +3dBm, 0xF = -17.2dBm)",t);
				USART_printf_trm(debugs);
				
				m=get_RADIO_PANID_filter();
				sprintf(debugs, "RF PAN ID: r0x%X/v0x%X",m,MAC_PAN_ID);
				USART_printf_trm(debugs);
				
				m=get_RADIO_SHORT_filter();	
				sprintf(debugs, "RF my short adr: r0x%X/v0x%X",m, MAC_SHORT);
				USART_printf_trm(debugs);
					
				sprintf(debugs, "RF destination adr: 0x%X",MAC_SHORT_dest);
				USART_printf_trm(debugs);
				
				t=get_IRQ_status();
				sprintf(debugs, "IRQ status reg: 0x%X",t);
				USART_printf_trm(debugs);
				IRQ_STATUS = 0xFF;
						
				for (t = 1; t < RTG_table[0].node_short_adr+1; t++)
				{
					sprintf(debugs, "Routing table[%i]: 0x%X Live:%X",t,RTG_table[t].node_short_adr,RTG_table[t].live);
					USART_printf_trm(debugs);
				}
			break;
					
			default: 
				sprintf(debugs, "UNKNOWN COMMAND, CMD 0x%X not supported",cmd);
				USART_printf_trm(debugs);
			break;
		} //switch (cmd)		
			
		RX1_int_flag=0;
		trm_cmd_int_flag=0;
		
	} //if (RX0_int_flag) 	
}


/*! \brief The RF network interface
 *
 * Monitor the RF traffic and respond according to the functions implemented
 *
 * 
 */

void task_RF(void)
{
	
	
	extern unsigned char MAC_RANDOM1; // number used to identify node when requesting new address
	extern unsigned char MAC_RANDOM2;
	
//Global variables	
	extern unsigned char RF_TX_END_int_flag; // Indicates the completion of a frame transmission
	extern unsigned char RF_RX_END_int_flag; // Indicates the completion of a frame reception
	extern unsigned char RF_int_flag; //General RF IRQ 
	extern unsigned char last_LQI;
	
//Local variables	

	RADIO_frame RF_f;
	unsigned char state;
	unsigned char status;

	uint16_t i, i2;
	//uint64_t ieee_adr;
	
	unsigned char t;
	char debugs[0x50];
	
	if (RF_TX_END_int_flag) // Indicates the completion of a frame transmission
	{
		LED_off(RED);//Reset error indicator
		RF_TX_END_int_flag = 0;
		t = get_RADIO_TX_status();
		if (t != 0) //Error?
		{
			LED_on(RED);
			sprintf(debugs,"Frame transmission ERROR, 0x%X",t);
			USART_printf_trm(debugs);
							
		}// else USART_printf_trm("Frame transmission OK");
						
		RX_enable(); // Start listen for incoming frames again after transmission
				
	} //if RF_TX_END_int_flag 
	
	if (RF_int_flag) // any other interrupt from RF part detected which is not Tx or RX, i.e. unexpected
	{
		RF_int_flag= 0x0; // interrupt handled

		status=get_IRQ_status();	// Read interrupt status register (IRQ_STATUS)
		state= get_RADIO_state();	// Get RADIO current state
		LED_on(RED);
		sprintf(debugs,"Other RF interrupt IRQ_STATUS: 0x%X  RF state: 0x%X",status,state);
		USART_printf_trm(debugs);	
	}	
				
	if (RF_RX_END_int_flag) // Indicates the completion of a frame reception - frame received, decode command
	{
		LED_toggle(YELLOW);
		RF_RX_END_int_flag = 0;
		//USART_printf_trm("RX int received");
		RF_f = get_RADIO_rx_frame(); // get received frame
		last_LQI = RF_f.LQI;
					
		if (RF_f.ForMe == 0x01)  //ABOT destination address = mine or broadcast
		{
			sprintf(debugs, "RF frame received from 0x%X cmd 0x%X",RF_f.source_adr,RF_f.PSDU[0]);
			USART_printf_trm(debugs);	
	 
			switch (RF_f.PSDU[0])  //command byte
			{		
					
				case PING_A:  // Reply to ping from external RF
					switch (RF_f.PSDU[1]){
						LED_off(RED);
						case 0x2A: USART_printf_trm("PING END (0x2A) received"); break; //PING END - no reply
						case 0xAB: // Request for Abot family name
							USART_printf_trm("PING NAME (0xAB) received"); 
							transmit_RADIO_PING_name();
						break;		
						default: // any other payload
							USART_printf_trm("Reply 0x2A to PING");
							transmit_RADIO_PING(0x2A); //Reply to PING
						break;
					}//Switch 	
				break;
						
				case JOYSTICK: 
						ABOT_motor_ctrl(RF_f.PSDU[1]);  // motor control cmd to ABOT
						switch (RF_f.PSDU[1]){
							case KEY_STATE_UP: USART_printf_trm("UP"); break;
							case KEY_STATE_DOWN: USART_printf_trm("DOWN"); break;	
							case KEY_STATE_RIGHT: USART_printf_trm("RIGHT"); break;
							case KEY_STATE_LEFT:USART_printf_trm("LEFT"); break;
							default: USART_printf_trm("JOY ERR"); break;
						}//Switch JOYSTICK
				break;
						
				case VERSION: transmit_RADIO_VERSION(); break;
						
				case GETCRTR: transmit_RADIO_CRTR(); break;
															
				case ABOT: ABOT_command(RF_f.PSDU[1]); break;
						
				case ABOT_MOTOR_DIRECTION: ABOT_motor_speed_ctrl(RF_f.PSDU[1],RF_f.PSDU[2]);break; // motor control cmd to ABOT
						
				//case GET_ABOT_SENSOR: USART_GET_SENSOR(RF_f.PSDU[1]); break; // Send request to Hugin
						
				case ABOT_UIO:
					switch (RF_f.PSDU[1])
					{
						case UIO_CONFIGURE: break;
						case UIO_SET:
							switch (RF_f.PSDU[2])
							{
								case 0x01: 
									LED_on(RF_f.PSDU[3]);
								break;
								case 0x02:
									LED_off(RF_f.PSDU[3]);
								break;
								case 0x03: 
									LED_toggle(RF_f.PSDU[3]);
								break;
							}
						break;
						case UIO_REQUEST:break;
					}//Switch 
				break;
						
				case SET_RF_CH: // Change network communication channel
					set_RF_channel(RF_f.PSDU[1]); 
					sprintf(debugs, "Changed channel to: %d",RF_f.PSDU[1]);
					USART_printf_trm(debugs);
				break; 
						
				case SET_RF_SHORT: // Change Node SHORT address
					if ((RF_f.PSDU[1]==MAC_RANDOM1) && (RF_f.PSDU[2]==MAC_RANDOM2)) //request from this node
					{
						i = RF_f.PSDU[3];//Low byte
						i2 = RF_f.PSDU[4]<< 8;;//High byte
						i = i | i2;
						set_RF_SHORT(i); // Set new SHORT address
						LED_on(GREEN);
						LED_off(RED);
						sprintf(debugs, "Changed short address to: 0x%X",i);
						USART_printf_trm(debugs);
					}
				break;
															
				case SET_RF_PWR: // Change node TX pwr
					set_RF_output_pwr(RF_f.PSDU[1]); 
					sprintf(debugs, "Changed TX pwr to: 0x%X",RF_f.PSDU[1]);
					USART_printf_trm(debugs);
				break; 
															
				case TEST: // can be used for any debugging
					switch (RF_f.PSDU[1])
					{
						case 0x01:
							USART_printf_trm("Trix1");
							//TODO add your trix
							ABOT_Trix(1);
						break;
						case 0x02:
							USART_printf_trm("Trix2");
							//TODO add your trix
							ABOT_Trix(2);
						break;
						case 0x03:
							USART_printf_trm("Trix3");
							//TODO add your trix
							ABOT_Trix(3);
						break;
					}		
				break;
						
				case CLR_RF_SHORT: transmit_RADIO_REQSHORT(rand_seed); break;
						
											
				default: 
					sprintf(debugs, "CMD 0x%X not supported",RF_f.PSDU[0]);
					LED_on(RED);
					USART_printf_trm(debugs);
					transmit_RADIO_DISPLAY(debugs);
				break;
				
			} //switch (RF_f.PSDU[0])
		}//if (RF_f.ForMe == 0x01)
	}//if RF_RX_END_int_flag; 				 		
}


int main(void)
{
	extern unsigned char ABOT_int_flag; // ABOT switch interrupt
	extern unsigned char USR_SWITCH_int_flag; //User Switch interrupt
	extern unsigned char ABOT_SWITCH_ENABLED; //ABOT switch action enabled
	uint16_t short_address;
	
	//CLKPR � Clock Prescale Register
	CLKPR = 0x80;//Bit 7 � The CLKPCE bit must be written to logic one to enable change of the CLKPS bits.
	CLKPR = 0x01; //Division factor 2 - 8MHz internal system clock.
	
	switch_ABOT_init();
	USART1_ABOT_init();
	RADIO_init();
	Motor_init();
	LED_init();
	sei(); //Enable interrupts
	
	short_address=get_MAC_SHORT(); //Get short address based on 64bit preprogrammed MAC
	set_RF_SHORT(short_address);   //Set node short address
	transmit_RADIO_PING_name();    //register in LAN routing table
	//transmit_RADIO_REQSHORT(rand_seed); // Get address in Abot network, if MAC not available
	RX_enable(); // Start listen for incoming frames
	USART_printf_trm(":-)");
	
	
	while(1)
    {
		task_RF(); //RADIO message received or to be transmitted
		
		task_USART_rx_trm(); //Take action on received message on USART from terminal
		
		if (ABOT_int_flag)// some of ABOT's switches has triggered an interrupt
		{  
			ABOT_int_flag = 0; //Clr flag
			if (ABOT_SWITCH_ENABLED)
			{
				transmit_RADIO_DISPLAY("Switch interrupt");
				USART_printf_trm("Switch int");
				ABOT_reverse_turn_forward();
			}	
		}//if (ABOT_int_flag)		
		
		if (USR_SWITCH_int_flag)	// The board user switch has triggered an interrupt
		{
			USR_SWITCH_int_flag = 0; //Clr flag
			USART_printf_trm("RF ping transmitted");
			transmit_RADIO_PING_name();             // Transmit PING on RF on request from terminal
			
		}//if (USR_SWITCH_int_flag)
		

	}//while(1)

}
 

