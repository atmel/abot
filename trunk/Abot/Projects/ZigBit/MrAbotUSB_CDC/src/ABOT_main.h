/*
 * ABOT_main.h
 *
 * Created: 05.10.2013 16:51:35
 *  Author: tbjoernvold
 */ 


#ifndef ABOT_MAIN_H_
#define ABOT_MAIN_H_

void task_RF(void);

void cmd_decode_task(void); //Data from terminal, computer mode

void terminal_task(uint8_t rx_char); //Data from terminal, user mode


#endif /* ABOT_MAIN_H_ */