/**
 * \file
 *
 * \brief XMEGA_RF233_ZIGBIT board header file.
 *
 * This file contains definitions and services related to the features of the
 * XMEGA_RF233_ZIGBIT Xplained board.
 *
 * To use this board define BOARD=XMEGA_RF233_ZIGBIT
 *
 * Copyright (c) 2013 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#ifndef _XMEGA_ZIGBIT_RF233_H_
#define _XMEGA_ZIGBIT_RF233_H_

#include <compiler.h>
#include <conf_board.h>



//! \name Miscellaneous data
//@{
//! Validate board support for the common sensor service.
#define COMMON_SENSOR_PLATFORM
//@}


//@{
#define MCU_SOC_NAME        "ATxmega256A3U"
#ifdef ZIGBIT_USB
	#define BOARD_NAME          "ATZB-X-233-USB"
	#define LED0_GPIO                       IOPORT_CREATE_PIN(PORTA, 4)
	#define LED1_GPIO                       IOPORT_CREATE_PIN(PORTA, 5)

	#define LED0                            LED0_GPIO //Green
	#define LED1                            LED1_GPIO  //Yellow

	//! Number of LEDs.
	#define LED_COUNT                       2
	
	
	// Define SW version SW ID (text string 10 characters max)
	#define VERSION_APP      "X233 USBot" //10 char

	// Default PAN network addresses
	#define MAC_SHORT_DEFAULT 0x4242
	#define MAC_SHORT_DEFAULT_DEST 0xFFFF  //Broadcast

	// Define Mr. Abot's family name (text string 10 characters max)
	#define FAMILY_NAME      "BJORNVOLD"
	

#endif

#ifdef ZIGBIT_EXT

	#define BOARD_NAME          "ATZB-X-233-XPRO"
	#define LED0_GPIO                       IOPORT_CREATE_PIN(PORTA, 6)
	#define LED1_GPIO                       IOPORT_CREATE_PIN(PORTA, 4)
	#define LED2_GPIO                       IOPORT_CREATE_PIN(PORTA, 5)

	#define LED0                            LED0_GPIO
	#define LED1                            LED1_GPIO
	#define LED2                            LED2_GPIO

	//! Number of LEDs.
	#define LED_COUNT                       3

	#define GPIO_PUSH_BUTTON_0              IOPORT_CREATE_PIN(PORTF, 2)

	//! \name Communication interfaces on header J1
	//@{
	#define TWID_SDA                        IOPORT_CREATE_PIN(PORTD, 0)
	#define TWID_SCL                        IOPORT_CREATE_PIN(PORTD, 1)
	#define USARTE0_RXD                     IOPORT_CREATE_PIN(PORTE, 2)
	#define USARTE0_TXD                     IOPORT_CREATE_PIN(PORTE, 3)
	#define SPID_SS                         IOPORT_CREATE_PIN(PORTD, 4)
	#define SPID_MOSI                       IOPORT_CREATE_PIN(PORTD, 5)
	#define SPID_MISO                       IOPORT_CREATE_PIN(PORTD, 6)
	#define SPID_SCK                        IOPORT_CREATE_PIN(PORTD, 7)
	//@}

/**
 * \def CONF_BOARD_ENABLE_USARTD0
 * \brief Initialize IO pins for USART 0 on port D
 */
#  if !defined(CONF_BOARD_ENABLE_USARTE0)
#    define CONF_BOARD_ENABLE_USARTE0
#  endif


#endif

//@}


/*! \name Connections of the AT86RFX transceiver
 */
//! @{
#define AT86RFX_MISC_PIN             IOPORT_CREATE_PIN(PORTC, 1)


//! @}



/**
 * @}
 */

/**
 * \defgroup xmega_a3bu_xplained_config_group Configuration options
 * @{
 */

#if defined(__DOXYGEN__)

/**
 * \name Initialization
 * \note Define these symbols in \ref conf_board.h to enable the corresponding
 * features.
 */
//@{




//@}

#endif // __DOXYGEN__

/**
 * @}
 */

/**
 * @}
 */

#endif /* _XMEGA_ZIGBIT_RF233_H_ */
