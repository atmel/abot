/*
 * ABOT_main.c
 *
 * Created: 05.10.2013 16:51:12
 *  Author: tbjoernvold
 */ 
/**
 * \file RZUSBSTICK.c
 *
 * \brief main for the RAVEN USB stick - ATAVRRZUSBSTICK
 *
 * \author    
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */


#include <stdio.h>
#include <asf.h>
#include <services/ABOT_protocol.h>
#include <services/RADIO_ABOT_cmd.h>
#include <components/RADIO_RF233_MAC.h>
#include "ABOT_main.h"
#include "xmega_rf233_zigbit.h"

// global variables
unsigned char uart_sub_menu_active = 0x0;              // says sub menu is active or not
uint8_t uart_sub_menu;	                        // says which sub menu should be shown
unsigned char motorOnOff = 0; //Motor control mode 0=FALSE;
unsigned char nextNode = 0; //node to comminicate with

char debugs[80];


/*! \brief The RF network interface
 *
 * Monitor the RF traffic and respond according to the functions implemented
 *
 * RF230 interrupts enabled:
 *  IRQ_3: TRX_END
 *  IRQ 1: PLL unlock
 *  IRQ_0: PLL_LOCK
 */
void task_RF(void)
{
	extern unsigned char RF_int_flag;
	RADIO_frame RF_f;
	unsigned char state;
	unsigned char status;
	unsigned char t;
	char text[MAX_PAYLOAD];
	uint16_t v1,v2;
	//uint16_t data16;
	//EEPROM_byte E_data;
	
	if (RF_int_flag){ // interrupt from RF230 detected
		RF_int_flag= 0x0; // interrupt handled, clr flag

		status=get_IRQ_status();	// Read interrupt status register 0x0F (IRQ_STATUS), reg cleared
		state= get_RADIO_state();	// Get RADIO current state
			
		if ((status & 0x02) != 0) //IRQ 1: PLL unlock
		{
			printf("RZ>PLL ERR state= %x \r\n", state);
		}
			
		if ((status & 0x08) != 0)  //IRQ_3: TRX_END 
		{
				
			/*IRQ_3: TRX_END	RX:  Indicates the completion of a frame reception. 
								TX:  Indicates the completion of a frame transmission. */
				
			if ((state == 0x06) || (state == 0x16) ) //  frame received, decode command
			{
					RF_f = get_RADIO_rx_frame(); // get received frame
				 
					switch (RF_f.PSDU[0])  //command byte
					{		
						case PING_A:
							switch (RF_f.PSDU[1])
							{
								case 0x2A: printf("RZ>RF PING replpy received (0x2A) from 0x%X \r\n", RF_f.source_adr); break; //reply from PING issued, no reply
								case 0xAB:
									update_routing_table(RF_f);
								break;
								default:
									printf("RZ>Received RF PING 0x%x from 0x%X \r\n",RF_f.PSDU[1],RF_f.source_adr);
									transmit_RADIO_PING(0x2A); // Respond to ping
								break;
							}//Switch 
						break;
						
						case FOURHEX:
							printf("4hex>  0x%x%x %x%x \r\n",RF_f.PSDU[4],RF_f.PSDU[3],RF_f.PSDU[2],RF_f.PSDU[1]);  
						break;
						
						case DISPLAY: //printf from ABOT
							for (t=0;t<RF_f.PHR;t++)
							{
								text[t]=RF_f.PSDU[t+1];
							}
							printf ("printf 0x%X>> %s \r\n",RF_f.source_adr,text);
						break;
												
						case JOYSTICK:	//Debug Joystick cmd
							printf (" 0x%X>>",RF_f.source_adr);							
							switch (RF_f.PSDU[1])
							{
								case KEY_STATE_UP: printf("UP"); break;
								case KEY_STATE_DOWN: printf("DOWN"); break;	
								case KEY_STATE_RIGHT: printf("RIGHT"); break;
								case KEY_STATE_LEFT: printf("LEFT"); break;
								default:  printf("JOYSTICK ERROR"); break;
							}//Switch JOYSTICK
						break;
						
						case VERSION: transmit_RADIO_VERSION(); break;  //Reply on request for SW version
						
						case ABOT_SENSOR: // Display reseived sensor values
							v1 = RF_f.PSDU[2]; //Sensor value low byte
							v2 = RF_f.PSDU[3]<<8; //Sensor value high byte
							v1 = v1 | v2;
							
							printf (" 0x%X>>",RF_f.source_adr);
							switch (RF_f.PSDU[1])
							{
								case SUPPLY_VOLTAGE: printf(" Board Supply voltage: 0x%X (%d) \r\n",v1,v1); break;
								case VOLTAGE_VCC: printf(" uC voltage: 0x%X (%d) \r\n",v1,v1); break;
								case VOLTAGE_EXT: printf(" External voltage: 0x%X (%d) \r\n",v1,v1); break;
								case TEMP_SENSOR:printf(" Temperature 0x%X (%d) \r\n",v1,v1); break;
								case LIGHT_SENSOR:printf(" light: 0x%X (%d) \r\n",v1,v1); break;
								default:  printf("SENSOR ERROR, unknown type \r\n"); break;
							}//Switch ABOT_SENSOR
						break;
						
						case ABOT_UIO:
							v1 = RF_f.PSDU[2]; //I/O low byte
							v2 = RF_f.PSDU[3]<<8; //I/O high byte
							v1 = v1 | v2;
							printf (" 0x%X>>",RF_f.source_adr);
							printf("RZ>USER IO 0x%X \r\n",v1);
						break;	
															
						case TEST:
							printf (" 0x%X>>",RF_f.source_adr);
							if (RF_f.PSDU[1] == 0x01)
							{
								printf("RZ>TEST1");
								//status = write_EEPROM_byte(0x55,0x20);
								//USART0_print4hex((unsigned int)status);
							} else 
							{
								printf("RZ>TEST2");
								/*E_data = read_EEPROM_byte(0x01);
								data16 = E_data.status;
								data16 = data16<<8;
								data16 = data16 | E_data.data;
								USART0_print4hex(data16);
								*/
							}						
						break;
						
						case REQ_RF_SHORT:
							v1 = get_RF_SHORT(0);
							transmit_RADIO_SETSHORT(v1,RF_f.PSDU[1],RF_f.PSDU[2]);
							printf("RZ> New SHORT address 0x%X to 0x%X \r\n",v1,RF_f.source_adr);
						break;
						
						default: 
							printf("RZ>Unknown command 0x%x via RF from 0x%X \r\n",RF_f.PSDU[0],RF_f.source_adr); // RADIO Command Error
						break;
					} //switch (RF_f.PSDU[0])
				 
			} else // frame transmitted
			{
				
				t = get_RADIO_TX_status();
				if (t != 0)
				{
					printf("RZ>RF TX ERROR 0x%x",t);
					switch (t){
						case 0x01: printf(" SUCCESS_DATA_PENDING \r\n"); break;
						case 0x03: printf(" CHANNEL_ACCESS_FAILUR \r\n"); break;
						case 0x05: printf(" NO_ACK  \r\n"); break;
						case 0x07: printf(" INVALID FRAME \r\n"); break;
					}//Switch 
					
				} else 
				{
					//printf("RZ>RF TX  OK 0x%x",t);
				}
											
				RX_enable(); // Start listen for incoming frames
			}
			
		} //if ((status & 0x08) != 0)  //IRQ_3: TRX_END 
	
	}	//if (RF230_int_flag)			
			
} // task_RF(void)


void sensors_menu(unsigned char menu_function)
{
	
	printf ("\r\n ------ Sensor sub menu -------\r\n");
	printf ("Ctrl-a    : All sensors \r\n");
	printf ("Ctrl-b    : Supply/Battery voltage \r\n");
	printf ("Ctrl-c    : External voltage connected to J401.6 \r\n");
	printf ("Ctrl-d    : External voltage connected to J401.5 \r\n");
	printf ("Ctrl-e    : Temperature reading in degC \r\n");
	printf ("Ctrl-f    : Light sensor \r\n");
	printf ("Ctrl-z    : exit sub menu \r\n");
	
	switch (menu_function)
	{
		case ALL_SENSORS:
			transmit_RADIO_GETSENSOR(ALL_SENSORS);
		break;
	
		case SUPPLY_VOLTAGE:
			transmit_RADIO_GETSENSOR(SUPPLY_VOLTAGE);
		break;
	
		case VOLTAGE_VCC:
			transmit_RADIO_GETSENSOR(VOLTAGE_VCC);
		break;
	
		case VOLTAGE_EXT:
			transmit_RADIO_GETSENSOR(VOLTAGE_EXT);
		break;
	
		case TEMP_SENSOR:
			transmit_RADIO_GETSENSOR(TEMP_SENSOR);
		break;
	
		case LIGHT_SENSOR:
			transmit_RADIO_GETSENSOR(LIGHT_SENSOR);
		break;
	
		case 0x00: break; // Menu only
	
		default:
			printf("Invalid sub menu option");
		break;
	}	
}
	
void calibration_menu(unsigned char menu_function)
{
	printf("\r\n ----- Motor Calibration ---- \r\n");
	printf("Up arrow    : Run motor forward full speed\r\n");
	printf("Left arrow  : Decrease calibration offset\r\n");
	printf("Right arrow : Increase calibration offset\r\n");
	printf("Down arrow  : Calibrate motor forward speed\r\n");
	printf("Ctrl-f      : Save calibration\r\n");
	printf("Ctrl-s      : Stop motor\r\n");
	printf("Ctrl-z      : Exit calibration menu\r\n");

	switch(menu_function)
	{
		case 0x41: // Up arrow - Run motor
			printf("RZ>Run motor \r\n");
			transmit_RADIO_ABOT_motor(FORWARD,255);
		break;
		
		case 0x42:  //Down arrow
			printf ("RZ>Calibrate motor speed \r\n");
			transmit_RADIO_ABOT(CALIBRATE_SPEED);
		break;
		
		case 0x43:  //RIGHT arrow - Increase calibration offset
			printf ("RZ>Calibration positive \r\n");
			transmit_RADIO_ABOT(CALIBRATE_P);
		break;
	
		case 0x44:  //LEFT arrow - Decrease calibration offset
			printf("RZ>Calibration negative \r\n");
			transmit_RADIO_ABOT(CALIBRATE_N);
		break;
	
		case 0x06: // ctrl-f - Save calibration offset in EEPROM
			printf("RZ>Save calibration \r\n");
			transmit_RADIO_ABOT(CAL_SAVE);
		break;
	
		case 0x13: // ctrl-s - Stop motor
			printf("RZ>Stop motor \r\n");
			transmit_RADIO_ABOT(STOP);
		break;
		
		case 0x14:  //Ctrl-t TEST
			printf ("RZ>Transmit TEST 1 \r\n");
			transmit_RADIO_TEST(1);
		break;
			
		case 0x00: break; // Menu only
		
		default:
			printf("Invalid option !\r\n");
		break;
	}	
}

/*! \brief The COM interface
 *
 * Provide the user with terminal interface via CDC COM 
 *
 */
void terminal_task(uint8_t rx_char)
{
	extern uint16_t MAC_SHORT; // current MAC short address
	extern uint16_t MAC_SHORT_dest; //  MAC short address
	extern uint16_t MAC_PAN_ID; // current MAC PAN ID
	extern node_info RTG_table[ROUTING_TABLE_SIZE]; // Coordinators Routing Table for short addresses, first byte contains last address
	
	extern unsigned char uart_sub_menu_active;      // says sub menu is active or not
	extern uint8_t uart_sub_menu;	       // says which sub menu should be shown
	
	
	
	unsigned char c,i,t;
	uint16_t a;
		
		if (uart_sub_menu_active)  
		{
			//Process the sub menu using the received characters
			switch(uart_sub_menu)
			{
				case 0x09:  // Sensors sub menu
					if (rx_char==0x1A) //ctrl-z - sub menu exit
					{
						uart_sub_menu_active = 0x0;
						printf("Sub menu exit \r\n");
					}else
					{
						sensors_menu(rx_char);
					}
				break;
					
				case 0x10: // Motor Calibration sub menu
					if (rx_char==0x1A) //ctrl-z - sub menu exit
					{
						uart_sub_menu_active = 0x0;
						printf("Sub menu exit \r\n");
					}else
					{
						calibration_menu(rx_char);
					}
				break;
					
					
				default: break;
					
			}//switch(uart_sub_menu)
				
		}else //if (!uart_sub_menu_active)
		{				
			switch (rx_char)
			{
				case 0x2b: //?
				case 0x08: // ctrl-h
					printf("\r\n -------- Help menu ---------------\r\n");
					printf ("ctrl-c    : Change network channel \r\n");
					printf ("ctrl-d    : Set destination Short address (default=broadcast) \r\n");
					printf ("ctrl-e    : Calibrate Motor \r\n");
					printf ("ctrl-f    : Enable Auto Motor control \r\n");
					printf ("ctrl-g    : Disable Auto Motor control \r\n");
					printf ("ctrl-i    : Get ABOT sensor values \r\n");
					printf ("ctrl-k    : Enable distance sensor\r\n");
					printf ("ctrl-l    : Disable distance sensor\r\n");
					printf ("ctrl-m    : Play tune  \r\n");
					printf ("ctrl-p    : PING \r\n");
					printf ("ctrl-r    : Get RF info \r\n");					
					printf ("ctrl-t    : Transmit Test cmd \r\n");
					printf ("ctrl-u    : Request User I/O \r\n");
					printf ("ctrl-v    : Get SW version \r\n");
					printf ("ctrl-w    : Set RF output pwr on node\r\n");
					printf ("ctrl-x    : Clr network short addresses \r\n");
					printf ("ctrl-y    : Next node in routing table\r\n");
					printf ("ctrl-z    : Toggle motor control mode \r\n");
					printf ("ctrl-s    : ABOT stop \r\n");
					printf ("1         : Trix1 \r\n");
					printf ("2         : Trix1 \r\n");
					printf ("3         : Trix1 \r\n");
					printf ("arrows    : Motor control \r\n");	
					
					
				break;

				case 0x01: transmit_RADIO_GETCRTR(); break; //Ctrl-a 
				/*
				case 0x03: //Ctrl-c change channel value, valid number from 11 - 26
					printf ("RZ> Enter new channel, two digits (11-26) \r\n");
					rx_char = uart_usb_getchar(); //ASCII 48 = 0
					a=rx_char-48;
					printf("RZ> 0x%x \r\n",a);
					rx_char = uart_usb_getchar();
					a=(a*10) + (rx_char-48);
					printf("RZ> 0x%x \r\n",a);
					if ((a<27)&(a>10))
					{
						transmit_RADIO_SETCH(a); // Ask nodes to change channel
						printf ("RZ> New channel: 0x%X \r\n",a);
						set_RF_channel(a); // Change USBSTICK network channel
					}	else printf("RZ> channel not valid \r\n");
				break;
				
				case 0x04: //Ctrl-d Set destination address
					printf ("RZ> Enter Destination address, two decimal digits (00 =>FFFF) \r\n");
					rx_char = uart_usb_getchar(); //ASCII 48 = 0
					a=rx_char-48;
					rx_char = uart_usb_getchar();
					a=(a*10) + (rx_char-48);
					if (a == 0x0)
					{
						a=0xFFFF;
					}else
					{
						a=a+0x4200;
					}
					set_RF_SHORT_DESTINATION(a);
					printf ("RZ> Destination address: 0x%X \r\n",a);
				break;
				*/
				case 0x05: // ctrl-e Calibrate Abot motors
					calibration_menu(0x00); // Print menu
					uart_sub_menu = 0x10;	// Motor calibration sub menu
					uart_sub_menu_active = 0x01; // sub menu process flag
				break;
				
				case 0x06: //ctrl-f    : Enable Auto Motor control
					transmit_RADIO_ABOT(ENABLE_AUTO);
					printf("RZ>Abot auto enabled \r\n");
				break;
				
				case 0x07: //ctrl-g    : Disable Auto Motor control
					transmit_RADIO_ABOT(DISABLE_AUTO);
					printf("RZ>Abot auto disabled \r\n");
				break;
				
				case 0x0D: //ctrl-M
					transmit_RADIO_AUDIO(AUDIO_TUNE_PLY,0x01,0x01);
				break;
				
				case 0x09: // Ctrl-i get ABOT sensor values
					sensors_menu(0x00);   // Print menu
					uart_sub_menu = 0x09; // get sensor has sub menu items
					uart_sub_menu_active = 0x01; // sub menu process flag
				break;
				
				case 0x0B: //Ctrl-k Enable distance sensor
					transmit_RADIO_ABOT(ENABLE_SWITCH);
					printf("RZ>Abot distance sensor enabled \r\n");
				break;
				
				case 0x0C: //Ctrl-l Disable distance sensor
					transmit_RADIO_ABOT(DISABLE_SWITCH);
					printf("RZ>Abot distance sensor disabled \r\n");
				break;
			
					
				case 0x10:  //Ctrl-p PING
					printf(">PING(0x4C) from USB stick \r\n");
					transmit_RADIO_PING(0x4C);
				break;
				
				case 0x12:  //Ctrl-r RF info
					c=get_RADIO_state();
					switch (c){
						case 0x06: printf("RZ>RF state: RX_ON 0x%X \r\n",c);  break;
						case 0x16: printf("RZ>RF state: RX_AACK_ON 0x%X \r\n",c); break;
						default:   printf("RZ>RF state:      0x%X \r\n",c); 
					}//Switch state
					c=get_RADIO_RSSI();
					printf("RZ>RADIO RSSI:      %u (current received signal strength of a received frame) \r\n",c); 
					c=get_RF_channel();
					printf("RZ>RF channel:      0x%X \r\n",c);
					c= get_RF_output_pwr();
					printf("RZ>RF output pwr:   0x%X (0x0 = +3dBm, 0xF = -17.2dBm) \r\n",c);
					printf("RZ>RF PAN ID:       0x%X \r\n",MAC_PAN_ID);
					a = get_RADIO_PANID_filter();
					printf("RZ>RF PAN ID:       0x%X from register\r\n",a);
					printf("RZ>RF my short adr  0x%X from gloabal variable\r\n",MAC_SHORT);
					a = get_RADIO_SHORT_filter();
					printf("RZ>RF my short adr  0x%X from register\r\n",a);
					printf("RZ>RF dest adr:     0x%X \r\n",MAC_SHORT_dest);
					for (i = 1; i < RTG_table[0].node_short_adr+1; i++)
					{
						printf("RZ>Routing table[%i]: 0x%X Live:%X Mr. Abot ",i,RTG_table[i].node_short_adr,RTG_table[i].live);
						for (t = 0; t < 10; t++)
						{
							debugs[t] = RTG_table[i].node_family_name[t];
						}
						debugs[10] = NULL;
						//debugs[10] = "0/";
						printf(debugs);
						printf("\r\n");
					}
				break;
				
				
				
				case 0x14:  //Ctrl-t TEST
					printf ("RZ>Transmit TEST 1 \r\n");
					//transmit_RADIO_TEST(1);
					set_RF_SHORT_DESTINATION(0xFFFF);//broadcast to all
					transmit_RADIO_PING(0xAB);
				break;
				
				case 0x15: transmit_RADIO_USERIO(UIO_REQUEST,0x0,0x0); break;  //Ctrl-u request user I/O
				
				case 0x16:  //Ctrl-v
					printf ("RZ>Unit SW version: %s %s %s \r\n",__DATE__,__TIME__,VERSION_APP);
					transmit_RADIO_GETVERSION();
				break;
				/*
				case 0x17:  //Ctrl-W RF output pwr
					printf ("RZ> Enter new RF output pwr level (0-9) \r\n");
					rx_char = uart_usb_getchar(); //ASCII 48 = 0
					a=rx_char-48;
					printf ("RZ> New RF pwr level: 0x%X \r\n",a);
					transmit_RADIO_SETPWR(a);
				break;
				*/
				case 0x18:  //Ctrl-x
					printf ("RZ>Clr all network addresses\r\n");
					transmit_RADIO_CLRSHORT();
				break;
				
				case 0x19:  //Ctrl-y
					printf ("RZ>next node = %X\r\n",motorOnOff);
					if (nextNode < RTG_table[0].node_short_adr) 
					{
						nextNode++;
					} else
					{
						nextNode = 0;
					}
					if (nextNode == 0) set_RF_SHORT_DESTINATION(0xFFFF);// Broadcast
					else set_RF_SHORT_DESTINATION(RTG_table[nextNode].node_short_adr);// Only communicate with this node
				break;
				
				case 0x1A:  //Ctrl-Z
					if (motorOnOff) motorOnOff = 0; else motorOnOff = 1;
					printf ("RZ>Toggle speed ctrl mode, on/Off = %X\r\n",motorOnOff);
				break;

				case 0x13:  //Ctrl-s STOP ABOT
					printf(">STOP ABOT \r\n");
					transmit_RADIO_ABOT(STOP);
				break;
				
				case 0x31:  //1 TEST Trix1
					transmit_RADIO_TEST(1);
					printf(">Trix1 \r\n");
				break;
				
				case 0x32:  //2 TEST Trix2
					transmit_RADIO_TEST(2);
					printf(">Trix2 \r\n");
				break;
				
				case 0x33:  //3 TEST Trix3
					transmit_RADIO_TEST(3);
					printf(">Trix3 \r\n");
				break;
				
				case 0x1b:
					rx_char= udi_cdc_getc(); //0x5B
					rx_char= udi_cdc_getc();
					switch (rx_char)
					{			
						case 0x41:  //UP arrow
							printf(">UP \r\n");
							if (motorOnOff) transmit_RADIO_ABOT_motor(FORWARD,255);
							else transmit_RADIO_joy(KEY_STATE_UP);
						break;
							
						case 0x42:  //DOWN arrow
							printf(">DOWN \r\n");
							if (motorOnOff) transmit_RADIO_ABOT_motor(REVERSE,20);
							else transmit_RADIO_joy(KEY_STATE_DOWN);
						break;			
						
						case 0x44:  //LEFT arrow
							printf(">LEFT \r\n");
							if (motorOnOff) transmit_RADIO_ABOT_motor(LEFT_FORWARD,255);
							else transmit_RADIO_joy(KEY_STATE_LEFT);						
						break;		
				
						case 0x43:  //RIGHT arrow
							printf(">RIGHT \r\n");
							if (motorOnOff) transmit_RADIO_ABOT_motor(RIGHT_FORWARD,255);
							else transmit_RADIO_joy(KEY_STATE_RIGHT);
						break;
					}
				break;
		
				default:
					printf("TB>Hei:-) you typed:0x%X \r\n",rx_char);
				break;
				
			}//switch (rx_char)
		}//else (!uart_sub_menu_active)

}

void cmd_decode_task(void)
{
	RADIO_frame computer_msg; //received msg from external computer
	unsigned char t=0;
	unsigned char i=0;
	unsigned char recived_byte;
	unsigned char destination;
	uint16_t MAC_destination;
	extern unsigned char ABOT_seq_no; //Abot msg header sequence number
	
	// ABOT protocol frame,  SOH | destination | source | frame_length | cmd | payload | EOT
	
	//transmit_RADIO_DISPLAY("waiting");
	destination = udi_cdc_getc();//Destination address
	computer_msg.source_adr = udi_cdc_getc();//ABOT LAN Source address	
	computer_msg.PHR = udi_cdc_getc(); //# of Payload byte including cmd 
	while ((t<MAX_PAYLOAD) && (t<computer_msg.PHR)) //receive payload
	{
		computer_msg.PSDU[t] = udi_cdc_getc(); //Waits and gets a value on CDC line
		t++;
	}
	
	recived_byte = udi_cdc_getc();//EOT
	
	if (destination == 0x42) //msg to me
	{
		switch (computer_msg.PSDU[0]) //cmd byte as defined in ABOT_prototcol.h
		{
			case PING_A: //Connection test from external computer
				if (ABOT_seq_no>250) ABOT_seq_no = 0; else ABOT_seq_no++;
				
				udi_cdc_putc(SOH);
				udi_cdc_putc(computer_msg.source_adr); //Destination
				udi_cdc_putc(0x42); //source
				udi_cdc_putc(ABOT_seq_no);
				udi_cdc_putc(0x02); //frame_length
				udi_cdc_putc(PING_A);//cmd
				udi_cdc_putc(0x2A);// Ping reply
				udi_cdc_putc(EOT);
				LED_Toggle(LED1);	//YELLOW
				set_RF_SHORT_DESTINATION(0xFFFF);//broadcast to all
				transmit_RADIO_PING(0xAB);//RTG 
				//transmit_RADIO_DISPLAY("ping transmitted");
				// clean routing table, ping and remove not active nodes
				//update_active_abot_nodes();
			break;
			case ROUTING_TABLE: //request routing table info
				LED_Toggle(LED1);	//YELLOW
				if (ABOT_seq_no>250) ABOT_seq_no = 0; else ABOT_seq_no++;
				
				udi_cdc_putc(SOH);	
				udi_cdc_putc(computer_msg.source_adr); //Destination
				udi_cdc_putc(0x42); //source
				udi_cdc_putc(ABOT_seq_no);
				udi_cdc_putc(0x0C); //frame_length
				udi_cdc_putc(ROUTING_TABLE);//cmd
				udi_cdc_putc(RTG_table[0].node_short_adr);// #of nodes in RTG
				if (computer_msg.PSDU[1] > RTG_table[0].node_short_adr ) computer_msg.PSDU[1] = RTG_table[0].node_short_adr;//end of table
				udi_cdc_putc(computer_msg.PSDU[1]);//The requested node index
				for (i = 0; i < 10; i++)//the family name for the requested index
				{
					udi_cdc_putc(RTG_table[computer_msg.PSDU[1]].node_family_name[i]);
				}
				udi_cdc_putc(EOT);
			break;
			default:
				sprintf(debugs,"Received unknown cmd 0x%x",computer_msg.PSDU[0]);
				transmit_RADIO_DISPLAY(debugs);	
			break;
		}	
	} else // msg to external node
	{
		//LED_Toggle(LED1);	//YELLOW
		if (RTG_table[0].node_short_adr < destination)
		{
			MAC_destination = 0xFFFF; //Broadcast
		} else 
		{	
			MAC_destination = RTG_table[destination].node_short_adr;//Get Destination address from Routing table
		}
		set_RF_SHORT_DESTINATION(MAC_destination); 
		RADIO_tx_frame(computer_msg); 
	}
}