/**
 * \file MIC_RAVEN.c
 *
 * \brief The Raven mic component
 *
 * \author    TB
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

//GCC includes
#include <avr/io.h>

#include <avr/interrupt.h> 

//Solution includes
#include "MIC_RAVEN.h"

/* ADC Conversion Complete Interrupt  
	ADIF is cleared by hardware when executing the corresponding interrupt handling vector.
*/
ISR(ADC_vect)
{
	extern unsigned char ADC_int_flag;
	extern uint16_t mic_value;
	
		ADC_int_flag = 1;
		
		mic_value = ADC;
				
/*
ADCL and ADCH � The ADC Data Register
Right adjust, Data from bit 9:0
ADCL must be read first, then ADCH.
*/

}


void mic_disable(void)
{
	
	PORTE |= (1<<PORTE7); // PE7 driven high - AUDIO power disabled
	
	//ADCSRA &= ~(1<<ADEN);  // Disable ADC
   
	//PRR |= (1 << PRADC);  //Enable power reduction mode
	
	DIDR0 |= (1<<ADC0D); //Disable channel 0
	
}


void mic_enable(void)
{
	
	PORTE &= ~(1<<PORTE7); // PE7 driven low - AUDIO power enabled
	
	ADCSRA |= (1<<ADEN) | (1<<ADSC); //Enable ADC and start conversion
	
}


/*
   Microphone connected to ADC0 - PF0
   AUDIO power ctrl connected to PE7 - when pulled low power provided to mic and speaker control circuits
*/
void mic_init(void)
{
	extern unsigned char ADC_int_flag;
	extern unsigned char mic_on;
	
	ADC_int_flag=0;
	mic_on=1;
	
 /*
 PRR � Power Reduction Register
	Bit 0 - PRADC: Power Reduction ADC
		Writing logic one to this bit shuts down the ADC. The ADC must be disabled before shut down.
		The analog comparator cannot use the ADC input MUX when the ADC is shut down.	
*/
	 PRR &= ~(1 << PRADC);  //Disable power reduction mode
	
/*	
	ADMUX � ADC Multiplexer Selection Register 0100 0000
		Bit 7:6 � REFS1:0: Reference Selection Bits
			AVCC with external capacitor at AREF pin
		Bit 5 � ADLAR: ADC Left Adjust Result, 
			0= Right adjust
		Bits 4:0 � MUX4:0: Analog Channel Selection Bits
			ADCO single ended
*/
	ADMUX = 0x40;  //ADCO single ended
/*	
	ADCSRA � ADC Control and Status Register A	0011 1111
		Bit 7 � ADEN: ADC Enable
			Writing this bit to one enables the ADC. By writing it to zero, the ADC is turned off. 
		Bit 6 � ADSC: ADC Start Conversion
			In Single Conversion mode, write this bit to one to start each conversion. In Free Running mode,
			write this bit to one to start the first conversion. 
			ADSC will read as one as long as a conversion is in progress. When the conversion is complete,
			it returns to zero.
		Bit 5 � ADATE: ADC Auto Trigger Enable
			When this bit is written to one, Auto Triggering of the ADC is enabled. 
		Bit 4 � ADIF: ADC Interrupt Flag
			This bit is set when an ADC conversion completes and the Data Registers are updated. 
			ADIF is cleared by hardware when executing the corresponding interrupt handling vector. Alter-
			natively, ADIF is cleared by writing a logical one to the flag. 
		Bit 3 � ADIE: ADC Interrupt Enable
			When this bit is written to one and the I-bit in SREG is set, the ADC Conversion Complete Inter-
			rupt is activated.

		Bits 2:0 � ADPS2:0: ADC Prescaler Select Bits
			Table 22-4. These bits determine the division factor between the XTAL frequency and the 
			input clock to the ADC.
*/		
   ADCSRA = 0x3F;  //Interrupt enabled Division Factor =  128

/*
	ADCSRB � ADC Control and Status Register B
		Bit 2:0 � ADTS2:0: ADC Auto Trigger Source	
*/
	ADCSRB = 0x00; //Free Running mode
	
/*
	DIDR0 � Digital Input Disable Register 0
		When this bit is written logic one, the digital input buffer on the corresponding ADC pin is dis-
		abled.  
		When an analog signal is applied to the ADC7:0 pin and the digital input from this pin is not needed, this
		bit should be written logic one to reduce power consumption in the digital input buffer. 
*/

/*
Each port pin consists of three register bits: DDxn, PORTxn, and PINxn. 
	The DDxn bit in the DDRx Register selects the direction of this pin. If DDxn is written logic one,
	Pxn is configured as an output pin.
	If PORTxn is written logic one when the pin is configured as an input pin, the pull-up resistor is
	activated. 
	If PORTxn is written logic one when the pin is configured as an output pin, the port pin is driven
	high (one). 
	Writing a logic one to PINxn toggles the value of PORTxn, independent on the value of DDRxn.
*/	


//PE7 - AUDIO power control

	DDRE |= (1<<DDE7); // PE7 set as output
	
//   Microphone connected to ADC0 - PF0

	DDRF &= ~(1<<DDF0)	; //PF0 set as input
	
	mic_enable();

}
