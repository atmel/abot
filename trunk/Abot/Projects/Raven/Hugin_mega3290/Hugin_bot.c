/*
 * Hugin_bot.c
 *
 * Created: 14/01/2012 13:45:52
 *  Author: TB
 *
 * main loop for Hugin (mega3290) robot application running on the RAVEN kit
 *
 */ 

//GCC includes
#include <avr/io.h>

#include <avr/interrupt.h>  // sei()

//#include <avr/pgmspace.h>  //LCD PSTR


//Solution includes
#include <services/ABOT_protocol.h>
#include <services/USART_ABOT_cmd.h>
#include <components/LCD_RAVEN.h>
#include <components/Joystick_RAVEN.h>
#include <components/Hugin_sensor.h>
#include <drivers/drv_3290_timer.h>
#include <components/SPEAKER_RAVEN.h>

//Project includes

#include "MIC_RAVEN.h"

//extern mode switch - if = 1, joystick is monitored and key press transmitted
unsigned char joy_mode;

uint8_t LCD_on;		// toggle LCD on/off


/* ============= Set Fuses ======================================================================== */
/*
Fuse Low Byte =0xE7   1110 0111
CKDIV8 1 (disabled)
CKOUT  1 (disabled)
 Start-up times are determined by the SUT Fuses 
SUT1..0 = 10  Additional Delay from Reset (VCC = 5.0V) set to 4 CK + 65 ms
 The Low-frequency Crystal Oscillator must be selected by setting the CKSEL Fuses to �0110� or �0111�  
CKSEL3...0 = 0111 Start-up Time from Power-down and Power-save set to 32K CK (recommended)

Fuse Low Byte =0x62   0110 0010
CKDIV8 0 (enabled)
CKOUT  1 (disabled)
 Start-up times are determined by the SUT Fuses 
SUT1..0 = 10  Additional Delay from Reset (VCC = 5.0V) set to 4 CK + 65 ms
 
CKSEL3...0 = 0010 Internal RC Oscillator

 Fuse High Byte =0x1F   0001 1111
 OCDEN = 0 (enabled)
 JTAG  = 0 (enabled)
 ISP   = 0 (enabled)
 WDTON = 1 (disabled) Watchdog Timer always off
 EESAVE= 1 (disabled) EEPROM not preserved through the Chip Erase
 BOOTSZ1:0=11 Boot Size = 256 word Boot Reset Address = 0x3F00
 The Flash memory is organized in two main sections, the Application section and the Boot
	Loader section (see Figure 26-2). The size of the different sections is configured by the
	BOOTSZ Fuses as shown in Table 26-9 on page 300 and Figure 26-2
 BOOTRST= 1 (disabled) Select Reset Vector disabled, Reset Vector = Application Reset (address 0x0000)
 
 Extended Fuse Byte =0xFF (factory setting)
 No BOD
 
 Note that the fuses are read as logical zero, �0�, if they are programmed.
 */

#include <avr/fuse.h>
FUSES =
	{
		.low = 0x62,
		.high = 0x1F,
		.extended = 0xff,
	};
	
/* ============= Set Fuses END======================================================================== */



/*
	Detect joystick actions
	Display menu
	Execute selected action
*/
void Joystick_menu(void)
{
	unsigned int t; //time used in delay
	extern unsigned char joy_mode;
	extern unsigned char PCINT0_int_flag;
	extern uint8_t LCD_on;		// toggle LCD on/off
	extern unsigned char mic_on; //toggle mic on/off
		
	// menu variables
	unsigned char key;		// Joystick key
	unsigned char HML;		// Horizontal Menu Level < 0x0F
	unsigned char VML;		// Vertical Menu Level < 0x0F
	unsigned char MTI;		// Menu tree indicator - where you are in the menu tree
	unsigned char enter;	// Enter key pressed
	unsigned char exit;		// Exit menu mode

	
	LCD_set_symbol(LCD_SYMBOL_KEY);// indicate Hugin in key detect mode
	exit=0; //exit = FALSE
	enter=0; //enter key pressed = FALSE;
	HML=1;
	VML=1;
	LCD_printf("HUGIN"); //0x11
			
	if (!LCD_on) // Always turn LCD on when key detect mode entered
	{
		LCD_init(); // LCD init
		LCD_on=1; //TRUE
	}
					
	while(!exit) //Stay in Key detect mode until exit selected
	{
		
		key=KEY_STATE_NO_KEY; 
		while (key==KEY_STATE_NO_KEY) // only go further if key pressed
		{
			key = get_key_state();
		}
		
		if (joy_mode)	// if active detect and transmit joystick key
		{
			switch (key) // Decode joystick position and take action
			{
				case KEY_STATE_UP: USART_joy(KEY_STATE_UP); break;
				case KEY_STATE_DOWN: USART_joy(KEY_STATE_DOWN); break;
				case KEY_STATE_RIGHT: USART_joy(KEY_STATE_RIGHT); break;
				case KEY_STATE_LEFT: USART_joy(KEY_STATE_LEFT); break;
				case KEY_STATE_ENTER: // Exit joy mode
					USART_ABOT(STOP);
					exit=1; 
					joy_mode=0;
					LCD_clr_symbol( LCD_SYMBOL_ENV_OP);
					LCD_clr_symbol( LCD_SYMBOL_ENV_MAIN);
				break;
				default:  break;
			}//Switch get key
			MTI=0; //No menu
		} else
		{
			switch (key) // Move in menu
			{
				case KEY_STATE_ENTER: enter=1; break; // Enter key pressed = TRUE, i.e. execute selected
				case KEY_STATE_UP: if (VML>0) VML--; break; //menu level one up
				case KEY_STATE_DOWN: if (VML<15) VML++; break; //menu level one down
				case KEY_STATE_RIGHT: if (HML<15) HML++; break; //menu level one right
				case KEY_STATE_LEFT: if (HML>1) HML--; break; //menu level one left
				default: LCD_printf("NO KEY"); break;
			}//Switch get key	
				
			MTI = HML<<4;
			MTI = MTI | VML;
			LCD_print4hex((uint16_t)MTI);
		}
						
		switch (MTI) // Display menu
		{
			case 0x10: LCD_printf("EXIT"); break;
			case 0x11: LCD_printf("HUGIN"); break;
			case 0x12: LCD_printf("ABOT"); break;
			case 0x13: LCD_printf("SPEAKER"); break;
			case 0x14: LCD_printf("DISPLAY"); break;
			case 0x15: LCD_printf("TEMP"); break;
			case 0x16: LCD_printf("RADIO"); break;
			case 0x17: LCD_printf("MIC"); break;
			case 0x18: LCD_printf("TEST"); break;

			case 0x21: LCD_printf("PING MU"); break;	// Tx RAVEN PING to Munin				
			case 0x22: LCD_printf("JOYSTIC"); break;
			case 0x23: LCD_printf("SPKR ON"); break;
			case 0x24: LCD_printf("DSP OFF"); break;	
			case 0x25: LCD_printf("TMP ON"); break;
			case 0x26: LCD_printf("G STATE"); break;  // GET RADIO STATE
			case 0x27: LCD_printf("MIC ON"); break; 
			case 0x28: LCD_printf("TEST 1"); break; 
					
			case 0x32: LCD_printf("JOY OFF"); break;
			case 0x33: LCD_printf("SPK OFF"); break;
			case 0x35: LCD_printf("TMP OFF"); break;
			case 0x36: LCD_printf("PING RF"); break;
			case 0x37: LCD_printf("MIC OFF"); break;
			case 0x38: LCD_printf("TEST 2"); break;
			
			case 0x42: LCD_printf("CAL P"); break;
			case 0x46: LCD_printf("G RSSI"); break;
			
			case 0x52: LCD_printf("CAL N"); break;
			case 0x56: LCD_printf("G SHORT"); break;
			
			case 0x62: LCD_printf("CAL SAV"); break;
						
			default:LCD_printf("NO MENU"); break;
		}//switch (MTI)	Display menu			
				
		if (enter)	// Enter pressed and selected menu choice is executed
		{
			switch (MTI) // execute selected command
			{
				case 0x10: exit=1; break;//exit
				case 0x11: break;//System and first selected, most have entry or default will set exit
				case 0x12: USART_ABOT(STOP); exit=1; break; //Stop ABOT motor
				
				case 0x21: USART_RADIOPING(0x42); exit=1; break; //Send a RAVEN PING to Munin to test internal communication
				case 0x22: //Enter joy mode, Send joystick state to Munin
					//exit=1; 
					joy_mode=1;
					LCD_set_symbol(LCD_SYMBOL_ENV_MAIN);// indicate Hugin in ABOT ctrl mode
					LCD_set_symbol(LCD_SYMBOL_ENV_OP);
				break; 
				case 0x23: LCD_set_symbol(LCD_SYMBOL_SPEAKER); break; //Speaker on
				case 0x24: LCD_on=0;exit=1; break;
				case 0x25: LCD_set_symbol(LCD_SYMBOL_C); break; //Temperature on
				case 0x26: USART_GET_STATE(); exit=1; break; //Send GET STATE cmd to Munin
				case 0x27: LCD_set_symbol(LCD_SYMBOL_MIC); mic_on=1; mic_init(); exit=1; break; //MIC on
				
				case 0x28: USART_TEST(TEST1); exit=1; break; // issue test command #1
						
				case 0x32: // Exit ABOT joystick ctrl mode
					exit=1; 
					joy_mode=0;
					LCD_clr_symbol( LCD_SYMBOL_ENV_OP); 
					LCD_clr_symbol( LCD_SYMBOL_ENV_MAIN);
				break; 
				case 0x33: LCD_clr_symbol(LCD_SYMBOL_SPEAKER); break; //Speaker off
				case 0x35: LCD_clr_symbol(LCD_SYMBOL_C); break; //Temperature off	
				case 0x36: USART_RADIOPING(0x4C); exit=1; break; //Send a RADIO PING to Munin to test RADIO communication
				case 0x37: LCD_clr_symbol(LCD_SYMBOL_MIC); mic_on=0; exit=1; break; //MIC off
				
				case 0x38: USART_printf("TEST2-USB"); exit=1; break; // issue test command #2
				
				case 0x42: USART_ABOT(CALIBRATE_P); exit=1; break; //Initiate ABOT motor calibration, increase speed
				case 0x46: USART_GET_RSSI(); exit=1; break; //Get Munin RADIO RSSI
				
				case 0x52: USART_ABOT(CALIBRATE_N); exit=1; break; //Initiate ABOT motor calibration, decrease speed
				case 0x56: USART_GET_SHORT(); exit=1; break; //Get Munin Short address
				
				case 0x62: USART_ABOT(CAL_SAVE); exit=1; break;// Save calibration
				
						
				default:exit=1; break;
			}//switch execute
		}//if (enter)					
		enter=0; // Enter key status clr
		
		//only repeat loop if key has been released
		if (!exit)
		{
			while (key!=KEY_STATE_NO_KEY) // only go further if key released
			{
				key = get_key_state();
			}
		}
		
	}//while(!exit)			
	
	if (!LCD_on) LCD_disable();// Turn LCD off
			
	// Delay for "enter interrupt" to be handled
	for (t=1;t<0xFFFF;t++);
	/*
	EIFR � External Interrupt Flag Register
	Bit 4 � PCIF0: Pin Change Interrupt Flag 0
		When a logic change on any PCINT7:0 pin triggers an interrupt request, PCIF0 becomes set
		(one). If the I-bit in SREG and the PCIE0 bit in EIMSK are set (one), the MCU will jump to the
		corresponding Interrupt Vector. The flag is cleared when the interrupt routine is executed. Alternatively,
		the flag can be cleared by writing a logical one to it.
	*/
	EIFR |= (1<<PCIF0); // clr enter interrupt
	PCINT0_int_flag=0;
	LCD_clr_symbol(LCD_SYMBOL_KEY); // Exit key detect mode
	
}	
	
	

/* Transmit joystick position*/
void transmit_joy_state(void)
{
	unsigned int t; //time used in delay
	unsigned char key;		// Joystick key


	//Delay to avoid reading same key twice
	t = 0x1FFF;
	while (t>0) t--;
	
	key = get_key_state();  // any key pressed ? read only once then serve other functions in main loop	
	switch (key) // Get key 
	{
		case KEY_STATE_UP: USART_joy(KEY_STATE_UP); LCD_printf("ABOT UP"); break; 
		case KEY_STATE_DOWN: USART_joy(KEY_STATE_DOWN); LCD_printf("A DOWN"); break;
		case KEY_STATE_RIGHT: USART_joy(KEY_STATE_RIGHT); LCD_printf("A RIGHT"); break;
		case KEY_STATE_LEFT: USART_joy(KEY_STATE_LEFT); LCD_printf("A LEFT"); break;
		default:  break;
	}//Switch get key	
		
}
	
void Joystick_task(void)
{	
	extern unsigned char PCINT0_int_flag;
	extern unsigned char joy_mode;
	
		if (PCINT0_int_flag) // If ENTER pressed enter key detect mode
		{
			LCD_clr_symbol(LCD_SYMBOL_ATT); // clr previous error indicator
			mic_disable();
			LCD_clr_symbol(LCD_SYMBOL_MIC); // clr mic symbol
			Joystick_init();
			Joystick_menu();
		}
		/*
		if (joy_mode)	// if active detect and transmit joystick key	
		{
			transmit_joy_state();
		}*/	
}


void USART_rx_task(void){ //Take action on received msg on USART from Munin

extern unsigned char    RX_int_flag;
extern rx_buf           rxbuf; //receive buffer	
ABOT_frame				rx_f;  //receive frame
uint16_t				numt, numt1; // 4HEX
extern tone_t james_bond[];
extern tone_t anthem[];
extern const tone_t marry[];

	//RX interrupt - see if frame received and execute command
		if (RX_int_flag) 
		{
			led_on(); // indicate traffic from behind the wall
			LCD_set_symbol(LCD_SYMBOL_RX); 
			
			rx_f = USART_Get_frame();
			
			if (rx_f.error==0) //if frame ok - execute command
			{
				switch (rx_f.cmd)
				{
					case DISPLAY: LCD_printf((char*)rx_f.payload); break;	// Display payload in text field
						
					case FOURHEX:												//Display 4 hex digit
						numt = rx_f.payload[0]; //rightmost digit
						numt1 = rx_f.payload[1];
						numt1 = numt1<<4;
						numt = numt | numt1;
						
						numt1 = rx_f.payload[2];
						numt1 = numt1<<8;
						numt = numt | numt1;
						
						numt1 = rx_f.payload[3];
						numt1 = numt1<<12;
						numt = numt | numt1;
						
						LCD_print4hex(numt);
					break;
									
					case PING_A://Reply on PING from Munin
						if (rx_f.payload[0]==0x2A) // Response on ping from Hugin
						{
							LCD_printf("MU END"); 
						} 
						else
						{
							LCD_printf("MU PING"); 
							USART_RADIOPING(0x2A); // Respond to PING from Munin
						}
						LCD_print4hex((uint16_t)rx_f.payload[0]);//Display PING character
					break;
					
					case GET_ABOT_SENSOR:
						numt = read_sensor_value(rx_f.payload[0]);
						USART_SENSOR(rx_f.payload[0], numt);
					break;
					
					case ABOT_UIO:
						//TODO: get user io, config, and set output
						USART_USERIO(UIO_STATUS,(uint16_t)0x1234,(uint16_t)0x5678);
					break;
					
					
					case ABOT_AUDIO:
						if (rx_f.payload[0]==AUDIO_TUNE_PLY)
						{
							switch (rx_f.payload[1])
							{
								case 0x01: play_music(james_bond,16); break;
								case 0x02: play_music(anthem,16); break;
								case 0x03: play_music(marry,16); break;
								default: play_music(james_bond,16); break;
							}							
						}
						
						//stop_music();
					break;
										
					default: 
						LCD_printf("rxDEFAULT"); 
						LCD_print4hex((uint16_t)rx_f.cmd);
					break;
				}	
							
			} else // display error, Error definitions in ABOT_protocol.h
			{
				LCD_set_symbol(LCD_SYMBOL_ATT);						//Error symbol on
				numt = rx_f.error;
				numt1 = rx_f.cmd;
				numt1 = numt1<<12;
				numt = numt | numt1;
				LCD_print4hex(numt); // Commnad to the left - error to the right
				LCD_printf((char*)rx_f.payload);
			}
			
			led_off();
			if (rxbuf.tail == rxbuf.head) 
			{
				RX_int_flag = 0; // clr receive flag if receive buffer empty
				LCD_clr_symbol(LCD_SYMBOL_RX); 
			}			
			
		} //if (RX0_int_flag) 
		
}
	


void mic_task(void)
{
	extern unsigned char ADC_int_flag;
	extern unsigned char mic_on;
	extern uint16_t mic_value;

	//if ((mic_on) && (ADC_int_flag)) //Microphone enabled and interrupt from ADC with new value
	if (ADC_int_flag)
	{ 
		ADC_int_flag = 0;
		if (mic_value>0x03EF)
		{
			USART_TEST(0x01);
			LCD_print4hex(mic_value);
		}
	}
}	
 
int main(void)
{
	LCD_init();
	LCD_set_symbol(LCD_SYMBOL_RAVEN);
	USART_ABOT_init();
	Joystick_init();
	mic_init();
	hugin_sensor_init();
	
	sei(); //Enable interrupts
	
	LCD_printf("ABOT");
	led_off();
	while(1)
    {
		USART_rx_task();
		Joystick_task();
		mic_task();				
		//test_temp();
    }//while(1)
}
