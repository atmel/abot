/**
 * \file Munin_bot.c
 *
 * \brief main loop for Munin (mega1284) robot application running on the RAVEN kit
 *
 * \author    Torgeir Bjornvold
 *
 * Copyright (c) 2011 - 2012 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */



//GCC includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

//Abot common includes
#include <services/ABOT_protocol.h>
#include <services/USART_ABOT_cmd.h>
#include <components/RADIO_RF230_MAC.h>
#include <components/Compass_AK8975.h>
#include <components/Accelerometer_BMA150.h>
#include <components/MOTOR_SERVO_360.h>
#include <components/switch_ABOT.h>
#include <components/EEPROM_24C02.h>
#include <components/Gyro_ITG3200.h>
#include <drivers/drv_1284_TWI.h>
#include <drivers/drv_1284_TC3_counter.h>
#include <drivers/drv_1284_adc.h>



/* ============= Set Fuses ======================================================================== */
/*
Fuse Low Byte =0xE7   1110 0111
CKDIV8 1 (disabled)
CKOUT  1 (disabled)
 Start-up times are determined by the SUT Fuses 
SUT1..0 = 10  Additional Delay from Reset (VCC = 5.0V) set to 4 CK + 65 ms
 The Low-frequency Crystal Oscillator must be selected by setting the CKSEL Fuses to �0110� or �0111�  
CKSEL3...0 = 0111 Start-up Time from Power-down and Power-save set to 32K CK (recommended)

Fuse Low Byte =0x62   0110 0010
CKDIV8 0 (enabled)
CKOUT  1 (disabled)
 Start-up times are determined by the SUT Fuses 
SUT1..0 = 10  Additional Delay from Reset (VCC = 5.0V) set to 4 CK + 65 ms
 
CKSEL3...0 = 0010 Internal RC Oscillator

 Fuse High Byte =0x1F   0001 1111
 OCDEN = 0 (enabled)
 JTAG  = 0 (enabled)
 ISP   = 0 (enabled)
 WDTON = 1 (disabled) Watchdog Timer always off
 EESAVE= 1 (disabled) EEPROM not preserved through the Chip Erase
 BOOTSZ1:0=11 Boot Size = 256 word Boot Reset Address = 0x3F00
 The Flash memory is organized in two main sections, the Application section and the Boot
	Loader section (see Figure 26-2). The size of the different sections is configured by the
	BOOTSZ Fuses as shown in Table 26-9 on page 300 and Figure 26-2
 BOOTRST= 1 (disabled) Select Reset Vector disabled, Reset Vector = Application Reset (address 0x0000)
 
 Extended Fuse Byte =0xFF (factory setting)
 No BOD
 
 Note that the fuses are read as logical zero, �0�, if they are programmed.
 */

#include <avr/fuse.h>
FUSES =
	{
		.low = 0x62,
		.high = 0x1F,
		.extended = 0xff,
	};
/* ============= Set Fuses END======================================================================== */
	

	
/*
If some pins are unused, it is recommended to ensure that these pins have a defined level. Even
though most of the digital inputs are disabled in the deep sleep modes as described above, floating
inputs should be avoided to reduce current consumption in all other modes where the digital
inputs are enabled (Reset, Active mode and Idle mode).
The simplest method to ensure a defined level of an unused pin, is to enable the internal pull-up.
In this case, the pull-up will be disabled during reset. If low power consumption during reset is
important, it is recommended to use an external pull-up or pull-down. Connecting unused pins
directly to VCC or GND is not recommended, since this may cause excessive currents if the pin is
accidentally configured as an output.
*/

uint8_t rand_seed; // random number for MAC short address request


void task_USART_rx(void){ //Take action on received msg on USART from Hugin

	extern unsigned char RX_int_flag;
	ABOT_frame rx_f; //receive frame
	unsigned char state;
	unsigned char status;
	unsigned char i;
	char debugs[MAX_PAYLOAD];
	uint16_t t, t2;
	
	if (RX_int_flag){ // Data from Hugin
			
		rx_f = USART_Get_frame();
		if (rx_f.error==0) //if frame ok - execute command
		{
			switch (rx_f.cmd)
			{		
				case PING_A:	 
					switch(rx_f.payload[0])
					{
						case 0x2A:transmit_RADIO_DISPLAY("PING reply from Hugin"); break;// Hugin responded to a PING
						case 0x42: USART_RADIOPING(0x2A); break;                         // Respond on a PING request from Hugin
						default: transmit_RADIO_PING(rx_f.payload[0]); break;            // Transmit PING on RF on request from Hugin
					}					
				break; 
				
				case GETSTATE:                                       // Respond on a radio state request from Hugin
					state= get_RADIO_state();
					USART_print4hex((unsigned int)state);
				break;
				
				case GETRSSI:                                        // Respond on a radio RSSI request from Hugin
					status = get_RADIO_RSSI();
					USART_print4hex((unsigned int)status);
				break;	
				case GETSHORT:                                        // Respond on a radio Short address request from Hugin
					USART_printf("MAC SHORT"); // Unit node address
					USART_print4hex(MAC_SHORT);
				break;
											
				//case ABOT:  ABOT_command(rx_f.payload[0]); break;   // Receive ABOT command from Hugin
				case ABOT:  transmit_RADIO_ABOT(rx_f.payload[0]); break; //Transmit ABOT cmd from Hugin to Radio
				
				case JOYSTICK:	 // Transmit joystick position from Hugin to Radio
					switch (rx_f.payload[0]){
						case KEY_STATE_UP: USART_printf("UP"); transmit_RADIO_ABOT_motor(FORWARD,255);break;
						case KEY_STATE_DOWN: USART_printf("DOWN"); transmit_RADIO_ABOT_motor(REVERSE,20);break;
						case KEY_STATE_RIGHT: USART_printf("RIGHT"); transmit_RADIO_ABOT_motor(RIGHT_FORWARD,255);break;
						case KEY_STATE_LEFT:USART_printf("LEFT");transmit_RADIO_ABOT_motor(LEFT_FORWARD,255); break;
						default: USART_printf("JOY ERR"); break;
					}//Switch JOYSTICK
				break;
				
				case ABOT_SENSOR: 
					t= rx_f.payload[1]; // Low byte
					t2= rx_f.payload[2] << 8; // high byte
					t = t | t2;
					transmit_RADIO_A_SENSOR(rx_f.payload[0],t);
				break;
				
				case ABOT_UIO:  // Always UIO_STATUS from Hugin
					t = rx_f.payload[1];// Low byte
					t2 = rx_f.payload[2]<< 8;;// High byte
					t = t | t2;
					transmit_RADIO_USERIO(rx_f.payload[0],t,t);
				break;	
				
				case DISPLAY:  // Relay printf from Hugin
					for (i = 0; i < rx_f.length; i++)
					{
					  debugs[i] = rx_f.payload[i];
					}
					rx_f.payload[i]='\0';
					transmit_RADIO_DISPLAY(debugs);
				break;	
				
				case TEST:                                           //Transmit TEST command from Hugin to Radio
					ABOT_auto(); 
				break;
					
				//case TEST: transmit_RADIO_TEST(rx_f.payload[0]); break; //Transmit TEST command from Hugin to Radio
													
				default: 
					USART_printf("CMD ERR");
					USART_print4hex((unsigned int)rx_f.cmd);
					sprintf(debugs, "CMD 0x%X not supported",rx_f.cmd);
					transmit_RADIO_DISPLAY(debugs);
				break;
			} //switch (rx_f.cmd)		
		} else //if frame error print error msg Error definitions in ABOT_protocol.h
		{
			USART_printf("MRx ERR");
			USART_print4hex(rx_f.error);
			sprintf(debugs, "USART receive error 0x%X",rx_f.error);
			transmit_RADIO_DISPLAY(debugs);
		}
		RX_int_flag=0;
	} //if (RX0_int_flag) 	
}


/*! \brief The RF network interface
 *
 * Monitor the RF traffic and respond according to the functions implemented
 *
 * RF230 interrupts enabled:
 *  IRQ_3: TRX_END
 *  IRQ 1: PLL unlock
 *  IRQ_0: PLL_LOCK
 */
void task_RF(void)
{
	
	extern unsigned int ABOT_PW_A; //Pulse Width A, starboard motor
	extern unsigned int ABOT_PW_B; //Pulse Width B, port motor
	extern unsigned int ABOT_CAL_A; //Calibration value starboard motor
	extern unsigned int ABOT_CAL_B; //Calibration value port motor
	extern unsigned int ABOT_CAL_SPEED; //Calibration value forward speed
	
	extern unsigned char RF230_int_flag;
	extern unsigned char MAC_RANDOM1;
	extern unsigned char MAC_RANDOM2;
	RADIO_frame RF_f;
	unsigned char state;
	unsigned char status;
	char debugs[MAX_PAYLOAD];
	unsigned char t;
	uint16_t i, i2, i3;
	//uint64_t ieee_adr;
	
	if (RF230_int_flag){ // interrupt from RF230 detected
		RF230_int_flag= 0x0; // interrupt handled

		status=get_IRQ_status();	// Read interrupt status register 0x0F (IRQ_STATUS), reg cleared
		state= get_RADIO_state();	// Get RADIO current state
			
		if ((status & 0x02) != 0) //IRQ 1: PLL unlock
		{
			USART_printf("PLL ERR");
			USART_print4hex((unsigned int)status);
		}
			
		if ((status & 0x08) != 0)  //IRQ_3: TRX_END 
		{
				
			/*IRQ_3: TRX_END	RX:  Indicates the completion of a frame reception. 
								TX:  Indicates the completion of a frame transmission. */
				
			if ((state == RF_RX_ON_STATE) || (state == RF_RX_AACK_ON_STATE) ) //  frame received, decode command
			{
				RF_f = get_RADIO_rx_frame(); // get received frame
					
				if (RF_f.ForMe == 0x01)  //ABOT destination address = mine or broadcast
				{
					USART_printf("FOR ME");
					USART_print4hex(RF_f.source_adr);
				 
					switch (RF_f.PSDU[0])  //command byte
					{		
					
						case PING_A:  // Reply to ping from external RF
							switch (RF_f.PSDU[1]){
								case 0x2A: USART_printf("RF END"); break; //PING END - no reply
								
								default:
									USART_printf("RF PING");
									USART_print4hex((unsigned int)RF_f.PSDU[1]);
									transmit_RADIO_PING(0x2A); //Reply to PING
									USART_RADIOPING(0x4C);  //Ping Hugin
								break;
							}//Switch 
						
						break;
						
						//case PINGRAVEN: USART_RavenPING(RF_f.PSDU[1]); break; // PING Hugin 
						
						case JOYSTICK: 
								ABOT_motor_ctrl(RF_f.PSDU[1]);  // motor control cmd to ABOT
								switch (RF_f.PSDU[1]){
									case KEY_STATE_UP: USART_printf("UP"); break;
									case KEY_STATE_DOWN: USART_printf("DOWN"); break;	
									case KEY_STATE_RIGHT: USART_printf("RIGHT"); break;
									case KEY_STATE_LEFT:USART_printf("LEFT"); break;
									default: USART_printf("JOY ERR"); break;
								}//Switch JOYSTICK
						break;
						
						case VERSION: transmit_RADIO_VERSION(); break;
						
						case GETCRTR: transmit_RADIO_CRTR(); break;
															
						case ABOT: ABOT_command(RF_f.PSDU[1]); break;
						
						case ABOT_MOTOR_DIRECTION: ABOT_motor_speed_ctrl(RF_f.PSDU[1],RF_f.PSDU[2]);break; // motor control cmd to ABOT
						
						case GET_ABOT_SENSOR: USART_GET_SENSOR(RF_f.PSDU[1]); break; // Send request to Hugin
						
						case ABOT_UIO:
							switch (RF_f.PSDU[1])
							{
								case UIO_CONFIGURE:
								case UIO_SET:
									i = RF_f.PSDU[2];//Low byte
									i2 = RF_f.PSDU[3]<< 8;;//High byte
									i = i | i2;
									i3 = RF_f.PSDU[4];//Low byte
									i2 = RF_f.PSDU[5]<< 8;;//High byte
									i3 = i3 | i2;
									USART_USERIO(RF_f.PSDU[1],i,i3);
								break;
								case UIO_REQUEST:
									USART_USERIO(RF_f.PSDU[1],0x0,0x0);
								break;
							}//Switch 
						break;
						
						case SET_RF_CH: // Change network communication channel
							set_RF_channel(RF_f.PSDU[1]); 
							USART_printf("RF CH");
							USART_print4hex((unsigned int)RF_f.PSDU[1]);
						break; 
						
						case SET_RF_SHORT: // Change Node SHORT address
							if ((RF_f.PSDU[1]==MAC_RANDOM1) && (RF_f.PSDU[2]==MAC_RANDOM2))
							{
								i = RF_f.PSDU[3];//Low byte
								i2 = RF_f.PSDU[4]<< 8;;//High byte
								i = i | i2;
								set_RF_SHORT(i); // Set new SHORT address
								USART_printf("NEW SHORT");
								USART_print4hex(i);
							}
						break;
						
						case REQ_RF_SHORT: break; //Node do not reply on request for new net address
															
						case SET_RF_PWR: set_RF_output_pwr(RF_f.PSDU[1]); break; // Change node TX pwr
															
						case TEST: // can be used for any debugging
							if (RF_f.PSDU[1] == 0x01){
								USART_printf("TEST1");
								transmit_RADIO_DISPLAY("TEST1");
															
								sprintf(debugs, "Starboard speed: %d ",ABOT_PW_A);
								transmit_RADIO_DISPLAY(debugs);
								sprintf(debugs, "Starboard cal  : %d ",ABOT_CAL_A);
								transmit_RADIO_DISPLAY(debugs);
								sprintf(debugs, "Port      speed: %d ",ABOT_PW_B);
								transmit_RADIO_DISPLAY(debugs);
								sprintf(debugs, "Port      cal  : %d ",ABOT_CAL_B);
								transmit_RADIO_DISPLAY(debugs);
								sprintf(debugs, "Speed     cal  : %d ",ABOT_CAL_SPEED);
								transmit_RADIO_DISPLAY(debugs);
							} else {
								USART_printf("TESTx");
								transmit_RADIO_DISPLAY("TESTx");
							}						
						break;
						
						case CLR_RF_SHORT: transmit_RADIO_REQSHORT(rand_seed); break;
						
						case ABOT_AUDIO: 
							transmit_RADIO_DISPLAY("Audio cmd received");
							i = RF_f.PSDU[3];//Low byte
							i2 = RF_f.PSDU[4]<< 8;;//High byte
							i = i | i2;
							if ((i>0) && (i<MAX_PAYLOAD))
							{
								for (i2 = 0; i2 < i; i2++)
								{
									debugs[i2]=RF_f.PSDU[i2+4];
								}
							}
							USART_AUDIO(RF_f.PSDU[1],i,debugs);
						break; // Audio stuff
											
						default: 
							USART_printf("RFC ERR"); // RADIO Command Error
							USART_print4hex((unsigned int)RF_f.PSDU[0]);
							sprintf(debugs, "CMD 0x%X not supported",RF_f.PSDU[0]);
							transmit_RADIO_DISPLAY(debugs);
						break;
					} //switch (RF_f.PSDU[0])
				}//if (RF_f.ForMe == 0x01)
				 
			} else // frame transmitted, 
			{
				//Only in Extended mode
				if (RF_OPERATION_MODE == RF_EXT_OP_MODE) //Extended Operation Mode
				{
					t = get_RADIO_TX_status();
					if (t != 0)
					{
						USART_printf("TX ERROR");
						USART_print4hex((unsigned int)t);				
					} //else USART_printf("TX OK");
				}				
				
				RX_enable(); // Start listen for incoming frames
				
			}
			
		} //if ((status & 0x08) != 0)  //IRQ_3: TRX_END 
	
	}	//if (RF230_int_flag)			
			
}


void task_compass(void)
{
	extern unsigned char compass_int_flag;	
	compass_field compass_raw_data;
	compass_heading c_heading;
	char debugs[0x2F];
	
		compass_raw_data=get_compass_raw_data();
		c_heading=calculate_heading(compass_raw_data);
		
		//set_compass_mode(AK8975_MODE_POWERDOWN);
		
		//sprintf(debugs, "Compass raw %g & dir %e ",compass_raw_data.X_axis, c_heading.direction);
		//sprintf(debugs, "C_dir %f ", c_heading.direction);
		//sprintf(debugs, "Compass raw %i - %i & dir %i bus0x%x s 0x%x",(int)compass_raw_data.X_axis, (int)compass_raw_data.Y_axis,(int)c_heading.direction, compass_raw_data.bus_status,compass_raw_data.sensor_status);
		//sprintf(debugs, "Compass raw %i - %i - %i & dir %i ",compass_raw_data.X_axis, compass_raw_data.Y_axis,compass_raw_data.Z_axis, c_heading.direction);
		sprintf(debugs, "raw %i - %i - %i & d:%i i:%i s:%i ",(int)compass_raw_data.X_axis, (int)compass_raw_data.Y_axis, (int)compass_raw_data.Z_axis,(int)c_heading.direction,(int)c_heading.inclination,(int)c_heading.strength);
		transmit_RADIO_DISPLAY(debugs);			
		 
		compass_int_flag=0; //clr interrupt
		//After power-down mode is set, at least 100?s(Twat) is needed before setting another mode.
		set_compass_mode(AK8975_MODE_SNG_MEASURE);  //initiate compass measurement - interrupt generated when data ready again
		//set_compass_mode(AK8975_MODE_SELF_TEST);
}

void task_accelerometer(uint8_t mode)
{
	accelerometer_data_t accelerometer_data;
	extern position_t position;
	char debugs[MAX_PAYLOAD];
	static volatile accelerometer_data_t previous_accelerometer_data = {0, 0, 0, 0};
	static volatile uint8_t printPos = 0;
	
	if (mode == ACCELEROMETER_MODE_POSITION){
		// Note we need to use last sample of acceleration data recorded and the current 
		// counter value for calculations since the previous acceleration has lasted for 
		// a time corresponding to the counter value
		accelerometer_data = previous_accelerometer_data;
	
		// Record timestamp (read counter and reset it)
		accelerometer_data.timestamp = counter_get_value();
		counter_reset();
	
		// Read the accelerometer data from the BMA150 device
		accelerometer_get_raw_data((accelerometer_data_t*) &previous_accelerometer_data);
	
		// If timestamp is 0xFFFF we might have had an overflow condition
		if (accelerometer_data.timestamp == 0xFFFF){
			sprintf(debugs, "Warning! Accelerometer timestamp overflow!");
			// Send the string to the host
			transmit_RADIO_DISPLAY(debugs);		
		}		
	
		// Calculate new position
		position = accelerometer_get_posistion (accelerometer_data, position);
	
		// Only printing position occasionally to save CPU time
		if (printPos){
			printPos--;	
		}		
		else{
			// Note that the long int conversion will limit the max values, however printing 64 bit integers is not implemented for sprintf
			sprintf(debugs, "Pos [mm]: x: %li; y: %li; z: %li;", (long int) (position.pos_x/1000), (long int)(position.pos_y/1000), (long int)(position.pos_z/1000));
			// Send the string to the host
			transmit_RADIO_DISPLAY(debugs);		
		
			//sprintf(debugs, "timestamp: %i", (int) accelerometer_data.timestamp);
			// Send the string to the host
			//transmit_RADIO_DISPLAY(debugs);		
		
			printPos = 20;
		}		
	
		//sprintf(debugs, "Speed: x: %i mm/s; y: %i mm/s; z: %i mm/s", (long int) position.vel_x, (long int) position.vel_y, (long int) position.vel_z);
		// Send the string to the host
		//transmit_RADIO_DISPLAY(debugs);		
	}		
	else if (mode == ACCELEROMETER_MODE_MOTION_ALERT){
		if (accelerometer_int_flag){
			sprintf(debugs, "Change in motion detected");
			// Send the string to the host
			transmit_RADIO_DISPLAY(debugs);		
		
			// Clear flag
 			accelerometer_int_flag = 0;	
		}		
	}		 
}

void task_gyro(void)
{
	gyro_data_t gyro_data;
	char debugs[MAX_PAYLOAD];

        if (gyro_get_raw_data(&gyro_data) == ITG3200_STATUS_OK)
        {
            gyro_data = gyro_convert_data(gyro_data);
            sprintf(debugs, "Gyro [deg/sec] x=%i;y=%i;z=%i",(int)gyro_data.x_axis, (int)gyro_data.y_axis, (int)gyro_data.z_axis);
            transmit_RADIO_DISPLAY(debugs);			
        }
        else
        {
            sprintf(debugs, "ERROR when reading gyro");
            // Send the string to the host
            transmit_RADIO_DISPLAY(debugs);	
        }	
	
        // Clear flag
        accelerometer_int_flag = 0;	
}

int main(void)
{
	extern unsigned char RX_int_flag;
	extern unsigned char RF230_int_flag;
	extern unsigned char ABOT_int_flag; // ABOT switch
	extern unsigned char compass_int_flag;
	extern unsigned char gyro_int_flag;
	extern uint16_t MAC_SHORT; // current MAC short address	
	unsigned char ABOT_SWITCH_ENABLED; //ABOT switch action enabled
	
	
	// Accelerometer mode. 
	// The motion alert mode can be used to detect change in the ABOTs motion (stop, start, crash, turn etc.)
	// The position mode can be used to track relative position. Note however that this mode is not very useful 
	// because of the large error quickly building up due to the double integration of the acceleration.
	uint8_t accelerometer_mode = ACCELEROMETER_MODE_MOTION_ALERT;
	
	switch_ABOT_init();
	ABOT_SWITCH_ENABLED = 0; //Disable Switch
	USART_ABOT_init();
	RADIO_init();
	Motor_init();
	Compass_init();
	accelerometer_init(accelerometer_mode);
	gyro_init();	
	adc_simple_init(ADC_CHAN_ADC14);  //For generating random number
	sei(); //Enable interrupts
	RX_enable(); // Start listen for incoming frames
	rand_seed = (uint8_t)adc_read(); //measure ADC bandgap channel with Analog VCC -
							//slightly varies across boards due to design tolerance	

	USART_printf("MAC SHORT"); // Unit node address
	USART_print4hex(MAC_SHORT);
	transmit_RADIO_REQSHORT(rand_seed); // Get address in Abot network
	
	set_compass_mode(AK8975_MODE_SNG_MEASURE);  //initiate compass measurement - interrupt generated when data ready
	
	while(1)
    {
		task_RF(); //RADIO message received or to be transmitted
		
		task_USART_rx(); //Take action on messages received on UART if any
		
		if (ABOT_int_flag)// some of ABOT's switches has triggered an interrupt
		{  
			if (ABOT_SWITCH_ENABLED)
			{
				transmit_RADIO_DISPLAY("Switch interrupt");
				ABOT_reverse_turn_forward();
			}	
		}//if (ABOT_int_flag)
		
		if (compass_int_flag){  // Data ready from compass
			//transmit_RADIO_DISPLAY("Compass interrupt");
			//task_compass();
		} //if (compass_int_flag)

		// Add this line to start using the accelerometer
		//task_accelerometer(accelerometer_mode);
		
		if (gyro_int_flag){
			// Add this line to start reading gyro data
			//task_gyro();	
		}			

	}//while(1)

}
 

