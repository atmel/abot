package com.atmel.abot;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public final class AppPreferences {
	/** Preference keys */
	public static final String KEY_NETWORK_PREFERENCE    = "network_preference";
	public static final String KEY_IP_ADDRESS_PREFERENCE = "ip_address_preference";
	public static final String KEY_PORT_NUMBER_PREFERENCE = "port_number_preference";
	
	/** Network Constants */
	public static final Integer ABOT_VIRTUAL_ROUTER = 1;
	public static final Integer OTHER_NETWORK       = 2;
	
	/** Preference members */
	private SharedPreferences mAppSharedPrefs;
	private SharedPreferences.Editor mAppPrefsEditor;
	
	public AppPreferences(Context context) {
		mAppSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		mAppPrefsEditor = mAppSharedPrefs.edit();
	}
	
	/** Is "Abot Virtual Router" selected */
	public boolean isAbotRouterSelected() {
		return (Integer.parseInt(mAppSharedPrefs.getString(KEY_NETWORK_PREFERENCE, null)) == ABOT_VIRTUAL_ROUTER);
	}
	
	/** IP Address on which the server is listening */
	public String getIpAddress() {
		return mAppSharedPrefs.getString(KEY_IP_ADDRESS_PREFERENCE, null);
	}
	
	/** Port number of the server */
	public String getPortNumber() {
		return mAppSharedPrefs.getString(KEY_PORT_NUMBER_PREFERENCE, null);
	}
	
	public void setIpAddress(String ipAddress) {
		Log.d("AppPreferences","Changing IP Address to " + ipAddress);
		mAppPrefsEditor.putString(KEY_IP_ADDRESS_PREFERENCE, ipAddress);
		mAppPrefsEditor.commit();	
	}
	
	public void setPortNumber(String portNumber) {
		Log.d("AppPreferences","Changing Port number to " + portNumber);
		mAppPrefsEditor.putString(KEY_PORT_NUMBER_PREFERENCE, portNumber);
		mAppPrefsEditor.commit();
	}
	
	public void selectAbotRouter() {
		mAppPrefsEditor.putString(KEY_NETWORK_PREFERENCE, String.valueOf(ABOT_VIRTUAL_ROUTER));
		mAppPrefsEditor.commit();
	}
}
