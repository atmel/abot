package com.atmel.abot;

import android.app.Application;

public class AbotControllerApplication extends Application {
	
	/* Global AbotManager object to be used by all activities */
	public AbotManager mGlobalAbotManager;
	
	@Override
	public void onCreate() {
		super.onCreate();
		// Create AbotManager
		mGlobalAbotManager = new AbotManager(this);
	}

	public void onTerminate() {
		super.onTerminate();
	}
}
