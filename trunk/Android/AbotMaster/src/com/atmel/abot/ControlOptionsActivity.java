package com.atmel.abot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

public class ControlOptionsActivity extends BaseControlActivity implements OnItemSelectedListener{
	
	/** Dialog IDs */
	private static final int NO_ABOT_FOUND_ID = 0x01;
	private static final int SELECTING_ABOT_FAILED_ID = 0x02;
	
	/** Flags */
	private boolean mAbotSelectedFlag = false;
	private boolean mNoAbotRefreshFlag = false;
	
	
	/** Abot */
	AbotManager.Abot mAbot;
	
	/** UI elements */
	private Spinner mAbotSpinner;
	private ImageButton mAbotRefreshButton;
	private ImageButton mMotorCalibrationButton;
	private ImageButton mBasicControlButton;
	private ImageButton mAccelerometerControlButton;
	private ProgressDialog mAbotSearchProgressDialog;
	
	/** */
	HandlerThread mControlThread;
	
	Handler mControlHandler;
	
	/** Abot Handler to carry out Abot operations  */
	class ControlHandler extends Handler {
		
		/** Commands */
		private static final int GET_AVAILABLE_ABOTS = 0x10;
		
		public ControlHandler(Looper looper) {
			super(looper);
		}
		
		private void showNoAbotInUiThread() {
			runOnUiThread(new Runnable() {
				public void run() {
					// Cancel progress dialog
					mAbotSearchProgressDialog.dismiss();
					showDialog(NO_ABOT_FOUND_ID);	
				}
				
			});
		}
		
		private void updateAbotSpinner(final byte[] abots) {
			runOnUiThread(new Runnable() {
				public void run() {
					// Change Abot IDs to strings
					String[] abotArray;
					if(abots.length!=1) {
						abotArray = new String[abots.length+1];
						// Add option to select all abots
						abotArray[abots.length] = "All Abots   ";
					} else {
						abotArray = new String[abots.length];
					}
					for(int i=0; i<abots.length; i++ ) {
						abotArray[i] = "Abot-" + abots[i]+ "  ";                 // Add some space to move text towards left on spinner display (its a workaround, need to good way)
					}
					
					// Create adapter with the Abot IDs
					ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(getApplicationContext(),R.layout.abot_spinner_textview,abotArray);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					mAbotSpinner.setAdapter(adapter);
					
					// Dismiss "Abot progress dialog"
					mAbotSearchProgressDialog.dismiss();
				}
			});
		}
		
		@Override
		public void handleMessage(Message message) {
			switch(message.what) {
			case GET_AVAILABLE_ABOTS:
				// Read available abots
				byte[] abots = mAbotManager.getAvailableAbots();
				if(abots!=null) {
					// Found some Abots
					Log.d("ControlHandler","Found some abots - " + abots.toString());
					updateAbotSpinner(abots);	
				} else {
					// No Abot Found
					showNoAbotInUiThread();
					
				}
				break;
				
			default:
				break;
			}
		}
	}
	
	View.OnClickListener mControlModeListener = new View.OnClickListener() {
		public void onClick(View view) {
			if(! mAbotSelectedFlag) {
				// No Abot is selected, select all Abots
				if(!mAbotManager.selectAllAbots()) {
					// Unable to select abots, show error
					showDialog(SELECTING_ABOT_FAILED_ID);
					return;
				}
			}
			if(view.equals(mBasicControlButton)) {
				// Chosen basic control mode
				Intent intent = new Intent(ControlOptionsActivity.this, BasicControlActivity.class);
				startActivity(intent);
				
			} else if(view.equals(mAccelerometerControlButton)) {
				// Chosen accelerometer based control mode
				Intent intent = new Intent(ControlOptionsActivity.this, AcceleroControlActivity.class);
				startActivity(intent);
				
			}
		}
	};
	
	View.OnClickListener mAbotRefreshListener = new View.OnClickListener() {
		
		public void onClick(View v) {
			// Refresh Abot spinner with Active abots
			getAvailableAbots();
			
		}
	};
	
	View.OnClickListener mMotorCalibrationListener = new View.OnClickListener() {
		
		public void onClick(View v) {
			// Before calling motor calibration activity, set 'mNoAbotRefreshFlag' to avoid refreshing AbotSpinner on 'onResume()'
			mNoAbotRefreshFlag = true;
			Intent intent = new Intent(ControlOptionsActivity.this, MotorCalibrationActivity.class);
			startActivity(intent);
		}
	};
	
	/** Called when an Abot is chosen in the spinner */
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		String abotString = (String) parent.getItemAtPosition(position);
		// Check if all abots are chosen
		if (abotString.substring(0,3).equals("All")) {
			if(mAbotManager.selectAllAbots()) {
				// Selected all Abots successfully
				mAbotSelectedFlag = true;
				// Disable motor calibration as All abots cannot be calibrated simultaneously
				mMotorCalibrationButton.setEnabled(false);
				Log.d("ControlOptionsActivity","Selected all Abots");
			} else {
				// Unable to select all abots
				showDialog(SELECTING_ABOT_FAILED_ID);
			}
		} else {
			int abotId = Integer.parseInt(abotString.substring(5,abotString.indexOf(" ")));            // Leave "Abot-" from "Abot-xx" to get Abot ID
			if(mAbotManager.selectAbot(abotId)) {
				// Selected the chosen Abot successfully
				mAbotSelectedFlag = true;
				mMotorCalibrationButton.setEnabled(true);
				Log.d("ControlOptionsActivity","Selected Abot-" + abotId + " successfully");
			} else {
				// Unable to get chosen Abot
				showDialog(SELECTING_ABOT_FAILED_ID);
			}
		}
	}
	
	
	public void onNothingSelected(AdapterView<?> parent) {
			
	}
	
	public void getAvailableAbots() {
		mAbotSelectedFlag = false;
		mMotorCalibrationButton.setEnabled(false);
		mAbotSearchProgressDialog.show();
		mControlHandler.sendEmptyMessage(ControlHandler.GET_AVAILABLE_ABOTS);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.control_options);
		
		// Get handle to view objects
		mAbotSpinner = (Spinner) findViewById(R.id.abot_spinner);
		mAbotRefreshButton = (ImageButton) findViewById(R.id.abot_refresh);
		mMotorCalibrationButton = (ImageButton) findViewById(R.id.motor_calibration_button);
		mBasicControlButton = (ImageButton) findViewById(R.id.basic_control_mode_button);
		mAccelerometerControlButton = (ImageButton) findViewById(R.id.accelerometer_control_mode_button);
		
		// Register listener for spinner, refresh and control mode buttons
		mAbotSpinner.setOnItemSelectedListener(this);
		mAbotRefreshButton.setOnClickListener(mAbotRefreshListener);
		mMotorCalibrationButton.setOnClickListener(mMotorCalibrationListener);
		mBasicControlButton.setOnClickListener(mControlModeListener);
		mAccelerometerControlButton.setOnClickListener(mControlModeListener);
		
		// Disable Motor Calibration button till an Abot is chosen
		mMotorCalibrationButton.setEnabled(false);
		
		// Start control thread
		mControlThread = new HandlerThread("ControlThread");
		mControlThread.start();
		mControlHandler = new ControlHandler(mControlThread.getLooper());
		
		// Initialize progress dialog
		mAbotSearchProgressDialog = new ProgressDialog(this);
		mAbotSearchProgressDialog.setMessage(getResources().getString(R.string.get_available_abots_progress));
		mAbotSearchProgressDialog.setIndeterminate(true);
		mAbotSearchProgressDialog.setCancelable(false);
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		mAbotSelectedFlag = false;
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		if(mAbotManager.isConnected()) {
			if(mNoAbotRefreshFlag) {
				// Do not refresh Abots, just reset flag
				mNoAbotRefreshFlag = false;
			} else {
				getAvailableAbots();
			}
			
		}
	}
	
	@Override 
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
    @Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog;
    	switch(id) {
    	case NO_ABOT_FOUND_ID:
    		dialog = new AlertDialog.Builder(this)
    						.setTitle(R.string.alert_dialog_no_abot_found_title)
    						.setMessage(R.string.alert_dialog_no_abot_found_msg)
							.setPositiveButton(R.string.alert_dialog_retry, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// Dismiss dialog
									dialog.dismiss();
									// Search for abots again
									getAvailableAbots();
								}
							})
							.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// Cancel and go to MainActivity
									dialog.dismiss();
									Intent intent = new Intent(ControlOptionsActivity.this,MainActivity.class);
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);
								}
							})
							.create();
    		break;
    		
    	case SELECTING_ABOT_FAILED_ID:
    		dialog = new AlertDialog.Builder(this)
    						.setIcon(android.R.drawable.ic_dialog_alert)
    						.setTitle(R.string.alert_dialog_selecting_abot_failed_title)
    						.setMessage(R.string.alert_dialog_selecting_abot_failed_msg)
    						.setOnCancelListener(new DialogInterface.OnCancelListener() {

								public void onCancel(DialogInterface dialog) {
									// Dismiss dialog 
									dialog.dismiss();
									getAvailableAbots();	
								}
    							
    						})
    						.create();
    						
    						
    		
    		dialog = null;
    		break;
    	default:
    		dialog = null;
    	}
    	return dialog;
    	
    }
}
