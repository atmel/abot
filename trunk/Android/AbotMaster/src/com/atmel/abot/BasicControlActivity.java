package com.atmel.abot;

import com.atmel.abot.AbotManager.Abot;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

public class BasicControlActivity extends BaseControlActivity {
	
	/** Control buttons */
	private ImageButton mForwardButton;
	private ImageButton mReverseButton;
	private ImageButton mLeftButton;
	private ImageButton mRightButton;
	
	/** Separate thread for abot operations */
	HandlerThread mControlThread;
	
	Handler mControlHandler;
	
	/** Abot */
	AbotManager.Abot mAbot;
	
	View.OnTouchListener mButtonListener = new View.OnTouchListener() {
		
		public boolean onTouch(View view, MotionEvent event) {
			view.onTouchEvent(event);
			if(view.equals(mForwardButton)) {
				Log.d("ButtonListener","Forward");
				if(MotionEvent.ACTION_DOWN == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.FORWARD,0));
				}
				else if(MotionEvent.ACTION_UP == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.STOP,0));
				}
				return true;
			}
			else if(view.equals(mReverseButton)) {
				Log.d("ButtonListener","Reverse");
				if(MotionEvent.ACTION_DOWN == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.REVERSE,0));
				}
				else if(MotionEvent.ACTION_UP == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.STOP,0));

				}
				return true;
			}
			else if(view.equals(mLeftButton)) {
				Log.d("ButtonListener","Left");
				if(MotionEvent.ACTION_DOWN == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.LEFT,0));
					//mControlHandler.sendMessageDelayed(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.FORWARD,0),ControlHandler.COMMAND_INTERVAL);
				}
				else if(MotionEvent.ACTION_UP == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.STOP,0));

				}
				return true;
			}
			else if(view.equals(mRightButton)) {
				Log.d("ButtonListener","Right");
				if(MotionEvent.ACTION_DOWN == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.RIGHT,0));
					//mControlHandler.sendMessageDelayed(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.FORWARD,0),ControlHandler.COMMAND_INTERVAL);
				}
				else if(MotionEvent.ACTION_UP == event.getAction()) {
					mControlHandler.sendMessage(mControlHandler.obtainMessage(ControlHandler.COMMAND,ControlHandler.STOP,0));

				}
				return true;
			}
			else {
				Log.d("ButtonListener","Reached else condition");
				return false;
			}
		}
	};
	
	class ControlHandler extends Handler {
		
		public static final long COMMAND_INTERVAL = 20;        // in milliseconds
		
		public static final int COMMAND = 0x10;
		
		public static final int FORWARD = 0x01;
		public static final int REVERSE = 0x02;
		public static final int LEFT    = 0x03;
		public static final int RIGHT   = 0x04;
		public static final int STOP    = 0x05;
		private static final byte INITIAL_SPEED = 50;
		private static final byte INITIAL_TURN = 20;
		
		private static final byte INCREMENT_TURN = 5;
		private static final byte MAX_TURN = (byte) 255;
		
		private int mTurnValue = 0;
		
		private boolean mAccelerate = false;
		
		public ControlHandler(Looper looper) {
			super(looper);
		}
		
		public void handleMessage(Message message) {
			if(message.what == COMMAND) {
				switch(message.arg1) {
				case FORWARD:
					if (!mAccelerate) {
						if(mAbot.forward(INITIAL_SPEED)) mAccelerate = true;
					} else {
						mAbot.forward();
					}
					break;
				case REVERSE:
					if (!mAccelerate) {
						if(mAbot.reverse(INITIAL_SPEED)) mAccelerate = true;
					} else {
						mAbot.reverse();
					}
					break;
				case LEFT:
					if (!mAccelerate) {
						mTurnValue = INITIAL_TURN;
						if(mAbot.left((byte) mTurnValue,Abot.FORWARD)) mAccelerate = true;
					} else {
						// Increment counter to increase speed gradually
						mTurnValue += INCREMENT_TURN;
						if (mTurnValue >= MAX_TURN) mTurnValue = MAX_TURN;
						mAbot.left((byte) (mTurnValue), Abot.FORWARD);
					}
					break;
				case RIGHT:
					if (!mAccelerate) {
						mTurnValue = INITIAL_TURN;
						if(mAbot.right((byte) mTurnValue,Abot.FORWARD)) mAccelerate = true;
					} else {
						// Increment counter to increase speed gradually
						mTurnValue += INCREMENT_TURN;
						if (mTurnValue >= MAX_TURN) mTurnValue = MAX_TURN;
						mAbot.right((byte) (mTurnValue), Abot.FORWARD);
					}
					break;
				case STOP:
					mAbot.stop();
					mAccelerate = false;
					removeMessages(COMMAND);
					break;
				default:
					Log.d("ControlHandler","Invalid case - " + message.what);
					break;
				}
				
				if(message.arg1 != STOP) {
					// Continue sending command 
					sendMessageDelayed(Message.obtain(message), COMMAND_INTERVAL);
				}
			}
			
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Show the basic control UI screen
		setContentView(R.layout.basic_control);
		
		// Get buttons
		mForwardButton = (ImageButton) findViewById(R.id.forward_button);
		mReverseButton = (ImageButton) findViewById(R.id.reverse_button);
		mLeftButton = (ImageButton) findViewById(R.id.left_button);
		mRightButton = (ImageButton) findViewById(R.id.right_button);
			
		// Register OnTouchListener for all buttons
		mForwardButton.setOnTouchListener(mButtonListener);
		mReverseButton.setOnTouchListener(mButtonListener);
		mLeftButton.setOnTouchListener(mButtonListener);
		mRightButton.setOnTouchListener(mButtonListener);
		
		// Create control thread
		HandlerThread mControlThread = new HandlerThread("ControlThread");
		mControlThread.start();
		mControlHandler = new ControlHandler(mControlThread.getLooper());
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		// Get Abot here
		mAbot = mAbotManager.getSelectedAbot();
		if(mAbot==null) {
			Log.d("BasicControlActivity","onResume - No Abot selected");
			//alertConnectionLost();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		Log.d("BasicControlActivity","Reached onCreateContextMenu");
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		menu.setHeaderTitle(R.string.control_interface_menu_title);
		inflater.inflate(R.menu.context_menu, menu);	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d("BasicControlActivity","Reached onOptionsItemSelected");
		return true;
		
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Log.d("BasicControlActivity","Reached onContextItemSelected");
		return true;
		
	}

}
