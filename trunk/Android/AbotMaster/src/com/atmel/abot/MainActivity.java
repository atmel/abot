package com.atmel.abot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends SettingsActivity {
	
	/** Dialog IDs */
	private static final int DIALOG_WIFI_NETWORK_NEEDED_ID   = 20;
	private static final int DIALOG_SOCKET_CONNECTION_FAILED = 21;
	private static final int DIALOG_EXIT_APP_ID              = 30;
    
	/** Connection states */
	private static final int CONNECTING_ABOT  =  40;
	private static final int CONNECTED        =  41;
	private static final int IDLE             =  42;
	private static final int CONNECTING_WIFI  =  43;
	
	/** View variables */
	private ImageButton mConnectButton;
	private ProgressBar mConnectionProgressBar;         // Initial visibility is "gone"
	private TextView mConnectionState;                  // Initial visibility is "gone"
	private ImageButton mSettingsButton;
	private ImageButton mHelpButton;
	
	private ImageView mConnectedImage;                   // Image representing that the connection is established
	
	private EditText mIpAddress;
	private EditText mPortNumber;
	
	
	private boolean mInvokedWifiSettings = false;
	
    /** AbotManager */
	private AbotManager mAbotManager;
	
	/** Network connection manager */
	private CustomNetworkManager mNetworkManager;
	
	/** Application preferences */
	private AppPreferences mAppPreferences;
	
	/** Thread to execute Connection handler */
	private HandlerThread mConnectionThread;
	
	/** Connection handler to carry out wifi operations */
	class ConnectionHandler extends Handler {
		
		public static final int INITIAL_CONNECTION_CHECK       = 10;
		public static final int CHECK_ABOTBRIDGE_CONNECTION    = 11;
		public static final int CONNECT_ABOTBRIDGE             = 12;
		public static final int START_CONTROL_OPTIONS_ACTIVITY = 13;
		
		public ConnectionHandler(Looper looper) {
			super(looper);
		}
		private void showDialogInUiThread(final int dialogId) {
			Log.d("ConnectionHandler","showDialogInUiThread");
			runOnUiThread(new Runnable() {
				public void run() {
					if(MainActivity.this.hasWindowFocus()) {
						showDialog(dialogId);
					}	
				}
				
			});
		}
		private void showConnectionStateInUiThread(final int connectionState) {
			Log.d("ConnectionHandler","showDialogInUiThread");
			runOnUiThread(new Runnable() {
				public void run() {
					showConnectionState(connectionState);
				}
			});
		}
		
		/** Function which shows the status of successful establishment of connection
		 * with AbotBridge and also starts the user interface control activity */
	    private void connectedToAbotBridge() {
	    	Log.d("ConnectionHandler","connectedToAbotBridge");
	    	// Show connected status
	    	showConnectionStateInUiThread(CONNECTED);
	    	this.sendEmptyMessageDelayed(START_CONTROL_OPTIONS_ACTIVITY, 1500);
	
	    }
	    
	    private void promptSocketInfoInUiThread() {
	    	Log.d("ConnectionHandler","Prompt Socket Info in UI Thread");
	    	runOnUiThread(new Runnable() {
	    		public void run() {
	    			promptSocketInfo();
	    		}
	    	});
	    }
	    
		@Override
		public void handleMessage(Message message) {
			switch(message.what) {
			case INITIAL_CONNECTION_CHECK:
				Log.d("ConnectionHandler","INITIAL_CONNECTION_CHECK");
				if(!mNetworkManager.isWifiEnabled()) {
					// WiFi is not enabled
					showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);
				} else {
					if(!mNetworkManager.isNetworkConnected()) {
						// Network not connected
						showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);
					} else {
						// Connected to a WiFi network
						// Try to connect to AbotBridge
						if(mAbotManager.connect(mAppPreferences.getIpAddress(), mAppPreferences.getPortNumber())) {
							// Successfully established connection with "AbotBridge"
							connectedToAbotBridge();			
						} else {
							// Connection establishment failed;
							if(mNetworkManager.isAbotRouterConnected()) {
								// Connected to Abot Virtual Router, so ask for IP Address and Port Number
								promptSocketInfoInUiThread();
								break;
							} else {
								// Connected to some network, so show user WiFi settings to decide
								showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);	
							}			
						}
					}
				}
			
				break;
			
			case CHECK_ABOTBRIDGE_CONNECTION:
				Log.d("ConnectionHandler","CHECK_ABOTBRIDGE_CONNECTION");
				if(!mNetworkManager.isWifiEnabled()) {
					// WiFi not enabled
					showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);
					break;
				} else {
					if(!mNetworkManager.isNetworkConnected()) {
						// Network not found
						showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);
						break;						
					} else {
						// Show that you are contacting "AbotBridge" ; 
						showConnectionStateInUiThread(CONNECTING_ABOT);
						// Connected to a network :-)
						if(mAbotManager.connect(mAppPreferences.getIpAddress(), mAppPreferences.getPortNumber())) {
							// Successfully established connection with "AbotBridge"
							connectedToAbotBridge();	
						} else {
							// Connection establishment failed, prompt user for correct socket info
							promptSocketInfoInUiThread();
							break;
						}
					}
				}
				break;
			case CONNECT_ABOTBRIDGE:
				Log.d("ConnectionHandler","CONNECT_ABOTBRIDGE");
				if(!mNetworkManager.isWifiEnabled()) {
					// WiFi not enabled
					showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);
					break;
				} else {
					if(!mNetworkManager.isNetworkConnected()) {
						// Network not found
						showDialogInUiThread(DIALOG_WIFI_NETWORK_NEEDED_ID);
						break;						
					} else {
						// Connected to a network :-)
						if(mAbotManager.connect(mAppPreferences.getIpAddress(), mAppPreferences.getPortNumber())) {
							// Successfully established connection with "AbotBridge"
							connectedToAbotBridge();	
						} else {
							// Connection establishment failed, intimate user
							showDialogInUiThread(DIALOG_SOCKET_CONNECTION_FAILED);								
						}
					}
				}
				break;
				
			case START_CONTROL_OPTIONS_ACTIVITY:
				// Start Basic Control Activity
				startActivity(new Intent(MainActivity.this,ControlOptionsActivity.class));
				break;
			default:
				break;
			}
			
		}
		
	}
	
	private ConnectionHandler mConnectionHandler;
	
	 /** Listener for the connect button */
    View.OnClickListener mConnectListener = new View.OnClickListener() {
		
		public void onClick(View v) {
			// Initiate Wifi network connection
			Log.d("MainActivity","Clicked connect button");
			showConnectionState(CONNECTING_WIFI);
			mConnectionHandler.sendEmptyMessage(ConnectionHandler.INITIAL_CONNECTION_CHECK);
		}
    };
    
    
    /** Sends intent to WiFi settings (default one present in Android) */
    private void showWifiSettings() {
    	// Set state 
    	mInvokedWifiSettings = true;
    	Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
    	startActivity(intent);
    }
	
    /** Show connection state in the UI */
    private void showConnectionState(int state) {
    	switch(state) {
    	case CONNECTING_ABOT:
    		// Make connect button invisible
    		mConnectButton.setVisibility(View.GONE);
    		mConnectionProgressBar.setVisibility(View.VISIBLE);
    		mConnectionState.setText(R.string.state_connecting_abot);
    		mConnectionState.setTextSize(20);
    		mConnectionState.setVisibility(View.VISIBLE);
    		break;
    	case CONNECTED:
    		// Connection is established hurray !!!
    		mConnectionState.setText(R.string.state_connected_abot);
    		mConnectionState.setTextSize(20);
    		mConnectionState.setVisibility(View.VISIBLE);
    		mConnectionProgressBar.setVisibility(View.GONE);
    		mConnectedImage.setVisibility(View.VISIBLE);
    		break;
    		
    	case IDLE:
    		mConnectionState.setVisibility(View.GONE);
    		mConnectionProgressBar.setVisibility(View.GONE);
    		mConnectedImage.setVisibility(View.GONE);
    		mConnectButton.setVisibility(View.VISIBLE);
    		break;
    		
    	case CONNECTING_WIFI:
    		mConnectButton.setVisibility(View.GONE);
    		mConnectionProgressBar.setVisibility(View.VISIBLE);
    		mConnectionState.setText(R.string.state_connecting_wifi);
    		mConnectionState.setTextSize(20);
    		mConnectionState.setVisibility(View.VISIBLE);
    		
    		break;
    	default:
    		break;		
    	}
    }

    private void promptSocketInfo() {
    	Log.d("MainActivity","promptSocketInfo()");
    	if(this.isFinishing()) {
    		// Just leave the activity is about to finish
    		return;
    	}
   		LayoutInflater factory = LayoutInflater.from(MainActivity.this);
		final View socketEntryView = factory.inflate(R.layout.socket_info_entry, null);
		Dialog dialog;
		dialog = new AlertDialog.Builder(MainActivity.this)
						.setTitle(R.string.alert_dialog_socket_info_needed_title)
						.setView(socketEntryView)
						.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
			
								public void onClick(DialogInterface dialog, int which) {
									// User has provided IP address and Port number
									dialog.dismiss();
									String temp = mIpAddress.getText().toString();
									if(temp != "" && temp != null) {
										mAppPreferences.setIpAddress(temp);
									}
									temp = mPortNumber.getText().toString();
									if(temp != "" && temp != null) {
										mAppPreferences.setPortNumber(temp);
									}
									// Initiate connection again
									mConnectionHandler.sendEmptyMessage(ConnectionHandler.CONNECT_ABOTBRIDGE);
								}
						})
						.setOnCancelListener(new DialogInterface.OnCancelListener() {
							
							public void onCancel(DialogInterface dialog) {
								// User neglected to provide IP address and Port number
								showConnectionState(IDLE);
							}
						})
						.create();
		
		dialog.show();
		mIpAddress = (EditText) socketEntryView.findViewById(R.id.ip_address_edit);
		mPortNumber = (EditText) socketEntryView.findViewById(R.id.port_number_edit);
		mIpAddress.setText(mAppPreferences.getIpAddress());
		mPortNumber.setText(mAppPreferences.getPortNumber());
		
    }
    
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
		// Get mAbotManager from application context
		mAbotManager = ((AbotControllerApplication)this.getApplication()).mGlobalAbotManager;
        
        // Get handle for the view objects
        mConnectButton = (ImageButton) findViewById(R.id.connect_button);
        mConnectionProgressBar = (ProgressBar) findViewById(R.id.connection_progress_bar);
        mConnectionState = (TextView) findViewById(R.id.connection_state_text);
        mSettingsButton = (ImageButton) findViewById(R.id.settings_button);
        mHelpButton = (ImageButton) findViewById(R.id.help_button);
        mConnectedImage = (ImageView) findViewById(R.id.connected_image);

        //
        mNetworkManager = new CustomNetworkManager(this);
        mAppPreferences = new AppPreferences(getApplicationContext());
        
        // Start ConnectionThread
        mConnectionThread = new HandlerThread("ConnectionThread");
        mConnectionThread.start();
        mConnectionHandler = new ConnectionHandler(mConnectionThread.getLooper());
       
        
        // Register onClickListener for the connect button
        mConnectButton.setOnClickListener(mConnectListener);
        
        // Register listener for settings and help menu
        mSettingsButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Call Settings activity
				Intent intent = new Intent(getApplicationContext(),AppPreferenceActivity.class);
				startActivity(intent);
			}
        	
        });
        
        mHelpButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Call help activity
				Intent intent = new Intent(getApplicationContext(),HelpActivity.class);
				startActivity(intent);	
			}
        	
        });
        
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	if(mInvokedWifiSettings) {
    		mInvokedWifiSettings = false;
    		if(!mConnectionHandler.sendEmptyMessage(ConnectionHandler.CHECK_ABOTBRIDGE_CONNECTION)) {
    			// Failed to place message in handler queue
    			// Should never happen
    			Log.e("MainActivity","onResume, Failed to handle message");
    			showConnectionState(IDLE);
    		}
    	} else {
    		showConnectionState(IDLE);
    	}
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    }
    
    @Override
    protected void onDestroy() {
		try {
			mConnectionThread.quit();
		} catch (Exception e) {
			Log.e("BasicControlActivity","Exception while stopping thread");
			e.printStackTrace();
		}
    	mAbotManager.disconnect();
    	super.onDestroy();
    }
    @Override
    public void onBackPressed() {
    	showDialog(DIALOG_EXIT_APP_ID);
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog;
    	Log.d("MainActivity","onCreateDialog ID - " + id);
    	switch(id) {
    	case DIALOG_WIFI_NETWORK_NEEDED_ID:
    		dialog = new AlertDialog.Builder(this)
    						.setTitle(R.string.alert_dialog_wifi_needed_title)
    						.setMessage(R.string.alert_dialog_wifi_needed_msg)
    						.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
								// Dismiss dialog
									mNetworkManager.enableWifi();
									dialog.dismiss();
									showWifiSettings();
								}
							})
							.setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// Change connection state
									showConnectionState(IDLE);
								}
							})
							.create();
    		break;		
    	case DIALOG_SOCKET_CONNECTION_FAILED:
    		dialog = new AlertDialog.Builder(this)
    						.setIcon(android.R.drawable.ic_dialog_alert)
    						.setTitle(R.string.alert_dialog_socket_connection_failed_title)
    						.setMessage(R.string.alert_dialog_socket_connection_failed_msg)
    						.setOnCancelListener(new DialogInterface.OnCancelListener() {
								
								public void onCancel(DialogInterface dialog) {
									// Get the UI back to initial state
									showConnectionState(IDLE);
								}
							})
							.create();
    		break;
    	case DIALOG_EXIT_APP_ID:
    		dialog = new AlertDialog.Builder(this)
    					.setIcon(android.R.drawable.ic_dialog_alert)
    					.setTitle(R.string.alert_dialog_exit_app_title)
    					.setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
    						
							public void onClick(DialogInterface dialog, int which) {
								finish();
							}
						})
						.setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
							
							public void onClick(DialogInterface dialog, int which) {
								// Just leave
							}
						})
						.create();
    		break;
    	default:
    		Log.d("AlertDialog","Invalid ID - " + id);
    		dialog = null;
    		break;
    	}
    	return dialog;
    }
   
}