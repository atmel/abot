package com.atmel.abot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class SettingsActivity extends Activity {
	
	public boolean onCreateOptionsMenu(Menu menu) {
    	Log.d("AbotSettings","Reached on create Menu");
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
    
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.settings:
				Log.d("AbotSettings","Menu");
				// Intent to AppPreferenceActivity
				Intent intent = new Intent();
				intent.setClass(this, AppPreferenceActivity.class);
				startActivity(intent);
				return true;
			case R.id.help:
				Log.d("AbotSettings","Help");
				// Call help activity
				Intent intent1 = new Intent(this,HelpActivity.class);
				startActivity(intent1);	
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
    }
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }
    

}