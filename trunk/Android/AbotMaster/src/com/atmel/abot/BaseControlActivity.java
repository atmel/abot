package com.atmel.abot;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class BaseControlActivity extends Activity implements AbotManager.AbotBridgeConnectionListener {
	
	protected AbotManager mAbotManager;
	private NetworkConnectivityReceiver mReceiver;
	private Dialog mDialog;
	private boolean mAbotVirtualRouter;
	Handler mHandler = new Handler();
	
	/** Network connection manager */
	private CustomNetworkManager mNetworkManager;
	
	/** Alert dialog used to show alert message */
	private Dialog mAlertDialog;
	
	private class NetworkConnectivityReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("NetworkConnectivityReceiver","Intent: " + intent.toString());
			if(intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
				NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
				
				/* Check whether network is connected */
				if(networkInfo.getState().equals(NetworkInfo.State.DISCONNECTED) || networkInfo.getState().equals(NetworkInfo.State.DISCONNECTING)) {
					// Check if the network is Virtual Router
					if (mAbotVirtualRouter) {
						mHandler.postDelayed(new Runnable() {

							public void run() {
								if (!mNetworkManager.isNetworkConnected() ||  !mNetworkManager.isAbotRouterConnected()) {
									// Connection is lost, so inform user
									BaseControlActivity.this.runOnUiThread(new Runnable() {

										public void run() {
											alertConnectionLost();
										}
									});
								}
							}
							
						}, 3000);
					}
					// Network lost
					BaseControlActivity.this.runOnUiThread(new Runnable() {

						public void run() {
							alertConnectionLost();
						}
						
					});
				}
			}
			
		}
		
	}
		
	
	protected void alertConnectionLost() {
		Log.d("BaseControlActivity","alertConnectionLost()");
		if(mDialog != null) {
			if(mDialog.isShowing()) {
				// Already dialog is showing, so no need to show it again
				return;
			}
		}
		if(BaseControlActivity.this.isFinishing()) {
			// Activity is about to finish just ignore
			return;
		}
		// Build connection lost alert dialog
		mDialog = new AlertDialog.Builder(this)
								.setTitle(R.string.alert_dialog_connection_lost_title)
								.setOnCancelListener(new DialogInterface.OnCancelListener() {
									
									public void onCancel(DialogInterface dialog) {
										// Send intent to MainActivity
										dialog.dismiss();
										Intent activityIntent = new Intent(getApplicationContext(),MainActivity.class);
										activityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										startActivity(activityIntent);	
									}
								})
								.create();
		mDialog.show();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get mAbotManager from application context
		Log.d("BaseControlActivity","onCreate");
		mAbotManager = ((AbotControllerApplication)this.getApplication()).mGlobalAbotManager;
		mNetworkManager = new CustomNetworkManager(this);
		
		mAlertDialog = new AlertDialog.Builder(this).create();
		// Create Broadcast Receiver
		mReceiver = new NetworkConnectivityReceiver();
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
    @Override
    protected void onResume() {
    	super.onResume();
    	// Check if Connection exists
    	if(!mAbotManager.isConnected()) {
    		Log.d("BaseControlActivity","Lost connection in onResume()");
    		alertConnectionLost();
    		return;
    	}
    	// Check if the connection is Abot Virtual Router
    	mAbotVirtualRouter = mNetworkManager.isAbotRouterConnected();
    	
    	// Register broadcast receiver
    	IntentFilter intentFilter = new IntentFilter();
    	intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
    	intentFilter.addAction(AbotManager.ABOT_CONNECTION_LOST_ACTION);
    	this.registerReceiver(mReceiver,intentFilter);
    	mAbotManager.registerListener(this);
    	
    }
    
    @Override
    protected void onPause() {
    	// Unregister receiver before we get out of this activity
    	super.onPause();
    	try {
    		mAbotManager.unregisterListener(this);
        	this.unregisterReceiver(mReceiver);
    	} catch (Exception e) {
    		// Ignore exceptions
    		e.printStackTrace();
    	}
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void onConnect() {
		// Connection with Abotbridge is established
		
	}

	public void onDisconnect() {
		// Lost connection with Abot/Abotbridge
		Log.d("BaseControlActivity","onDisconnect()");
		
		synchronized(mAlertDialog) {
			if (mAlertDialog.isShowing()) return;
		}
		runOnUiThread(new Runnable() {

			public void run() {
				if(BaseControlActivity.this.hasWindowFocus()) {
					mAlertDialog.setTitle("Abot not responding !");
					mAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						
						public void onCancel(DialogInterface dialog) {
							// Send intent to MainActivity
							dialog.dismiss();	
						}
					});
					mAlertDialog.show();
				}
			}
		});

	}
	
	

}
