import os
from distutils.core import setup
import py2exe

setup(console=['abotbridge.py'],
      options={"py2exe":
                       {
                        "bundle_files": 1,
                        "dll_excludes":['w9xpopen.exe'],
                        "dist_dir": "bin/",
                        }
               },
      zipfile = None
      )
